//
//  NotificationArticleView.m
//  Public
//
//  Created by Глеб on 25/11/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import "NotificationArticleView.h"
#import "TeoriusHelper.h"
#import "SharedClass.h"

@implementation NotificationArticleView

- (instancetype)init {
    self = [super init];
    
    if (!self) {
        return nil;
    }
    
    [self baseInit];
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (!self) {
        return nil;
    }
    
    [self baseInit];
    
    return self;
}

- (void)baseInit {
    [self addLabels];
}

- (void)addLabels {
    _textLabel = [UILabel new];
    _textLabel.numberOfLines = 0;
    
    [self addSubview:_textLabel];
}

- (void)placeSubviews {
    [super placeSubviews];
    CGFloat x = SCALED_F(17.f);
    self.textLabel.frame = CGRectMake(x, SCALED_F(30.f), self.width - x - SCALED_F(36.f), 0);
    [self.textLabel sizeToFit];
}

- (CGFloat)calculateHeight {
    [self placeSubviews];
    
    return self.textLabel.maxY + self.textLabel.y;
}

#pragma mark - View's lifecycle

- (void)willMoveToSuperview:(UIView *)newSuperview {
    [super willMoveToSuperview:newSuperview];
    [self placeSubviews];
}
@end
