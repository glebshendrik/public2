//
//  BaseButton.m
//  Namaz
//
//  Created by Tagi on 09.10.15.
//
//

#import "BaseButton.h"

@implementation BaseButton

- (void)setHighlighted:(BOOL)highlighted {
    [super setHighlighted:highlighted];
    
    if (highlighted) {
        self.alpha = .4f;
    }
    else {
        self.alpha = 1.f;
    }
}

- (void)setEnabled:(BOOL)enabled {
    [super setEnabled:enabled];
    
    if (enabled) {
        self.alpha = 1.f;
    }
    else {
        self.alpha = .4f;
    }
}

@end
