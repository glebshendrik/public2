//
//  TwoLeftOneRightLabelsCell.h
//  Public
//
//  Created by Tagi on 16.11.15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TwoLeftOneRightLabelsCell : UITableViewCell

@property (nonatomic, readonly) UILabel *rightLabel;

@end
