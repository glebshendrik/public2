//
//  BorderlessButton.h
//  Public
//
//  Created by Arslan Zagidullin on 11/09/15.
//  Copyright (c) 2015 Teoria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BorderlessButton : UIButton

@property (nonatomic, strong) UIColor* mainColor;
@property float buttonAnimDuration;

- (id)initWithFrame:(CGRect)frame text:(NSString*)text;
- (id)initWithFrame:(CGRect)frame text:(NSString*)text color:(UIColor*)color;

@end
