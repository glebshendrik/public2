//
//  UnderlineButton.m
//  Public
//
//  Created by Arslan Zagidullin on 30/09/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import "UnderlineButton.h"
#import "SharedClass.h"


@interface UnderlineButton ()

@property (nonatomic) UIColor* mainColor;
@property (nonatomic) float buttonAnimDuration;
@property (nonatomic) UIFont* font;

@property (nonatomic) CALayer* underline;

@end


@implementation UnderlineButton

- (id)initWithFrame:(CGRect)frame text:(NSString*)text {
    if (self = [super initWithFrame:frame]) {
        self.mainColor = [UIColor whiteColor];
        self.buttonAnimDuration = BUTTON_ANIM_DURATION;
        self.font = [SharedClass getRegularFontWithSize:14.0f];
        
        [self setTitle:text forState:UIControlStateNormal];
        [self setTitleColor:self.mainColor forState:UIControlStateNormal];
        [self.titleLabel setFont:self.font];
        
        float intervalLabelWidth = [text sizeWithAttributes:@{NSFontAttributeName: self.font}].width;
        CALayer *underline = [CALayer layer];
        underline.frame = [TeoriusHelper rectFromCenter:CGPointMake(frame.size.width/2, frame.size.height/2 + 6.5)
                                                   size:CGSizeMake(intervalLabelWidth, 1)];
        underline.backgroundColor = [self.mainColor CGColor];
        [self.layer addSublayer:underline];
        self.underline = underline;
        
        [self addTarget:self action:@selector(buttonTouchDown:) forControlEvents:UIControlEventTouchDown];
        [self addTarget:self action:@selector(buttonCancelTouch:) forControlEvents:UIControlEventTouchUpInside];
        [self addTarget:self action:@selector(buttonCancelTouch:) forControlEvents:UIControlEventTouchDragOutside];
    }
    return self;
}

- (void)buttonTouchDown:(id)sender {
    [UIView animateWithDuration:self.buttonAnimDuration animations:^{
        UIColor* newColor = [self.mainColor colorWithAlphaComponent:0.3];
        [self setTitleColor:newColor forState:UIControlStateNormal];
        self.underline.backgroundColor = [newColor CGColor];
    }];
}

- (void)buttonCancelTouch:(id)sender {
    [UIView animateWithDuration:self.buttonAnimDuration animations:^{
        UIColor* newColor = self.mainColor;
        [self setTitleColor:newColor forState:UIControlStateNormal];
        self.underline.backgroundColor = [newColor CGColor];
    }];
}

@end
