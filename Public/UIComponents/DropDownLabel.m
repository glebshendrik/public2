//
//  DropDownLabel.m
//  Public
//
//  Created by Arslan Zagidullin on 30/09/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import "DropDownLabel.h"
#import "TeoriusHelper+Extension.h"
#import "SharedClass.h"


@interface DropDownLabel ()

@property (nonatomic) UIImageView* dropDownArrowImg;

@property (nonatomic) UIFont* intervalLabelFont;
@property (nonatomic) float dropDownArrowImgWidth;
@property (nonatomic) float distanceBetweenLabelAndImg;

@end


@implementation DropDownLabel

- (id)initWithFrame:(CGRect)frame text:(NSString*)text {
    if (self = [super initWithFrame:frame]) {
        self.intervalLabelFont = [SharedClass getRegularFontWithSize:14];
        
        self.text = text;
        self.font = self.intervalLabelFont;
        self.textColor = COLOR_LIGHTERGREEN;
        
        self.dropDownArrowImgWidth = 11;
        self.distanceBetweenLabelAndImg = 6;
        float intervalLabelWidth = [text sizeWithAttributes:@{NSFontAttributeName: self.intervalLabelFont}].width;
        float imgOffset = intervalLabelWidth/SCREEN_SCALE + self.distanceBetweenLabelAndImg;
        UIImageView* dropDownArrowImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"DropDownArrow"]];
        dropDownArrowImg.frame = [TeoriusHelper scaledRect:CGRectMake(imgOffset, 7, self.dropDownArrowImgWidth, 11)];
        [self addSubview:dropDownArrowImg];
        
        self.dropDownArrowImg = dropDownArrowImg;
        
        frame.size.width = imgOffset + self.dropDownArrowImgWidth;
        self.frame = frame;
    }
    return self;
}

- (void)setNewText:(NSString*)text {
    self.text = text;
    float intervalLabelWidth = [text sizeWithAttributes:@{NSFontAttributeName: self.intervalLabelFont}].width;
    float imgOffset = intervalLabelWidth/SCREEN_SCALE + self.distanceBetweenLabelAndImg;
    
    CGRect dropDownArrowImgFrame = self.dropDownArrowImg.frame;
    dropDownArrowImgFrame.origin.x = imgOffset;
    self.dropDownArrowImg.frame = dropDownArrowImgFrame;
    
    CGRect labelFrame = self.frame;
    labelFrame.size.width = imgOffset + self.dropDownArrowImgWidth;
    self.frame = labelFrame;
}

@end
