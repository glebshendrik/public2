//
//  PinkBorderButton.m
//  Public
//
//  Created by Arslan Zagidullin on 16/09/15.
//  Copyright (c) 2015 Teoria. All rights reserved.
//

#import "PinkBorderButton.h"
#import "SharedClass.h"


@interface PinkBorderButton ()

@property (nonatomic, strong) UIColor* mainColor;
@property float buttonAnimDuration;

@end


@implementation PinkBorderButton

- (id)initWithFrame:(CGRect)frame text:(NSString*)text {
    if (self = [super initWithFrame:frame]) {
        _mainColor = COLOR_PINK;
        _buttonAnimDuration = BUTTON_ANIM_DURATION;
        
        self.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0];
        [self.layer setBorderWidth: [TeoriusHelper scaledFloat:0.5f]];
        [self.layer setBorderColor: [_mainColor CGColor]];
        [self.layer setCornerRadius: [TeoriusHelper scaledFloat:2.0f]];
        
        [self setTitle:text forState:UIControlStateNormal];
        [self setTitleColor:_mainColor forState:UIControlStateNormal];
        //    [greenButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        
        [self.titleLabel setFont:[SharedClass getBoldFontWithSize:14.0f]];
        
        [self addTarget:self action:@selector(ButtonTouchDown:) forControlEvents:UIControlEventTouchDown];
        [self addTarget:self action:@selector(ButtonCancelTouch:) forControlEvents:UIControlEventTouchUpInside];
        [self addTarget:self action:@selector(ButtonCancelTouch:) forControlEvents:UIControlEventTouchDragOutside];
    }
    return self;
}

- (void)ButtonTouchDown:(id)sender {
    UIButton* greenButton = (UIButton*)sender;
    [UIView animateWithDuration:_buttonAnimDuration animations:^{
        greenButton.backgroundColor = _mainColor;
        [greenButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }];
}

- (void)ButtonCancelTouch:(id)sender {
    UIButton* greenButton = (UIButton*)sender;
    [UIView animateWithDuration:_buttonAnimDuration animations:^{
        greenButton.backgroundColor = [_mainColor colorWithAlphaComponent:0];
        [greenButton setTitleColor:_mainColor forState:UIControlStateNormal];
    }];
}


@end
