//
//  BigPlusButton.m
//  Public
//
//  Created by Arslan Zagidullin on 11/09/15.
//  Copyright (c) 2015 Teoria. All rights reserved.
//

#import "BigPlusButton.h"
#import "SharedClass.h"
#import "TeoriusHelper+Extension.h"

@implementation BigPlusButton

- (id)initWithFrame:(CGRect)frame text:(NSString*)text {
    
    // Превращаем прямоугольник в квадрат по ширине
    CGRect squareFrame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, frame.size.width);
    
    if (self = [super initWithFrame:squareFrame]) {
        _mainColor = COLOR_LIGHTERGREEN;
        _buttonAnimDuration = BUTTON_ANIM_DURATION;
        
        CGRect plusLabelFrame = [TeoriusHelper rectFromCenter:CGPointMake(frame.size.width/2, 25.0f) size:[TeoriusHelper scaledSizeWithWidth:50 height:50]];
        UIImageView* plusIcon = [[UIImageView alloc] initWithFrame:plusLabelFrame];
        plusIcon.image = [UIImage imageNamed:@"PlusIcon"];
//        UILabel* plusLabel = [[UILabel alloc] initWithFrame:plusLabelFrame];
//        plusLabel.text = @"+";
//        plusLabel.textAlignment = NSTextAlignmentCenter;
//        plusLabel.font = [plusLabel.font fontWithSize:90.0f];
//        nameLabel.font = [SharedClass getRegularFontWithSize:30.0f];
//        plusLabel.textColor = self.mainColor;
        //    [nameLabel sizeToFit];
        [self addSubview:plusIcon];
        
        self.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0];
        [self.layer setBorderWidth: [TeoriusHelper scaledFloat:1.0f]];
        [self.layer setBorderColor: [[self.mainColor colorWithAlphaComponent:0] CGColor]];
        [self.layer setCornerRadius: [TeoriusHelper scaledFloat:5.0f]];
        
        NSMutableParagraphStyle *parStyle = [NSMutableParagraphStyle new];
        [parStyle setLineHeightMultiple:1.3f];
        [parStyle setAlignment:NSTextAlignmentCenter];
        [parStyle setLineBreakMode:NSLineBreakByWordWrapping];
        
        NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:text attributes:@{NSParagraphStyleAttributeName: parStyle, NSFontAttributeName: [SharedClass getRegularFont], NSForegroundColorAttributeName: _mainColor}];
        
        CGRect titleLabelRect = CGRectMake(0, self.bounds.size.width/2, self.bounds.size.width, self.bounds.size.height/2);
        UILabel* titleLabel = [[UILabel alloc] initWithFrame:titleLabelRect];
        [titleLabel setNumberOfLines:0];
        [titleLabel setAttributedText:attrString];
        [titleLabel setFont:[SharedClass getRegularFontWithSize:14.0f]];
        [titleLabel setTextColor:_mainColor];
        [self addSubview:titleLabel];
        
        [self addTarget:self action:@selector(ButtonTouchDown:) forControlEvents:UIControlEventTouchDown];
        [self addTarget:self action:@selector(ButtonCancelTouch:) forControlEvents:UIControlEventTouchUpInside];
        [self addTarget:self action:@selector(ButtonCancelTouch:) forControlEvents:UIControlEventTouchDragOutside];
    }
    return self;
}

- (void)ButtonTouchDown:(id)sender {
    UIButton* greenButton = (UIButton*)sender;
    [UIView animateWithDuration:_buttonAnimDuration animations:^{
        greenButton.alpha = 0.3f;
    }];
}

- (void)ButtonCancelTouch:(id)sender {
    UIButton* greenButton = (UIButton*)sender;
    [UIView animateWithDuration:_buttonAnimDuration animations:^{
        greenButton.alpha = 1.0f;
    }];
}

@end
