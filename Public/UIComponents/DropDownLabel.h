//
//  DropDownLabel.h
//  Public
//
//  Created by Arslan Zagidullin on 30/09/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface DropDownLabel : UILabel

- (id)initWithFrame:(CGRect)frame text:(NSString*)text;

- (void)setNewText:(NSString*)text;

@end
