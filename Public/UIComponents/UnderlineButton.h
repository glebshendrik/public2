//
//  UnderlineButton.h
//  Public
//
//  Created by Arslan Zagidullin on 30/09/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UnderlineButton : UIButton

- (id)initWithFrame:(CGRect)frame text:(NSString*)text;

@end
