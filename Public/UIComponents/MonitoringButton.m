//
//  MonitoringButton.m
//  Public
//
//  Created by Arslan Zagidullin on 28/09/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import "MonitoringButton.h"
#import "SharedClass.h"


@interface MonitoringButton ()

@property (nonatomic, strong) UIColor* mainColor;
@property float buttonAnimDuration;

@end


@implementation MonitoringButton

- (id)initWithFrame:(CGRect)frame text:(NSString*)text {
    if (self = [super initWithFrame:frame]) {
        self.mainColor = [SharedClass getGrayColor];
        self.buttonAnimDuration = BUTTON_ANIM_DURATION;
        
        self.titleLabel.font = [SharedClass getRegularFont];
        [self setTitle:text forState:UIControlStateNormal];
        [self setTitleColor:[SharedClass getGreenColor] forState:UIControlStateNormal];
        self.backgroundColor = [_mainColor colorWithAlphaComponent:0];
        
        CAShapeLayer *border = [CAShapeLayer layer];
        border.strokeColor = [self.mainColor CGColor];
        border.fillColor = nil;
        border.lineDashPattern = @[@1, @3];
        [self.layer addSublayer:border];
        
        border.path = [UIBezierPath bezierPathWithRect:self.bounds].CGPath;
        border.frame = self.bounds;
        
        [self addTarget:self action:@selector(buttonTouchDown:) forControlEvents:UIControlEventTouchDown];
        [self addTarget:self action:@selector(buttonCancelTouch:) forControlEvents:UIControlEventTouchUpInside];
        [self addTarget:self action:@selector(buttonCancelTouch:) forControlEvents:UIControlEventTouchDragOutside];
    }
    return self;
}

- (void)buttonTouchDown:(id)sender {
    UIButton* button = (UIButton*)sender;
    [UIView animateWithDuration:_buttonAnimDuration animations:^{
        button.backgroundColor = [_mainColor colorWithAlphaComponent:0.2];
    }];
}

- (void)buttonCancelTouch:(id)sender {
    UIButton* button = (UIButton*)sender;
    [UIView animateWithDuration:_buttonAnimDuration animations:^{
        button.backgroundColor = [_mainColor colorWithAlphaComponent:0];
    }];
}

@end
