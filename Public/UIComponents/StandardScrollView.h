//
//  StandardScrollView.h
//  Public
//
//  Created by Teoria 5 on 21/09/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StandardScrollView : UIScrollView

// @arslan: временное решение
@property (nonatomic) UIView* hostingView;

- (id)initWithParentVeiwFrame:(CGRect)frame;

@end
