//
//  DetailButton.m
//  Public
//
//  Created by Arslan Zagidullin on 15/09/15.
//  Copyright (c) 2015 Teoria. All rights reserved.
//

#import "DetailButton.h"

@implementation DetailButton

+ (DetailButton*)buttonWithFrame:(CGRect)frame {
    DetailButton* detailButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [detailButton setFrame:frame];
    
    [detailButton setBackgroundImage:[UIImage imageNamed:@"DetailButton"] forState:UIControlStateNormal];
//    [button setBackgroundImage:[UIImage imageNamed:@"DetailButton"] forState:UIControlStateHighlighted];
    
    return detailButton;
}

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        
    }
    return self;
}

@end
