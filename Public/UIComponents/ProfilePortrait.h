//
//  ProfilePortait.h
//  Public
//
//  Created by Arslan Zagidullin on 06/10/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import <UIKit/UIKit.h>


#define PORTRAIT_BIG_DIMENSION 95
#define PORTRAIT_SMALL_DIMENSION 37


@interface ProfilePortrait : UIImageView

- (id)initWithFrame:(CGRect)frame imageName:(NSString*)imageName borderWidth:(float)borderWidth;

@end
