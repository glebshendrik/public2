//
//  StandardScrollView.m
//  Public
//
//  Created by Teoria 5 on 21/09/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import "StandardScrollView.h"
#import "TeoriusHelper+Extension.h"

@implementation StandardScrollView

- (id)initWithParentVeiwFrame:(CGRect)frame {
    CGRect scrollViewRect = CGRectMake(0, HEADER_HEIGHT, frame.size.width, frame.size.height - HEADER_HEIGHT);
    return [self initWithFrame:scrollViewRect];
}

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
        self.showsVerticalScrollIndicator = NO;
        self.contentInset = UIEdgeInsetsMake(-frame.origin.y, 0, 0, 0);
        self.contentSize = CGSizeMake(frame.size.width, frame.size.height-frame.origin.y);
        self.alwaysBounceVertical = YES;
    }
    return self;
}

//- (BOOL)touchesShouldBegin:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event inContentView:(UIView *)view {
//    if (self.hostingView)
//        [self.hostingView touchesBegan:touches withEvent:event];
//    return YES;
//}

//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
//    NSLog(@"top touch began");
//    if (!self.dragging){
//        [self.nextResponder touchesBegan: touches withEvent:event];
//    }
//    else{
//        [super touchesBegan: touches withEvent: event];
//    }
//}
//-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
//    
//    // If not dragging, send event to next responder
//    if (!self.dragging){
//        [self.nextResponder touchesEnded: touches withEvent:event];
//    }
//    else{
//        [super touchesEnded: touches withEvent: event];
//    }
//}

@end
