//
//  ThinsBorderButton.m
//  Public
//
//  Created by Arslan Zagidullin on 11/09/15.
//  Copyright (c) 2015 Teoria. All rights reserved.
//

#import "ThinBorderButton.h"
#import "SharedClass.h"

@implementation ThinBorderButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)initWithFrame:(CGRect)frame text:(NSString*)text {
    return [self initWithFrame:frame text:text color:COLOR_GREEN1];
}

- (id)initWithFrame:(CGRect)frame text:(NSString*)text color:(UIColor*)color {
    if (self = [super initWithFrame:frame]) {
        _mainColor = color;
        _buttonAnimDuration = BUTTON_ANIM_DURATION;
        
        self.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0];
        [self.layer setBorderWidth: [TeoriusHelper scaledFloat:1.0f]];
        [self.layer setBorderColor: [_mainColor CGColor]];
        [self.layer setCornerRadius: [TeoriusHelper scaledFloat:5.0f]];
        
        [self setTitle:text forState:UIControlStateNormal];
        [self setTitleColor:_mainColor forState:UIControlStateNormal];
        //    [greenButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        
        [self.titleLabel setFont:[SharedClass getRegularFont]];
        
        [self addTarget:self action:@selector(ButtonTouchDown:) forControlEvents:UIControlEventTouchDown];
        [self addTarget:self action:@selector(ButtonCancelTouch:) forControlEvents:UIControlEventTouchUpInside];
        [self addTarget:self action:@selector(ButtonCancelTouch:) forControlEvents:UIControlEventTouchDragOutside];
    }
    return self;
}

- (void)ButtonTouchDown:(id)sender {
    UIButton* greenButton = (UIButton*)sender;
//    [UIView animateWithDuration:_buttonAnimDuration animations:^{
        greenButton.backgroundColor = _mainColor;
        [greenButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    }];
}

- (void)ButtonCancelTouch:(id)sender {
    UIButton* greenButton = (UIButton*)sender;
//    [UIView animateWithDuration:_buttonAnimDuration animations:^{
        greenButton.backgroundColor = [_mainColor colorWithAlphaComponent:0];
        [greenButton setTitleColor:_mainColor forState:UIControlStateNormal];
//    }];
}

@end
