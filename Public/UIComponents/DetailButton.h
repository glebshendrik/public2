//
//  DetailButton.h
//  Public
//
//  Created by Arslan Zagidullin on 15/09/15.
//  Copyright (c) 2015 Teoria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailButton : UIButton

+ (DetailButton*)buttonWithFrame:(CGRect)frame;

@end
