//
//  PinkButton.m
//  Public
//
//  Created by Arslan Zagidullin on 30/09/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import "PinkButton.h"
#import "SharedClass.h"


@interface PinkButton ()

@property (nonatomic, strong) UIColor* mainColor;
@property float buttonAnimDuration;

@end


@implementation PinkButton

- (id)initWithFrame:(CGRect)frame text:(NSString*)text {
    if (self = [super initWithFrame:frame]) {
        _mainColor = COLOR_PINK;
        _buttonAnimDuration = BUTTON_ANIM_DURATION;
        
        self.backgroundColor = self.mainColor;
//        [self.layer setBorderWidth: 0];
//        [self.layer setBorderColor: [_mainColor CGColor]];
        [self.layer setCornerRadius: [TeoriusHelper scaledFloat:2.0f]];
        
        [self setTitle:text forState:UIControlStateNormal];
        [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//        [greenButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        
        [self.titleLabel setFont:[SharedClass getRegularFontWithSize:14.0f]];
        
        [self addTarget:self action:@selector(buttonTouchDown:) forControlEvents:UIControlEventTouchDown];
        [self addTarget:self action:@selector(buttonCancelTouch:) forControlEvents:UIControlEventTouchUpInside];
        [self addTarget:self action:@selector(buttonCancelTouch:) forControlEvents:UIControlEventTouchDragOutside];
    }
    return self;
}

- (void)buttonTouchDown:(id)sender {
    UIButton* button = (UIButton*)sender;
    [UIView animateWithDuration:self.buttonAnimDuration animations:^{
        button.backgroundColor = [self.mainColor colorWithAlphaComponent:0.5];
    }];
}

- (void)buttonCancelTouch:(id)sender {
    UIButton* button = (UIButton*)sender;
    [UIView animateWithDuration:self.buttonAnimDuration animations:^{
        button.backgroundColor = self.mainColor;
    }];
}

@end
