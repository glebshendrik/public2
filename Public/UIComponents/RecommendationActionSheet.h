//
//  RecommendationActionSheet.h
//  Public
//
//  Created by Arslan Zagidullin on 02.10.15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecommendationActionSheet : UIActionSheet

@property (nonatomic) UIView* recommendationView;

@end
