//
//  PinkBorderButton.h
//  Public
//
//  Created by Arslan Zagidullin on 16/09/15.
//  Copyright (c) 2015 Teoria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PinkBorderButton : UIButton

- (id)initWithFrame:(CGRect)frame text:(NSString*)text;

@end
