//
//  ProfilePortait.m
//  Public
//
//  Created by Arslan Zagidullin on 06/10/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import "ProfilePortrait.h"
#import "TeoriusHelper+Extension.h"
#import "SharedClass.h"


@implementation ProfilePortrait

- (id)initWithFrame:(CGRect)frame imageName:(NSString *)imageName borderWidth:(float)borderWidth {
    frame.size.height = frame.size.width;
    if (self = [super initWithFrame:frame]) {
        UIImage *image = [UIImage imageNamed:imageName];
        self.image = image;
        self.layer.cornerRadius = frame.size.width/2;
        self.layer.masksToBounds = YES;
        self.layer.borderColor = [COLOR_LIGHTERGREEN CGColor];
        self.layer.borderWidth = borderWidth;
    }
    return self;
}

@end
