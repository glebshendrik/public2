//
//  AppointmentHelper.h
//  Public
//
//  Created by Tagi on 18.11.15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Service;
@class Resource;
@class TimeInterval;
@class Clinic;

@interface AppointmentHelper : NSObject

@property (nonatomic) Service *service;
@property (nonatomic) Resource *resource;
@property (nonatomic) Clinic *clinic;
@property (nonatomic) NSUInteger year;
@property (nonatomic) NSUInteger month;
@property (nonatomic) NSUInteger day;
@property (nonatomic) TimeInterval *timeInterval;
@property (nonatomic) NSString *phone;
@property (nonatomic) NSString *dateAndTimeString;

@property (nonatomic) BOOL needToUpdateList;

+ (instancetype)instance;

@end
