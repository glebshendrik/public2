//
//  PersistencyManager.m
//  Public
//
//  Created by Глеб on 13/10/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import "PersistencyManager.h"
#import "TeoriusHelper.h"

NSString *const kClass = @"class";
NSString *const kProperties = @"properties";

@implementation PersistencyManager {
    AccountRLM *_account;
}

- (instancetype)init
{
//    При первом входе не работает
    self = [super init];
    if (self) {
        [self generateDummyData];
    }
    return self;
}

#pragma mark - Common

- (void)addOrUpdateObject:(RLMObject *)object {
    RLMRealm *rlm = [RLMRealm defaultRealm];
    
    [rlm beginWriteTransaction];
    [rlm addOrUpdateObject:object];
    [rlm commitWriteTransaction];
    
//    RLMResults *allAcc = [AccountRLM allObjects];
//    NSLog(@"all accounts: %@", allAcc);
}

- (void)deleteObjectsFromResults:(RLMResults *)result completion:(void (^)(BOOL success))completion {
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    
    [realm beginWriteTransaction];
    [realm deleteObjects:result];
    [realm commitWriteTransaction];
    
    completion(YES);
}

- (UserRLM *)getUserByID: (NSString *)idUser {
    
//    RLMResults *users = [UserRLM allObjects];
    RLMResults *user = [UserRLM objectsWhere:@"ID == %@", idUser];
    
    
//    NSLog(@"%@", user);
    return user.firstObject;
}

- (RLMObject *)createObjectFromDict:(NSDictionary *)objectDict {
    // берем имя класса и свойства
    NSString *className = objectDict[kClass];
    
    if (![TeoriusHelper stringIsValid:className]) {
        return nil;
    }
    
    //создаем временный объект, чтобы проверить, существует ли такой класс и чтобы с его помощью можно было обратиться к методу createOrUpdateInDefaultRealmWithValue нужного класса
    RLMObject *object = [[NSClassFromString(className) alloc] init];
    
    if (!object) {
        return nil;
    }
    
    NSDictionary *propertiesDict = objectDict[kProperties];
    
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    
    RLMObject *rlmObject = nil;
    
    // создаем сам объект с данными свойствами
    if ([[object class] primaryKey]) {
        rlmObject = [[object class] createOrUpdateInDefaultRealmWithValue:propertiesDict];
//        NSLog(@"%@", rlmObject);
    }
    else {
        rlmObject = [[object class] createInDefaultRealmWithValue:propertiesDict];
//        NSLog(@"%@", rlmObject);
    }
    
    [realm commitWriteTransaction];
    
    return rlmObject;
}


#pragma mark - Account

- (void)deleteAccount {
    [[RLMRealm defaultRealm] beginWriteTransaction];
    [[RLMRealm defaultRealm] deleteAllObjects];
    [[RLMRealm defaultRealm] commitWriteTransaction];
}

#pragma mark - User

- (BOOL)accountContainsUserWithID:(NSString *)ID {
    AccountRLM *acc = self.account;
    
    RLMResults *res = [acc.family.members objectsWhere:@"ID == %@", ID];
    return res.count > 0;
}

- (void)assignCurrentUser:(NSString *)accountID {
    RLMResults *results = [UserRLM allObjects];
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    
    for (AccountRLM *account in results) {
//        if ([account.ID isEqualToString:accountID]) {
////            account.current = YES;
//        }
//        else {
////            account.current = NO;
//        }
    }
    
    [realm commitWriteTransaction];
}

- (NSDictionary *)objectWithID:(NSString *)ID class:(NSString *)className {
    // находим существующий объект по айдишнику
    RLMObject *object = [[NSClassFromString(className) alloc] init];
    
    if (!object) {
        return nil;
    }
    
    RLMObject *existingObject = [[object class] objectForPrimaryKey:ID];
    
    if (!existingObject) {
        return nil;
    }
    
    return [self objectRepresentation:existingObject ofClass:className];
}

- (NSDictionary *)objectRepresentation:(RLMObject *)object ofClass:(NSString *)className {
    // берем схему объекта и составляем представление объекта ввиде словаря
    RLMObjectSchema *schema = object.objectSchema;
    
    NSMutableDictionary *propertiesDict = [[NSMutableDictionary alloc] initWithCapacity:schema.properties.count];
    
    for (RLMProperty *property in schema.properties) {
        if (property.type == RLMPropertyTypeObject) {
            RLMObject *propertyObject = [object valueForKey:property.name];
            
            if (propertyObject) {
                NSDictionary *propertyObjectDict = [self objectRepresentation:propertyObject ofClass:property.objectClassName];
                if (propertyObjectDict) {
                    propertiesDict[property.name] = propertyObjectDict;
                }
            }
        }
        else if (property.type == RLMPropertyTypeArray) {
            RLMArray *propertyArray = [object valueForKey:property.name];
            
            if (propertyArray) {
                NSMutableArray *propertyObjectsArray = [[NSMutableArray alloc] initWithCapacity:propertyArray.count];
                
                for (RLMObject *propertyObject in propertyArray) {
                    NSDictionary *propertyObjectDict = [self objectRepresentation:propertyObject ofClass:property.objectClassName];
                    
                    if (propertyObjectDict) {
                        [propertyObjectsArray addObject:propertyObjectDict];
                    }
                }
                
                if (propertyObjectsArray.count > 0) {
                    propertiesDict[property.name] = propertyObjectsArray;
                }
            }
        }
        else {
            id propertyValue = [object valueForKey:property.name];
            
            if (propertyValue) {
                propertiesDict[property.name] = propertyValue;
            }
        }
    }
    
    NSDictionary *objectDict = @{kClass: className, kProperties: propertiesDict};
    
    return objectDict;
}

#pragma mark - Measurement

- (void)generateDummyData {
    RLMRealm* realm = [RLMRealm defaultRealm];
    
    RLMResults* array = [MeasurementRLM allObjects];
    if (array.count > 0)
        return;
    
//    NSLog(@"generating dummy data");
    [realm beginWriteTransaction]; {
        [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Пульс (покой)",
                                                        @"shortName": @"Пульс (покой)",
                                                        @"plotID": @"kPulseCalm",
                                                        @"unitName": @"Уд/мин",
                                                        @"decimalDigits": @0,
                                                        @"rangeOfValuesMin": @30,
                                                        @"rangeOfValuesMax": @250,
                                                        @"normalValueLow": @80,
                                                        @"normalValueHigh": @90,
                                                        @"defaultValue": @80
                                                        }],
        [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Пульс (активность)",
                                                        @"shortName": @"Активность",
                                                        @"plotID": @"kPulseActive",
                                                        @"unitName": @"Уд/мин",
                                                        @"decimalDigits": @0,
                                                        @"rangeOfValuesMin": @30,
                                                        @"rangeOfValuesMax": @250,
                                                        @"normalValueLow": @100,
                                                        @"normalValueHigh": @120,
                                                        @"defaultValue": @80
                                                        }],
        [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Температура",
                                                        @"shortName": @"Температура",
                                                        @"plotID": @"kTemperature",
                                                        @"unitName": @"°C",
                                                        @"decimalDigits": @1,
                                                        @"rangeOfValuesMin": @34,
                                                        @"rangeOfValuesMax": @43,
                                                        @"normalValueLow": @36.4,
                                                        @"normalValueHigh": @36.9,
                                                        @"defaultValue": @36.6
                                                        }],
        [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Артериальное Давление (систолическое)",
                                                        @"shortName": @"Систола",
                                                        @"plotID": @"kSystolicBloodPressure",
                                                        @"unitName": @"-",
                                                        @"decimalDigits": @0,
                                                        @"rangeOfValuesMin": @80,
                                                        @"rangeOfValuesMax": @280,
                                                        @"normalValueLow": @110,
                                                        @"normalValueHigh": @120,
                                                        @"defaultValue": @120
                                                        }],
        [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Артериальное Давление (диастолическое)",
                                                        @"shortName": @"Диастола",
                                                        @"plotID": @"kDiastolicBloodPressure",
                                                        @"unitName": @"-",
                                                        @"decimalDigits": @0,
                                                        @"rangeOfValuesMin": @30,
                                                        @"rangeOfValuesMax": @140,
                                                        @"normalValueLow": @70,
                                                        @"normalValueHigh": @80,
                                                        @"defaultValue": @80
                                                        }],
        [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Рост",
                                                        @"shortName": @"Рост",
                                                        @"plotID": @"kHeight",
                                                        @"unitName": @"См",
                                                        @"decimalDigits": @0,
                                                        @"rangeOfValuesMin": @40,
                                                        @"rangeOfValuesMax": @230,
                                                        @"normalValueLow": @170,
                                                        @"normalValueHigh": @180,
                                                        @"defaultValue": @165
                                                        }],
        [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Вес",
                                                        @"shortName": @"Вес",
                                                        @"plotID": @"kWeight",
                                                        @"unitName": @"Кг",
                                                        @"decimalDigits": @1,
                                                        @"rangeOfValuesMin": @3,
                                                        @"rangeOfValuesMax": @150,
                                                        @"normalValueLow": @70,
                                                        @"normalValueHigh": @80,
                                                        @"defaultValue": @60
                                                        }],
        [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Сахар (натощак)",
                                                        @"shortName": @"Натощак",
                                                        @"plotID": @"kSugarOnAnEmptyStomach",
                                                        @"unitName": @"ммоль/л",
                                                        @"decimalDigits": @1,
                                                        @"rangeOfValuesMin": @0.1f,
                                                        @"rangeOfValuesMax": @20.0f,
                                                        @"normalValueLow": @5.0f,
                                                        @"normalValueHigh": @6.0f,
                                                        @"defaultValue": @5.f
                                                        }],
        [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Сахар (до еды)",
                                                        @"shortName": @"До еды",
                                                        @"plotID": @"kSugarBeforeMeals",
                                                        @"unitName": @"ммоль/л",
                                                        @"decimalDigits": @1,
                                                        @"rangeOfValuesMin": @0.1f,
                                                        @"rangeOfValuesMax": @20.0f,
                                                        @"normalValueLow": @5.0f,
                                                        @"normalValueHigh": @6.0f,
                                                        @"defaultValue": @5.f
                                                        }],
        [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Сахар (2 часа после еды)",
                                                        @"shortName": @"После еды",
                                                        @"plotID": @"kSugarAfterMeals",
                                                        @"unitName": @"ммоль/л",
                                                        @"decimalDigits": @1,
                                                        @"rangeOfValuesMin": @0.1f,
                                                        @"rangeOfValuesMax": @20.0f,
                                                        @"normalValueLow": @5.0f,
                                                        @"normalValueHigh": @6.0f,
                                                        @"defaultValue": @5.f
                                                        }],
        [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Сахар (ночью)",
                                                        @"shortName": @"Ночью",
                                                        @"plotID": @"kSugarAtNight",
                                                        @"unitName": @"ммоль/л",
                                                        @"decimalDigits": @1,
                                                        @"rangeOfValuesMin": @0.1f,
                                                        @"rangeOfValuesMax": @20.0f,
                                                        @"normalValueLow": @5.0f,
                                                        @"normalValueHigh": @6.0f,
                                                        @"defaultValue": @5.f
                                                        }],
        [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Холестерин (общий)",
                                                        @"shortName": @"Общий",
                                                        @"plotID": @"kCholesterolGeneral",
                                                        @"unitName": @"ммоль/л",
                                                        @"decimalDigits": @1,
                                                        @"rangeOfValuesMin": @0.0f,
                                                        @"rangeOfValuesMax": @15.9f,
                                                        @"normalValueLow": @5.0f,
                                                        @"normalValueHigh": @6.0f,
                                                        @"defaultValue": @3.f
                                                        }],
        [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Холестерин (ЛПНВ)",
                                                        @"shortName": @"ЛПВН",
                                                        @"plotID": @"kCholesterolLPNP",
                                                        @"unitName": @"ммоль/л",
                                                        @"decimalDigits": @1,
                                                        @"rangeOfValuesMin": @0.0f,
                                                        @"rangeOfValuesMax": @15.9f,
                                                        @"normalValueLow": @5.0f,
                                                        @"normalValueHigh": @6.0f,
                                                        @"defaultValue": @3.f
                                                        }],
        [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Холестерин (ЛПВП)",
                                                        @"shortName": @"ЛПВП",
                                                        @"plotID": @"kCholesterolLPVP",
                                                        @"unitName": @"ммоль/л",
                                                        @"decimalDigits": @1,
                                                        @"rangeOfValuesMin": @0.0f,
                                                        @"rangeOfValuesMax": @15.9f,
                                                        @"normalValueLow": @5.0f,
                                                        @"normalValueHigh": @6.0f,
                                                        @"defaultValue": @3.f
                                                        }],
        [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Холестерин (ТГ)",
                                                        @"shortName": @"ТГ",
                                                        @"plotID": @"kCholesterolTG",
                                                        @"unitName": @"ммоль/л",
                                                        @"decimalDigits": @1,
                                                        @"rangeOfValuesMin": @0.0f,
                                                        @"rangeOfValuesMax": @15.9f,
                                                        @"normalValueLow": @5.0f,
                                                        @"normalValueHigh": @6.0f,
                                                        @"defaultValue": @3.f
                                                        }];
        //                                         [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Объем груди",
        //                                                                                         @"shortName": @"Грудь",
        //                                                                                         @"plotID": @"kBreastVolume",
        //                                                                                         @"unitName": @"См",
        //                                                                                         @"decimalDigits": @0,
        //                                                                                         @"rangeOfValuesMin": @30,
        //                                                                                         @"rangeOfValuesMax": @200,
        //                                                                                         @"normalValueLow": @90,
        //                                                                                         @"normalValueHigh": @100
        //                                                                                         }],
        //                                         [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Объем талии",
        //                                                                                         @"shortName": @"Талия",
        //                                                                                         @"plotID": @"kWaistVolume",
        //                                                                                         @"unitName": @"См",
        //                                                                                         @"decimalDigits": @0,
        //                                                                                         @"rangeOfValuesMin": @30,
        //                                                                                         @"rangeOfValuesMax": @200,
        //                                                                                         @"normalValueLow": @60,
        //                                                                                         @"normalValueHigh": @70
        //                                                                                         }],
        //                                         [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Объем бедер",
        //                                                                                         @"shortName": @"Бедра",
        //                                                                                         @"plotID": @"kThighsVolume",
        //                                                                                         @"unitName": @"См",
        //                                                                                         @"decimalDigits": @0,
        //                                                                                         @"rangeOfValuesMin": @30,
        //                                                                                         @"rangeOfValuesMax": @200,
        //                                                                                         @"normalValueLow": @90,
        //                                                                                         @"normalValueHigh": @100
        //                                                                                         }];
        //        NSInteger n = 7;
        //        NSDate* startDate = [NSDate dateWithTimeIntervalSinceNow:-7*24*60*60];
        //        for (MeasurementRLM* m in ma) {
        //            float d = (m.normalValueHigh-m.normalValueLow)/(n+1-4);
        //            float base = m.normalValueLow - d*2;
        //
        //            for (int i=0; i<=n; i++) {
        //                float r = ((float)rand()/RAND_MAX - 0.5f) * d;
        //                [m.readings addObject:
        //                 [ReadingRLM createInRealm:realm withValue:@{@"date": [NSDate dateWithTimeInterval:i*24*60*60 sinceDate:startDate],
        //                                                             @"value": @(base + i*d + r)
        //                                                             }]];
        //            }
        //        }
        
    }
    [realm commitWriteTransaction];
}

- (void)deleteDummyData {
    RLMRealm* realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction]; {
        [realm deleteAllObjects];
    }
    [realm commitWriteTransaction];
}

- (RLMResults *)myMeasurementsResult {
    RLMResults *res = [MeasurementRLM allObjects];
    return res;
}

- (RLMResults *)myReadingsResult {
    UserRLM *user = self.account.family.currentMember;
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"user = %@",
                         user];
    RLMResults *res = [ReadingRLM objectsWithPredicate: pred];
    return res;
}

- (RLMResults *)myMeasurementsByName:(NSString *)name {
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"name CONTAINS %@",
                         name];
    RLMResults *res = [MeasurementRLM objectsWithPredicate: pred];
    return res;
}


- (MeasurementRLM *)getMeasurementRLMByPlotID:(NSString *)plotID {
    MeasurementRLM *meas = [MeasurementRLM objectForPrimaryKey:plotID];
    
    return meas;
}

#pragma mark - Setters/Getters

- (AccountRLM *)account {
    RLMResults *res = [AccountRLM allObjects];
//    RLMResults *res2 = [UserRLM allObjects];
//    NSLog(@"%@", res);
//    NSLog(@"%@", res2);
    
    return res.firstObject;
}


@end