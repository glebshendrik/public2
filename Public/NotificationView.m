//
//  NotificationView.m
//  Public
//
//  Created by Глеб on 25/11/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import "NotificationView.h"
#import "TeoriusHelper.h"
#import "SharedClass.h"
#import "RecommendationActionSheet.h"

@implementation NotificationView

- (void)placeSubviews {
    CGFloat x = SCALED_F(30.f);
    CGFloat y = SCALED_F(17.f);
    self.optionsButton.frame = CGRectMake(self.width-x, y, SCALED_F(16.5f), SCALED_F(10.f));
    
    [self.optionsButton setImage:[UIImage imageNamed:@"greenArrow"] forState:UIControlStateNormal];
    [self.optionsButton addTarget:self action:@selector(optionsButtonPressed) forControlEvents:UIControlEventTouchUpInside];
}

- (CGFloat)calculateHeight {
    return 200.f;
}

- (instancetype)init {
    self = [super init];
    
    if (!self) {
        return nil;
    }
    
    [self bInit];
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (!self) {
        return nil;
    }
    
    [self bInit];
    
    return self;
}

- (void)bInit {
    [self addButton];
}

- (void)addButton {
    _optionsButton = [UIButton new];
    
    [self addSubview:_optionsButton];
}

- (void)optionsButtonPressed {
    [self.delegate showPlainRecommendationOptionsForView:self];
    
}


#pragma mark - View's lifecycle

- (void)willMoveToSuperview:(UIView *)newSuperview {
    [super willMoveToSuperview:newSuperview];
    
    [self placeSubviews];
}


@end
