//
//  GraphPointInfoView.m
//  Public
//
//  Created by Tagi on 08.12.15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import "GraphPointInfoView.h"
#import "TeoriusHelper+Extension.h"

@interface GraphPointInfoView() {
    UIEdgeInsets _contentInsets;
    CGFloat _verticalSpace;
    CGFloat _horizontalSpace;
}

@property (nonatomic) BOOL contentInsetsWasSet;
@property (nonatomic) BOOL horizontalSpaceWasSet;
@property (nonatomic) BOOL verticalSpaceWasSet;

@end

@implementation GraphPointInfoView


#pragma mark - Init

- (instancetype)init {
    self = [super init];
    
    if (!self) {
        return nil;
    }
    
    [self baseInit];
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (!self) {
        return nil;
    }
    
    [self baseInit];
    
    return self;
}


- (void)baseInit {
    self.backgroundColor = [UIColor whiteColor];
    
    _titleLabel = [UILabel new];
    [self addSubview:_titleLabel];
    
    _leftDetailLabel = [UILabel new];
    [self addSubview:_leftDetailLabel];

    _rightDetailLabel = [UILabel new];
    [self addSubview:_rightDetailLabel];
}


#pragma mark - View's lifecycle

- (void)willMoveToSuperview:(UIView *)newSuperview {
    [super willMoveToSuperview:newSuperview];
    
    [self placeSubviews];
}


#pragma mark - Subviews

- (void)placeSubviews {
    self.titleLabel.y = self.contentInsets.top;
    self.titleLabel.centerX = self.halfWidth;
    
    [self.leftDetailLabel sizeToFit];
    self.leftDetailLabel.y = self.titleLabel.maxY + self.verticalSpace;
    self.leftDetailLabel.x = self.contentInsets.left;
    
    [self.rightDetailLabel sizeToFit];
    self.rightDetailLabel.y = self.leftDetailLabel.minY + (floor(self.leftDetailLabel.font.ascender) - floor(self.rightDetailLabel.font.ascender));
    self.rightDetailLabel.x = self.leftDetailLabel.maxX + self.horizontalSpace;
}

- (void)sizeToFit {
    [super sizeToFit];
    
    [self.titleLabel sizeToFit];
    [self.leftDetailLabel sizeToFit];
    [self.rightDetailLabel sizeToFit];
    
    CGFloat width = MAX(self.titleLabel.width, self.leftDetailLabel.width + self.horizontalSpace + self.rightDetailLabel.width);
    
    self.size = CGSizeMake(width + self.contentInsets.left + self.contentInsets.right, self.rightDetailLabel.height + self.contentInsets.bottom + self.titleLabel.height + self.contentInsets.top + self.verticalSpace);
    [self placeSubviews];
}


#pragma mark - Setters/Getters

- (UIEdgeInsets)contentInsets {
    if (!self.contentInsetsWasSet) {
        _contentInsets = UIEdgeInsetsMake(SCALED_F(6.f), SCALED_F(6.f), SCALED_F(6.f), SCALED_F(6.f));
    }
    
    return _contentInsets;
}

- (void)setContentInsets:(UIEdgeInsets)contentInsets {
    _contentInsets = contentInsets;
    self.contentInsetsWasSet = YES;
}

- (CGFloat)verticalSpace {
    if (!self.verticalSpaceWasSet) {
        _verticalSpace = 0;//SCALED_F(1.f);
    }
    
    return _verticalSpace;
}

- (void)setVerticalSpace:(CGFloat)verticalSpace {
    _verticalSpace = verticalSpace;
    self.verticalSpaceWasSet = YES;
}

- (CGFloat)horizontalSpace {
    if (!self.horizontalSpaceWasSet) {
        _horizontalSpace = SCALED_F(3.f);
    }
    
    return _horizontalSpace;
}

- (void)setHorizontalSpace:(CGFloat)horizontalSpace {
    _horizontalSpace = horizontalSpace;
    self.horizontalSpaceWasSet = YES;
}

@end
