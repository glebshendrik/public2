//
//  NotificationView.h
//  Public
//
//  Created by Глеб on 25/11/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol NotificationViewDelegate <NSObject>

- (void)showPlainRecommendationOptionsForView:(UIView*)recommendationView;

@end

@interface NotificationView : UIView


@property (nonatomic, readonly) UIButton *optionsButton;

@property (nonatomic) id <NotificationViewDelegate>delegate;

- (void)placeSubviews;
- (CGFloat)calculateHeight;

@end
