//
//  MainAPI.h
//  Electro_nrj
//
//  Created by Tagi on 26.05.15.
//  Copyright (c) 2015 Pavel Zarudnev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "UserRLM.h"
#import "Measurement.h"

@class User;

extern NSString *const kMainAPIErrorUnknown;
extern NSString *const kMainAPIErrorMergingAccountsRequest;
extern NSString *const kNotificationPersonalDataWasChanged;
extern NSString *const kNotificationPaymentWasSent;
extern NSString *const kNotificationLogoutMade;

@interface MainAPI : NSObject

@property (nonatomic) NSString *previousCardID;
@property (nonatomic, copy) NSString *currentDeviceToken;
@property (nonatomic) NSDictionary *deviceTokensDictionary;
@property (nonatomic) User *currentUser;
@property (nonatomic, readonly) NSArray *familyMembers;
@property (nonatomic, readonly) NSArray *measurementsArray;

+ (MainAPI *)defaultAPI;

- (void)loginWithLogin:(NSString *)login password:(NSString *)password completion:(void (^)(BOOL success, NSError *error))completion;
- (void)registerWithLogin:(NSString *)login password:(NSString *)password completion:(void (^)(BOOL success, NSError *error))completion;
- (void)getUserInfoForUserID:(NSString *)userID completion:(void (^)(BOOL success, NSError *error))completion;
- (void)authWithPassportNumber:(NSString *)number passportSeries:(NSString *)series birthDate:(NSString *)birthDate completion:(void (^)(BOOL, NSError *))completion;
- (void)authWithPolisNumber:(NSString *)number  birthDate:(NSString *)birthDate completion:(void (^)(BOOL, NSError *))completion;

- (void)registerDeviceToken;

- (void)getTotalAppointments:(void (^)(BOOL, NSError *))completion;
- (void)getFutureAppointments:(void (^)(NSArray *, NSError *))completion;
- (void)getClinics:(void (^)(NSArray *, NSError *))completion;
- (void)getResourcesForClinicID:(NSString *)clinicID completion:(void (^)(NSArray *, NSError *))completion;
- (void)getScheduleForClinicID:(NSString *)clinicID serviceID:(long)serviceID resourceID:(long)resourceID year:(int)year month:(int)month completion:(void (^)(NSDictionary *, NSError *))completion;
- (void)createAppointmentForClinicId:(NSString *)clinicId ServiceId:(long)serviceId resourceId:(long)resourceId year:(NSUInteger)year month:(NSUInteger)month day:(NSUInteger)day timeIntervalId:(long)timeIntervalId completion:(void (^)(BOOL, NSError *))completion;
- (void)createAppointmentForClinicId:(NSString *)clinicId ServiceId:(long)serviceId resourceId:(long)resourceId year:(NSUInteger)year month:(NSUInteger)month day:(NSUInteger)day timeIntervalId:(long)timeIntervalId phone:(NSString *)phone completion:(void (^)(BOOL, NSError *))completion;
- (void)deleteAppointmentWithId:(NSString *)appointmentId completion:(void (^)(BOOL, NSError *))completion;

- (void)getUserCitizen: (void (^)(NSArray *, NSError *))completion;
- (void)createNewUser: (User *)user completion: (void (^)(NSDictionary *arr, NSError *err))completion;
- (void)updateUser: (User *)user completion: (void (^)(NSDictionary *dic, NSError *err))completion;
- (void)getAllActiveMeasure:(User *)user completion: (void (^)(NSDictionary *dic, NSError *err))completion;
- (void)getAllMeasure:(User *)user completion:(void (^)(NSDictionary *, NSError *))completion;
- (void)createPulse:(NSString *)fixTime status:(NSString *)status beat:(NSString *)beat completion:(void(^)(NSDictionary *, NSError *))completion;
- (void)newReading:(Measurement *)meas date:(NSDate *)date value:(NSArray *)value completion:(void(^)(BOOL success, NSError *error))completion;

- (void)getAllNotificationsAfterTapped:(void(^)(NSMutableArray *, NSMutableArray *, NSError *))completion;
- (void)sendAnswerAndStateNotifications;

- (void)logout;

- (NSArray *)myReadingsArr;
- (NSArray *)getReadingsForGraphByName:(NSString *)str fromReadings:(NSArray *)readings;
- (NSArray *)getReadingsByCorePlotName:(NSString *)str fromReadings:(NSArray *)readings;
- (NSArray *)getReadingsForMeasurements:(NSArray *)measurements fromReadings:(NSArray *)readings;
- (NSArray *)getMeasurementsByGraphName:(NSString *)graphName fromMeasurements:(NSArray *)measurements;
- (NSDictionary *)groupReadings:(NSArray *)readings byMeasurements:(NSArray *)measurements;

@end