//
//  NotificationTextFieldView.h
//  Public
//
//  Created by Глеб on 27/11/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import "NotificationInterView.h"

typedef NS_ENUM(int, FieldType) {
    FieldTypeText,
    FieldTypePicker
};

@interface NotificationTextFieldView : NotificationInterView

@property (nonatomic) FieldType typeField;
@property (nonatomic) UITextField *textField;
@property (nonatomic) UIDatePicker *picker;

@end
