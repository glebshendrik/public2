//
//  NSObject+PerformBlockAfterDelay.h
//  Namaz
//
//  Created by Tagi on 20.06.14.
//
//

#import <Foundation/Foundation.h>

@interface NSObject (PerformBlockAfterDelay)

- (void)performBlock:(void (^)(void))block afterDelay:(NSTimeInterval)delay;

@end
