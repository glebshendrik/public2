//
//  GraphPointInfoView.h
//  Public
//
//  Created by Tagi on 08.12.15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GraphPointInfoView : UIView

@property (nonatomic, readonly) UILabel *titleLabel;
@property (nonatomic, readonly) UILabel *leftDetailLabel;
@property (nonatomic, readonly) UILabel *rightDetailLabel;
@property (nonatomic) UIEdgeInsets contentInsets;
@property (nonatomic) CGFloat verticalSpace;
@property (nonatomic) CGFloat horizontalSpace;

- (void)placeSubviews;

@end
