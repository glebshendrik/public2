//
//  RESTAPIManager.h
//  Helterbook
//
//  Created by Tagi on 18.03.15.
//  Copyright (c) 2015 infoshell. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const kRESTAPIError;
extern NSString *const kRESTAPIMessage;
extern NSString *const kRESTAPIUUID;
extern NSString *const kRESTAPIList;
extern NSString *const kRESTAPIText;
extern NSString *const kRESTAPIConsts;
extern NSString *const kRESTAPIName;
extern NSString *const kRESTAPIRegion;
extern NSString *const kRESTAPINumberOfResidents;
extern NSString *const kRESTAPISettlementType;
extern NSString *const kRESTAPIStove;
extern NSString *const kRESTAPICounterType;
extern NSString *const kRESTAPIMeterReadingMonth;
extern NSString *const kRESTAPIMeterReadingYear;
extern NSString *const kRESTAPIMeterDataOld;
extern NSString *const kRESTAPIMeterDataDayOld;
extern NSString *const kRESTAPIMeterDataNightOld;
extern NSString *const kRESTAPIMeterData;
extern NSString *const kRESTAPIMeterDataDay;
extern NSString *const kRESTAPIMeterDataNight;
extern NSString *const kRESTAPIExtra;
extern NSString *const kRESTAPIErrorMessageCode;
extern NSString *const kRESTAPIDefaultProvider;
extern NSString *const kRESTAPICommission;
extern NSString *const kRESTAPICommissionMin;
extern NSString *const kRESTAPIINN;
extern NSString *const kRESTAPIKPP;
extern NSString *const kRESTAPIRS;
extern NSString *const kRESTAPIKS;
extern NSString *const kRESTAPIBankName;
extern NSString *const kRESTAPIBIC;
extern NSString *const kRESTAPIIsSocNorm;
extern NSString *const kRESTAPIProviderUUID;
extern NSString *const kRESTAPIRoomAccount;
extern NSString *const kRESTAPIRoomOwner;
extern NSString *const kRESTAPIAddress;
extern NSString *const kRESTAPISocNorm;
extern NSString *const kRESTAPIProvider;
extern NSString *const kRESTAPIUser;
extern NSString *const kRESTAPIEmail;
extern NSString *const kRESTAPIFIO;
extern NSString *const kRESTAPIPhone;
extern NSString *const kRESTAPIDoc;
extern NSString *const kRESTAPIIsDefaultProvider;
extern NSString *const kRESTAPIRegisterCard;
extern NSString *const kRESTAPICardUuid;
extern NSString *const kRESTAPIRoomUuid;
extern NSString *const kRESTAPIRoomUuid2;
extern NSString *const kRESTAPIAmount;
extern NSString *const kRESTAPIRedirectUrl;
extern NSString *const kRESTAPIUrl;
extern NSString *const kRESTAPIStatusCode;
extern NSString *const kRESTAPISumm;
extern NSString *const kRESTAPIBaseSumm;
extern NSString *const kRESTAPICounterNumber;

@interface RESTAPIManager : NSObject

@property (nonatomic, copy) NSString *base64String;
@property (nonatomic) NSString *jsessionID;

// User
- (void)loginWithLogin:(NSString *)login password:(NSString *)password completion:(void (^)(NSDictionary *responseDict, NSError *error))completion;
- (void)registerWithLogin:(NSString *)login password:(NSString *)password completion:(void (^)(NSDictionary *, NSError *))completion;
- (void)getUserInfoForUserID:(NSString *)userID completion:(void (^)(NSDictionary *, NSError *))completion;
//authWithDocument
- (void)authWithDocument:(NSString *)typeDoc docNumber:(NSString *)docNumber docSeries:(NSString *)docSeries birthDate:(NSString *)birthDate completion:(void (^)(NSDictionary *, NSError *))completion;

- (void)registerDeviceToken:(NSString *)deviceToken;

- (void)getTotalAppointments:(void (^)(NSDictionary *, NSError *))completion;
- (void)getFutureAppointments:(void (^)(NSDictionary *, NSError *))completion;
- (void)getClinics:(void (^)(NSDictionary *, NSError *))completion;
- (void)getResourcesForClinicID:(NSString *)clinicID completion:(void (^)(NSDictionary *, NSError *))completion;
- (void)getScheduleForClinicID:(NSString *)clinicID serviceID:(NSString *)serviceID resourceID:(NSString *)resourceID year:(int)year month:(int)month completion:(void (^)(NSDictionary *, NSError *))completion;
- (void)createAppointmentForClinicId:(NSString *)clinicId ServiceId:(long)serviceId resourceId:(long)resourceId year:(NSUInteger)year month:(NSUInteger)month day:(NSUInteger)day timeIntervalId:(long)timeIntervalId completion:(void (^)(NSDictionary *, NSError *))completion;
- (void)createAppointmentForClinicId:(NSString *)clinicId ServiceId:(long)serviceId resourceId:(long)resourceId year:(NSUInteger)year month:(NSUInteger)month day:(NSUInteger)day timeIntervalId:(long)timeIntervalId phone:(NSString *)phone completion:(void (^)(NSDictionary *, NSError *))completion;
- (void)deleteAppointmentWithId:(NSString *)appointmentId completion:(void (^)(NSDictionary *, NSError *))completion;
- (void)sendAppointmentInfoWithDict:(NSDictionary *)dict completion:(void (^)(NSDictionary *, NSError *))completion;
- (void)sendAppointmentCancellingInfoWithId:(NSString *)ID completion:(void (^)(NSDictionary *, NSError *))completion;

- (void)getUserCitizen:(void (^)(NSArray *, NSError *))completion;
- (void)createNewUser:(NSDictionary *)bodyDict completion:(void (^)(NSDictionary *, NSError *))completion;
- (void)updateUser:(NSDictionary *)bodyDict idUser:(NSString *)idUser completion:(void (^)(NSDictionary *, NSError *))completion;
- (void)getAllActiveMeasure:(NSString *)idUser completion:(void (^)(NSDictionary *, NSError *))completion;
- (void)getAllMeasure:(NSString *)idUser date:(NSString *)date limit:(NSNumber *)limit completion:(void (^) (NSDictionary *, NSError *))completion;
- (void)createReading:(NSArray *)arr plotID:(NSString *)plotID completion:(void (^)(NSArray *, NSError *))completion;

- (void)getAllNotificationsAfterTapped:(NSString *)date completion:(void (^) (NSDictionary *, NSError *))completion;
- (void)sendAnswerAndStateNotifications:(NSArray *)arr completion:(void (^)(NSArray *, NSError *))completion;

- (void)logoutWithCompletion:(void (^)(NSDictionary *, NSError *))completion;

@end
