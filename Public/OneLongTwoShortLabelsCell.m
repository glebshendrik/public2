//
//  OneLongTwoShortLabelsCell.m
//  
//
//  Created by Tagi on 03.12.15.
//
//

#import "OneLongTwoShortLabelsCell.h"
#import "TeoriusHelper+Extension.h"

#define VERTICAL_DISTANCE 2.f
#define LABELS_INSET_LEFT_RIGHT 16.f
#define LABELS_INSET_TOP_BOTTOM 14.f

@implementation OneLongTwoShortLabelsCell

- (instancetype)init {
    self = [super initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:nil];
    
    if (!self) {
        return nil;
    }
    
    [self baseInit];
    
    return self;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    
    if (!self) {
        return nil;
    }
    
    [self baseInit];
    
    return self;
}

- (void)baseInit {
    _labelOne = [UILabel new];
    _labelOne.numberOfLines = 0;
    [self.contentView addSubview:_labelOne];
    
    _labelTwo = [UILabel new];
    [self.contentView addSubview:_labelTwo];
    
    _labelThree = [UILabel new];
    [self.contentView addSubview:_labelThree];
}


#pragma mark - View's lifecycle

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat x = LABELS_INSET_LEFT_RIGHT;
    CGFloat originX = self.shortLabelsDoubleOffset ? 2 * x : x;
    CGFloat maxWidth = self.contentView.width - (x + originX);
    
    self.labelOne.size = CGSizeMake(maxWidth, 0);
    [self.labelOne sizeToFit];
    
    self.labelOne.origin = CGPointMake(originX, LABELS_INSET_TOP_BOTTOM);
    
    [self.labelTwo sizeToFit];
    [self.labelTwo cutWidthTo:maxWidth];
    self.labelTwo.origin = CGPointMake(self.labelOne.minX, self.labelOne.maxY + VERTICAL_DISTANCE);
    
    [self.labelThree sizeToFit];
    [self.labelThree cutWidthTo:maxWidth];
    self.labelThree.origin = CGPointMake(self.labelTwo.minX, self.labelTwo.maxY + VERTICAL_DISTANCE);
}


#pragma mark - Class methods

+ (CGFloat)heightOfLabelOneWithText:(NSString *)text font:(UIFont *)font viewWidth:(CGFloat)viewWidth {
    return [text boundingRectWithSize:CGSizeMake(viewWidth - LABELS_INSET_LEFT_RIGHT * 2.f, 0) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName: font} context:nil].size.height;
}

+ (CGFloat)heightOfLabelOneWithText:(NSString *)text font:(UIFont *)font viewWidth:(CGFloat)viewWidth withDoubleOffset:(BOOL)doubleOffset {
    CGFloat offset = doubleOffset ? LABELS_INSET_LEFT_RIGHT * 3.f : LABELS_INSET_LEFT_RIGHT * 2.f;
    return [text boundingRectWithSize:CGSizeMake(viewWidth - offset, 0) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName: font} context:nil].size.height;
}

+ (CGFloat)heightOfLabelTwoWithText:(NSString *)text font:(UIFont *)font viewWidth:(CGFloat)viewWidth {
    return [text boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName: font} context:nil].size.height;
}

+ (CGFloat)heightOfLabelThreeWithText:(NSString *)text font:(UIFont *)font viewWidth:(CGFloat)viewWidth {
    return [text boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName: font} context:nil].size.height;
}

+ (CGFloat)additionalHeight {
    return LABELS_INSET_TOP_BOTTOM * 2.f + VERTICAL_DISTANCE * 2.f;
}

+ (CGFloat)additionalHeight1 {
    return LABELS_INSET_TOP_BOTTOM * 2.f + VERTICAL_DISTANCE;
}

+ (CGFloat)additionalHeight2 {
    return LABELS_INSET_TOP_BOTTOM * 2.f;
}

@end
