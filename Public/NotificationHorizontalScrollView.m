//
//  NotificationHorizontalScrollView.m
//  Public
//
//  Created by Глеб on 27/11/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import "NotificationHorizontalScrollView.h"
#import "TeoriusHelper.h"
#import "SharedClass.h"

@interface NotificationHorizontalScrollView()

@property (nonatomic) UIScrollView *hScroll;

@end

@implementation NotificationHorizontalScrollView

- (instancetype)init {
    self = [super init];
    
    if (!self) {
        return nil;
    }
    
    [self baseInit];
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (!self) {
        return nil;
    }
    
    [self baseInit];
    
    return self;
}

- (void)baseInit {
    [self addScrollH];
    self.clipsToBounds = YES;
}

- (void)addScrollH {
    _hScroll = [UIScrollView new];
    _hScroll.pagingEnabled = YES;
    _hScroll.contentSize = CGSizeMake(SCALED_F(1000.f), self.height);
    _hScroll.showsHorizontalScrollIndicator = NO;
    [self addSubview:_hScroll];
}

- (void)placeSubviews {
    [super placeSubviews];
//    CGFloat x = SCALED_F(17.f);
//    self.textLabel.frame = CGRectMake(x, SCALED_F(30.f), self.width - x - SCALED_F(36.f), 0);
//    [self.textLabel sizeToFit];
    
    self.hScroll.frame = CGRectMake(0, 0, self.width, self.height);
}

- (CGFloat)calculateWidth {
    [self placeSubviews];
    
    return 400;
}

#pragma mark - View's lifecycle

- (void)willMoveToSuperview:(UIView *)newSuperview {
    [super willMoveToSuperview:newSuperview];
    [self placeSubviews];
}


#pragma mark - Setters/Getters

- (void)setElements:(NSArray *)elements {
    CGFloat maxX = 0;
    
    for (UIView *view in elements) {
        if (![view isKindOfClass:[UIView class]]) {
            continue;
        }
        
        view.x = maxX;
        view.y = 0;
        [self.hScroll addSubview:view];

        maxX = view.maxX;
        
    }
}


@end