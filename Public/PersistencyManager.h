//
//  PersistencyManager.h
//  Public
//
//  Created by Глеб on 13/10/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AccountRLM.h"
#import "UserRLM.h"
#import "AppointmentRLM.h"
#import "ReadingRLM.h"

@interface PersistencyManager : NSObject

extern NSString *const kClass;
extern NSString *const kProperties;

@property (nonatomic, readonly) AccountRLM *account;

- (void)addOrUpdateObject:(RLMObject *)object;
- (UserRLM *)getUserByID: (NSString *)idUser;

- (RLMObject *)createObjectFromDict:(NSDictionary *)objectDict;
- (NSDictionary *)objectWithID:(NSString *)ID class:(NSString *)className;
- (void)deleteAccount;
- (void)deleteObjectsFromResults:(RLMResults *)result completion:(void (^)(BOOL success))completion;
- (BOOL)accountContainsUserWithID:(NSString *)ID;
- (RLMResults *)myMeasurementsResult;
- (RLMResults *)myReadingsResult;

- (MeasurementRLM *)getMeasurementRLMByPlotID:(NSString *)plotID;
- (void)assignCurrentUser:(NSString *)accountID;
- (RLMResults *)myMeasurementsByName:(NSString *)name;

+ (void)generateDummyData;
//- (RLMObject *)createApartmentFromDict:(NSDictionary *)apartmentDict addingToCurrentUserApartments:(BOOL)adding;
//- (NSArray *)getLocalApartments;
//- (NSArray *)getCurrentUserApartments;
//- (void)deleteApartmentWithID:(NSString *)ID;
//- (ApartmentRLM *)apartmentWithID:(NSString *)ID;

//- (void)addPaymentWithID:(NSString *)paymentID toApartmentWithID:(NSString *)apartmentID;
//- (PaymentRLM *)paymentWithID:(NSString *)ID;

@end
