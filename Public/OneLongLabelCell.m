//
//  OneLongLabelCell.m
//  Public
//
//  Created by Tagi on 03.12.15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import "OneLongLabelCell.h"
#import "TeoriusHelper+Extension.h"

#define LABELS_INSET_LEFT_RIGHT 16.f
#define LABELS_INSET_TOP_BOTTOM 14.f

@implementation OneLongLabelCell

- (instancetype)init {
    self = [super initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:nil];
    
    if (!self) {
        return nil;
    }
    
    [self baseInit];
    
    return self;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    
    if (!self) {
        return nil;
    }
    
    [self baseInit];
    
    return self;
}

- (void)baseInit {
    _longTextLabel = [UILabel new];
    _longTextLabel.numberOfLines = 0;
    [self.contentView addSubview:_longTextLabel];
}


#pragma mark - View's lifecycle

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat x = LABELS_INSET_LEFT_RIGHT;
    CGFloat maxWidth = self.contentView.width - x * 2.f;
    
    self.longTextLabel.size = CGSizeMake(maxWidth, 0);
    [self.longTextLabel sizeToFit];
    
    self.longTextLabel.origin = CGPointMake(x, LABELS_INSET_TOP_BOTTOM);
}


#pragma mark - Class methods

+ (CGFloat)heightOfLabelWithText:(NSString *)text font:(UIFont *)font viewWidth:(CGFloat)viewWidth {
    return [text boundingRectWithSize:CGSizeMake(viewWidth - LABELS_INSET_LEFT_RIGHT * 2.f, 0) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName: font} context:nil].size.height;
}

+ (CGFloat)additionalHeight {
    return LABELS_INSET_TOP_BOTTOM * 2.f;
}

@end
