//
//  NotificationInterView.m
//  Public
//
//  Created by Глеб on 25/11/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import "NotificationInterView.h"
#import "TeoriusHelper.h"
#import "SharedClass.h"

@implementation NotificationInterView

- (instancetype)init {
    self = [super init];
    
    if (!self) {
        return nil;
    }
    
    [self baseInit];
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (!self) {
        return nil;
    }
    
    [self baseInit];
    
    return self;
}

- (void)baseInit {
    [self addLabels];
}

- (void)addLabels {
    _textLabel = [UILabel new];
    _textLabel.numberOfLines = 0;
    
    [self addSubview:_textLabel];
}

- (void)placeSubviews {
    
    
    //    self.textLabel
    
    [super placeSubviews];
    
    //    [self.imageView sizeToFit];
    //    [self.imageView setIntegralCenter:CGPointMake(1, 1)];
    
    //    CGFloat textlabelX = self.imageView.maxX + x - [TeoriusHelper scaledFloat:1.f];
    CGFloat x = SCALED_F(17.f);
    
    
    self.textLabel.frame = CGRectMake(x, SCALED_F(30.f), self.width - x - SCALED_F(10.f), 0);
    [self.textLabel sizeToFit];
    
    if (self.buttonsArr.count > 0) {
        CGFloat h = SCALED_F(20.f);
        CGFloat y = self.textLabel.maxY;
        
        for (int i = 0; i<self.buttonsArr.count; i++) {
            UIButton *button = self.buttonsArr[i];
            if (![self.subviews containsObject:button]) {
                button.frame = CGRectMake(x, y+h, self.width - x - SCALED_F(10.f), h);
                
                [self addSubview:button];
                
                y = y+h + SCALED_F(14.f);
            }
        }
    }
}

- (CGFloat)calculateHeight {
    [self placeSubviews];
    
    UIButton *b = [self.buttonsArr lastObject];
    if (!b)
        return self.textLabel.y + self.textLabel.maxY;
    
    return self.textLabel.y + b.maxY;
}


#pragma mark - View's lifecycle

- (void)willMoveToSuperview:(UIView *)newSuperview {
    [super willMoveToSuperview:newSuperview];
    
    [self placeSubviews];
}

@end
