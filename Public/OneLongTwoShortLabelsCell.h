//
//  OneLongTwoShortLabelsCell.h
//  
//
//  Created by Tagi on 03.12.15.
//
//

#import <UIKit/UIKit.h>

@interface OneLongTwoShortLabelsCell : UITableViewCell

@property (nonatomic, readonly) UILabel *labelOne;
@property (nonatomic, readonly) UILabel *labelTwo;
@property (nonatomic, readonly) UILabel *labelThree;
@property (nonatomic) BOOL shortLabelsDoubleOffset;

+ (CGFloat)heightOfLabelOneWithText:(NSString *)text font:(UIFont *)font viewWidth:(CGFloat)viewWidth;
+ (CGFloat)heightOfLabelOneWithText:(NSString *)text font:(UIFont *)font viewWidth:(CGFloat)viewWidth withDoubleOffset:(BOOL)doubleOffset;
+ (CGFloat)heightOfLabelTwoWithText:(NSString *)text font:(UIFont *)font viewWidth:(CGFloat)viewWidth;
+ (CGFloat)heightOfLabelThreeWithText:(NSString *)text font:(UIFont *)font viewWidth:(CGFloat)viewWidth;
+ (CGFloat)additionalHeight;
+ (CGFloat)additionalHeight1;
+ (CGFloat)additionalHeight2;

@end
