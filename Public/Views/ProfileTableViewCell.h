//
//  NameTableViewCell.h
//  Public
//
//  Created by Teoria 5 on 02/09/15.
//  Copyright (c) 2015 Teoria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileTableViewCell : UITableViewCell

@property (nonatomic) UIImageView *overlayImageView;
@property (nonatomic) UIImageView *imageView;
@property (nonatomic) UILabel *textLabel;

@end
