//
//  MonitoringTableViewCell.m
//  Public
//
//  Created by Teoria 5 on 18/09/15.
//  Copyright (c) 2015 Teoria. All rights reserved.
//

#import "MonitoringTableViewCell.h"

@interface MonitoringTableViewCell () <CPTPlotDataSource, CPTPlotDelegate>

@end





@implementation MonitoringTableViewCell {

}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (!self) {
        return nil;
    }
    
    _titleLabel = [self createLabel];
    _hostView = [self createView];
    
    
    [self initPlot];
    [self initData];
    return self;
}


#pragma mark - scatterPLot behavior


- (void)initData {
}

-(void)initPlot {
    
    [self configureHost];
    [self configureGraph];
    [self configurePlots];
    [self configureAxes];
}

- (void)configureHost {

}

- (void)configureGraph {
    CPTTheme *theme = [CPTTheme new];
    
    CPTGraph *graph = [[CPTXYGraph alloc]initWithFrame:self.hostView.frame];
    [graph applyTheme:theme];
    self.hostView.hostedGraph = graph;
    
    [graph.plotAreaFrame setPaddingLeft:.0f];
    [graph.plotAreaFrame setPaddingBottom:40.0f];
    [graph.plotAreaFrame setPaddingTop:.0f];
    
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *) graph.defaultPlotSpace;
    plotSpace.allowsUserInteraction = NO;

}

- (void)configurePlots {
    CPTGraph *graph = self.hostView.hostedGraph;
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *) graph.defaultPlotSpace;
    
    CPTScatterPlot *activePlot = [[CPTScatterPlot alloc]init];
    activePlot.delegate = self;
    activePlot.dataSource = self;
    activePlot.interpolation = CPTScatterPlotInterpolationCurved;
    CPTColor *activeColor = [CPTColor colorWithComponentRed:.33 green:.75 blue:.69 alpha:1];
    [graph addPlot:activePlot toPlotSpace:plotSpace];
    
    CPTScatterPlot *passivePlot = [[CPTScatterPlot alloc]init];
    passivePlot.delegate = self;
    passivePlot.dataSource = self;
    passivePlot.interpolation = CPTScatterPlotInterpolationCurved;
    CPTColor *passiveColor = [CPTColor colorWithComponentRed:.33 green:.75 blue:.69 alpha:1];
    [graph addPlot:passivePlot toPlotSpace:plotSpace];
    
    [plotSpace scaleToFitPlots:@[activePlot,passivePlot]];
    CPTMutablePlotRange *xRange = [plotSpace.xRange mutableCopy];
    [xRange expandRangeByFactor:CPTDecimalFromCGFloat(1.3f)];
    plotSpace.xRange = xRange;
    CPTMutablePlotRange *yRange = [plotSpace.yRange mutableCopy];
    [yRange expandRangeByFactor:CPTDecimalFromCGFloat(1.4f)];
    plotSpace.yRange = yRange;
    
    CPTMutableLineStyle *activeLineStyle = [activePlot.dataLineStyle mutableCopy];
    activeLineStyle.lineWidth = 1.f;
    activeLineStyle.lineColor = activeColor;
    activePlot.dataLineStyle = activeLineStyle;
    CPTMutableLineStyle *activeSymbolLineStyle = [CPTMutableLineStyle lineStyle];
    activeSymbolLineStyle.lineColor = activeColor;
    CPTPlotSymbol *activeSymbol = [CPTPlotSymbol ellipsePlotSymbol];
    activeSymbol.fill = [CPTFill fillWithColor:activeColor];
    activeSymbol.lineStyle = activeSymbolLineStyle;
    activeSymbol.size = CGSizeMake(5.f, 5.f);
    activePlot.plotSymbol = activeSymbol;
    
    CPTMutableLineStyle *passiveLineStyle = [passivePlot.dataLineStyle mutableCopy];
    passiveLineStyle.lineWidth = 1.f;
    passiveLineStyle.lineColor = passiveColor;
    passivePlot.dataLineStyle = passiveLineStyle;
    CPTMutableLineStyle *passiveSymbolLineStyle = [CPTMutableLineStyle lineStyle];
    passiveSymbolLineStyle.lineColor = passiveColor;
    CPTPlotSymbol *passiveSymbol = [CPTPlotSymbol ellipsePlotSymbol];
    passiveSymbol.fill = [CPTFill fillWithColor:passiveColor];
    passiveSymbol.lineStyle = passiveSymbolLineStyle;
    passiveSymbol.size = CGSizeMake(5.f, 5.f);
    passivePlot.plotSymbol = passiveSymbol;
    
    [self findExceptionPoints];
    

}

- (void)configureAxes {

}

- (void)findExceptionPoints {
}

- (CPTScatterPlot *)createExceptionScatterPlot {
    
    CPTGraph *graph = self.hostView.hostedGraph;
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *) graph.defaultPlotSpace;
    
    CPTScatterPlot *exceptionPointPlot = [[CPTScatterPlot alloc]init];
    exceptionPointPlot.delegate = self;
    exceptionPointPlot.dataSource = self;
    exceptionPointPlot.interpolation = CPTScatterPlotInterpolationCurved;
    CPTColor *exceptionColor = [CPTColor colorWithComponentRed:.84 green:.44 blue:.39 alpha:1];
    [graph addPlot:exceptionPointPlot toPlotSpace:plotSpace];
    
    CPTMutableLineStyle *exceptionLineStyle = [exceptionPointPlot.dataLineStyle mutableCopy];
    exceptionLineStyle.lineWidth = .0f;
    exceptionLineStyle.lineColor = exceptionColor;
    exceptionPointPlot.dataLineStyle = exceptionLineStyle;
    CPTMutableLineStyle *exceptionSymbolLineStyle = [CPTMutableLineStyle lineStyle];
    exceptionSymbolLineStyle.lineColor = exceptionColor;
    CPTPlotSymbol *exceptionSymbol = [CPTPlotSymbol ellipsePlotSymbol];
    exceptionSymbol.fill = [CPTFill fillWithColor:exceptionColor];
    exceptionSymbol.lineStyle = exceptionSymbolLineStyle;
    exceptionSymbol.size = CGSizeMake(6.f, 6.f);
    exceptionPointPlot.plotSymbol = exceptionSymbol;
    
    return exceptionPointPlot;
}


#pragma mark - layoutSubviews 

- (void)layoutSubviews {
    [super layoutSubviews];
    
    
    
}

- (UILabel *)createLabel {
    UILabel *label =[UILabel new];
    label.backgroundColor = [UIColor clearColor];
    label.numberOfLines = 0;
    return label;
}

- (CPTGraphHostingView *)createView {
    CPTGraphHostingView *view = [CPTGraphHostingView new];
    view.allowPinchScaling = NO;
    view.backgroundColor = [UIColor clearColor];
    
    return view;
}


#pragma mark - CPTPlotDataSource

-(NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot {
    return 0;
}

- (NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)idx {
    
    return nil;
}


@end
