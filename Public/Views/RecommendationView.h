//
//  RecommendationView.h
//  Public
//
//  Created by Teoria 5 on 08/09/15.
//  Copyright (c) 2015 Teoria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Recommendation.h"


extern CGFloat const kRecommendationViewSpaceY;
extern CGFloat const kRecommendationViewSpaceX;


@protocol RecommendationViewDelegate <NSObject>

- (void)showPlainRecommendationOptionsForView:(UIView*)recommendationView;
- (void)showPollOptionsForView:(UIView*)recommendationView;
- (void)viewTapped:(UIGestureRecognizer *)gestureRecognizer;
- (void)reactToRecommendationClosing:(UIView*)recommendationView;

@end


@interface RecommendationView : UIView

@property (nonatomic) Recommendation *recommendation;
@property (nonatomic) id <RecommendationViewDelegate>delegate;

- (instancetype)initWithFrame:(CGRect)frame withRecommendation:(Recommendation *)recommendation;

- (void)reactToRecommendationPostponing;

@end
