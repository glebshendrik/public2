//
//  MonitoringTableViewCell.h
//  Public
//
//  Created by Teoria 5 on 18/09/15.
//  Copyright (c) 2015 Teoria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CorePlot/ios/CorePlot-CocoaTouch.h>

@interface MonitoringTableViewCell : UITableViewCell

@property (nonatomic) UILabel *titleLabel;
@property (nonatomic) CPTGraphHostingView *hostView;

@end
