//
//  RecommendationView.m
//  Public
//
//  Created by Teoria 5 on 08/09/15.
//  Copyright (c) 2015 Teoria. All rights reserved.
//

#import "RecommendationView.h"
#import "TeoriusHelper+Extension.h"
#import "SharedClass.h"
#import "UIButton+HitArea.h"


CGFloat const kRecommendationViewSpaceY = 20.f;
CGFloat const kRecommendationViewSpaceX = 15.f;


@interface RecommendationView()

// @arslan: скорее всего временное решение
@property (nonatomic) UILabel* tmpTitleLabel;

@end


@implementation RecommendationView

- (instancetype)initWithFrame:(CGRect)frame withRecommendation:(Recommendation *)recommendation {
    if (self = [super initWithFrame:frame]) {
        self.recommendation = recommendation;
        self.clipsToBounds = YES;
        [self configView];
    }
    return self;
}

- (void)configView {
    switch (self.recommendation.type) {
        case RECOMMENDATION_TYPE_BIRTHDAY:
            [self configBirthdayPoll];
            break;
        case RECOMMENDATION_TYPE_BAD_HABITS:
            [self configBadHabitsPoll];
            break;
        default:
            [self configPlainRecommendationView];
            break;
    }
}

- (void)configPlainRecommendationView {
    self.backgroundColor = [UIColor whiteColor];
    self.layer.cornerRadius = 3.f;
    if (self.recommendation.isNew) {
        self.layer.borderWidth = 1.5f;
        self.layer.borderColor = [COLOR_LIGHTERGREEN CGColor];
    }
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(viewTapped:)];
    [self addGestureRecognizer:tapGesture];
    
    UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(kRecommendationViewSpaceX, kRecommendationViewSpaceY, 15, 15)];
    [button setImage:[UIImage imageNamed:@"greenArrow"] forState:UIControlStateNormal];
    button.center = CGPointMake(self.frame.size.width - kRecommendationViewSpaceX - button.frame.size.width / 2.f, button.center.y);
    [button addTarget:self action:@selector(plainRecommendationOptionsButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [button sizeToFit];
    button.hitTestEdgeInsets = UIEdgeInsetsMake(-20, -20, -20, -20);
    [self addSubview:button];
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(kRecommendationViewSpaceX, kRecommendationViewSpaceY, self.frame.size.width - 3 * kRecommendationViewSpaceX, 0)];
    titleLabel.text = self.recommendation.title;
    titleLabel.font = self.recommendation.isNew ? [UIFont fontWithName:@"Roboto-Bold" size:16] : [SharedClass getRegularFontWithSize:16.0f];
    titleLabel.textColor = self.recommendation.isNew ? [UIColor blackColor] : COLOR_DARKGRAY;
    [titleLabel sizeToFit];
    [self addSubview:titleLabel];
    
    UILabel *dateLabel = [[UILabel alloc]initWithFrame:CGRectMake(kRecommendationViewSpaceX, CGRectGetMaxY(titleLabel.frame) + 2, self.frame.size.width - 3 * kRecommendationViewSpaceX, 0)];
    dateLabel.text = @"Сегодня";
    dateLabel.font = [SharedClass getRegularFontWithSize:11.0f];
    dateLabel.textColor = COLOR_LIGHTERGRAY;
    [dateLabel sizeToFit];
    [self addSubview:dateLabel];
    
    NSMutableParagraphStyle *parStyle = [NSMutableParagraphStyle new];
    parStyle.lineHeightMultiple = 1.3f;
    NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:self.recommendation.shortText
                                                                     attributes:@{NSParagraphStyleAttributeName: parStyle,
                                                                                  NSFontAttributeName: [SharedClass getRegularFontWithSize:14.0f],
                                                                                  NSForegroundColorAttributeName: COLOR_DARKGRAY}];
    
    UILabel *textLabel = [[UILabel alloc]initWithFrame:CGRectMake(kRecommendationViewSpaceX, CGRectGetMaxY(dateLabel.frame)+11, self.frame.size.width - 2 * kRecommendationViewSpaceX, 0)];
    textLabel.attributedText = attrString;
    textLabel.numberOfLines = 0;
    [textLabel sizeToFit];
    [self addSubview:textLabel];
    
    CGRect rect = self.frame;
    rect.size = CGSizeMake(rect.size.width, CGRectGetMaxY(textLabel.frame) + kRecommendationViewSpaceY + 4);
    self.frame = rect;
}

- (void)configBirthdayPoll {
    self.layer.cornerRadius = 3.f;
    self.backgroundColor = !self.recommendation.isPostponed ? UIColorFromRGB(0x333333) : [UIColor whiteColor];
    
    UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(kRecommendationViewSpaceX, kRecommendationViewSpaceY, 15, 15)];
    [button setImage:[UIImage imageNamed:@"greenArrow"] forState:UIControlStateNormal];
    button.center = CGPointMake(self.frame.size.width - kRecommendationViewSpaceX - button.frame.size.width / 2.f, button.center.y);
    [button addTarget:self action:@selector(pollOptionsButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [button sizeToFit];
    button.hitTestEdgeInsets = UIEdgeInsetsMake(-20, -20, -20, -20);
    [self addSubview:button];
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(kRecommendationViewSpaceX, kRecommendationViewSpaceY, 230, 0)];
    titleLabel.text = self.recommendation.title;
    titleLabel.textColor = !self.recommendation.isPostponed ? [UIColor whiteColor] : COLOR_LIGHTERGREEN;
    titleLabel.numberOfLines = 0;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.font = !self.recommendation.isPostponed ? [SharedClass getBoldFontWithSize:15] : [SharedClass getRegularFontWithSize:15];
    [titleLabel sizeToFit];
    titleLabel.center = CGPointMake(self.frame.size.width/SCREEN_SCALE/2, titleLabel.center.y);
    [self addSubview:titleLabel];
    self.tmpTitleLabel = titleLabel;
    
    CGRect birthdayFieldFrame = [TeoriusHelper scaledRectFromCenter:CGPointMake(self.frame.size.width/SCREEN_SCALE / 2, CGRectGetMaxY(titleLabel.frame)/SCREEN_SCALE + 40)
                                                               size:CGSizeMake(130, 25)];
    UITextField* birthdayField = [[UITextField alloc] initWithFrame:birthdayFieldFrame];
    birthdayField.textAlignment = NSTextAlignmentCenter;
    birthdayField.font = [SharedClass getRegularFontWithSize:13];
    birthdayField.textColor = COLOR_LIGHTERGREEN;
    birthdayField.tintColor = COLOR_LIGHTERGREEN;
//    loginField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Логин" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor], NSFontAttributeName: font}];
    CALayer *birthdayFieldBottomBorder = [CALayer layer];
    birthdayFieldBottomBorder.frame = CGRectMake(CGRectGetMinX(birthdayField.frame), CGRectGetMaxY(birthdayField.frame)+2, birthdayField.bounds.size.width, 1.0f);
    birthdayFieldBottomBorder.backgroundColor = [COLOR_LIGHTERGREEN CGColor];
    [self.layer addSublayer:birthdayFieldBottomBorder];
    [self addSubview:birthdayField];
    birthdayField.enabled = false;
    
    CGRect answerButtonFrame = [TeoriusHelper scaledRectFromCenter:CGPointMake(self.frame.size.width/SCREEN_SCALE / 2, CGRectGetMaxY(birthdayField.frame)/SCREEN_SCALE + 50)
                                                        size:CGSizeMake(150, 40)];
    UIButton* answerButton = [[UIButton alloc] initWithFrame:answerButtonFrame];
    [answerButton setTitle:@"Ответить" forState:UIControlStateNormal];
    [answerButton setTitleColor:COLOR_LIGHTERGREEN forState:UIControlStateNormal];
    [answerButton setTitleColor:[COLOR_LIGHTERGREEN colorWithAlphaComponent:0.3] forState:UIControlStateHighlighted];
    answerButton.titleLabel.font = [SharedClass getRegularFontWithSize:16];
    [self addSubview:answerButton];
    
//    plotButton.tag = i;
    [answerButton addTarget:self action:@selector(birthdayButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    
    CGRect rect = self.frame;
    rect.size = CGSizeMake(rect.size.width, CGRectGetMaxY(answerButton.frame) + kRecommendationViewSpaceY - 10);
    self.frame = rect;
}

- (void)configBadHabitsPoll {
    self.layer.cornerRadius = 3.f;
    self.backgroundColor = !self.recommendation.isPostponed ? UIColorFromRGB(0x333333) : [UIColor whiteColor];
    
    UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(kRecommendationViewSpaceX, kRecommendationViewSpaceY, 15, 15)];
    [button setImage:[UIImage imageNamed:@"greenArrow"] forState:UIControlStateNormal];
    button.center = CGPointMake(self.frame.size.width - kRecommendationViewSpaceX - button.frame.size.width / 2.f, button.center.y);
    [button addTarget:self action:@selector(pollOptionsButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [button sizeToFit];
    button.hitTestEdgeInsets = UIEdgeInsetsMake(-20, -20, -20, -20);
    [self addSubview:button];
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(kRecommendationViewSpaceX, kRecommendationViewSpaceY, 230, 0)];
    titleLabel.text = self.recommendation.title;
    titleLabel.textColor = !self.recommendation.isPostponed ? [UIColor whiteColor] : COLOR_LIGHTERGREEN;
    titleLabel.numberOfLines = 0;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.font = !self.recommendation.isPostponed ? [SharedClass getBoldFontWithSize:15] : [SharedClass getRegularFontWithSize:15];
    [titleLabel sizeToFit];
    titleLabel.center = CGPointMake(self.frame.size.width/SCREEN_SCALE/2, titleLabel.center.y);
    [self addSubview:titleLabel];
    self.tmpTitleLabel = titleLabel;
    
    CGRect yesButtonFrame = [TeoriusHelper scaledRectFromCenter:CGPointMake(self.frame.size.width/SCREEN_SCALE / 2, CGRectGetMaxY(titleLabel.frame)/SCREEN_SCALE + 30)
                                                           size:CGSizeMake(150, 40)];
    UIButton* yesButton = [[UIButton alloc] initWithFrame:yesButtonFrame];
    [yesButton setTitle:@"Да" forState:UIControlStateNormal];
    [yesButton setTitleColor:COLOR_LIGHTERGREEN forState:UIControlStateNormal];
    [yesButton setTitleColor:[COLOR_LIGHTERGREEN colorWithAlphaComponent:0.3] forState:UIControlStateHighlighted];
    yesButton.titleLabel.font = [SharedClass getRegularFontWithSize:16];
    [self addSubview:yesButton];
    //    plotButton.tag = i;
    [yesButton addTarget:self action:@selector(closeView) forControlEvents:UIControlEventTouchUpInside];
    
    CGRect noButtonFrame = [TeoriusHelper scaledRectFromCenter:CGPointMake(self.frame.size.width/SCREEN_SCALE / 2, CGRectGetMaxY(yesButton.frame)/SCREEN_SCALE + 20)
                                                          size:CGSizeMake(150, 40)];
    UIButton* noButton = [[UIButton alloc] initWithFrame:noButtonFrame];
    [noButton setTitle:@"Нет" forState:UIControlStateNormal];
    [noButton setTitleColor:COLOR_LIGHTERGREEN forState:UIControlStateNormal];
    [noButton setTitleColor:[COLOR_LIGHTERGREEN colorWithAlphaComponent:0.3] forState:UIControlStateHighlighted];
    noButton.titleLabel.font = [SharedClass getRegularFontWithSize:16];
    [self addSubview:noButton];
    //    plotButton.tag = i;
    [noButton addTarget:self action:@selector(closeView) forControlEvents:UIControlEventTouchUpInside];
    
    CGRect dontUseButtonFrame = [TeoriusHelper scaledRectFromCenter:CGPointMake(self.frame.size.width/SCREEN_SCALE / 2, CGRectGetMaxY(noButton.frame)/SCREEN_SCALE + 20)
                                                               size:CGSizeMake(150, 40)];
    UIButton* dontUseButton = [[UIButton alloc] initWithFrame:dontUseButtonFrame];
    [dontUseButton setTitle:@"Не употребляю" forState:UIControlStateNormal];
    [dontUseButton setTitleColor:COLOR_LIGHTERGREEN forState:UIControlStateNormal];
    [dontUseButton setTitleColor:[COLOR_LIGHTERGREEN colorWithAlphaComponent:0.3] forState:UIControlStateHighlighted];
    dontUseButton.titleLabel.font = [SharedClass getRegularFontWithSize:16];
    [self addSubview:dontUseButton];
    //    plotButton.tag = i;
    [dontUseButton addTarget:self action:@selector(closeView) forControlEvents:UIControlEventTouchUpInside];
    
    CGRect rect = self.frame;
    rect.size = CGSizeMake(rect.size.width, CGRectGetMaxY(dontUseButton.frame) + kRecommendationViewSpaceY - 10);
    self.frame = rect;
}

- (void)reactToRecommendationPostponing {
    [UIView animateWithDuration:1 animations:^{
        self.backgroundColor = [UIColor whiteColor];
        self.tmpTitleLabel.textColor = COLOR_LIGHTERGREEN;
        self.tmpTitleLabel.font = [SharedClass getRegularFontWithSize:15.0f];
    }];
}

#pragma mark - RecommendationViewDelegate

- (void)viewTapped:(UITapGestureRecognizer *)gestureRecognizer {
    [self.delegate viewTapped:gestureRecognizer];
}

- (void)birthdayButtonPressed {
    [self closeView];
}

#pragma mark - actions

- (void)plainRecommendationOptionsButtonPressed {
    [self.delegate showPlainRecommendationOptionsForView:self];
}

- (void)pollOptionsButtonPressed {
    [self.delegate showPollOptionsForView:self];
}

#pragma mark - helpers

// TODO not close
- (void)closeView {
    RLMRealm* realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction]; {
        self.recommendation.isPassed = YES;
    }
    [realm commitWriteTransaction];
    [self.delegate reactToRecommendationClosing:self];
}

@end