//
//  AppointmentHelper.m
//  Public
//
//  Created by Tagi on 18.11.15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import "AppointmentHelper.h"

@implementation AppointmentHelper

+ (instancetype)instance {
    static AppointmentHelper *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [self new];
    });
    
    return instance;
}

@end
