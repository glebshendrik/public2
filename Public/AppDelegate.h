//
//  AppDelegate.h
//  Public
//
//  Created by Teoria 5 on 02/09/15.
//  Copyright (c) 2015 Teoria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

