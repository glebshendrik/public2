//
//  NotificationTextFieldView.m
//  Public
//
//  Created by Глеб on 27/11/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import "NotificationTextFieldView.h"
#import "SharedClass.h"
#import "TeoriusHelper.h"

@implementation NotificationTextFieldView

- (instancetype)init {
    self = [super init];
    
    if (!self) {
        return nil;
    }
    
    [self baseInit];
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (!self) {
        return nil;
    }
    
    [self baseInit];
    
    return self;
}

- (void)baseInit {
    [self addField];
}

- (void)addField {
    _textField = [UITextField new];
    [self addSubview:_textField];
    
}

- (void)placeSubviews {
    [super placeSubviews];
    
    CGFloat x = SCALED_F(17.f);
    self.textField.frame = CGRectMake(x, SCALED_F(30.f), self.width - x - SCALED_F(36.f), 0);
    [self.textField sizeToFit];
}

- (CGFloat)calculateHeight {
    [self placeSubviews];
    
    return self.textField.maxY + self.textField.y;
}


#pragma mark - View's lifecycle

- (void)willMoveToSuperview:(UIView *)newSuperview {
    [super willMoveToSuperview:newSuperview];
    
    [self placeSubviews];
}


@end
