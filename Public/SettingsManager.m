//
//  SettingsManager.m
//  Public
//
//  Created by Глеб on 13/10/15.
//  Copyright © 2015 Teoria. All rights reserved.
//


#import "SettingsManager.h"
#import "MainAPI.h"

NSString *const kSettingsLoggedIn = @"kSettingsLoggedIn";
NSString *const kAPNSToken = @"kAPNSToken";

@implementation SettingsManager


#pragma mark - Class methods

+ (SettingsManager *)sharedManager {
    static SettingsManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [SettingsManager new];
    });
    
    return sharedManager;
}


#pragma mark - Setters/Getters

- (BOOL)isLoggedIn {
    return [[NSUserDefaults standardUserDefaults] boolForKey:kSettingsLoggedIn];
}

- (void)setLoggedIn:(BOOL)loggedIn {
    [[NSUserDefaults standardUserDefaults] setBool:loggedIn forKey:kSettingsLoggedIn];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
//    if (loggedIn) {
//        [[MainAPI defaultAPI] sendDeviceTokenToServer];
//    }
}

- (NSString *)apnsToken {
    return [[NSUserDefaults standardUserDefaults] objectForKey:kAPNSToken];
}

- (void)setApnsToken:(NSString *)apnsToken {
    [[NSUserDefaults standardUserDefaults] setObject:apnsToken forKey:kAPNSToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end

