//
//  NotificationMessageView.h
//  Public
//
//  Created by Глеб on 25/11/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import "NotificationView.h"

@interface NotificationMessageView : NotificationView

@property (nonatomic, readonly) UILabel *textLabel;

@end
