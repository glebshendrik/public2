//
//  ProfileImageData.h
//  Public
//
//  Created by Глеб on 26/10/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import <UIKit/UIKit.h>


#define PORTRAIT_BIG_DIMENSION 95
#define PORTRAIT_SMALL_DIMENSION 37


@interface PortraitImageView : UIImageView

- (id)initWithFrame:(CGRect)frame imageData:(NSData *)imageData borderWidth:(float)borderWidth;

@end