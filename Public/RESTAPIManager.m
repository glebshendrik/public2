//
//  RESTAPIManager.m
//  Helterbook
//
//  Created by Tagi on 18.03.15.
//  Copyright (c) 2015 infoshell. All rights reserved.
//

#import "RESTAPIManager.h"
#import "TeoriusHelper.h"
#import "MainAPI.h"
#import "SettingsManager.h"

#define TIMEOUT_INTERVAL_FOR_REQUEST 30

NSString *const kRESTAPIServerURL = @"https://publicapp.i-novus.ru/publicapp-0.1/";
NSString *const kRESTAPIServerInovusURL = @"https://testmed.tatar.ru/pp/";  //@"https://demo.i-novus.ru/pp/";
NSString *const kRESTAPIError = @"err";
NSString *const kRESTAPIMessage = @"msg";
NSString *const kRESTAPIUUID = @"uuid";
NSString *const kRESTAPIList = @"list";
NSString *const kRESTAPIText = @"text";
NSString *const kRESTAPIConsts = @"consts";
NSString *const kRESTAPIName = @"name";
NSString *const kRESTAPIRegion = @"region";
NSString *const kRESTAPINumberOfResidents = @"peoplesCount";
NSString *const kRESTAPISettlementType = @"settlementType";
NSString *const kRESTAPIStove = @"electricStove";
NSString *const kRESTAPICounterType = @"counterType";
NSString *const kRESTAPIMeterReadingMonth = @"meterReadingMonth";
NSString *const kRESTAPIMeterReadingYear = @"meterReadingYear";
NSString *const kRESTAPIMeterDataOld = @"meterDataOld";
NSString *const kRESTAPIMeterDataDayOld = @"meterDataDayOld";
NSString *const kRESTAPIMeterDataNightOld = @"meterDataNightOld";
NSString *const kRESTAPIMeterData = @"meterData";
NSString *const kRESTAPIMeterDataDay = @"meterDataDay";
NSString *const kRESTAPIMeterDataNight = @"meterDataNight";
NSString *const kRESTAPIExtra = @"extra";
NSString *const kRESTAPIErrorMessageCode = @"ERROR_MSG_CODE";
NSString *const kRESTAPIDefaultProvider = @"defProvider";
NSString *const kRESTAPICommission = @"commission";
NSString *const kRESTAPICommissionMin = @"commission_min";
NSString *const kRESTAPIINN = @"inn";
NSString *const kRESTAPIKPP = @"kpp";
NSString *const kRESTAPIRS = @"rs";
NSString *const kRESTAPIKS = @"ks";
NSString *const kRESTAPIBankName = @"bankName";
NSString *const kRESTAPIBIC = @"bic";
NSString *const kRESTAPIIsSocNorm = @"is_soc_norm";
NSString *const kRESTAPIProviderUUID = @"provider_uuid";
NSString *const kRESTAPIRoomAccount = @"roomAccount";
NSString *const kRESTAPIRoomOwner = @"roomOwner";
NSString *const kRESTAPIAddress = @"address";
NSString *const kRESTAPISocNorm = @"socNorm";
NSString *const kRESTAPIProvider = @"provider";
NSString *const kRESTAPIUser = @"user";
NSString *const kRESTAPIEmail = @"email";
NSString *const kRESTAPIFIO = @"fio";
NSString *const kRESTAPIPhone = @"phone";
NSString *const kRESTAPIDoc = @"doc";
NSString *const kRESTAPIIsDefaultProvider = @"def_provider";
NSString *const kRESTAPIRegisterCard = @"registerCard";
NSString *const kRESTAPICardUuid = @"cardUuid";
NSString *const kRESTAPIRoomUuid = @"roomUuid";
NSString *const kRESTAPIRoomUuid2 = @"room_uuid";
NSString *const kRESTAPIAmount = @"amount";
NSString *const kRESTAPIRedirectUrl = @"redirectUrl";
NSString *const kRESTAPIUrl = @"url";
NSString *const kRESTAPIStatusCode = @"status_code";
NSString *const kRESTAPISumm = @"summ";
NSString *const kRESTAPIBaseSumm = @"baseSumm";
NSString *const kRESTAPICounterNumber = @"counterNum";

@interface RESTAPIManager()

@property (nonatomic) NSURLSessionConfiguration *urlSessionConfiguration;

@end


@implementation RESTAPIManager


#pragma mark - Setters/Getters

- (NSURLSessionConfiguration *)urlSessionConfiguration {
    if (!_urlSessionConfiguration) {
        _urlSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
        _urlSessionConfiguration.URLCache = nil;
        _urlSessionConfiguration.URLCredentialStorage = nil;
        _urlSessionConfiguration.timeoutIntervalForRequest = TIMEOUT_INTERVAL_FOR_REQUEST;
//        _urlSessionConfiguration.HTTPCookieStorage = nil;
    }
    
    return _urlSessionConfiguration;
}

- (NSString *)jsessionID {
    NSString *jsessionIDs = [[NSUserDefaults standardUserDefaults] stringForKey:@"jsessionID"];
    return jsessionIDs;
}

- (void)setJsessionID:(NSString *)jsessionIDs {
    [[NSUserDefaults standardUserDefaults] setObject:jsessionIDs forKey:@"jsessionID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


#pragma mark - Common

- (void)sendGetRequestWithAPIMethod:(NSString *)apiMethod baseAuth:(BOOL)baseAuth completion:(void (^)(id dictOrArray, NSError *error))completion {
    NSURLSession *session = [NSURLSession sessionWithConfiguration:self.urlSessionConfiguration];
    NSString *urlString = apiMethod;
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSLog(@"%@", urlString);
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"GET";
//    [request setValue:@"application/json; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
//    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"XMLHttpRequest" forHTTPHeaderField:@"X-Requested-With"];
    [request setValue:@"keep-alive" forHTTPHeaderField:@"Connection"];

    if (baseAuth) {
        [request setValue: [NSString stringWithFormat:@"Basic %@", self.base64String] forHTTPHeaderField:@"Authorization"];
    }
    else {
        NSString *jsessID = self.jsessionID;
        jsessID = [NSString stringWithFormat:@"JSESSIONID=%@", jsessID];

        if ([urlString rangeOfString:kRESTAPIServerInovusURL].location == NSNotFound)
        {
            [request setValue: jsessID forHTTPHeaderField:@"Cookie"];
        }
    }

    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"Error: %@", error.localizedDescription);
            completion(nil, error);
            return;
        }
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        
        NSArray *cookies = [NSHTTPCookie
                            cookiesWithResponseHeaderFields:[httpResponse allHeaderFields]
                            forURL:[httpResponse URL]];
        for (NSHTTPCookie *cookie in cookies) {
            NSLog(@"Cookie name: %@, value: %@", cookie.name, cookie.value);
            if ([cookie.name  isEqualToString: @"JSESSIONID"]) {
                self.jsessionID =cookie.value;
            }
        }
        
        if (httpResponse.statusCode != 200 && httpResponse.statusCode != 201) {
            NSLog(@"HTTP response description: %@", [NSHTTPURLResponse localizedStringForStatusCode:httpResponse.statusCode]);
            NSLog(@"HTTP response status code: %d", (int)httpResponse.statusCode);
            
            NSLog(@"%@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
            
            NSError *error2;
            id dictOrArray = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error2];
            
            if ([dictOrArray isKindOfClass:[NSDictionary class]] || [dictOrArray isKindOfClass:[NSArray class]]) {
                NSError *error1 = [self errorWithText:[NSHTTPURLResponse localizedStringForStatusCode:httpResponse.statusCode] code:httpResponse.statusCode];
                completion(dictOrArray, error1);
            }
            else {
                NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                
                NSError *error1 = [self errorWithText:[NSString stringWithFormat:@"%d\n%@\n%@", (int)httpResponse.statusCode, [NSHTTPURLResponse localizedStringForStatusCode:httpResponse.statusCode], responseString] code:httpResponse.statusCode];
                completion(nil, error1);
            }
            
            return;
        }
        
        if (!data) {
            completion(nil, nil);
            return;
        }
        
        
        
        NSError *error1;
        
        id dictOrArray = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error1];
        NSLog(@"%@", dictOrArray);
        completion(dictOrArray, error1);
    }];
    [task resume];
}

- (void)sendDeleteRequestWithAPIMethod:(NSString *)apiMethod  bodyString:(NSString *)bodyString completion:(void (^)(id dictOrArray, NSError *error))completion {
    NSURLSession *session = [NSURLSession sessionWithConfiguration:self.urlSessionConfiguration];
    NSString *urlString = apiMethod;
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:urlString];
    NSData *body = [bodyString dataUsingEncoding:NSUTF8StringEncoding];

    NSLog(@"%@", urlString);

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPBody = body;
    request.HTTPMethod = @"DELETE";
    [request setValue:@"application/json; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    //    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"XMLHttpRequest" forHTTPHeaderField:@"X-Requested-With"];
    [request setValue:@"keep-alive" forHTTPHeaderField:@"Connection"];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"Error: %@", error.localizedDescription);
            completion(nil, error);
            return;
        }
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        
        if (httpResponse.statusCode != 200 && httpResponse.statusCode != 201) {
            NSLog(@"HTTP response description: %@", [NSHTTPURLResponse localizedStringForStatusCode:httpResponse.statusCode]);
            NSLog(@"HTTP response status code: %d", (int)httpResponse.statusCode);
            
            NSLog(@"%@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
            
            NSError *error2;
            id dictOrArray = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error2];
            
            if ([dictOrArray isKindOfClass:[NSDictionary class]] || [dictOrArray isKindOfClass:[NSArray class]]) {
                NSError *error1 = [self errorWithText:[NSHTTPURLResponse localizedStringForStatusCode:httpResponse.statusCode] code:httpResponse.statusCode];
                completion(dictOrArray, error1);
            }
            else {
                NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                
                NSError *error1 = [self errorWithText:[NSString stringWithFormat:@"%d\n%@\n%@", (int)httpResponse.statusCode, [NSHTTPURLResponse localizedStringForStatusCode:httpResponse.statusCode], responseString] code:httpResponse.statusCode];
                completion(nil, error1);
            }
            
            return;
        }
        
        if (!data) {
            completion(nil, nil);
            return;
        }
        
        NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        if ([TeoriusHelper stringIsValid:responseString]) {
            if ([responseString isEqualToString:@"true"]) {
                completion(@{}, nil);
            }
            else {
                completion(@{}, [self errorWithText:@"Неизвестная ошибка" code:200]);
            }
        }
        else {
            NSError *error1;
            id dictOrArray = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error1];
            completion(dictOrArray, error1);
        }
    }];
    
    [task resume];
}

- (void)sendPostRequestWithAPIMethod:(NSString *)apiMethod baseAuth:(BOOL)baseAuth bodyString:(NSString *)bodyString completion:(void (^)(id dictOrArray, NSError *error))completion {
    NSURLSession *session = [NSURLSession sessionWithConfiguration:self.urlSessionConfiguration];
    NSString *urlString = apiMethod;
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:urlString];
    NSData *body = [bodyString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"%@", urlString);

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    if ([TeoriusHelper dataIsValid:body]) {
        request.HTTPBody = body;
    }
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
//    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"XMLHttpRequest" forHTTPHeaderField:@"X-Requested-With"];
    [request setValue:@"keep-alive" forHTTPHeaderField:@"Connection"];
    
    if (baseAuth) {
        [request setValue: [NSString stringWithFormat:@"Basic %@", self.base64String] forHTTPHeaderField:@"Authorization"];
    }
    else {
        NSString *jsessID = self.jsessionID;
        jsessID = [NSString stringWithFormat:@"JSESSIONID=%@", jsessID];
        
        if ([urlString rangeOfString:kRESTAPIServerInovusURL].location == NSNotFound) {
            [request setValue: jsessID forHTTPHeaderField:@"Cookie"];
        }

    }
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"Error: %@", error.localizedDescription);
            completion(nil, error);
            return;
        }
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        
        if (httpResponse.statusCode != 200 && httpResponse.statusCode != 201) {
            NSLog(@"HTTP response description: %@", [NSHTTPURLResponse localizedStringForStatusCode:httpResponse.statusCode]);
            NSLog(@"HTTP response status code: %d", (int)httpResponse.statusCode);
            
            NSLog(@"%@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
            
            NSError *error2;
            id dictOrArray = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error2];

            if ([dictOrArray isKindOfClass:[NSDictionary class]] || [dictOrArray isKindOfClass:[NSArray class]]) {
                NSError *error1 = [self errorWithText:[NSHTTPURLResponse localizedStringForStatusCode:httpResponse.statusCode] code:httpResponse.statusCode];
                completion(dictOrArray, error1);
            }
            else {
                NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                
                NSError *error1 = [self errorWithText:responseString code:httpResponse.statusCode];/*[self errorWithText:[NSString stringWithFormat:@"%d\n%@\n%@", (int)httpResponse.statusCode, [NSHTTPURLResponse localizedStringForStatusCode:httpResponse.statusCode], responseString] code:httpResponse.statusCode]*/;
                completion(nil, error1);
            }
            
            return;
        }
        
        if (!data) {
            completion(nil, nil);
            return;
        }
        
        NSError *error1;
        
        id dictOrArray = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error1];
        
        completion(dictOrArray, error1);
    }];
    
    [postDataTask resume];
}

- (void)sendPutRequestWithAPIMethod:(NSString *)apiMethod  baseAuth:(BOOL)baseAuth bodyString:(NSString *)bodyString completion:(void (^)(id dictOrArray, NSError *error))completion {
    NSURLSession *session = [NSURLSession sessionWithConfiguration:self.urlSessionConfiguration];
    NSString *urlString = apiMethod;
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:urlString];
    NSData *body = [bodyString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"%@", urlString);

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPBody = body;
    request.HTTPMethod = @"PUT";
    [request setValue:@"application/json; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"XMLHttpRequest" forHTTPHeaderField:@"X-Requested-With"];
    [request setValue:@"keep-alive" forHTTPHeaderField:@"Connection"];
    
    if (baseAuth) {
        [request setValue: [NSString stringWithFormat:@"Basic %@", self.base64String] forHTTPHeaderField:@"Authorization"];
    }
    else {

        NSString *jsessID = self.jsessionID;
        jsessID = [NSString stringWithFormat:@"JSESSIONID=%@", jsessID];
        
        if ([urlString rangeOfString:kRESTAPIServerInovusURL].location == NSNotFound)
        {
            [request setValue: jsessID forHTTPHeaderField:@"Cookie"];
        }
        
    }
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"Error: %@", error.localizedDescription);
            completion(nil, error);
            return;
        }
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        
        NSArray *cookies = [NSHTTPCookie
                            cookiesWithResponseHeaderFields:[httpResponse allHeaderFields]
                            forURL:[httpResponse URL]];
        for (NSHTTPCookie *cookie in cookies) {
            NSLog(@"Cookie name: %@, value: %@", cookie.name, cookie.value);
            [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookie];
        }
        
        if (httpResponse.statusCode != 200 && httpResponse.statusCode != 201) {
            NSLog(@"HTTP response description: %@", [NSHTTPURLResponse localizedStringForStatusCode:httpResponse.statusCode]);
            NSLog(@"HTTP response status code: %d", (int)httpResponse.statusCode);
            
            NSLog(@"%@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
            
            NSError *error2;
            id dictOrArray = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error2];
            
            if ([dictOrArray isKindOfClass:[NSDictionary class]] || [dictOrArray isKindOfClass:[NSArray class]]) {
                NSError *error1 = [self errorWithText:[NSHTTPURLResponse localizedStringForStatusCode:httpResponse.statusCode] code:httpResponse.statusCode];
                completion(dictOrArray, error1);
            }
            else {
                NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                
                NSError *error1 = [self errorWithText:[NSString stringWithFormat:@"%d\n%@\n%@", (int)httpResponse.statusCode, [NSHTTPURLResponse localizedStringForStatusCode:httpResponse.statusCode], responseString] code:httpResponse.statusCode];
                completion(nil, error1);
            }
            
            return;
        }
        
        if (!data) {
            completion(nil, nil);
            return;
        }
        
        NSError *error1;
        
        id dictOrArray = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error1];
        completion(dictOrArray, error1);
    }];
    
    [postDataTask resume];
}


#pragma mark - Authorization and registration

- (void)registerDeviceToken:(NSString *)deviceToken {
    NSString *apiMethod = [NSString  stringWithFormat:@"%@account/deviceRegister/?token=%@", kRESTAPIServerURL, deviceToken];
    [self sendGetRequestWithAPIMethod:apiMethod baseAuth:NO completion:^(id dictOrArray, NSError *error) {
        
    }];
}

- (void)loginWithLogin:(NSString *)login password:(NSString *)password completion:(void (^)(NSDictionary *responseDict, NSError *error))completion {
    NSData *nsdata = [[NSString stringWithFormat:@"%@:%@", login, password]
                      dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64Encoded = [nsdata base64EncodedStringWithOptions:0];
    
    self.base64String = base64Encoded;
    NSString *apiMethod = [NSString stringWithFormat:@"%@auth", kRESTAPIServerURL];
    [self sendGetRequestWithAPIMethod:apiMethod baseAuth:YES completion:completion];
}

- (void)registerWithLogin:(NSString *)login password:(NSString *)password completion:(void (^)(NSDictionary *, NSError *))completion {
    NSDictionary *bodyDict = @{@"email": login, @"password": password, @"confirmPassword": password, @"application": @"true"};
    NSString *bodyString = [bodyDict jsonString];
    NSString *apiMethod = [NSString stringWithFormat:@"%@account/", kRESTAPIServerURL];
    [self sendPostRequestWithAPIMethod:apiMethod baseAuth:NO bodyString:bodyString completion:completion];
}

- (void)logoutWithCompletion:(void (^)(NSDictionary *, NSError *))completion {
    NSString *apiMethod = [NSString stringWithFormat:@"%@logout", kRESTAPIServerURL];
    [self sendGetRequestWithAPIMethod:apiMethod baseAuth:YES completion:completion];
}


#pragma mark - Appoinments

- (void)authWithDocument:(NSString *)typeDoc docNumber:(NSString *)docNumber docSeries:(NSString *)docSeries birthDate:(NSString *)birthDate completion:(void (^)(NSDictionary *, NSError *))completion {
    NSDictionary *bodyDict = @{@"authDocumentTypeId": typeDoc, @"authDocumentNumber": docNumber, @"authDocumentSeries": docSeries, @"birthDate": birthDate};
    NSString *bodyString = [bodyDict jsonString];
    NSString *apiMethod = [NSString stringWithFormat:@"%@auth", kRESTAPIServerInovusURL];
    
    [self sendPostRequestWithAPIMethod:apiMethod baseAuth:NO bodyString:bodyString completion:completion];
}

- (void)getTotalAppointments:(void (^)(NSDictionary *, NSError *))completion {
    NSString *apiMethod = [NSString stringWithFormat:@"%@office/appointments", kRESTAPIServerInovusURL];
    
    [self sendGetRequestWithAPIMethod:apiMethod baseAuth:NO completion:completion];
}

- (void)getFutureAppointments:(void (^)(NSDictionary *, NSError *))completion {
    NSString *apiMethod = [NSString stringWithFormat:@"%@office/appointments/future?withResponsibleData=true&page=1&size=10", kRESTAPIServerInovusURL];
    [self sendGetRequestWithAPIMethod:apiMethod baseAuth:NO completion:completion];
}

- (void)getClinics:(void (^)(NSDictionary *, NSError *))completion {
    NSString *apiMethod = [NSString  stringWithFormat:@"%@getClinicsWithDepartments?page=1&size=20&byAttachment=all", kRESTAPIServerInovusURL];
    [self sendGetRequestWithAPIMethod:apiMethod baseAuth:NO completion:completion];
}

- (void)getResourcesForClinicID:(NSString *)clinicID completion:(void (^)(NSDictionary *, NSError *))completion  {
    NSString *apiMethod = [NSString stringWithFormat:@"%@getResourcesByClinic/%@?page=1&size=20&byAttachment=all", kRESTAPIServerInovusURL, clinicID/*@"department_29115"*/];
    
    [self sendGetRequestWithAPIMethod:apiMethod baseAuth:NO completion:completion];
}

- (void)getScheduleForClinicID:(NSString *)clinicID serviceID:(NSString *)serviceID resourceID:(NSString *)resourceID year:(int)year month:(int)month completion:(void (^)(NSDictionary *, NSError *))completion {
    NSString *apiMethod = [NSString stringWithFormat:@"%@group/%@/service/%@/resource/%@/planning/%d/%d", kRESTAPIServerInovusURL, clinicID, serviceID, resourceID, year, month];
    
    
    [self sendGetRequestWithAPIMethod:apiMethod baseAuth:NO completion:completion];
}

- (void)createAppointmentForClinicId:(NSString *)clinicId ServiceId:(long)serviceId resourceId:(long)resourceId year:(NSUInteger)year month:(NSUInteger)month day:(NSUInteger)day timeIntervalId:(long)timeIntervalId completion:(void (^)(NSDictionary *, NSError *))completion {
    int salt = arc4random() % 900000 + 100000;

    NSString *apiMethod = [NSString stringWithFormat:@"%@group/%@/service/%ld/resource/%ld/day/%d/%d/%d/time/%ld/appointment?_salt=%d", kRESTAPIServerInovusURL, clinicId, serviceId, resourceId, (int)year, (int)month, (int)day, timeIntervalId, salt];
    
    NSDictionary *dict  = @{@"groupId":@"department_31072", @"serviceId":[NSString  stringWithFormat:@"%ld", serviceId], @"resourceId":[NSString  stringWithFormat:@"%ld", resourceId], @"year":[NSString  stringWithFormat:@"%ld", year], @"month":[NSString  stringWithFormat:@"%ld", month], @"date":[NSString  stringWithFormat:@"%ld", day], @"timeId":[NSString  stringWithFormat:@"%ld", timeIntervalId], @"page":@1, @"size":@12};
    
//    department_31072/service/4018/resource/1757
    [self sendPostRequestWithAPIMethod:apiMethod baseAuth:NO bodyString:dict.jsonString completion:completion];
}

- (void)createAppointmentForClinicId:(NSString *)clinicId ServiceId:(long)serviceId resourceId:(long)resourceId year:(NSUInteger)year month:(NSUInteger)month day:(NSUInteger)day timeIntervalId:(long)timeIntervalId phone:(NSString *)phone completion:(void (^)(NSDictionary *, NSError *))completion {
    int salt = arc4random() % 900000 + 100000;
    
    NSString *apiMethod = [NSString stringWithFormat:@"%@group/%@/service/%ld/resource/%ld/day/%d/%d/%d/time/%ld/contact/%@/appointment?_salt=%d", kRESTAPIServerInovusURL, clinicId, serviceId, resourceId, (int)year, (int)month, (int)day, timeIntervalId, phone, salt];
    
    NSDictionary *dict  = @{@"groupId":@"department_31072", @"serviceId":[NSString  stringWithFormat:@"%ld", serviceId], @"resourceId":[NSString  stringWithFormat:@"%ld", resourceId], @"year":[NSString  stringWithFormat:@"%ld", year], @"month":[NSString  stringWithFormat:@"%ld", month], @"date":[NSString  stringWithFormat:@"%ld", day], @"timeId":[NSString  stringWithFormat:@"%ld", timeIntervalId], @"contact":phone, @"page":@1, @"size":@12};
    
    //    department_31072/service/4018/resource/1757
    [self sendPostRequestWithAPIMethod:apiMethod baseAuth:NO bodyString:dict.jsonString completion:completion];
}

- (void)deleteAppointmentWithId:(NSString *)appointmentId completion:(void (^)(NSDictionary *, NSError *))completion {
    NSString *apiMethod = [NSString  stringWithFormat:@"%@office/appointment/%@", kRESTAPIServerInovusURL, appointmentId];
    
    NSDictionary *dict = @{@"appointmentId":appointmentId};
    
    [self sendDeleteRequestWithAPIMethod:apiMethod bodyString:dict.jsonString completion:completion];
}

- (void)sendAppointmentInfoWithDict:(NSDictionary *)dict completion:(void (^)(NSDictionary *, NSError *))completion {
    NSString *apiString = [NSString  stringWithFormat:@"%@moAppointment", kRESTAPIServerURL];
    NSString *bodyString = dict.jsonString;
    
    [self sendPostRequestWithAPIMethod:apiString baseAuth:NO bodyString:bodyString completion:completion];
}

- (void)sendAppointmentCancellingInfoWithId:(NSString *)ID completion:(void (^)(NSDictionary *, NSError *))completion {
    NSString *apiMethod = [NSString  stringWithFormat:@"%@moAppointment/activate/?rmisId=%@&active=false", kRESTAPIServerURL, ID];
    
    [self sendGetRequestWithAPIMethod:apiMethod baseAuth:NO completion:completion];
}


#pragma mark - Users

- (void)getUserInfoForUserID:(NSString *)userID completion:(void (^)(NSDictionary *, NSError *))completion {
    NSString *apiMethod = [NSString stringWithFormat:@"%@account/%@", kRESTAPIServerURL, userID];
    [self sendGetRequestWithAPIMethod:apiMethod baseAuth:NO completion:completion];
}

- (void)getUserInfoWithCompletion:(void (^)(NSDictionary *, NSError *))completion {
    NSString *apiMethod = @"user";

    [self sendGetRequestWithAPIMethod:apiMethod baseAuth:YES completion:completion];
}

- (void)getUserCitizen:(void (^)(NSArray *, NSError *))completion {

    NSString *apiMethod = [NSString stringWithFormat:@"%@userCitizen", kRESTAPIServerURL];
    
    [self sendGetRequestWithAPIMethod:apiMethod baseAuth:NO completion:completion];
}

- (void)createNewUser:(NSDictionary *)bodyDict completion:(void (^)(NSDictionary *, NSError *))completion {
    
    NSString *apiMethod = [NSString stringWithFormat:@"%@userCitizen", kRESTAPIServerURL];
    
    NSString *bodyString = [bodyDict jsonString];

    [self sendPostRequestWithAPIMethod:apiMethod baseAuth:NO bodyString:bodyString completion:completion];
    
}

- (void)updateUser:(NSDictionary *)bodyDict idUser:(NSString *)idUser completion:(void (^)(NSDictionary *, NSError *))completion {
    
    NSString *apiMethod = [NSString stringWithFormat:@"%@userCitizen/%@", kRESTAPIServerURL, idUser];
    
    NSString *bodyString = [bodyDict jsonString];
    
    [self sendPutRequestWithAPIMethod:apiMethod baseAuth:NO bodyString:bodyString completion:completion];
    
}


#pragma mark - Monitoring

- (void)getAllActiveMeasure:(NSString *)idUser completion:(void (^)(NSDictionary *, NSError *))completion {
    NSString *apiMethod = [NSString stringWithFormat:@"%@measureActivate/?query(userCitizen=%@)", kRESTAPIServerURL, idUser];

    //      Новый запрос, более красивый
    //    NSString *apiMethod = [NSString stringWithFormat:@"%@measureActivate/", kRESTAPIServerURL];
    
    [self sendGetRequestWithAPIMethod:apiMethod baseAuth:NO completion:completion];
    
}

- (void)getAllMeasure:(NSString *)idUser date:(NSString *)date limit:(NSNumber *)limit completion:(void (^) (NSDictionary *, NSError *))completion {
    NSString *apiMethod = [NSString stringWithFormat:@"%@measurement/?withNorms=true&query(userCitizen=%@&fixedTime=%@)&limit(%@)", kRESTAPIServerURL, idUser, date, limit];
    
    [self sendGetRequestWithAPIMethod:apiMethod baseAuth:NO completion:completion];
}

- (void)createReading:(NSArray *)arr plotID:(NSString *)plotID completion:(void (^)(NSArray *, NSError *))completion {
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:arr options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSString *apiMethod;
    if ([plotID containsString:@"kPulse"]) {
        apiMethod = [NSString stringWithFormat:@"%@measurePulse", kRESTAPIServerURL];
    }
    else if ([plotID containsString:@"kTemperature"]) {
        apiMethod = [NSString stringWithFormat:@"%@measureTemperature", kRESTAPIServerURL];
    }
    else if ([plotID containsString:@"BloodPressure"]) {
        apiMethod = [NSString stringWithFormat:@"%@measurePressure", kRESTAPIServerURL];
    }
    else if ([plotID containsString:@"kHeight"]) {
        apiMethod = [NSString stringWithFormat:@"%@measureStature", kRESTAPIServerURL];
    }
    else if ([plotID containsString:@"kWeight"]) {
        apiMethod = [NSString stringWithFormat:@"%@measureWeight", kRESTAPIServerURL];
    }
    else if ([plotID containsString:@"kSugar"]) {
        apiMethod = [NSString stringWithFormat:@"%@measureSugar", kRESTAPIServerURL];
    }
    else if ([plotID containsString:@"kCholesterol"]) {
        apiMethod = [NSString stringWithFormat:@"%@measureCholesterin", kRESTAPIServerURL];
    }
   
    [self sendPostRequestWithAPIMethod:apiMethod baseAuth:NO bodyString:jsonString completion:completion];
    
}


#pragma mark - Notification methods

- (void)getAllNotificationsAfterTapped:(NSString *)date completion:(void (^) (NSDictionary *, NSError *))completion {

    NSString *apiMethod = [NSString stringWithFormat:@"%@notification/?query(readyTime=2015-03-01..2015-11-30)&limit(0,150)&sort(-readyTime)", kRESTAPIServerURL];
    
//    NSString *token = [SettingsManager sharedManager].apnsToken;
//    NSString *apiMethod = [NSString stringWithFormat:@"%@account/pushActivate/?token=%@&disabled=false", kRESTAPIServerURL, token];
    
//    NSString *apiMethod = [NSString stringWithFormat:@"%@notification/?token=%@&query(readyTime=2015-03-01..2015-11-31)&limit(0,150)&sort(-readyTime)", kRESTAPIServerURL, token];
    
    
    [self sendGetRequestWithAPIMethod:apiMethod baseAuth:NO completion:completion];
}

- (void)sendAnswerAndStateNotifications:(NSArray *)arr completion:(void (^)(NSArray *, NSError *))completion {
    NSString *apiMethod = [NSString stringWithFormat:@"%@notification", kRESTAPIServerURL];
    
    NSDictionary *dicTest1 = @{ @"type": @"1",
                                @"code": @"99001",
                                @"status" : @"4"
                            };
    NSDictionary *dicTest2 = @{ @"type": @"1",
                                @"code": @"99002",
                                @"status" : @"3"
                                };
    
    NSMutableArray *arrTest = [NSMutableArray new];
    
    [arrTest addObject:dicTest1];
    [arrTest addObject:dicTest2];
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:arrTest options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];

    
    
    [self sendPostRequestWithAPIMethod:apiMethod baseAuth:NO bodyString:jsonString completion:completion];
}


#pragma mark - Other

- (NSError *)errorWithText:(NSString *)text code:(NSUInteger)code {
    NSMutableDictionary *details = [NSMutableDictionary dictionary];
    details[NSLocalizedDescriptionKey] = text;
    
    return [NSError errorWithDomain:@"world" code:code userInfo:details];
}

@end
