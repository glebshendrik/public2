//
//  AppDelegate.m
//  Public
//
//  Created by Teoria 5 on 02/09/15.
//  Copyright (c) 2015 Teoria. All rights reserved.
//

#import "AppDelegate.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "HeaderViewController.h"
#import "BaseViewController.h"
#import "AuthorizationViewController.h"
#import <Realm/Realm.h>
#import "DummyDataLight.h"
#import "SharedClass.h"
#import "SettingsManager.h"
#import "MainAPI.h"


#import "AppointmentDateAndTimeViewController.h"
#import "Resource.h"
#import "PhoneEnteringViewController.h"


@interface AppDelegate ()

@end


@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [Fabric with:@[[Crashlytics class]]];

    self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
    [self.window makeKeyAndVisible];
    
    // REALM
    
//    RLMRealmConfiguration* config = [RLMRealmConfiguration defaultConfiguration];
//    config.schemaVersion = 15; // Gender object
//    config.migrationBlock = ^(RLMMigration* migration, uint64_t oldSchemaVersion) {
//        if (oldSchemaVersion < 1) {
//            // it just works
//        }
//    };
//    [RLMRealmConfiguration setDefaultConfiguration:config];
//    [RLMRealm defaultRealm];
    
    // CONFIGURATION
    
//    [DummyData deleteDummyData];
    
    [self configApplicationStyle];
    
    // AUTHORIZATION
    
    UIViewController *vc;
    if ([SettingsManager sharedManager].isLoggedIn) {
        vc = [BaseViewController new];
    }
    else {
        vc = [AuthorizationViewController new];
    }
    
//    vc = [AppointmentDateAndTimeViewController new];
//    vc = [PhoneEnteringViewController new];

    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:vc];
    self.window.rootViewController = nc;
    return YES;
}

- (void)configApplicationStyle {
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: UIColorFromRGB(0xD2695B),
                                                           NSFontAttributeName: [SharedClass getBoldFontWithSize:14]}];
    [[UIBarButtonItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: UIColorFromRGB(0xD2695B),
                                                           NSFontAttributeName: [SharedClass getRegularFontWithSize:14]}
                                                forState:UIControlStateNormal];
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken  {
    NSString *token = deviceToken.description;
    token = [token stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (![token isEqualToString:[SettingsManager sharedManager].apnsToken]) {
        [SettingsManager sharedManager].apnsToken = token;
        [[MainAPI defaultAPI] registerDeviceToken];
    }
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"Failed to get token, error: %@", error);
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
