//
//  ProfileImageData.m
//  Public
//
//  Created by Глеб on 26/10/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import "PortraitImageView.h"
#import "TeoriusHelper+Extension.h"
#import "SharedClass.h"

@implementation PortraitImageView

- (id)initWithFrame:(CGRect)frame imageData:(NSData *)imageData borderWidth:(float)borderWidth {
    frame.size.height = frame.size.width;
    if (self = [super initWithFrame:frame]) {
        
//        UIImage *image = [UIImage imageNamed:imageData];
        UIImage *image = [UIImage imageWithData:imageData];
        self.image = image;
        self.layer.cornerRadius = frame.size.width/2;
        self.layer.masksToBounds = YES;
        self.layer.borderColor = [COLOR_LIGHTERGREEN CGColor];
        self.layer.borderWidth = borderWidth;
    }
    return self;
}

@end
