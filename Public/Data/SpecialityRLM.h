//
//  Speciality.h
//  Public
//
//  Created by Arslan Zagidullin on 28/09/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import <Realm/Realm.h>

@interface SpecialityRLM : RLMObject

@property NSString* name;

@end
