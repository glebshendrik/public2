//
//  Article.h
//  Public
//
//  Created by Глеб on 23/11/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import "Notification.h"

@interface Article : Notification

@property NSString *title;
@property (nonatomic) NotificationType typeNotification;
@property (nonatomic) StatusType status;
@property NSString *hide;
@property NSString *code;
@property NSString *cmsArticleId;
@property (nonatomic) InterfaceType typeInterface;


@end
