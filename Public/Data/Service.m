//
//  Service.m
//  Public
//
//  Created by Tagi on 11.11.15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import "Service.h"

@implementation Service

- (NSMutableArray *)resources {
    if (!_resources) {
        _resources = [NSMutableArray new];
    }
    
    return _resources;
}

@end
