//
//  ServiceRLM.m
//  Public
//
//  Created by Tagi on 11.11.15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import "ServiceRLM.h"

@implementation ServiceRLM

+ (NSString *)primaryKey {
    return @"ID";
}

+ (NSDictionary *)defaultPropertyValues {
    return @{@"ID": @"", @"name": @""};
}

//@property NSString *ID;
//@property NSString *name;

@end
