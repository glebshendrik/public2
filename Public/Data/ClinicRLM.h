//
//  Clinic.h
//  
//
//  Created by Teoria 5 on 10/09/15.
//
//

#import <Realm/Realm.h>

//@class Specialist;

@interface ClinicRLM : RLMObject

@property NSString *ID;
@property NSString* name;
@property NSString* address;
@property NSString* phone;
@property NSString* work_hours;
@property NSString* site;
@property BOOL parent;
@property BOOL attached;

@end

RLM_ARRAY_TYPE(Clinic)
