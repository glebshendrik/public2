//
//  DummyData.h
//  Public
//
//  Created by Arslan Zagidullin on 15/09/15.
//  Copyright (c) 2015 Teoria. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DummyData : NSObject

+ (void)generateDummyData;
+ (void)deleteDummyData;

@end
