//
//  Notification.h
//  Public
//
//  Created by Глеб on 23/11/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(int, InterfaceType) {
    InterfaceTypeListNotification = 101,
    InterfaceTypeVopros = 102,
    InterfaceTypeArticle = 103
};

typedef NS_ENUM(int, NotificationType) {
    NotificationTypeInterview = 1,
    NotificationTypeArticle = 2,
    NotificationTypeMessage = 3
};

typedef NS_ENUM(int, StatusType) {
    StatusTypeNew = 1,
    StatusTypePostponed = 2,
    StatusTypeHide = 3,
    StatusTypeReaded = 4,
    StatusTypeAnswered = 5,
    StatusTypeAnsweredPartly = 6
};

typedef NS_ENUM(int, ControlType) {
    ControlTypeButton = 1,
    ControlTypeText = 2,
    ControlTypeDate = 3,
    ControlTypeRadio = 4,
    ControlTypeSelect = 5,
    ControlTypeCheck = 6
};

@interface Notification : NSObject

//@property (nonatomic) InterfaceType typeInterface;
//@property (nonatomic) NotificationType notificationType;
//@property (nonatomic) StatusType typeStatus;

@end
