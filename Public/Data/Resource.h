//
//  Resource.h
//  Public
//
//  Created by Tagi on 11.11.15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Resource : NSObject

@property (nonatomic) long ID;
@property (nonatomic, copy) NSString *fullName;
@property (nonatomic, copy) NSString *room;
@property (nonatomic) long serviceID;
@property (nonatomic, copy) NSString *fio;
@property (nonatomic, copy) NSString *specialityName;
@property (nonatomic) long specialityID;
@property (nonatomic, copy) NSString *clinicID;

@end