//
//  TimeInterval.h
//  Public
//
//  Created by Tagi on 18.11.15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TimeInterval : NSObject

@property (nonatomic) long ID;
@property (nonatomic, copy) NSString *time;

- (instancetype)initWithID:(long)ID time:(NSString *)time;

@end
