//
//  Appointment.h
//  Public
//
//  Created by Arslan Zagidullin on 11/09/15.
//  Copyright (c) 2015 Teoria. All rights reserved.
//

#import <Realm/Realm.h>
#import "SpecialistRLM.h"

@interface AppointmentRLM : RLMObject

@property NSDate *date;
@property NSString *ID;
@property NSString *name;
@property NSString *ticketNumber;
@property NSString *clinicFullName;
@property NSString *clinicShortName;
@property NSString *resourceId;
@property NSString *resourceFullName;
@property NSString *resourceShortName;
@property NSString *serviceShortName;
@property NSString *resourceFio;
@property NSString *resourceSpeciality;
@property NSString *room;

@end

RLM_ARRAY_TYPE(AppointmentRLM)