//
//  Interview.h
//  Public
//
//  Created by Глеб on 23/11/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import "Notification.h"

@interface Interview : Notification

//{"title":"Помогите нам узнать о Вас больше",
//    "type": "1",
//    "status":"6", // <= статус "Отвечено частично"
//    "code":"10001",
//    "list":[{"status":"5", "control":"2",  "answer": "100555292", "label":"Укажите фамилию",       "constraint": {"minSize":"2", "maxSize":"70"}},
//            {"status":"4", "control":"2",  "answer": "100555293", "label":"Укажите имя",           "constraint": {"minSize":"2", "maxSize":"70"}},
//            {"status":"1", "control":"2",  "answer": "100555294", "label":"Укажите отчество",      "constraint": {"minSize":"2", "maxSize":"70"}},
//            {"status":"1", "control":"3",  "answer": "100555295", "label":"Укажите дату рождения", "constraint": {"min":"1895-01-01", "max":"31.12.2010"}},
//            {"status":"1", "control":"4",  "answer": "100555296", "label":"Укажите Ваш пол",       "options": [{"value":"1", "name":"Мужской"},{"value":"2", "name":"Женский"}]}]},


@property NSString *title;
@property (nonatomic) NotificationType typeNotification;
@property (nonatomic) StatusType status;
@property NSArray *questions;
@property NSArray *answers;
@property NSString *code;
@end
