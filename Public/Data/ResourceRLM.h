//
//  ResourceRLM.h
//  Public
//
//  Created by Tagi on 11.11.15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import <Realm/Realm.h>

@interface ResourceRLM : RLMObject

@property long ID;
@property NSString *fullName;
@property NSString *room;
@property long serviceID;
@property NSString *fio;
@property NSString *specialityName;
@property NSString *specialityID;
@property NSString *clinicID;

@end

RLM_ARRAY_TYPE(ResourceRLM)