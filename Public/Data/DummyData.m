////
////  DummyData.m
////  Public
////
////  Created by Arslan Zagidullin on 15/09/15.
////  Copyright (c) 2015 Teoria. All rights reserved.
////
//
//#import "DummyData.h"
//#import <Realm/Realm.h>
//
//#import "ClinicRLM.h"
//#import "AppointmentRLM.h"
//#import "Speciality.h"
//#import "SpecialistRLM.h"
//
//#import "MeasurementRLM.h"
//#import "ReadingRLM.h"
//
//#import "Recommendation.h"
//
//#import "FamilyRLM.h"
//#import "UserRLM.h"
//
//@implementation DummyData
//
//+ (void)generateDummyData {
//    RLMRealm* realm = [RLMRealm defaultRealm];
//    
//    RLMResults* array = [Speciality allObjects];
//    if (array.count > 0)
//        return;
//    
//    NSLog(@"generating dummy data");
//    [realm beginWriteTransaction]; {
//        Speciality* spTherapist =
//        [Speciality createInRealm:realm withValue:@{@"name": @"Терапевт",
//                                                    }];
//        Speciality* spSurgeon =
//        [Speciality createInRealm:realm withValue:@{@"name": @"Хирург",
//                                                    }];
//        Speciality* spOphthalmologist =
//        [Speciality createInRealm:realm withValue:@{@"name": @"Офтальмолог",
//                                                    }];
//        Speciality* spPediatrician =
//        [Speciality createInRealm:realm withValue:@{@"name": @"Педиатр",
//                                                    }];
//        ClinicRLM* c1 =
//        [ClinicRLM createInRealm:realm withValue:@{@"name": @"Городская поликлиника №7",
//                                                @"address": @"ул. Рихарда Зорге, 121",
//                                                @"phone": @"(843)276-46-03",
//                                                @"work_hours": @"Пн-вс 08.00-20.00",
//                                                @"site": @"gauz7-kazan.ru"
//                                                }];
//        ClinicRLM* c2 =
//        [ClinicRLM createInRealm:realm withValue:@{@"name": @"Городская поликлиника №21",
//                                                @"address": @"ул. Рихарда Зорге, 121",
//                                                @"phone": @"(843)123-45-67",
//                                                @"work_hours": @"Пн-вс 08.00-20.00",
//                                                @"site": @"gauz21-kazan.ru"
//                                                }];
//        ClinicRLM* c3 =
//        [ClinicRLM createInRealm:realm withValue:@{@"name": @"Городская поликлиника №20",
//                                                @"address": @"ул. Рихарда Зорге, 121",
//                                                @"phone": @"(843)123-45-67",
//                                                @"work_hours": @"Пн-вс 08.00-20.00",
//                                                @"site": @"gauz21-kazan.ru"
//                                                }];
//        ClinicRLM* c4 =
//        [ClinicRLM createInRealm:realm withValue:@{@"name": @"Городская поликлиника №4",
//                                                @"address": @"ул. Рихарда Зорге, 121",
//                                                @"phone": @"(843)123-45-67",
//                                                @"work_hours": @"Пн-вс 08.00-20.00",
//                                                @"site": @"gauz21-kazan.ru"
//                                                }];
//        ClinicRLM* c5 =
//        [ClinicRLM createInRealm:realm withValue:@{@"name": @"Городская поликлиника №1",
//                                                @"address": @"ул. Рихарда Зорге, 121",
//                                                @"phone": @"(843)123-45-67",
//                                                @"work_hours": @"Пн-вс 08.00-20.00",
//                                                @"site": @"gauz21-kazan.ru"
//                                                }];
//        ClinicRLM* c6 =
//        [ClinicRLM createInRealm:realm withValue:@{@"name": @"Городская поликлиника №2",
//                                                @"address": @"ул. Рихарда Зорге, 121",
//                                                @"phone": @"(843)123-45-67",
//                                                @"work_hours": @"Пн-вс 08.00-20.00",
//                                                @"site": @"gauz21-kazan.ru"
//                                                }];
//        ClinicRLM* c7 =
//        [ClinicRLM createInRealm:realm withValue:@{@"name": @"Городская поликлиника №6",
//                                                @"address": @"ул. Рихарда Зорге, 121",
//                                                @"phone": @"(843)123-45-67",
//                                                @"work_hours": @"Пн-вс 08.00-20.00",
//                                                @"site": @"gauz21-kazan.ru"
//                                                }];
//        ClinicRLM* c8 =
//        [ClinicRLM createInRealm:realm withValue:@{@"name": @"Городская поликлиника №16",
//                                                @"address": @"ул. Рихарда Зорге, 121",
//                                                @"phone": @"(843)123-45-67",
//                                                @"work_hours": @"Пн-вс 08.00-20.00",
//                                                @"site": @"gauz21-kazan.ru"
//                                                }];
//        ClinicRLM* c9 =
//        [ClinicRLM createInRealm:realm withValue:@{@"name": @"Городская поликлиника №31",
//                                                @"address": @"ул. Рихарда Зорге, 121",
//                                                @"phone": @"(843)123-45-67",
//                                                @"work_hours": @"Пн-вс 08.00-20.00",
//                                                @"site": @"gauz21-kazan.ru"
//                                                }];
//        SpecialistRLM* s1 =
//        [SpecialistRLM createInRealm:realm withValue:@{@"name": @"Галина",
//                                                    @"surname": @"Лунина",
//                                                    @"patronymic": @"Сергеевна",
//                                                    @"speciality": spTherapist,
//                                                    @"clinic": c1
//                                                    }];
//        SpecialistRLM* s2 =
//        [SpecialistRLM createInRealm:realm withValue:@{@"name": @"Владимир",
//                                                    @"surname": @"Дерюжов",
//                                                    @"patronymic": @"Матвеевич",
//                                                    @"speciality": spSurgeon,
//                                                    @"clinic": c1
//                                                    }];
//        SpecialistRLM* s3 =
//        [SpecialistRLM createInRealm:realm withValue:@{@"name": @"Мария",
//                                                    @"surname": @"Курбатина",
//                                                    @"patronymic": @"Михайловна",
//                                                    @"speciality": spOphthalmologist,
//                                                    @"clinic": c2
//                                                    }];
//        SpecialistRLM* s4 =
//        [SpecialistRLM createInRealm:realm withValue:@{@"name": @"Лилия",
//                                                    @"surname": @"Гадельшина",
//                                                    @"patronymic": @"Агзамовна",
//                                                    @"speciality": spSurgeon,
//                                                    @"clinic": c2
//                                                    }];
//        SpecialistRLM* s5 =
//        [SpecialistRLM createInRealm:realm withValue:@{@"name": @"Татьяна",
//                                                    @"surname": @"Зефирова",
//                                                    @"patronymic": @"Петровна",
//                                                    @"speciality": spPediatrician,
//                                                    @"clinic": c5
//                                                    }];
////        Specialist* s6 =
//        [SpecialistRLM createInRealm:realm withValue:@{@"name": @"Нина",
//                                                    @"surname": @"Миннулина",
//                                                    @"patronymic": @"Константиновна",
//                                                    @"speciality": spTherapist,
//                                                    @"clinic": c5
//                                                    }];
////        Specialist* s7 =
//        [SpecialistRLM createInRealm:realm withValue:@{@"name": @"Галина",
//                                                    @"surname": @"Лунина",
//                                                    @"patronymic": @"Сергеевна",
//                                                    @"speciality": spTherapist,
//                                                    @"clinic": c6
//                                                    }];
////        Specialist* s8 =
//        [SpecialistRLM createInRealm:realm withValue:@{@"name": @"Владимир",
//                                                    @"surname": @"Дерюжов",
//                                                    @"patronymic": @"Матвеевич",
//                                                    @"speciality": spSurgeon,
//                                                    @"clinic": c4
//                                                    }];
////        Specialist* s9 =
//        [SpecialistRLM createInRealm:realm withValue:@{@"name": @"Мария",
//                                                    @"surname": @"Курбатина",
//                                                    @"patronymic": @"Михайловна",
//                                                    @"speciality": spOphthalmologist,
//                                                    @"clinic": c4
//                                                    }];
////        Specialist* s10 =
//        [SpecialistRLM createInRealm:realm withValue:@{@"name": @"Галина",
//                                                    @"surname": @"Лунина",
//                                                    @"patronymic": @"Сергеевна",
//                                                    @"speciality": spTherapist,
//                                                    @"clinic": c3
//                                                    }];
////        Specialist* s11 =
//        [SpecialistRLM createInRealm:realm withValue:@{@"name": @"Владимир",
//                                                    @"surname": @"Дерюжов",
//                                                    @"patronymic": @"Матвеевич",
//                                                    @"speciality": spSurgeon,
//                                                    @"clinic": c7
//                                                    }];
////        Specialist* s12 =
//        [SpecialistRLM createInRealm:realm withValue:@{@"name": @"Мария",
//                                                    @"surname": @"Курбатина",
//                                                    @"patronymic": @"Михайловна",
//                                                    @"speciality": spOphthalmologist,
//                                                    @"clinic": c3
//                                                    }];
////        Specialist* s13 =
//        [SpecialistRLM createInRealm:realm withValue:@{@"name": @"Лилия",
//                                                    @"surname": @"Гадельшина",
//                                                    @"patronymic": @"Агзамовна",
//                                                    @"speciality": spSurgeon,
//                                                    @"clinic": c7
//                                                    }];
////        Specialist* s14 =
//        [SpecialistRLM createInRealm:realm withValue:@{@"name": @"Татьяна",
//                                                    @"surname": @"Зефирова",
//                                                    @"patronymic": @"Петровна",
//                                                    @"speciality": spPediatrician,
//                                                    @"clinic": c3
//                                                    }];
////        Specialist* s15 =
//        [SpecialistRLM createInRealm:realm withValue:@{@"name": @"Нина",
//                                                    @"surname": @"Миннулина",
//                                                    @"patronymic": @"Константиновна",
//                                                    @"speciality": spTherapist,
//                                                    @"clinic": c7
//                                                    }];
////        Specialist* s16 =
//        [SpecialistRLM createInRealm:realm withValue:@{@"name": @"Галина",
//                                                    @"surname": @"Лунина",
//                                                    @"patronymic": @"Сергеевна",
//                                                    @"speciality": spTherapist,
//                                                    @"clinic": c8
//                                                    }];
////        Specialist* s17 =
//        [SpecialistRLM createInRealm:realm withValue:@{@"name": @"Владимир",
//                                                    @"surname": @"Дерюжов",
//                                                    @"patronymic": @"Матвеевич",
//                                                    @"speciality": spSurgeon,
//                                                    @"clinic": c8
//                                                    }];
////        Specialist* s18 =
//        [SpecialistRLM createInRealm:realm withValue:@{@"name": @"Мария",
//                                                    @"surname": @"Курбатина",
//                                                    @"patronymic": @"Михайловна",
//                                                    @"speciality": spOphthalmologist,
//                                                    @"clinic": c9
//                                                    }];
//        NSDate* now = [NSDate date];
//        [AppointmentRLM createInRealm:realm withValue:@{@"date": [NSDate dateWithTimeInterval:1*24*60*60 sinceDate:now],
//                                                     @"specialist": s1
//                                                     }];
//        [AppointmentRLM createInRealm:realm withValue:@{@"date": [NSDate dateWithTimeInterval:2*24*60*60 sinceDate:now],
//                                                     @"specialist": s2
//                                                     }];
//        [AppointmentRLM createInRealm:realm withValue:@{@"date": [NSDate dateWithTimeInterval:3*24*60*60 sinceDate:now],
//                                                     @"specialist": s3
//                                                     }];
//        [AppointmentRLM createInRealm:realm withValue:@{@"date": [NSDate dateWithTimeInterval:6*24*60*60 sinceDate:now],
//                                                     @"specialist": s4
//                                                     }];
//        [AppointmentRLM createInRealm:realm withValue:@{@"date": [NSDate dateWithTimeInterval:9*24*60*60 sinceDate:now],
//                                                     @"specialist": s5
//                                                     }];
////        [Appointment createInRealm:realm withValue:@{@"date": [NSDate dateWithTimeInterval:1*24*60*60 sinceDate:now],
////                                                     @"specialist": s1
////                                                     }];
////        [Appointment createInRealm:realm withValue:@{@"date": [NSDate dateWithTimeInterval:2*24*60*60 sinceDate:now],
////                                                     @"specialist": s2
////                                                     }];
////        [Appointment createInRealm:realm withValue:@{@"date": [NSDate dateWithTimeInterval:3*24*60*60 sinceDate:now],
////                                                     @"specialist": s3
////                                                     }];
////        [Appointment createInRealm:realm withValue:@{@"date": [NSDate dateWithTimeInterval:6*24*60*60 sinceDate:now],
////                                                     @"specialist": s4
////                                                     }];
////        [Appointment createInRealm:realm withValue:@{@"date": [NSDate dateWithTimeInterval:9*24*60*60 sinceDate:now],
////                                                     @"specialist": s5
////                                                     }];
////        [Appointment createInRealm:realm withValue:@{@"date": [NSDate dateWithTimeInterval:1*24*60*60 sinceDate:now],
////                                                     @"specialist": s1
////                                                     }];
////        [Appointment createInRealm:realm withValue:@{@"date": [NSDate dateWithTimeInterval:2*24*60*60 sinceDate:now],
////                                                     @"specialist": s2
////                                                     }];
////        [Appointment createInRealm:realm withValue:@{@"date": [NSDate dateWithTimeInterval:3*24*60*60 sinceDate:now],
////                                                     @"specialist": s3
////                                                     }];
////        [Appointment createInRealm:realm withValue:@{@"date": [NSDate dateWithTimeInterval:6*24*60*60 sinceDate:now],
////                                                     @"specialist": s4
////                                                     }];
////        [Appointment createInRealm:realm withValue:@{@"date": [NSDate dateWithTimeInterval:9*24*60*60 sinceDate:now],
////                                                     @"specialist": s5
////                                                     }];
//        NSArray<MeasurementRLM*>* ma = @[
//        [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Пульс (покой)",
//                                                     @"shortName": @"Покой",
//                                                     @"plotID": @"kPulseCalm",
//                                                     @"unitName": @"Уд/мин",
//                                                     @"decimalDigits": @0,
//                                                     @"rangeOfValuesMin": @30,
//                                                     @"rangeOfValuesMax": @250,
//                                                     @"normalValueLow": @80,
//                                                     @"normalValueHigh": @90
//                                                     }],
//        [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Пульс (активность)",
//                                                     @"shortName": @"Активность",
//                                                     @"plotID": @"kPulseActive",
//                                                     @"unitName": @"Уд/мин",
//                                                     @"decimalDigits": @0,
//                                                     @"rangeOfValuesMin": @30,
//                                                     @"rangeOfValuesMax": @250,
//                                                     @"normalValueLow": @100,
//                                                     @"normalValueHigh": @120
//                                                     }],
//        [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Температура",
//                                                     @"shortName": @"Температура",
//                                                     @"plotID": @"kTemperature",
//                                                     @"unitName": @"°C",
//                                                     @"decimalDigits": @1,
//                                                     @"rangeOfValuesMin": @34,
//                                                     @"rangeOfValuesMax": @43,
//                                                     @"normalValueLow": @36.4,
//                                                     @"normalValueHigh": @36.9
//                                                     }],
//        [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Артериальное давление (систолическое)",
//                                                     @"shortName": @"Систола",
//                                                     @"plotID": @"kSystolicBloodPressure",
//                                                     @"unitName": @"-",
//                                                     @"decimalDigits": @0,
//                                                     @"rangeOfValuesMin": @80,
//                                                     @"rangeOfValuesMax": @280,
//                                                     @"normalValueLow": @110,
//                                                     @"normalValueHigh": @120
//                                                     }],
//        [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Артериальное давление (диастолическое)",
//                                                     @"shortName": @"Диастола",
//                                                     @"plotID": @"kDiastolicBloodPressure",
//                                                     @"unitName": @"-",
//                                                     @"decimalDigits": @0,
//                                                     @"rangeOfValuesMin": @30,
//                                                     @"rangeOfValuesMax": @140,
//                                                     @"normalValueLow": @70,
//                                                     @"normalValueHigh": @80
//                                                     }],
//        [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Рост",
//                                                     @"shortName": @"Рост",
//                                                     @"plotID": @"kHeight",
//                                                     @"unitName": @"См",
//                                                     @"decimalDigits": @0,
//                                                     @"rangeOfValuesMin": @30,
//                                                     @"rangeOfValuesMax": @300,
//                                                     @"normalValueLow": @170,
//                                                     @"normalValueHigh": @180
//                                                     }],
//        [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Вес",
//                                                     @"shortName": @"Вес",
//                                                     @"plotID": @"kWeight",
//                                                     @"unitName": @"Кг",
//                                                     @"decimalDigits": @1,
//                                                     @"rangeOfValuesMin": @30,
//                                                     @"rangeOfValuesMax": @300,
//                                                     @"normalValueLow": @70,
//                                                     @"normalValueHigh": @80
//                                                     }],
//        [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Сахар (натощак)",
//                                                     @"shortName": @"Натощак",
//                                                     @"plotID": @"kSugarOnAnEmptyStomach",
//                                                     @"unitName": @"ммоль/л",
//                                                     @"decimalDigits": @1,
//                                                     @"rangeOfValuesMin": @0.1f,
//                                                     @"rangeOfValuesMax": @20.0f,
//                                                     @"normalValueLow": @5.0f,
//                                                     @"normalValueHigh": @6.0f
//                                                     }],
//        [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Сахар (до еды)",
//                                                     @"shortName": @"До еды",
//                                                     @"plotID": @"kSugarBeforeMeals",
//                                                     @"unitName": @"ммоль/л",
//                                                     @"decimalDigits": @1,
//                                                     @"rangeOfValuesMin": @0.1f,
//                                                     @"rangeOfValuesMax": @20.0f,
//                                                     @"normalValueLow": @5.0f,
//                                                     @"normalValueHigh": @6.0f
//                                                     }],
//        [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Сахар (2 часа после еды)",
//                                                     @"shortName": @"После еды",
//                                                     @"plotID": @"kSugarAfterMeals",
//                                                     @"unitName": @"ммоль/л",
//                                                     @"decimalDigits": @1,
//                                                     @"rangeOfValuesMin": @0.1f,
//                                                     @"rangeOfValuesMax": @20.0f,
//                                                     @"normalValueLow": @5.0f,
//                                                     @"normalValueHigh": @6.0f
//                                                     }],
//        [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Сахар (ночью)",
//                                                     @"shortName": @"Ночью",
//                                                     @"plotID": @"kSugarAtNight",
//                                                     @"unitName": @"ммоль/л",
//                                                     @"decimalDigits": @1,
//                                                     @"rangeOfValuesMin": @0.1f,
//                                                     @"rangeOfValuesMax": @20.0f,
//                                                     @"normalValueLow": @5.0f,
//                                                     @"normalValueHigh": @6.0f
//                                                     }],
//        [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Холестерин (общий)",
//                                                     @"shortName": @"Общий",
//                                                     @"plotID": @"kCholesterolGeneral",
//                                                     @"unitName": @"ммоль/л",
//                                                     @"decimalDigits": @1,
//                                                     @"rangeOfValuesMin": @0.0f,
//                                                     @"rangeOfValuesMax": @15.9f,
//                                                     @"normalValueLow": @5.0f,
//                                                     @"normalValueHigh": @6.0f
//                                                     }],
//        [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Холестерин (ЛПНВ)",
//                                                     @"shortName": @"ЛПВН",
//                                                     @"plotID": @"kCholesterolLPNP",
//                                                     @"unitName": @"ммоль/л",
//                                                     @"decimalDigits": @1,
//                                                     @"rangeOfValuesMin": @0.0f,
//                                                     @"rangeOfValuesMax": @15.9f,
//                                                     @"normalValueLow": @5.0f,
//                                                     @"normalValueHigh": @6.0f
//                                                     }],
//        [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Холестерин (ЛПВП)",
//                                                     @"shortName": @"ЛПВП",
//                                                     @"plotID": @"kCholesterolLPVP",
//                                                     @"unitName": @"ммоль/л",
//                                                     @"decimalDigits": @1,
//                                                     @"rangeOfValuesMin": @0.0f,
//                                                     @"rangeOfValuesMax": @15.9f,
//                                                     @"normalValueLow": @5.0f,
//                                                     @"normalValueHigh": @6.0f
//                                                     }],
//        [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Холестерин (ТГ)",
//                                                     @"shortName": @"ТГ",
//                                                     @"plotID": @"kCholesterolTG",
//                                                     @"unitName": @"ммоль/л",
//                                                     @"decimalDigits": @1,
//                                                     @"rangeOfValuesMin": @0.0f,
//                                                     @"rangeOfValuesMax": @15.9f,
//                                                     @"normalValueLow": @5.0f,
//                                                     @"normalValueHigh": @6.0f
//                                                     }],
//        [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Объем груди",
//                                                     @"shortName": @"Грудь",
//                                                     @"plotID": @"kBreastVolume",
//                                                     @"unitName": @"См",
//                                                     @"decimalDigits": @0,
//                                                     @"rangeOfValuesMin": @30,
//                                                     @"rangeOfValuesMax": @200,
//                                                     @"normalValueLow": @90,
//                                                     @"normalValueHigh": @100
//                                                     }],
//        [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Объем талии",
//                                                     @"shortName": @"Талия",
//                                                     @"plotID": @"kWaistVolume",
//                                                     @"unitName": @"См",
//                                                     @"decimalDigits": @0,
//                                                     @"rangeOfValuesMin": @30,
//                                                     @"rangeOfValuesMax": @200,
//                                                     @"normalValueLow": @60,
//                                                     @"normalValueHigh": @70
//                                                     }],
//        [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Объем бедер",
//                                                     @"shortName": @"Бедра",
//                                                     @"plotID": @"kThighsVolume",
//                                                     @"unitName": @"См",
//                                                     @"decimalDigits": @0,
//                                                     @"rangeOfValuesMin": @30,
//                                                     @"rangeOfValuesMax": @200,
//                                                     @"normalValueLow": @90,
//                                                     @"normalValueHigh": @100
//                                                     }]];
//        NSInteger n = 7;
//        NSDate* startDate = [NSDate dateWithTimeIntervalSinceNow:-7*24*60*60];
//        for (MeasurementRLM* m in ma) {
//            float d = (m.normalValueHigh-m.normalValueLow)/(n+1-4);
//            float base = m.normalValueLow - d*2;
//            
//            for (int i=0; i<=n; i++) {
//                float r = ((float)rand()/RAND_MAX - 0.5f) * d;
//                [m.readings addObject:
//                 [ReadingRLM createInRealm:realm withValue:@{@"date": [NSDate dateWithTimeInterval:i*24*60*60 sinceDate:startDate],
//                                                          @"value": @(base + i*d + r)
//                                                          }]];
//            }
//        }
//        [Recommendation createInRealm:realm withValue:@{@"type": @(RECOMMENDATION_TYPE_PLAIN),
//                                                        @"title": @"Диспансеризация",
//                                                        @"fullText": @"Диспансеризация - это метод систематического врачебного наблюдения за состоянием здоровья определенных групп здорового населения или больных хроническими болезнями с целью предупреждения и раннего выявления заболеваний, своевременного лечения и профилактики обострений. \n\nДиспансеризация - это метод систематического врачебного наблюдения за состоянием здоровья определенных групп здорового населения или больных хроническими болезнями с целью предупреждения и раннего выявления заболеваний, своевременного лечения и профилактики обострений. \n\nДиспансеризация - это метод систематического врачебного наблюдения за состоянием здоровья определенных групп здорового населения или больных хроническими болезнями с целью предупреждения и раннего выявления заболеваний, своевременного лечения и профилактики обострений. \n\nДиспансеризация - это метод систематического врачебного наблюдения за состоянием здоровья определенных групп здорового населения или больных хроническими болезнями с целью предупреждения и раннего выявления заболеваний, своевременного лечения и профилактики обострений. \n\nДиспансеризация - это метод систематического врачебного наблюдения за состоянием здоровья определенных групп здорового населения или больных хроническими болезнями с целью предупреждения и раннего выявления заболеваний, своевременного лечения и профилактики обострений.",
//                                                        @"shortText": @"В этом году вы имеете право на бесплатную диспансеризацию, то есть можете пройти полное обследование в клинике, к которой прикреплены.",
//                                                        @"isNew": @YES,
//                                                        @"isPostponed": @NO,
//                                                        @"isClosed": @NO,
//                                                        @"isPassed": @NO
//                                                        }];
//        [Recommendation createInRealm:realm withValue:@{@"type": @(RECOMMENDATION_TYPE_BIRTHDAY),
//                                                        @"title": @"Когда Вы родились?",
//                                                        @"fullText": @"",
//                                                        @"shortText": @"",
//                                                        @"isNew": @YES,
//                                                        @"isPostponed": @NO,
//                                                        @"isClosed": @NO,
//                                                        @"isPassed": @NO
//                                                        }];
//        [Recommendation createInRealm:realm withValue:@{@"type": @(RECOMMENDATION_TYPE_BAD_HABITS),
//                                                        @"title": @"Случались ли у Вас неприятности из-за употребления алкоголя или наркотиков?",
//                                                        @"fullText": @"",
//                                                        @"shortText": @"",
//                                                        @"isNew": @NO,
//                                                        @"isPostponed": @YES,
//                                                        @"isClosed": @NO,
//                                                        @"isPassed": @NO
//                                                        }];
//        [Recommendation createInRealm:realm withValue:@{@"type": @(RECOMMENDATION_TYPE_PLAIN),
//                                                        @"title": @"Высокая температура",
//                                                        @"fullText": @"В большинстве случаев это инородные агенты в организме - бактерии, вирусы, простейшие или следствие физического воздействия на организм (ожог, обморожение, инородное тело). При повышенной температуре существование агентов в организме становится затруднительным, инфекции, например, погибают при температуре около 38 С.Но любой организм, как и механизм, не совершенен и может давать сбои. В случае с температурой мы это можем наблюдать тогда, когда организм вследствие индивидуальных особенностей иммунной системы слишком бурно реагирует на различные инфекции, и температура повышается слишком высоко, для большинства людей это 38,5 С.",
//                                                        @"shortText": @"В большинстве случаев, температура это инородные агенты в организме - бактерии, вирусы, простейшие или следствие физического воздействия на организм (ожог, обморожение, инородное тело).",
//                                                        @"isNew": @NO,
//                                                        @"isPostponed": @NO,
//                                                        @"isClosed": @NO,
//                                                        @"isPassed": @NO
//                                                        }];
//        Relationship* relationshipWife =
//        [Relationship createInRealm:realm withValue:@{@"name": @"Жена"}];
//        [Relationship createInRealm:realm withValue:@{@"name": @"Муж"}];
//        [Relationship createInRealm:realm withValue:@{@"name": @"Мать"}];
//        [Relationship createInRealm:realm withValue:@{@"name": @"Отец"}];
//        [Relationship createInRealm:realm withValue:@{@"name": @"Сестра"}];
//        [Relationship createInRealm:realm withValue:@{@"name": @"Брат"}];
//        [Relationship createInRealm:realm withValue:@{@"name": @"Дочь"}];
//        Relationship* relationshipSon =
//        [Relationship createInRealm:realm withValue:@{@"name": @"Сын"}];
//        
//        Gender* female =
//        [Gender createInRealm:realm withValue:@{@"name": @"Женский"}];
//        Gender* male =
//        [Gender createInRealm:realm withValue:@{@"name": @"Мужской"}];
//        
//        UserRLM* defaultUser =
//        [UserRLM createInRealm:realm withValue:@{@"portraitFilename": @"avatar",
//                                              @"name": @"Григорий",
//                                              @"surname": @"Лямкин",
//                                              @"patronymic": @"Михайлович",
//                                              @"gender": male,
//                                              @"passportSeries": @"9632",
//                                              @"passportNumber": @"946585",
//                                              @"polisNumber": @"456453463",
//                                              @"birthdayCertificate": @"95567678",
//                                              }];
//        UserRLM* wife =
//        [UserRLM createInRealm:realm withValue:@{@"relationship": relationshipWife,
//                                              @"portraitFilename": @"Default Avatar",
//                                              @"name": @"Елена",
//                                              @"surname": @"Лямкина",
//                                              @"patronymic": @"Андреевна",
//                                              @"gender": female,
//                                              @"passportSeries": @"9632",
//                                              @"passportNumber": @"946585",
//                                              @"polisNumber": @"456453463",
//                                              @"birthdayCertificate": @"95567678",
//                                              }];
//        UserRLM* son =
//        [UserRLM createInRealm:realm withValue:@{@"relationship": relationshipSon,
//                                              @"portraitFilename": @"Default Avatar",
//                                              @"name": @"Григорий",
//                                              @"surname": @"Лямкин",
//                                              @"patronymic": @"Григориевич",
//                                              @"gender": male,
//                                              @"passportSeries": @"9632",
//                                              @"passportNumber": @"946585",
//                                              @"polisNumber": @"456453463",
//                                              @"birthdayCertificate": @"95567678",
//                                              }];
//        FamilyRLM* family =
//        [FamilyRLM createInRealm:realm withValue:@{@"defaultMember": defaultUser}];
//        [family.members addObject:defaultUser];
//        [family.members addObject:wife];
//        [family.members addObject:son];
//    }
//    [realm commitWriteTransaction];
//}
//
//+ (void)deleteDummyData {
//    RLMRealm* realm = [RLMRealm defaultRealm];
//    [realm beginWriteTransaction]; {
//        [realm deleteAllObjects];
//    }
//    [realm commitWriteTransaction];
//}
//
//@end
