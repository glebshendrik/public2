//
//  Question.h
//  Public
//
//  Created by Глеб on 23/11/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Answer.h"

@interface Question : NSObject

@property StatusType status;
@property ControlType control;
@property Answer *answer;
@property NSString *answerID;
@property NSString *label;
@property NSString *value;
@property NSMutableArray *options;
@property NSMutableArray *answerArr;

@end