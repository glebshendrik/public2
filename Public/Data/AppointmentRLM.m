//
//  Appointment.m
//  Public
//
//  Created by Arslan Zagidullin on 11/09/15.
//  Copyright (c) 2015 Teoria. All rights reserved.
//

#import "AppointmentRLM.h"


@implementation AppointmentRLM

+ (NSDictionary *)defaultPropertyValues {
    NSDate* date = [NSDate date];
    return @{
             @"date": date
             ,@"ID": @""
             ,@"name": @""
             ,@"ticketNumber": @""
             ,@"clinicFullName": @""
             ,@"clinicShortName": @""
             ,@"resourceId": @""
             ,@"resourceFullName": @"",
             @"resourceShortName": @"",
             @"serviceShortName": @"",
             @"resourceFio": @"",
             @"resourceSpeciality": @"",
             @"room": @""
             };
}

@end
