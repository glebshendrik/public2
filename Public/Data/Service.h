//
//  Service.h
//  Public
//
//  Created by Tagi on 11.11.15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Resource.h"

@interface Service : NSObject

@property (nonatomic) long ID;
@property (nonatomic, copy) NSString *name;
@property (nonatomic) NSMutableArray *resources;
@property (nonatomic, copy) NSString *clinicID;

@end
