//
//  ResourceRLM.m
//  Public
//
//  Created by Tagi on 11.11.15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import "ResourceRLM.h"

@implementation ResourceRLM

+ (NSString *)primaryKey {
    return @"ID";
}

+ (NSDictionary *)defaultPropertyValues {
    return @{@"ID": @"", @"fullName": @"", @"fio": @"", @"specialityName": @"", @"specialityID": @"", @"room": @"", @"serviceID": @""};
}

//@property NSString *ID;
//@property NSString *fullName;
//@property NSString *room;
//@property NSString *serviceID;
//@property NSString *fio;
//@property NSString *specialityName;
//@property NSString *specialityID;

@end
