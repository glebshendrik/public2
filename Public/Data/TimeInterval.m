//
//  TimeInterval.m
//  Public
//
//  Created by Tagi on 18.11.15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import "TimeInterval.h"

@implementation TimeInterval

- (instancetype)initWithID:(long)ID time:(NSString *)time {
    self = [super init];
    
    if (!self) {
        return nil;
    }
    
    _ID = ID;
    _time = time;
    
    return self;
}

@end
