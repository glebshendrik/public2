//
//  Family.m
//  Public
//
//  Created by Arslan Zagidullin on 05/10/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import "FamilyRLM.h"

@implementation FamilyRLM

+ (NSString *)primaryKey {
    return @"ID";
}

@end
