//
//  Family.h
//  Public
//
//  Created by Arslan Zagidullin on 05/10/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import <Realm/Realm.h>
#import "UserRLM.h"


@interface FamilyRLM : RLMObject

@property NSString *ID;
@property UserRLM *defaultMember;
@property UserRLM *currentMember;
@property RLMArray<UserRLM *><UserRLM> *members;

@end
