//
//  Gender.h
//  Public
//
//  Created by Arslan Zagidullin on 07/10/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import <Realm/Realm.h>

@interface Gender : RLMObject

@property NSString* name;

@end
