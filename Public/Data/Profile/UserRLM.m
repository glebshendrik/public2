//
//  User.m
//  Public
//
//  Created by Arslan Zagidullin on 05/10/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import "UserRLM.h"

@implementation UserRLM

+ (NSDictionary *)defaultPropertyValues {
    return @{
             @"portraitFilename": @""
             ,@"name": @""
             ,@"surname": @""
             ,@"patronymic": @""
             ,@"passportSeries": @""
             ,@"passportNumber": @""
             ,@"polisNumber": @""
             ,@"birthdayCertificate": @""
             ,@"gender": @""
             ,@"relationship": @""
             ,@"portraitFilename": @""
             ,@"image": UIImagePNGRepresentation([UIImage imageNamed:@"Default Avatar"])
             ,@"passportSeries": @""
             ,@"passportNumber": @""
             ,@"polisNumber": @""
             ,@"birthdayCertificate": @""
             ,@"birthDay": [NSNull null]
             ,@"region": @"",
             @"phone": @""
             };
}

+ (NSString *)primaryKey {
    return @"ID";
}

@end
//@property NSString *ID;
//@property NSString *uuid;
//@property Relationship* relationship;
//@property NSString* portraitFilename;
//@property NSString* name;
//@property NSString* surname;
//@property NSString* patronymic;
//@property Gender* gender;
//@property NSString* passportSeries;
//@property NSString* passportNumber;
//@property NSString* polisNumber;
//@property NSString* birthdayCertificate;
