//
//  Person.m
//  Public
//
//  Created by Глеб on 12/10/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import "Person.h"

@implementation Person

// Specify default values for properties

//+ (NSDictionary *)defaultPropertyValues
//{
//    return @{};
//}

// Specify properties to ignore (Realm won't persist these)

//+ (NSArray *)ignoredProperties
//{
//    return @[];
//}

@end
