//
//  Appointment.h
//  Public
//
//  Created by Глеб on 16/10/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Appointment : NSObject
@property (nonatomic) NSDate *date;
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *ticketNumber;
@property (nonatomic, copy) NSString *clinicFullName;
@property (nonatomic, copy) NSString *clinicShortName;
@property (nonatomic, copy) NSString *resourceId;
@property (nonatomic, copy) NSString *resourceFullName;
@property (nonatomic, copy) NSString *resourceShortName;
@property (nonatomic, copy) NSString *serviceShortName;
@property (nonatomic, copy) NSString *resourceFio;
@property (nonatomic, copy) NSString *resourceSpeciality;
@property (nonatomic, copy) NSString *room;

@end