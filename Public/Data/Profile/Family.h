//
//  Family.h
//  Public
//
//  Created by Глеб on 15/10/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"

@interface Family : NSObject

@property (nonatomic, copy) NSString *ID;
@property (nonatomic) User *defaultMember;
@property (nonatomic) User *currentMember;
@property (nonatomic) NSMutableArray *members;

@end
