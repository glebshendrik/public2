//
//  AccountRLM.h
//  Public
//
//  Created by Глеб on 13/10/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import <Realm/Realm.h>
#import "UserRLM.h"
#import "FamilyRLM.h"

@interface AccountRLM : RLMObject

@property NSString* login;
@property NSString* password;
@property FamilyRLM *family;
@property NSString* ID;
@property NSString* uuid;

@end

// This protocol enables typed collections. i.e.:
// RLMArray<AccountRLM>
RLM_ARRAY_TYPE(AccountRLM)
