//
//  User.h
//  Public
//
//  Created by Глеб on 14/10/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Appointment.h"
#import "Measurement.h"

@interface User : NSObject

@property (nonatomic) NSString *ID;
@property (nonatomic) NSString *uuid;
@property (nonatomic) NSString *relationship;
@property (nonatomic) NSString *portraitFilename;
@property (nonatomic) NSData *image;
@property (nonatomic) NSString *name;
@property (nonatomic) NSString *surname;
@property (nonatomic) NSString *patronymic;
@property (nonatomic) NSString *gender;
@property (nonatomic) NSDate *birthDay;
@property (nonatomic) NSString *region;
@property (nonatomic) NSString *phone;

@property (nonatomic) NSString *passportSeries;
@property (nonatomic) NSString *passportNumber;
@property (nonatomic) NSString *polisNumber;
@property (nonatomic) NSString *birthdayCertificate;
@property (nonatomic) Appointment *appointment;

@end
