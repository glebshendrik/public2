//
//  Relationship.h
//  Public
//
//  Created by Arslan Zagidullin on 05/10/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import <Realm/Realm.h>


@interface Relationship : RLMObject

@property NSString* name;

@end
