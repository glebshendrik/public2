//
//  testUser.h
//  Public
//
//  Created by Глеб on 12/10/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import <Realm/Realm.h>

@interface TestUser : RLMObject

@property NSString* name;

@end

// This protocol enables typed collections. i.e.:
// RLMArray<testUser>
RLM_ARRAY_TYPE(testUser)
