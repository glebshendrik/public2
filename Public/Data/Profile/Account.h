//
//  Account.h
//  Public
//
//  Created by Глеб on 15/10/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Family.h"

@interface Account : NSObject

@property (nonatomic) NSString* login;
@property (nonatomic) NSString* password;
@property (nonatomic) Family *family;
@property (nonatomic) NSString* ID;
@property (nonatomic) NSString* uuid;

@end
