//
//  User.h
//  Public
//
//  Created by Arslan Zagidullin on 05/10/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import <Realm/Realm.h>
#import "Relationship.h"
#import "Gender.h"
#import "AppointmentRLM.h"
#import <UIKit/UIKit.h>
#import "MeasurementRLM.h"

@interface UserRLM : RLMObject

@property NSString *ID;
@property NSString *uuid;
@property NSString *relationship;
@property NSString *portraitFilename;
@property NSData   *image;
@property NSString *name;
@property NSString *surname;
@property NSString *patronymic;
@property NSString *gender;
@property NSDate *birthDay;
@property NSString *region;
@property NSString *phone;

@property NSString *passportSeries;
@property NSString *passportNumber;
@property NSString *polisNumber;
@property NSString *birthdayCertificate;

@property RLMArray<AppointmentRLM *><AppointmentRLM> *appointment;




@end

RLM_ARRAY_TYPE(UserRLM)