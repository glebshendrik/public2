//
//  Clinic.h
//  Public
//
//  Created by Глеб on 20/10/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Clinic : NSObject

@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, copy) NSString *work_hours;
@property (nonatomic, copy) NSString *site;
@property (nonatomic) BOOL parent;
@property (nonatomic) BOOL attached;
@property (nonatomic) BOOL oneOfChildrenAttached;

@end
