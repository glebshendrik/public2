//
//  MedParam.h
//  Public
//
//  Created by Глеб on 04/11/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import <Realm/Realm.h>
#import "PulseRLM.h"

@interface MedParamRLM : RLMObject

@property RLMArray<PulseRLM *><PulseRLM> *pulse;


@end

// This protocol enables typed collections. i.e.:
// RLMArray<MedParam>
//RLM_ARRAY_TYPE(MedParam)
