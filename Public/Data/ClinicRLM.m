//
//  Clinic.m
//  
//
//  Created by Teoria 5 on 10/09/15.
//
//

#import "ClinicRLM.h"


@implementation ClinicRLM

+ (NSString *)primaryKey {
    return @"ID";
}

+(NSDictionary *)defaultPropertyValues {
    return @{
             @"ID": @""
             ,@"name" : @""
             ,@"address" : @""
             ,@"phone" : @""
             ,@"work_hours" : @""
             ,@"site" : @"",
             @"parent": @NO,
             @"attached": @NO
             };
}
@end