//
//  DummyDataLight.h
//  Public
//
//  Created by Глеб on 09/11/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DummyDataLight : NSObject

+ (void)generateDummyData;
+ (void)deleteDummyData;

@end
