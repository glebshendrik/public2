//
//  Reading.h
//  Public
//
//  Created by Arslan Zagidullin on 18/09/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import <Realm/Realm.h>
#import "MeasurementRLM.h"
#import "UserRLM.h"

@interface ReadingRLM : RLMObject

@property NSString *ID;
@property NSDate* date;
@property float value;
@property MeasurementRLM *measurement;
@property UserRLM *user;

@end

RLM_ARRAY_TYPE(ReadingRLM)