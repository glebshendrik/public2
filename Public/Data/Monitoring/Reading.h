//
//  Reading.h
//  Public
//
//  Created by Глеб on 09/11/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Measurement.h"
#import "User.h"

@interface Reading : NSObject

@property (nonatomic) NSString *ID;
@property (nonatomic) NSDate *date;
@property (nonatomic) float value;
@property (nonatomic) Measurement *measurement;
@property (nonatomic) User* user;

@end