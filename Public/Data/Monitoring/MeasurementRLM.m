//
//  Measurement.m
//  Public
//
//  Created by Arslan Zagidullin on 18/09/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import "MeasurementRLM.h"

@implementation MeasurementRLM

+ (NSString *)primaryKey {
    return @"plotID";
}

@end
