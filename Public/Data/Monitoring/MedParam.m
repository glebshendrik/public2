//
//  MedParam.m
//  Public
//
//  Created by Глеб on 05/11/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import "MedParam.h"

@implementation MedParam

- (NSMutableArray *)pulse {
    if (!_pulse) {
        _pulse = [NSMutableArray new];
    }
    
    return _pulse;
}

@end
