//
//  Norm.h
//  Public
//
//  Created by Tagi on 01.12.15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Norm : NSObject

@property (nonatomic, readonly) float min;
@property (nonatomic, readonly) float max;

- (instancetype)initWithMin:(float)min max:(float)max;

@end
