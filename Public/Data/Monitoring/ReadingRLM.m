//
//  Reading.m
//  Public
//
//  Created by Arslan Zagidullin on 18/09/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import "ReadingRLM.h"


@implementation ReadingRLM

+ (NSString *)primaryKey {
    return @"ID";
}

@end
