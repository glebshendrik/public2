//
//  Pulse.h
//  Public
//
//  Created by Глеб on 05/11/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Pulse : NSObject

@property (nonatomic) NSString *fixedTime;
@property (nonatomic) NSString *beat;
@property (nonatomic) NSString *pulseStatus;

@end
