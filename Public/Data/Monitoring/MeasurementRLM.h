//
//  Measurement.h
//  Public
//
//  Created by Arslan Zagidullin on 18/09/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import <Realm/Realm.h>
//#import "ReadingRLM.h"

@interface MeasurementRLM : RLMObject

@property NSString* name;
@property NSString* shortName;
@property NSString* plotID;
@property NSString* unitName;
@property int decimalDigits;
@property float rangeOfValuesMin;
@property float rangeOfValuesMax;
@property float normalValueLow;
@property float normalValueHigh;
@property float defaultValue;

//@property RLMArray<ReadingRLM *><ReadingRLM> *readings;

@end

RLM_ARRAY_TYPE(Measurement)
