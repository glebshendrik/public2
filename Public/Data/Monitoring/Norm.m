//
//  Norm.m
//  Public
//
//  Created by Tagi on 01.12.15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import "Norm.h"

@interface Norm()

@end

@implementation Norm

- (instancetype)initWithMin:(float)min max:(float)max {
    self = [super init];
    
    if (!self) {
        return nil;
    }
    
    _min = min;
    _max = max;
    
    return self;
}

@end
