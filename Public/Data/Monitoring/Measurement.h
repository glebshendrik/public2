//
//  Measurement.h
//  Public
//
//  Created by Глеб on 09/11/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Norm.h"

@interface Measurement : NSObject

@property (nonatomic) NSString* name;
@property (nonatomic) NSString* shortName;
@property (nonatomic) NSString* plotID;
@property (nonatomic) NSString* unitName;
@property (nonatomic) int decimalDigits;
@property (nonatomic) float rangeOfValuesMin;
@property (nonatomic) float rangeOfValuesMax;
@property (nonatomic) float normalValueLow;
@property (nonatomic) float normalValueHigh;
@property (nonatomic) float defaultValue;

@property (nonatomic) Norm *norm;

@end
