//
//  Pulse.h
//  Public
//
//  Created by Глеб on 05/11/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import <Realm/Realm.h>

@interface PulseRLM : RLMObject

@property NSString *fixedTime;
@property NSString *beat;
@property NSString *pulseStatus;


@end

// This protocol enables typed collections. i.e.:
// RLMArray<Pulse>
RLM_ARRAY_TYPE(PulseRLM)