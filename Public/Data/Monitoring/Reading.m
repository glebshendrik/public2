//
//  Reading.m
//  Public
//
//  Created by Глеб on 09/11/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import "Reading.h"

@implementation Reading

- (NSString *)description {
    NSDateFormatter *df = [NSDateFormatter new];
    df.dateFormat = @"dd MMMM yyyy, hh:mm";
    
    return [NSString  stringWithFormat:@"%@, %f, %@", self.measurement.name, self.value, [df stringFromDate:self.date]];
}

@end
