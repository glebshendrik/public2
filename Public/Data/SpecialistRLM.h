//
//  Specialist.h
//  
//
//  Created by Teoria 5 on 10/09/15.
//
//

#import <Realm/Realm.h>
#import "ClinicRLM.h"
#import "SpecialityRLM.h"

@interface SpecialistRLM : RLMObject

@property NSString* name;
@property NSString* surname;
@property NSString* patronymic;
@property SpecialityRLM* speciality;
@property ClinicRLM* clinic;

@end

RLM_ARRAY_TYPE(Specialist)
