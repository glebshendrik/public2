//
//  ServiceRLM.h
//  Public
//
//  Created by Tagi on 11.11.15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import <Realm/Realm.h>
#import "ResourceRLM.h"

@interface ServiceRLM : RLMObject

@property long ID;
@property NSString *name;
@property NSString *clinicID;
@property RLMArray<ResourceRLM *><ResourceRLM> *resources;

@end
