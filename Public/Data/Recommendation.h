//
//  Recommendation.h
//  Public
//
//  Created by Teoria 5 on 08/09/15.
//  Copyright (c) 2015 Teoria. All rights reserved.
//

#import <Realm/Realm.h>


#define RECOMMENDATION_TYPE_PLAIN 0
#define RECOMMENDATION_TYPE_BIRTHDAY 1
#define RECOMMENDATION_TYPE_BAD_HABITS 2


typedef NS_ENUM(int, RecommendationType) {
    RecommendationTypeAnswer,
    RecommendationTypeVopros
};

@interface Recommendation : RLMObject

//@property int type;

@property (nonatomic) RecommendationType type;

@property NSString* title;
@property NSString* fullText;
@property NSString* shortText;

@property BOOL isNew;
@property BOOL isPostponed;
@property BOOL isClosed;
@property BOOL isPassed;

@end