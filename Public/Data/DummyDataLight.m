//
//  DummyDataLight.m
//  Public
//
//  Created by Глеб on 09/11/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import "DummyDataLight.h"
#import <Realm/Realm.h>

#import "ClinicRLM.h"
#import "AppointmentRLM.h"
#import "SpecialityRLM.h"
#import "SpecialistRLM.h"

#import "MeasurementRLM.h"
#import "ReadingRLM.h"

#import "Recommendation.h"

#import "FamilyRLM.h"
#import "UserRLM.h"



@implementation DummyDataLight

+ (void)generateDummyData {
    RLMRealm* realm = [RLMRealm defaultRealm];
    
    RLMResults* array = [SpecialityRLM allObjects];
    if (array.count > 0)
        return;
    
    NSLog(@"generating dummy data");
    [realm beginWriteTransaction]; {
                NSArray<MeasurementRLM*>* ma = @[
                                         [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Пульс (покой)",
                                                                                         @"shortName": @"Покой",
                                                                                         @"plotID": @"kPulseCalm",
                                                                                         @"unitName": @"Уд/мин",
                                                                                         @"decimalDigits": @0,
                                                                                         @"rangeOfValuesMin": @30,
                                                                                         @"rangeOfValuesMax": @250,
                                                                                         @"normalValueLow": @80,
                                                                                         @"normalValueHigh": @90
                                                                                         }],
                                         [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Пульс (активность)",
                                                                                         @"shortName": @"Активность",
                                                                                         @"plotID": @"kPulseActive",
                                                                                         @"unitName": @"Уд/мин",
                                                                                         @"decimalDigits": @0,
                                                                                         @"rangeOfValuesMin": @30,
                                                                                         @"rangeOfValuesMax": @250,
                                                                                         @"normalValueLow": @100,
                                                                                         @"normalValueHigh": @120
                                                                                         }],
                                         [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Температура",
                                                                                         @"shortName": @"Температура",
                                                                                         @"plotID": @"kTemperature",
                                                                                         @"unitName": @"°C",
                                                                                         @"decimalDigits": @1,
                                                                                         @"rangeOfValuesMin": @34,
                                                                                         @"rangeOfValuesMax": @43,
                                                                                         @"normalValueLow": @36.4,
                                                                                         @"normalValueHigh": @36.9
                                                                                         }],
                                         [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Артериальное давление (систолическое)",
                                                                                         @"shortName": @"Систола",
                                                                                         @"plotID": @"kSystolicBloodPressure",
                                                                                         @"unitName": @"-",
                                                                                         @"decimalDigits": @0,
                                                                                         @"rangeOfValuesMin": @80,
                                                                                         @"rangeOfValuesMax": @280,
                                                                                         @"normalValueLow": @110,
                                                                                         @"normalValueHigh": @120
                                                                                         }],
                                         [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Артериальное давление (диастолическое)",
                                                                                         @"shortName": @"Диастола",
                                                                                         @"plotID": @"kDiastolicBloodPressure",
                                                                                         @"unitName": @"-",
                                                                                         @"decimalDigits": @0,
                                                                                         @"rangeOfValuesMin": @30,
                                                                                         @"rangeOfValuesMax": @140,
                                                                                         @"normalValueLow": @70,
                                                                                         @"normalValueHigh": @80
                                                                                         }],
                                         [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Рост",
                                                                                         @"shortName": @"Рост",
                                                                                         @"plotID": @"kHeight",
                                                                                         @"unitName": @"См",
                                                                                         @"decimalDigits": @0,
                                                                                         @"rangeOfValuesMin": @30,
                                                                                         @"rangeOfValuesMax": @300,
                                                                                         @"normalValueLow": @170,
                                                                                         @"normalValueHigh": @180
                                                                                         }],
                                         [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Вес",
                                                                                         @"shortName": @"Вес",
                                                                                         @"plotID": @"kWeight",
                                                                                         @"unitName": @"Кг",
                                                                                         @"decimalDigits": @1,
                                                                                         @"rangeOfValuesMin": @30,
                                                                                         @"rangeOfValuesMax": @300,
                                                                                         @"normalValueLow": @70,
                                                                                         @"normalValueHigh": @80
                                                                                         }],
                                         [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Сахар (натощак)",
                                                                                         @"shortName": @"Натощак",
                                                                                         @"plotID": @"kSugarOnAnEmptyStomach",
                                                                                         @"unitName": @"ммоль/л",
                                                                                         @"decimalDigits": @1,
                                                                                         @"rangeOfValuesMin": @0.1f,
                                                                                         @"rangeOfValuesMax": @20.0f,
                                                                                         @"normalValueLow": @5.0f,
                                                                                         @"normalValueHigh": @6.0f
                                                                                         }],
                                         [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Сахар (до еды)",
                                                                                         @"shortName": @"До еды",
                                                                                         @"plotID": @"kSugarBeforeMeals",
                                                                                         @"unitName": @"ммоль/л",
                                                                                         @"decimalDigits": @1,
                                                                                         @"rangeOfValuesMin": @0.1f,
                                                                                         @"rangeOfValuesMax": @20.0f,
                                                                                         @"normalValueLow": @5.0f,
                                                                                         @"normalValueHigh": @6.0f
                                                                                         }],
                                         [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Сахар (2 часа после еды)",
                                                                                         @"shortName": @"После еды",
                                                                                         @"plotID": @"kSugarAfterMeals",
                                                                                         @"unitName": @"ммоль/л",
                                                                                         @"decimalDigits": @1,
                                                                                         @"rangeOfValuesMin": @0.1f,
                                                                                         @"rangeOfValuesMax": @20.0f,
                                                                                         @"normalValueLow": @5.0f,
                                                                                         @"normalValueHigh": @6.0f
                                                                                         }],
                                         [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Сахар (ночью)",
                                                                                         @"shortName": @"Ночью",
                                                                                         @"plotID": @"kSugarAtNight",
                                                                                         @"unitName": @"ммоль/л",
                                                                                         @"decimalDigits": @1,
                                                                                         @"rangeOfValuesMin": @0.1f,
                                                                                         @"rangeOfValuesMax": @20.0f,
                                                                                         @"normalValueLow": @5.0f,
                                                                                         @"normalValueHigh": @6.0f
                                                                                         }],
                                         [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Холестерин (общий)",
                                                                                         @"shortName": @"Общий",
                                                                                         @"plotID": @"kCholesterolGeneral",
                                                                                         @"unitName": @"ммоль/л",
                                                                                         @"decimalDigits": @1,
                                                                                         @"rangeOfValuesMin": @0.0f,
                                                                                         @"rangeOfValuesMax": @15.9f,
                                                                                         @"normalValueLow": @5.0f,
                                                                                         @"normalValueHigh": @6.0f
                                                                                         }],
                                         [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Холестерин (ЛПНВ)",
                                                                                         @"shortName": @"ЛПВН",
                                                                                         @"plotID": @"kCholesterolLPNP",
                                                                                         @"unitName": @"ммоль/л",
                                                                                         @"decimalDigits": @1,
                                                                                         @"rangeOfValuesMin": @0.0f,
                                                                                         @"rangeOfValuesMax": @15.9f,
                                                                                         @"normalValueLow": @5.0f,
                                                                                         @"normalValueHigh": @6.0f
                                                                                         }],
                                         [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Холестерин (ЛПВП)",
                                                                                         @"shortName": @"ЛПВП",
                                                                                         @"plotID": @"kCholesterolLPVP",
                                                                                         @"unitName": @"ммоль/л",
                                                                                         @"decimalDigits": @1,
                                                                                         @"rangeOfValuesMin": @0.0f,
                                                                                         @"rangeOfValuesMax": @15.9f,
                                                                                         @"normalValueLow": @5.0f,
                                                                                         @"normalValueHigh": @6.0f
                                                                                         }],
                                         [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Холестерин (ТГ)",
                                                                                         @"shortName": @"ТГ",
                                                                                         @"plotID": @"kCholesterolTG",
                                                                                         @"unitName": @"ммоль/л",
                                                                                         @"decimalDigits": @1,
                                                                                         @"rangeOfValuesMin": @0.0f,
                                                                                         @"rangeOfValuesMax": @15.9f,
                                                                                         @"normalValueLow": @5.0f,
                                                                                         @"normalValueHigh": @6.0f
                                                                                         }],
                                         [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Объем груди",
                                                                                         @"shortName": @"Грудь",
                                                                                         @"plotID": @"kBreastVolume",
                                                                                         @"unitName": @"См",
                                                                                         @"decimalDigits": @0,
                                                                                         @"rangeOfValuesMin": @30,
                                                                                         @"rangeOfValuesMax": @200,
                                                                                         @"normalValueLow": @90,
                                                                                         @"normalValueHigh": @100
                                                                                         }],
                                         [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Объем талии",
                                                                                         @"shortName": @"Талия",
                                                                                         @"plotID": @"kWaistVolume",
                                                                                         @"unitName": @"См",
                                                                                         @"decimalDigits": @0,
                                                                                         @"rangeOfValuesMin": @30,
                                                                                         @"rangeOfValuesMax": @200,
                                                                                         @"normalValueLow": @60,
                                                                                         @"normalValueHigh": @70
                                                                                         }],
                                         [MeasurementRLM createInRealm:realm withValue:@{@"name": @"Объем бедер",
                                                                                         @"shortName": @"Бедра",
                                                                                         @"plotID": @"kThighsVolume",
                                                                                         @"unitName": @"См",
                                                                                         @"decimalDigits": @0,
                                                                                         @"rangeOfValuesMin": @30,
                                                                                         @"rangeOfValuesMax": @200,
                                                                                         @"normalValueLow": @90,
                                                                                         @"normalValueHigh": @100
                                                                                         }]];
        NSInteger n = 7;
        NSDate* startDate = [NSDate dateWithTimeIntervalSinceNow:-7*24*60*60];
        for (MeasurementRLM* m in ma) {
            float d = (m.normalValueHigh-m.normalValueLow)/(n+1-4);
            float base = m.normalValueLow - d*2;
            
            for (int i=0; i<=n; i++) {
                float r = ((float)rand()/RAND_MAX - 0.5f) * d;
//                [m.readings addObject:
//                 [ReadingRLM createInRealm:realm withValue:@{@"date": [NSDate dateWithTimeInterval:i*24*60*60 sinceDate:startDate],
//                                                             @"value": @(base + i*d + r)
//                                                             }]];
            }
        }
        
//        [Recommendation createInRealm:realm withValue:@{@"type": @(RECOMMENDATION_TYPE_PLAIN),
//                                                        @"title": @"Диспансеризация",
//                                                        @"fullText": @"Диспансеризация - это метод систематического врачебного наблюдения за состоянием здоровья определенных групп здорового населения или больных хроническими болезнями с целью предупреждения и раннего выявления заболеваний, своевременного лечения и профилактики обострений. \n\nДиспансеризация - это метод систематического врачебного наблюдения за состоянием здоровья определенных групп здорового населения или больных хроническими болезнями с целью предупреждения и раннего выявления заболеваний, своевременного лечения и профилактики обострений. \n\nДиспансеризация - это метод систематического врачебного наблюдения за состоянием здоровья определенных групп здорового населения или больных хроническими болезнями с целью предупреждения и раннего выявления заболеваний, своевременного лечения и профилактики обострений. \n\nДиспансеризация - это метод систематического врачебного наблюдения за состоянием здоровья определенных групп здорового населения или больных хроническими болезнями с целью предупреждения и раннего выявления заболеваний, своевременного лечения и профилактики обострений. \n\nДиспансеризация - это метод систематического врачебного наблюдения за состоянием здоровья определенных групп здорового населения или больных хроническими болезнями с целью предупреждения и раннего выявления заболеваний, своевременного лечения и профилактики обострений.",
//                                                        @"shortText": @"В этом году вы имеете право на бесплатную диспансеризацию, то есть можете пройти полное обследование в клинике, к которой прикреплены.",
//                                                        @"isNew": @YES,
//                                                        @"isPostponed": @NO,
//                                                        @"isClosed": @NO,
//                                                        @"isPassed": @NO
//                                                        }];
//        [Recommendation createInRealm:realm withValue:@{@"type": @(RECOMMENDATION_TYPE_BIRTHDAY),
//                                                        @"title": @"Когда Вы родились?",
//                                                        @"fullText": @"",
//                                                        @"shortText": @"",
//                                                        @"isNew": @YES,
//                                                        @"isPostponed": @NO,
//                                                        @"isClosed": @NO,
//                                                        @"isPassed": @NO
//                                                        }];
//        [Recommendation createInRealm:realm withValue:@{@"type": @(RECOMMENDATION_TYPE_BAD_HABITS),
//                                                        @"title": @"Случались ли у Вас неприятности из-за употребления алкоголя или наркотиков?",
//                                                        @"fullText": @"",
//                                                        @"shortText": @"",
//                                                        @"isNew": @NO,
//                                                        @"isPostponed": @YES,
//                                                        @"isClosed": @NO,
//                                                        @"isPassed": @NO
//                                                        }];
//        [Recommendation createInRealm:realm withValue:@{@"type": @(RECOMMENDATION_TYPE_PLAIN),
//                                                        @"title": @"Высокая температура",
//                                                        @"fullText": @"В большинстве случаев это инородные агенты в организме - бактерии, вирусы, простейшие или следствие физического воздействия на организм (ожог, обморожение, инородное тело). При повышенной температуре существование агентов в организме становится затруднительным, инфекции, например, погибают при температуре около 38 С.Но любой организм, как и механизм, не совершенен и может давать сбои. В случае с температурой мы это можем наблюдать тогда, когда организм вследствие индивидуальных особенностей иммунной системы слишком бурно реагирует на различные инфекции, и температура повышается слишком высоко, для большинства людей это 38,5 С.",
//                                                        @"shortText": @"В большинстве случаев, температура это инородные агенты в организме - бактерии, вирусы, простейшие или следствие физического воздействия на организм (ожог, обморожение, инородное тело).",
//                                                        @"isNew": @NO,
//                                                        @"isPostponed": @NO,
//                                                        @"isClosed": @NO,
//                                                        @"isPassed": @NO
//                                                        }];
    }
    [realm commitWriteTransaction];
}

+ (void)deleteDummyData {
    RLMRealm* realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction]; {
        [realm deleteAllObjects];
    }
    [realm commitWriteTransaction];
}

@end
