//
//  Answer.h
//  Public
//
//  Created by Глеб on 24/11/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import "Notification.h"

@interface Answer : Notification

@property NSString *answerID;
@property NSString *label;
@property NSDictionary *value;

@end
