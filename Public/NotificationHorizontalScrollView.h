//
//  NotificationHorizontalScrollView.h
//  Public
//
//  Created by Глеб on 27/11/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import "NotificationView.h"

@interface NotificationHorizontalScrollView : NotificationView

@property (nonatomic) NSArray *elements;

@end
