//
//  NotificationsListViewController.m
//  Public
//
//  Created by Глеб on 25/11/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import "NotificationsListViewController.h"
#import "NotificationView.h"
#import "StandardScrollView.h"

//
#import "Notification.h"
#import "Article.h"
#import "Message.h"
#import "Interview.h"
//
#import "Answer.h"
#import "Question.h"

//views
#import "NotificationArticleView.h"
#import "NotificationMessageView.h"
#import "NotificationInterView.h"
#import "NotificationHorizontalScrollView.h"
#import "NotificationTextFieldView.h"

//libraries
#import "MainAPI.h"
#import "SharedClass.h"
#import "TeoriusHelper.h"

#define RECOMMENDATIONS_LIST_ALL 0
#define RECOMMENDATIONS_LIST_POSTPONED 1
#define RECOMMENDATIONS_LIST_CLOSED 2

@interface NotificationsListViewController ()

@property (nonatomic) NSArray *notificationsArray;
@property (nonatomic) UIScrollView* mainScrollView;
@property (nonatomic) int scrollViewTop;

@end

@implementation NotificationsListViewController

#pragma mark - View's lifecycle

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:nil
                                                                            action:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[TeoriusHelper instance]addWaitingViewWithText:@"Обновление" font:[SharedClass getRegularFont] toView:self.navigationController.view disablingInteractionEvents:YES];
    
    [[MainAPI defaultAPI] getAllNotificationsAfterTapped:^(NSMutableArray *arr, NSMutableArray *arrHidden,NSError *err) {
        NSLog(@"%@", arr);
        
        
        self.notificationsArray = arr.copy;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self configSubviews];
            
            [[TeoriusHelper instance]removeWaitingView];
        });
        
        //
        
    }];
}


#pragma mark - Subviews

- (void)configSubviews {
    const float kSegmentedControlHeight = 26;
    CGRect segmentedControlFrame = [TeoriusHelper scaledRectFromCenter:CGPointMake(LOGICAL_WIDTH/2, HEADER_HEIGHT+20)
                                                                  size:CGSizeMake(260, kSegmentedControlHeight)];
    UISegmentedControl* segmentedControl = [[UISegmentedControl alloc] initWithItems:@[@"Все", @"Отложенные", @"Удаленные"]];
    segmentedControl.frame = segmentedControlFrame;
    segmentedControl.tintColor = COLOR_LIGHTERGREEN;
    segmentedControl.selectedSegmentIndex = RECOMMENDATIONS_LIST_ALL;
    [segmentedControl setTitleTextAttributes:@{NSFontAttributeName: [SharedClass getRegularFontWithSize:10.0f]} forState:UIControlStateNormal];
    [segmentedControl addTarget:self action:@selector(segmentedControlValueChanged:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:segmentedControl];
    
    self.scrollViewTop = HEADER_HEIGHT+kSegmentedControlHeight+20;
    CGRect mainScrollViewFrame = [TeoriusHelper scaledRect:CGRectMake(0, self.scrollViewTop, LOGICAL_WIDTH, SCREEN_HEIGHT-self.scrollViewTop)];
    UIScrollView* mainScrollView = [[StandardScrollView alloc] initWithFrame:mainScrollViewFrame];
    [self.view addSubview:mainScrollView];
    
    self.mainScrollView = mainScrollView;
    [self showRecommendationList:RECOMMENDATIONS_LIST_ALL inParentScrollView:self.mainScrollView];
}

- (void)reloadSubviews {
    [self.mainScrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    
}

- (void)showRecommendationList:(int)recommendationList inParentScrollView:(UIScrollView*)scrollView {
    
    CGFloat dY = self.scrollViewTop;
    CGFloat dX = SCALED_F(7.f);
    NSMutableArray<UIView*>* recommendationViews = [NSMutableArray array];
    NSMutableArray<UIView*>* interViews = [NSMutableArray array];
    NSMutableArray *buttons = [NSMutableArray new];
    
    if (self.notificationsArray.count > 0) {
        for (int i = 0; i < self.notificationsArray.count; i++) {
            UIView *view;
            
            if ([self.notificationsArray[i] isKindOfClass:[Interview class]]) {
                
                NotificationHorizontalScrollView *viewScroll = [[NotificationHorizontalScrollView alloc] initWithFrame:CGRectMake(dX, dY, self.mainScrollView.width - dX*2, 300.f)];
                
                NotificationInterView *viewInterview = [[NotificationInterView alloc] initWithFrame:CGRectMake(dX, dY, self.mainScrollView.width - dX*2, 0)];
                
                viewScroll.backgroundColor = [UIColor blackColor];
                
                Interview *interview = self.notificationsArray[i];
                
//                viewScroll.backgroundColor = [UIColor blackColor];
//                UIView *viewww = [[UIView alloc] initWithFrame:viewScroll.bounds];
//                viewww.backgroundColor = [UIColor greenColor];
//                UIView *viewww1 = [[UIView alloc] initWithFrame:viewScroll.bounds];
//                viewww1.backgroundColor = [UIColor orangeColor];
//                view1.textLabel.text = interview.title;
//                view1.textLabel.textColor = [UIColor whiteColor];
//                view1.textLabel.textAlignment = NSTextAlignmentCenter;
//                view1.backgroundColor = [UIColor blackColor];
                
                if (interview.answers.count > 0) {
                    NSMutableArray *buttons = [NSMutableArray new];
                    for (int i = 0; i < interview.answers.count; i++) {
                        UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
                        Answer *a = interview.answers[i];
                        button.tintColor = COLOR_LIGHTERGREEN;
                        [button setTitle:a.label forState:UIControlStateNormal];
                        [buttons addObject: button];
                    }
                    
                    viewInterview.buttonsArr = buttons;
                    viewInterview.textLabel.text = interview.title;
                    viewInterview.textLabel.textColor = [UIColor whiteColor];
                    viewInterview.textLabel.textAlignment = ViewAlignmentCenter;
                    viewInterview.backgroundColor = [UIColor blackColor];
                    viewScroll.elements = @[viewInterview];
                }
                
//                view1.height = [view1 calculateHeight];
//                viewScroll.width = SCALED_F(1000.f);
//                viewScroll.height = [viewScroll calculateHeight];
                
                if (interview.questions.count > 0) {
                    
//                    NSMutableArray *questions = [NSMutableArray new];
                    
                    NotificationInterView *viewInterviewTop = [[NotificationInterView alloc] initWithFrame:CGRectMake(dX, dY, self.mainScrollView.width - dX*2, 1000.f)];
                    viewInterviewTop.textLabel.text = interview.title;
                    viewInterviewTop.textLabel.textColor = [UIColor whiteColor];
                    viewInterviewTop.backgroundColor = [UIColor blackColor];
                    [interViews addObject:viewInterviewTop];
                    
                    for (int i = 0; i < interview.questions.count; i++) {
                        Question *q = interview.questions[i];
                        
                        switch (q.control) {
                            case ControlTypeButton: {
                                break;
                            }
                            case ControlTypeText: {
//                                UIView *viewww = [[UIView alloc] initWithFrame:viewScroll.bounds];
//                                viewww.backgroundColor = [UIColor greenColor];
                                
                                NotificationInterView *viewInterview1 = [[NotificationInterView alloc] initWithFrame:CGRectMake(dX, dY, self.mainScrollView.width - dX*2, 1000.f)];
                                viewInterview1.textLabel.text = q.label;
                                viewInterview1.textLabel.textColor = [UIColor whiteColor];
                                viewInterview1.textLabel.textAlignment = NSTextAlignmentCenter;
                                viewInterview1.backgroundColor = [UIColor blackColor];
                                
//                                NotificationTextFieldView *questionText = [NotificationTextFieldView new];
//                                questionText.typeField = 1;

                                [interViews addObject:viewInterview1];
                                
                                break;
                            }
                            case ControlTypeDate: {
//                                NotificationTextFieldView *questionText = [NotificationTextFieldView new];
//                                questionText.typeField = 2;
                                
                                break;
                            }
                            case ControlTypeRadio: {
//                                NotificationTextFieldView *questionText = [NotificationTextFieldView new];
//                                questionText.typeField = 2;
                                
                                break;
                            }
                            case ControlTypeSelect: {
                                
                                break;
                            }
                            case ControlTypeCheck: {
                                
                                break;
                            }
                            default: {
                                break;
                            }
                        }
                    }
                }
        
                viewScroll.elements = interViews;
                
                
                
                viewInterview.height = [viewInterview calculateHeight];
                view = viewScroll;
            }
            else if ([self.notificationsArray[i] isKindOfClass:[Article class]]) {
                Article *a = self.notificationsArray[i];
                NotificationArticleView *view1 = [[NotificationArticleView alloc] initWithFrame:CGRectMake(dX, dY, self.mainScrollView.width - dX*2, 0)];
                view1.textLabel.text = a.title;
                view1.height = [view1 calculateHeight];
                view1.backgroundColor = [UIColor whiteColor];
                if (a.status == 1) {
                    view1.layer.borderWidth = SCALED_F(2.f);
                    view1.layer.borderColor = [COLOR_LIGHTERGREEN CGColor];
                    view1.textLabel.font = [UIFont fontWithName:@"Roboto-Bold" size:16];
                }
                view = view1;
            }
            else if ([self.notificationsArray[i] isKindOfClass:[Message class]]) {
                Message *m = self.notificationsArray[i];
                
                NotificationMessageView *view1 = [[NotificationMessageView alloc] initWithFrame:CGRectMake(dX, dY, self.mainScrollView.width - dX*2, 0)];
 
                view1.textLabel.text = m.title;
                view1.height = [view1 calculateHeight];
                view1.backgroundColor = [UIColor whiteColor];
                view1.textLabel.font = [SharedClass getRegularFontWithSize:16.f];
                
                if (m.status == 1) {
                    view1.layer.borderWidth = SCALED_F(2.f);
                    view1.layer.borderColor = [COLOR_LIGHTERGREEN CGColor];
                    view1.textLabel.font = [UIFont fontWithName:@"Roboto-Bold" size:16];
                }
                
                view = view1;
            }
            view.layer.cornerRadius = SCALED_F(6.f);
            [scrollView addSubview:view];
            [recommendationViews addObject:view];
            dY = view.maxY + SCALED_F(8.f);
        }
    }
    else {
        NSString* emptyLabelText;
        switch (recommendationList) {
            case RECOMMENDATIONS_LIST_POSTPONED:
                emptyLabelText = @"У Вас нет отложенных уведомлений";
                break;
            case RECOMMENDATIONS_LIST_CLOSED:
                emptyLabelText = @"У Вас нет удаленных уведомлений";
                break;
            default: //RECOMMENDATIONS_LIST_ALL
                emptyLabelText = @"Пока нет ни одного уведомления\n\nВозможно, вы не ввели достаточно информации в разделе \"Мониторинг\" или личной информации";
                break;
        }
        UIFont *emptyLabelFont = [SharedClass getRegularFontWithSize:14.0f];
        CGRect emptyLabelFrame = CGRectMake(0, dY+10, LOGICAL_WIDTH - 2*dX, 0);
        UILabel *emptyLabel = [[UILabel alloc]initWithFrame:[TeoriusHelper scaledRect:emptyLabelFrame]];
        emptyLabel.text = emptyLabelText;
        emptyLabel.numberOfLines = 0;
        emptyLabel.textAlignment = NSTextAlignmentCenter;
        emptyLabel.textColor = UIColorFromRGB(0x98999A);
        emptyLabel.font = emptyLabelFont;
        [emptyLabel sizeToFit];
        [scrollView addSubview:emptyLabel];
        emptyLabel.center = CGPointMake(scrollView.frame.size.width/2, emptyLabel.center.y);
        
        dY = CGRectGetMaxY(emptyLabel.frame)/SCREEN_SCALE + 5;
    }
    
    self.mainScrollView.contentSize = CGSizeMake(self.mainScrollView.width, dY);
}

- (void)segmentedControlValueChanged:(UISegmentedControl*)segmentedControl {
    [self showRecommendationList:(int)segmentedControl.selectedSegmentIndex inParentScrollView:self.mainScrollView];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
