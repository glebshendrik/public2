//
//  PolisViewController.m
//  Public
//
//  Created by Teoria 5 on 21/09/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import "PolisViewController.h"
#import "SharedClass.h"
#import "AppointmentsListViewController.h"
#import "MainAPI.h"
#import "User.h"

#define NO_PICKER 0
#define DATE_PICKER 1


@interface PolisViewController () <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate> {
    User *currentUser;
    NSDateFormatter *formatter;
}

@property (nonatomic, strong) UIDatePicker* datePicker;
@property int activePicker;

@property (nonatomic) NSDate *selectedDate;
@property (nonatomic) UITextField *polisTextField;
@property (nonatomic) UITextField *dateTextField;
@property (nonatomic) UITextField *currentTextField;
@property (nonatomic) UITableView *tableView;

@end


@implementation PolisViewController

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:nil
                                                                            action:nil];
    
    if (self.isMovingToParentViewController) {
        if (self.forceAuth) {
            [self authButtonPressed];
        }
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    currentUser = [MainAPI defaultAPI].currentUser;
    formatter = [TeoriusHelper dateFormatterUsingFormat:@"dd.MM.yyyy"];

    [self configSubviews];
}

- (void)configSubviews {
    const float kRowHeight = 50.0f;
    const float kButtonHeight = 36.0f;
    const float kButtonFrameVerticalMargin = 20.0f;
    const float kButtonContainerHeight = kButtonHeight + 2*kButtonFrameVerticalMargin;
    
    CGRect controlFrame = [TeoriusHelper scaledRect:CGRectMake(LOGICAL_WIDTH/2+15, 5, 200, 40)];

    self.polisTextField = [[UITextField alloc] initWithFrame:controlFrame];
    self.polisTextField.font = [SharedClass getRegularFont];
    self.polisTextField.keyboardType = UIKeyboardTypeNumberPad;
    self.polisTextField.text = currentUser.polisNumber;
    self.polisTextField.delegate = self;
    
    self.dateTextField = [[UITextField alloc] initWithFrame:controlFrame];
    self.dateTextField.font = [SharedClass getRegularFont];
    self.dateTextField.tintColor = UIColorFromRGB(0xEAE3E1);
    self.dateTextField.delegate = self;
    
    self.datePicker = [UIDatePicker new];
    [self.datePicker setMaximumDate:[NSDate date]];
    self.datePicker.datePickerMode = UIDatePickerModeDate;
    self.datePicker.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"ru_RU"];
    [self.datePicker addTarget:self action:@selector(pickerChanged:) forControlEvents:UIControlEventValueChanged];
    
    if ([TeoriusHelper dateIsValid:currentUser.birthDay]) {
        self.datePicker.date = currentUser.birthDay;
    }
    else {
        [self.datePicker setDate:[[NSDate date] dateByAddingTimeInterval:-86400]];
    }
    
    self.dateTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[TeoriusHelper dateToString:self.datePicker.date] attributes:@{NSForegroundColorAttributeName: COLOR_LIGHTGRAY, NSFontAttributeName: [SharedClass getRegularFont]}];
    self.dateTextField.inputView = self.datePicker;
    self.dateTextField.text = [TeoriusHelper dateToString:self.datePicker.date];

    
    UITableView* tableView = [[UITableView alloc] initWithFrame:[TeoriusHelper scaledRect:CGRectMake(0, HEADER_HEIGHT, LOGICAL_WIDTH, LOGICAL_HEIGHT_SUB_HH)]];
    
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.rowHeight = [TeoriusHelper scaledFloat:kRowHeight];
    tableView.showsVerticalScrollIndicator = NO;
    //    tableView.scrollEnabled = NO;
    tableView.allowsSelection = NO;
    tableView.backgroundColor = UIColorFromRGB(0xEAE3E1);
    tableView.contentInset = UIEdgeInsetsMake(-HEADER_HEIGHT, 0, 0, 0);
    //    tableView.separatorInset = UIEdgeInsetsMake(0, 15, 0, 0);
    //    self.tableView.separatorColor = [UIColor clearColor];
    [self.view addSubview:tableView];
    
    UIView* greenButtonContainer = [[UIView alloc] initWithFrame:[TeoriusHelper scaledRect:CGRectMake(0, 0, SCREEN_WIDTH, kButtonContainerHeight)]];
    tableView.tableFooterView = greenButtonContainer;
    self.tableView = tableView;
    
    CGRect frame = [TeoriusHelper scaledRectFromCenter:CGPointMake(LOGICAL_WIDTH/2/* - (15+9)/2*/, kButtonHeight/2 + kButtonFrameVerticalMargin) size:CGSizeMake(220, 36)];
    UIButton* greenButton = [SharedClass greenBorderButtonWithFrame:frame text:@"Войти"];
    [greenButtonContainer addSubview:greenButton];
    
    [greenButton addTarget:self action:@selector(authButtonPressed) forControlEvents:UIControlEventTouchUpInside];
}

- (void)authButtonPressed {
    NSString *dateString = [formatter stringFromDate:self.datePicker.date];
    
    if (![self validateUserInfo]) {
        return;
    }
    
    [self.currentTextField resignFirstResponder];

    [[TeoriusHelper instance] addWaitingViewWithText:@"Авторизация" font:[SharedClass getRegularFontWithSize:14] toView:self.navigationController.view disablingInteractionEvents:YES];
    
    [[MainAPI defaultAPI] authWithPolisNumber:self.polisTextField.text birthDate:dateString completion:^(BOOL success, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[TeoriusHelper instance] removeWaitingView];
            
            currentUser.polisNumber = self.polisTextField.text;
            currentUser.birthDay = self.datePicker.date;
            [MainAPI defaultAPI].currentUser = currentUser;

            if (success) {
                AppointmentsListViewController *vc = [[AppointmentsListViewController alloc] initWithTitle:@"Список записей" menuButtonVisible:NO];
                [self.navigationController pushViewController:vc animated:YES];
            }
            else {
                [TeoriusHelper showStandartAlertInMainThreadWithTitle:@"Ошибка" body:error.localizedDescription];
            }
        });
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray* cellTitles = @[@"Номер", @"Дата рождения"];
    
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"Cell"];
    cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0];
    
    UILabel* firstLabel = [[UILabel alloc] initWithFrame:[TeoriusHelper scaledRect:CGRectMake(20+15, 5, 150, 40)]];
    firstLabel.text = cellTitles[indexPath.row];
    firstLabel.font = [SharedClass getRegularFont];
    [cell addSubview:firstLabel];
    
    if (indexPath.row == 0) {
        [cell addSubview:self.polisTextField];
    }
    else if (indexPath.row == 1) {
        [cell addSubview:self.dateTextField];
        cell.separatorInset = UIEdgeInsetsMake(0.f, cell.bounds.size.width, 0.f, 0.f);
    }
    return cell;
}

#pragma mark - Other

- (BOOL)validateUserInfo {
    if (!self.polisTextField.hasText || !self.dateTextField.hasText) {
        [TeoriusHelper showStandartAlertInMainThreadWithTitle:@"Ошибка" body:@"Введите номер полиса и дату"];
        return NO;
    }
    
    return YES;
}

#define ACCEPTABLE_CHARECTERS @"0123456789"

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField == self.dateTextField) {
        return YES;
    }
    
    int maxLength = 16;
    
    if(range.length + range.location > textField.text.length)
        return NO;
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    if (maxLength < newLength)
        return NO;
    
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARECTERS] invertedSet];
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return [string isEqualToString:filtered];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    self.currentTextField = textField;
    
    return YES;
}

- (void)pickerChanged:(id)sender {
    self.dateTextField.text = [TeoriusHelper dateToString:[sender date]];
    
    self.dateTextField.selectedTextRange = nil;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsMake(0, 30, 0, 0)];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsMake(0, 30, 0, 0)];
    }
}

@end
