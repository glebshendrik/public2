//
//  PassportTableViewController.m
//  Public
//
//  Created by Teoria 5 on 10/09/15.
//  Copyright (c) 2015 Teoria. All rights reserved.
//

#import "BirthCertificateViewController.h"
#import "SharedClass.h"
#import "AppointmentsListViewController.h"

#define NO_PICKER 0
#define DATE_PICKER 1


@interface BirthCertificateViewController () <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>

@property (nonatomic, strong) UIDatePicker* datePicker;
@property int activePicker;

@property (nonatomic, strong) UILabel* dateLabel;
@property (nonatomic, strong) NSDate* selectedDate;

@property (nonatomic, strong) UITextField* seriaTextField;
@property (nonatomic, strong) UITextField* numberTextField;

@end


@implementation BirthCertificateViewController

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:nil
                                                                            action:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"ru_RU"]];
    self.selectedDate = [dateFormatter dateFromString: @"1983-12-05"];

    [self configSubviews];
}

- (void)configSubviews {
    const float kRowHeight = 50.0f;
    const float kButtonHeight = 36.0f;
    const float kButtonFrameVerticalMargin = 20.0f;
    const float kButtonContainerHeight = kButtonHeight + 2*kButtonFrameVerticalMargin;
    
    UITableView* tableView = [[UITableView alloc] initWithFrame:[TeoriusHelper scaledRect:CGRectMake(0, HEADER_HEIGHT, LOGICAL_WIDTH, LOGICAL_HEIGHT_SUB_HH)]];
    
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.rowHeight = [TeoriusHelper scaledFloat:kRowHeight];
    tableView.showsVerticalScrollIndicator = NO;
//    tableView.scrollEnabled = NO;
    tableView.allowsSelection = NO;
    tableView.backgroundColor = UIColorFromRGB(0xEAE3E1);
    tableView.contentInset = UIEdgeInsetsMake(-HEADER_HEIGHT, 0, 0, 0);
//    tableView.separatorInset = UIEdgeInsetsMake(0, 15, 0, 0);
//    self.tableView.separatorColor = [UIColor clearColor];
    [self.view addSubview:tableView];
    
    UIView* greenButtonContainer = [[UIView alloc] initWithFrame:[TeoriusHelper scaledRect:CGRectMake(0, 0, SCREEN_WIDTH, kButtonContainerHeight)]];
    tableView.tableFooterView = greenButtonContainer;
    
    CGRect frame = [TeoriusHelper scaledRectFromCenter:CGPointMake(LOGICAL_WIDTH/2/* - (15+9)/2*/, kButtonHeight/2 + kButtonFrameVerticalMargin) size:CGSizeMake(220, 36)];
    UIButton* greenButton = [SharedClass greenBorderButtonWithFrame:frame text:@"Войти"];
    [greenButtonContainer addSubview:greenButton];
    
    [greenButton addTarget:self action:@selector(buttonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)buttonTouchUpInside:(id)sender {
    UIViewController* vc = [[AppointmentsListViewController alloc] initWithTitle:@"Список записей" menuButtonVisible:NO];
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray* cellTitles = @[@"Серия", @"Номер", @"Дата рождения"];
    
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"Cell"];
    cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0];
    
    UILabel* firstLabel = [[UILabel alloc] initWithFrame:[TeoriusHelper scaledRect:CGRectMake(20+15, 5, 150, 40)]];
    firstLabel.text = cellTitles[indexPath.row];
    firstLabel.font = [SharedClass getRegularFont];
//    firstLabel.textColor = UIColorFromRGB(0xB3B3B3);
    [cell addSubview:firstLabel];
    
    CGRect controlFrame = [TeoriusHelper scaledRect:CGRectMake(LOGICAL_WIDTH/2+15, 5, 200, 40)];
    
    if (indexPath.row == 0 || indexPath.row == 1) {
        UITextField* textField = [[UITextField alloc] initWithFrame:controlFrame];
        textField.font = [SharedClass getRegularFont];
//        textField.textColor = UIColorFromRGB(0x);
        [cell addSubview:textField];
                
        switch (indexPath.row) {
            case 0:
                self.seriaTextField = textField;
                textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"VII-ЛЛ" attributes:@{NSForegroundColorAttributeName: COLOR_LIGHTGRAY, NSFontAttributeName: [SharedClass getRegularFont]}];
                textField.delegate = self;
                break;
            case 1:
                self.numberTextField = textField;
                textField.keyboardType = UIKeyboardTypeNumberPad;
                textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"●●●●●●" attributes:@{NSForegroundColorAttributeName: COLOR_LIGHTGRAY, NSFontAttributeName: [SharedClass getRegularFont]}];
                textField.secureTextEntry = YES;
                textField.delegate = self;
                break;
        }
    } else if (indexPath.row == 2) {
        UILabel* dateLabel = [[UILabel alloc] initWithFrame:controlFrame];
        dateLabel.font = [SharedClass getRegularFont];
        dateLabel.textColor = COLOR_LIGHTGRAY;
        [cell addSubview:dateLabel];
        
//        dateLabel.text = @"05.12.1983";
        dateLabel.text = [TeoriusHelper dateToString:self.selectedDate];
        dateLabel.userInteractionEnabled = YES;
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dateLabelTapped:)];
        [dateLabel addGestureRecognizer:tapGesture];
        
        self.view.userInteractionEnabled = YES;
        UITapGestureRecognizer *viewTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped:)];
        [self.view addGestureRecognizer:viewTapGesture];
        
        self.dateLabel = dateLabel;
        
        cell.separatorInset = UIEdgeInsetsMake(0.f, cell.bounds.size.width, 0.f, 0.f);
    }
    
    return cell;
}

#define ACCEPTABLE_NUMERIC_CHARECTERS @"0123456789"
#define ACCEPTABLE_ROMAN_NUMBER_CHARECTERS @"IVXLCDM"

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    int maxLength = 7;
    if (textField == self.numberTextField)
        maxLength = 6;
    
    if(range.length + range.location > textField.text.length)
        return NO;
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    if (maxLength < newLength)
        return NO;
    
    if (range.length == 1)
        return YES;
    
    if (string.length != 1)
        return NO;
    
    if (textField == self.seriaTextField) {
        string = [string uppercaseString];
        NSError *error = NULL;
        NSRegularExpression *regexRomanNumbers = [NSRegularExpression regularExpressionWithPattern:@"([IVXLCDM])"
                                                                                           options:NSRegularExpressionCaseInsensitive
                                                                                             error:&error];
        NSRegularExpression *regexRomanNumbersDash = [NSRegularExpression regularExpressionWithPattern:@"([IVXLCDM-])"
                                                                                  options:NSRegularExpressionCaseInsensitive
                                                                                      error:&error];
        NSRegularExpression *regexCyrillic = [NSRegularExpression regularExpressionWithPattern:@"([а-яА-Я])"
                                                                                       options:NSRegularExpressionCaseInsensitive
                                                                                         error:&error];
        NSInteger dashPos = [textField.text rangeOfString:@"-" options:NSBackwardsSearch].location;
        if (dashPos >= 7) {
            if (4 > [regexRomanNumbers numberOfMatchesInString:textField.text options:0 range:NSMakeRange(0, textField.text.length)]) {
                if (1 == [regexRomanNumbersDash numberOfMatchesInString:string options:0 range:NSMakeRange(0, string.length)])
                    textField.text = [textField.text stringByAppendingString:string];
            } else if ([string isEqualToString:@"-"])
                textField.text = [textField.text stringByAppendingString:string];
        } else {
            if (2 > [regexCyrillic numberOfMatchesInString:textField.text options:0 range:NSMakeRange(0, textField.text.length)])
                if (1 == [regexCyrillic numberOfMatchesInString:string options:0 range:NSMakeRange(0, string.length)])
                    textField.text = [textField.text stringByAppendingString:string];
        }
    } else if (textField == self.numberTextField) {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_NUMERIC_CHARECTERS] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        return [string isEqualToString:filtered];
    }
    return NO;
}

- (void)dateLabelTapped:(id)sender {
    [self.view endEditing:YES];
    
    if (self.activePicker == DATE_PICKER)
        return;
    self.activePicker = DATE_PICKER;
    
    UIDatePicker* datePicker = [self getDatePicker];
    datePicker.frame = [TeoriusHelper scaledRect:CGRectMake(0, LOGICAL_HEIGHT, LOGICAL_WIDTH, DATEPICKER_HEIGHT)];
    self.datePicker.alpha = 0;
    [self.datePicker setMaximumDate:[NSDate date]];
    [self.datePicker setDate:self.selectedDate];
    
    [UIView animateWithDuration:DATEPICKER_ANIM_DURATION animations:^{
        datePicker.frame = [TeoriusHelper scaledRect:CGRectMake(0, LOGICAL_HEIGHT-DATEPICKER_HEIGHT, LOGICAL_WIDTH, DATEPICKER_HEIGHT)];
        self.datePicker.alpha = 1;
    }];
}

- (void)viewTapped:(id)sender {
    if (self.activePicker != NO_PICKER) {
        self.activePicker = NO_PICKER;
        
        UIDatePicker* datePicker = [self getDatePicker];
        
        [UIView animateWithDuration:DATEPICKER_ANIM_DURATION animations:^{
            datePicker.frame = [TeoriusHelper scaledRect:CGRectMake(0, LOGICAL_HEIGHT, LOGICAL_WIDTH, DATEPICKER_HEIGHT)];
            self.datePicker.alpha = 0;
        }];
    }
}

- (UIDatePicker*)getDatePicker {
    if (self.datePicker != NULL)
        return self.datePicker;
    
    CGRect pickerFrame = [TeoriusHelper scaledRect:CGRectMake(0, LOGICAL_HEIGHT, LOGICAL_WIDTH, DATEPICKER_HEIGHT)];
    
    self.datePicker = [[UIDatePicker alloc] initWithFrame:pickerFrame];
    self.datePicker.backgroundColor = [UIColor whiteColor];
    self.datePicker.alpha = 0;
    self.datePicker.datePickerMode = UIDatePickerModeDate;
    self.datePicker.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"ru_RU"];
    [self.datePicker addTarget:self action:@selector(pickerChanged:) forControlEvents:UIControlEventValueChanged];
    
    [self.view addSubview:self.datePicker];
    
    return self.datePicker;
}

- (void)pickerChanged:(id)sender {
    self.dateLabel.text = [TeoriusHelper dateToString:[sender date]];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsMake(0, 30, 0, 0)];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsMake(0, 30, 0, 0)];
    }
}

@end
