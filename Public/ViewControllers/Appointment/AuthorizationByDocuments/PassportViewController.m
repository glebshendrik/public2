//
//  PassportTableViewController.m
//  Public
//
//  Created by Teoria 5 on 10/09/15.
//  Copyright (c) 2015 Teoria. All rights reserved.
//

#import "PassportViewController.h"
#import "SharedClass.h"
#import "AppointmentsListViewController.h"
#import "MainAPI.h"
#import "User.h"

#define NO_PICKER 0
#define DATE_PICKER 1


@interface PassportViewController () <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate> {
    User *currentUser;
    NSDateFormatter *formatter;
}

@property (nonatomic, strong) UIDatePicker* datePicker;
@property int activePicker;

@property (nonatomic, strong) UITextField* seriaTextField;
@property (nonatomic, strong) UITextField* numberTextField;
@property (nonatomic, strong) UITextField* dateTextField;
@property (nonatomic) UITextField *currentTextField;

@end


@implementation PassportViewController

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:nil
                                                                            action:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    currentUser = [MainAPI defaultAPI].currentUser;
    formatter = [TeoriusHelper dateFormatterUsingFormat:@"dd.MM.yyyy"];
    [self configSubviews];
}

- (void)configSubviews {
    const float kRowHeight = 50.0f;
    const float kButtonHeight = 36.0f;
    const float kButtonFrameVerticalMargin = 20.0f;
    const float kButtonContainerHeight = kButtonHeight + 2*kButtonFrameVerticalMargin;
    
    UITableView* tableView = [[UITableView alloc] initWithFrame:[TeoriusHelper scaledRect:CGRectMake(0, HEADER_HEIGHT, LOGICAL_WIDTH, LOGICAL_HEIGHT_SUB_HH)]];
    
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.rowHeight = [TeoriusHelper scaledFloat:kRowHeight];
    tableView.showsVerticalScrollIndicator = NO;
    tableView.allowsSelection = NO;
    tableView.backgroundColor = UIColorFromRGB(0xEAE3E1);
    tableView.contentInset = UIEdgeInsetsMake(-HEADER_HEIGHT, 0, 0, 0);
    [self.view addSubview:tableView];
    
    UIView* greenButtonContainer = [[UIView alloc] initWithFrame:[TeoriusHelper scaledRect:CGRectMake(0, 0, SCREEN_WIDTH, kButtonContainerHeight)]];
    tableView.tableFooterView = greenButtonContainer;
    
    CGRect frame = [TeoriusHelper scaledRectFromCenter:CGPointMake(LOGICAL_WIDTH/2/* - (15+9)/2*/, kButtonHeight/2 + kButtonFrameVerticalMargin) size:CGSizeMake(220, 36)];
    UIButton* greenButton = [SharedClass greenBorderButtonWithFrame:frame text:@"Войти"];
    [greenButtonContainer addSubview:greenButton];
    
    [greenButton addTarget:self action:@selector(buttonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)buttonTouchUpInside:(id)sender {
    NSString *dateString = [formatter stringFromDate:self.datePicker.date];
    if (![self validateUserInfoWithPassportValidation:YES]) {
        return;
    }
    
    [self.currentTextField resignFirstResponder];
    
    [[TeoriusHelper instance] addWaitingViewWithText:@"Авторизация" font: [SharedClass getRegularFontWithSize:14] toView:self.navigationController.view disablingInteractionEvents: YES];
    
    [[MainAPI defaultAPI] authWithPassportNumber:self.numberTextField.text passportSeries:self.seriaTextField.text birthDate:dateString completion:^(BOOL success, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[TeoriusHelper instance] removeWaitingView];
            
            currentUser.passportNumber = self.numberTextField.text;
            currentUser.passportSeries = self.seriaTextField.text;
            currentUser.birthDay = self.datePicker.date;
            [MainAPI defaultAPI].currentUser = currentUser;

            if (success) {
                UIViewController* vc = [[AppointmentsListViewController alloc] initWithTitle:@"Список записей" menuButtonVisible:NO];
                [self.navigationController pushViewController:vc animated:YES];
            }
            else {
                [TeoriusHelper showStandartAlertInMainThreadWithTitle:@"Ошибка" body:error.localizedDescription];
            }
        });
    }];
}


#pragma mark - Other

- (BOOL)validateUserInfoWithPassportValidation:(BOOL)withPasswordValidation {
    if (withPasswordValidation) {
        if (!self.numberTextField.hasText || !self.seriaTextField.hasText) {
            [TeoriusHelper showStandartAlertInMainThreadWithTitle:@"Ошибка" body:@"Введите серию и номер"];
            return NO;
        }
    }
    
    return YES;
}


#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray* cellTitles = @[@"Серия", @"Номер", @"Дата рождения"];
    
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"Cell"];
    cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0];
    
    UILabel* firstLabel = [[UILabel alloc] initWithFrame:[TeoriusHelper scaledRect:CGRectMake(20+15, 5, 150, 40)]];
    firstLabel.text = cellTitles[indexPath.row];
    firstLabel.font = [SharedClass getRegularFont];
    [cell addSubview:firstLabel];
    
    CGRect controlFrame = [TeoriusHelper scaledRect:CGRectMake(LOGICAL_WIDTH/2+15, 5, 200, 40)];
    
    if (indexPath.row == 0 || indexPath.row == 1) {
        UITextField* textField = [[UITextField alloc] initWithFrame:controlFrame];
        textField.font = [SharedClass getRegularFont];
        textField.keyboardType = UIKeyboardTypeNumberPad;
        [cell addSubview:textField];
                
        switch (indexPath.row) {
            case 0:
                self.seriaTextField = textField;
                textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"" attributes:@{NSForegroundColorAttributeName: COLOR_LIGHTGRAY, NSFontAttributeName: [SharedClass getRegularFont]}];
                textField.delegate = self;
                
                textField.text = currentUser.passportSeries;
                
                break;
            case 1:
                self.numberTextField = textField;
                textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"●●●●●●" attributes:@{NSForegroundColorAttributeName: COLOR_LIGHTGRAY, NSFontAttributeName: [SharedClass getRegularFont]}];
                textField.secureTextEntry = YES;
                textField.delegate = self;
                textField.text = currentUser.passportNumber;
                break;
        }
    }
    else if (indexPath.row == 2) {
        self.dateTextField = [[UITextField alloc] initWithFrame:controlFrame];
        self.dateTextField.font = [SharedClass getRegularFont];
        self.dateTextField.tintColor = UIColorFromRGB(0xEAE3E1);
        self.dateTextField.delegate = self;
        
        [cell addSubview:self.dateTextField];
        
        self.datePicker = [UIDatePicker new];
        [self.datePicker setMaximumDate:[NSDate date]];
        self.datePicker.datePickerMode = UIDatePickerModeDate;
        self.datePicker.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"ru_RU"];
        [self.datePicker addTarget:self action:@selector(pickerChanged:) forControlEvents:UIControlEventValueChanged];
        
        if ([TeoriusHelper dateIsValid:currentUser.birthDay]) {
            self.datePicker.date = currentUser.birthDay;
        }
        else {
            [self.datePicker setDate:[[NSDate date] dateByAddingTimeInterval:-86400]];
        }

        self.dateTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[TeoriusHelper dateToString:self.datePicker.date] attributes:@{NSForegroundColorAttributeName: COLOR_LIGHTGRAY, NSFontAttributeName: [SharedClass getRegularFont]}];
        self.dateTextField.inputView = self.datePicker;
        
        self.dateTextField.text = [TeoriusHelper dateToString:self.datePicker.date];
        cell.separatorInset = UIEdgeInsetsMake(0.f, cell.bounds.size.width, 0.f, 0.f);
    }
    
    return cell;
}


#define ACCEPTABLE_CHARECTERS @"0123456789"

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField == self.dateTextField) {
        return YES;
    }

    int maxLength = 4;
    if (textField == self.numberTextField)
        maxLength = 6;
    
    if(range.length + range.location > textField.text.length)
        return NO;
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    if (maxLength < newLength)
        return NO;
    
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARECTERS] invertedSet];
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return [string isEqualToString:filtered];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    self.currentTextField = textField;
    
    return YES;
}

- (void)pickerChanged:(id)sender {
    self.dateTextField.text = [TeoriusHelper dateToString:[sender date]];
    
    self.dateTextField.selectedTextRange = nil;
    
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsMake(0, 30, 0, 0)];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsMake(0, 30, 0, 0)];
    }
}

@end
