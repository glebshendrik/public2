//
//  SpecialitiesListViewController.m
//  Public
//
//  Created by Arslan Zagidullin on 28/09/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import "ServicesListViewController.h"
#import "MainAPI.h"
#import "TeoriusHelper+Extension.h"
#import "SharedClass.h"
#import <Realm/Realm.h>
#import "Service.h"
#import "ClinicsAndResourcesViewController.h"
#import "AppointmentHelper.h"
#import "OneLongLabelCell.h"

@interface ServicesListViewController ()

@property (nonatomic) UISearchDisplayController* searchDisplayController;
@property (nonatomic) NSArray *servicesArray;

@end


@implementation ServicesListViewController

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:nil
                                                                            action:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBar.barTintColor = UIColorFromRGB(0xE9E3E2);
    self.navigationController.navigationBar.translucent = NO;
    
    [[TeoriusHelper instance] addWaitingViewWithText:@"Получение списка услуг" font:[SharedClass getRegularFontWithSize:14] toView:self.navigationController.view disablingInteractionEvents:YES];
    
    [[MainAPI defaultAPI] getResourcesForClinicID:self.selectedClinic.ID completion:^(NSArray *arr, NSError *err) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[TeoriusHelper instance] removeWaitingView];
            
            if (err) {
                [TeoriusHelper showStandartAlertInMainThreadWithTitle:@"Ошибка" body:err.localizedDescription];
            }
            else {
                self.servicesArray = arr;
                [self.tableView reloadData];
            }
        });
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.servicesArray.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    const float kLabelOffset = 15.0f;
//    const float kLabelWidth = 250.0f;
    
    Service *service = self.servicesArray[indexPath.row];

    OneLongLabelCell *cell = [[OneLongLabelCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"SpecialityCell"];
    cell.longTextLabel.font = [SharedClass getRegularFontWithSize:15.0f];
    cell.longTextLabel.textColor = COLOR_LIGHTERGREEN;
    cell.longTextLabel.text = service.name;
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    Service *service = self.servicesArray[indexPath.row];

    CGFloat h1 = [OneLongLabelCell heightOfLabelWithText:service.name font:[SharedClass getRegularFontWithSize:15.0f] viewWidth:self.view.width - 34.f];
    CGFloat h2 = [OneLongLabelCell additionalHeight];
    
    return h1 + h2;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Service *service = self.servicesArray[indexPath.row];
    
    [AppointmentHelper instance].service = service;

    [TeoriusHelper tableViewDeselectSelectedCell:tableView];
    ClinicsAndResourcesViewController* vc = [[ClinicsAndResourcesViewController alloc] init];
    vc.currentMode = MODE_SPECIALISTS_OF_PARTICULAR_CLINIC;
    vc.selectedService = service;
//    vc.selectedClinic = self.selectedClinic;
//    vc.selectedSpeciality = self.specialityArray[indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

@end
