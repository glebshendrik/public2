//
//  ClinicsAndSpecialistsViewController.h
//  Public
//
//  Created by Teoria 5 on 21/09/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import <UIKit/UIKit.h>

#define TABLE_CLINICS 0
#define TABLE_SPECIALISTS 1

#define MODE_SEGMENTED_CONTROL 0
#define MODE_SPECIALISTS_OF_PARTICULAR_CLINIC 1

@class Service;

@interface ClinicsAndResourcesViewController : UITableViewController

@property (nonatomic) int activeTable;
@property (nonatomic) int currentMode;

@property (nonatomic, strong) Service *selectedService;

@end
