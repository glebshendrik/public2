//
//  ConfirmationViewController.m
//  Public
//
//  Created by Arslan Zagidullin on 17/09/15.
//  Copyright (c) 2015 Teoria. All rights reserved.
//

#import "ConfirmationViewController.h"
#import "TeoriusHelper+Extension.h"
#import "SharedClass.h"
#import "StandardScrollView.h"
#import "AppointmentHelper.h"
#import "Clinic.h"
#import "Resource.h"
#import "Service.h"
#import "TimeInterval.h"
#import "User.h"
#import "MainAPI.h"
#import "AppointmentsListViewController.h"

@interface ConfirmationViewController () {
    AppointmentHelper *appointmentsHelper;
}

@end


@implementation ConfirmationViewController

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:nil
                                                                            action:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    appointmentsHelper = [AppointmentHelper instance];
    
    [self configSubviews];
}

- (void)configSubviews {
    UIScrollView* mainScrollView = [[StandardScrollView alloc] initWithParentVeiwFrame:self.view.frame];
    [self.view addSubview:mainScrollView];
    
    const float kLabelLeftMargin = 60;
    
    User *user = [MainAPI defaultAPI].currentUser;
    NSString *fioString = [NSString  stringWithFormat:@"%@ %@ %@", user.surname, user.name, user.patronymic];
    
    CGRect labelFrame = [TeoriusHelper scaledRectFromCenter:CGPointMake(LOGICAL_WIDTH/2, 130 - HEADER_HEIGHT) size:CGSizeMake(LOGICAL_WIDTH-60, 140)];
    
    NSMutableParagraphStyle *parStyle = [NSMutableParagraphStyle new];
    parStyle.lineHeightMultiple = 1.3f;
    
    NSAttributedString *attrText = [[NSAttributedString alloc] initWithString:/*[NSString  stringWithFormat:@"Подтвердите запись: %@", fioString]*/fioString attributes:@{NSParagraphStyleAttributeName: parStyle, NSFontAttributeName: [SharedClass getRegularFontWithSize:15.0f], NSForegroundColorAttributeName: COLOR_DARKGRAY}];
    
    UILabel* textLabel = [[UILabel alloc] initWithFrame:labelFrame];
    textLabel.attributedText = attrText;
    textLabel.numberOfLines = 2;
    textLabel.textAlignment = NSTextAlignmentCenter;
    //    [nameLabel sizeToFit];
    [mainScrollView addSubview:textLabel];
    
    NSArray* buttonTitles = @[@"Клиника", @"Врач", @"Время приема"];
    
    float lastY = 210.0f - HEADER_HEIGHT;
    
    for (int row = 0; row<3; row++) {
        NSMutableParagraphStyle *parStyle = [NSMutableParagraphStyle new];
        parStyle.lineHeightMultiple = 1.3f;
        
        NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:buttonTitles[row] attributes:@{NSParagraphStyleAttributeName: parStyle, NSFontAttributeName: [SharedClass getRegularFontWithSize:10.0f], NSForegroundColorAttributeName: COLOR_DARKGRAY}];
        
        UILabel* firstLabel = [[UILabel alloc] initWithFrame:[TeoriusHelper scaledRect:CGRectMake(kLabelLeftMargin, lastY, LOGICAL_WIDTH-2*kLabelLeftMargin, 0)]];
        firstLabel.attributedText = attrString;
        [firstLabel sizeToFit];
        [mainScrollView addSubview:firstLabel];
        
        lastY = CGRectGetMaxY(firstLabel.frame) / SCREEN_SCALE;
        
        NSString* secondLabelText;
        switch (row) {
            case 0:
                secondLabelText = appointmentsHelper.clinic.name;
                break;
            case 1:
                secondLabelText = appointmentsHelper.resource.fio;
                break;
            case 2:
                secondLabelText = appointmentsHelper.dateAndTimeString;
                break;
        }
        
        attrString = [[NSAttributedString alloc] initWithString:secondLabelText attributes:@{NSParagraphStyleAttributeName: parStyle, NSFontAttributeName: [SharedClass getRegularFontWithSize:14.0f], NSForegroundColorAttributeName: COLOR_DARKGRAY}];
        
        UILabel* secondLabel = [[UILabel alloc] initWithFrame:[TeoriusHelper scaledRect:CGRectMake(kLabelLeftMargin, lastY, LOGICAL_WIDTH-2*kLabelLeftMargin, 0)]];
        secondLabel.numberOfLines = 0;
        secondLabel.attributedText = attrString;
        //        nameLabel.font = [SharedClass getRegularFontWithSize:14.0f];
        //        nameLabel.textColor = UIColorFromRGB(0x666666);
        [secondLabel sizeToFit];
        [mainScrollView addSubview:secondLabel];
        
        lastY = (CGRectGetMaxY(secondLabel.frame) + 15.0f) / SCREEN_SCALE;
        
        CGFloat buttonY = (IS_IPHONE_4_OR_LESS ? LOGICAL_HEIGHT-45 : LOGICAL_HEIGHT-75);
        buttonY -= HEADER_HEIGHT;
        
        CGRect frame = [TeoriusHelper scaledRectFromCenter:CGPointMake(LOGICAL_WIDTH/2, buttonY) size:CGSizeMake(130, 40)];
        UIButton* gb = [SharedClass pinkBorderButtonWithFrame:frame text:@"Подтверждаю"];
        [mainScrollView addSubview:gb];
        
        [gb addTarget:self action:@selector(buttonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
    }
}

- (void)buttonTouchUpInside:(id)sender {
    [[TeoriusHelper instance] addWaitingViewWithText:@"Подождите" font:[SharedClass getRegularFontWithSize:14] toView:self.navigationController.view disablingInteractionEvents:YES];
    
    void (^completion)(BOOL success, NSError *error) = ^(BOOL success, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[TeoriusHelper instance] removeWaitingView];
            
            if (success) {
                [TeoriusHelper showStandartAlertInMainThreadWithTitle:@"Сообщение" body:@"Вы успешно записаны"];
                
                AppointmentsListViewController *vc = nil;
                for (UIViewController *vc1 in self.navigationController.viewControllers) {
                    if ([vc1 isKindOfClass:[AppointmentsListViewController class]]) {
                        vc = (AppointmentsListViewController *)vc1;
                    }
                }
                
                if (vc) {
                    [AppointmentHelper instance].needToUpdateList = YES;
                    [self.navigationController popToViewController:vc animated:YES];
                }
            }
            else {
                [TeoriusHelper showStandartAlertInMainThreadWithTitle:@"Ошибка" body:error.localizedDescription];
            }
        });
    };
    
    if ([TeoriusHelper stringIsValid:appointmentsHelper.phone]) {
        [[MainAPI defaultAPI] createAppointmentForClinicId:appointmentsHelper.clinic.ID ServiceId:appointmentsHelper.service.ID resourceId:appointmentsHelper.resource.ID year:appointmentsHelper.year month:appointmentsHelper.month day:appointmentsHelper.day timeIntervalId:appointmentsHelper.timeInterval.ID phone:appointmentsHelper.phone completion:completion];
    }
    else {
        [[MainAPI defaultAPI] createAppointmentForClinicId:appointmentsHelper.clinic.ID ServiceId:appointmentsHelper.service.ID resourceId:appointmentsHelper.resource.ID year:appointmentsHelper.year month:appointmentsHelper.month day:appointmentsHelper.day timeIntervalId:appointmentsHelper.timeInterval.ID completion:completion];
    }
}

@end
