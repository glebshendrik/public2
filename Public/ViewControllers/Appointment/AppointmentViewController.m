//
//  AppointmentViewController.m
//  Public
//
//  Created by Teoria 5 on 04/09/15.
//  Copyright (c) 2015 Teoria. All rights reserved.
//

#import "AppointmentViewController.h"
#import "SharedClass.h"
#import "TeoriusHelper+Extension.h"
#import "PassportViewController.h"
#import "StandardScrollView.h"
#import "MainAPI.h"
#import "AppointmentsListViewController.h"
#import "AppointmentHelper.h"

@interface AppointmentViewController ()

@end


@implementation AppointmentViewController

#pragma mark - View's lifecycle

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:nil
                                                                            action:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.appointment == NULL)
        return;
    
    [self configSubviews];
}

- (void)configSubviews {
    UIScrollView* mainScrollView = [[StandardScrollView alloc] initWithParentVeiwFrame:self.view.frame];
    [self.view addSubview:mainScrollView];
    
    CGRect viewRect = [TeoriusHelper scaledRect:CGRectMake(0, 0, LOGICAL_WIDTH, 250.0f)];
    UIView* view = [[UIView alloc] initWithFrame:viewRect];
    
    view.backgroundColor = [UIColor whiteColor];
    [mainScrollView addSubview:view];
    
    NSArray* buttonTitles = @[@"Профиль", @"ФИО", @"Дата", @"Клиника"];
    
    float lastY = 95.0f - HEADER_HEIGHT;
    
    for (int row = 0; row < buttonTitles.count; row++) {
        NSMutableParagraphStyle *parStyle = [NSMutableParagraphStyle new];
        parStyle.lineHeightMultiple = 1.3f;
        
        NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:buttonTitles[row] attributes:@{NSParagraphStyleAttributeName: parStyle, NSFontAttributeName: [SharedClass getRegularFontWithSize:14.0f], NSForegroundColorAttributeName: COLOR_LIGHTERGRAY}];
        
        UILabel *firstLabel = [[UILabel alloc] initWithFrame:[TeoriusHelper scaledRect:CGRectMake(30, lastY, 150, 0)]];
        firstLabel.attributedText = attrString;
        [firstLabel sizeToFit];
        [mainScrollView addSubview:firstLabel];
        
        NSString* secondLabelText;
        switch (row) {
            case 0:
                secondLabelText = self.appointment.resourceSpeciality;
                break;
            case 1:
                secondLabelText = self.appointment.resourceFio;
                break;
            case 2:
                secondLabelText = [TeoriusHelper dateToString:self.appointment.date usingFormat:@"d MMMM yyyy, hh:mm"];
                break;
            case 3:
                secondLabelText = self.appointment.clinicShortName;
                break;
        }
        
        if (!secondLabelText) {
            secondLabelText = @"";
        }
        
        attrString = [[NSAttributedString alloc] initWithString:secondLabelText attributes:@{NSParagraphStyleAttributeName: parStyle, NSFontAttributeName: [SharedClass getRegularFontWithSize:14.0f], NSForegroundColorAttributeName: COLOR_DARKGRAY}];
        
        UILabel *secondLabel = [[UILabel alloc] initWithFrame:[TeoriusHelper scaledRect:CGRectMake(LOGICAL_WIDTH-170-30, lastY, 170, 0)]];
        secondLabel.numberOfLines = 0;
        secondLabel.attributedText = attrString;
        //        nameLabel.font = [SharedClass getRegularFontWithSize:14.0f];
        //        nameLabel.textColor = UIColorFromRGB(0x666666);
        [secondLabel sizeToFit];
        [mainScrollView addSubview:secondLabel];
        
        lastY = MAX(CGRectGetMaxY(secondLabel.frame), CGRectGetMaxY(firstLabel.frame)) + 10.0f;
    }
    
    viewRect.size.height = lastY + 30.0f;
    view.frame = viewRect;
    
    CGRect frame = [TeoriusHelper scaledRectFromCenter:CGPointMake(LOGICAL_WIDTH/2, lastY + 70.0f) size:CGSizeMake(220, 36)];
    UIButton* appointmentDeleteButton = [SharedClass greenBorderButtonWithFrame:frame text:@"Отменить запись"];
    [mainScrollView addSubview:appointmentDeleteButton];
    
    [appointmentDeleteButton addTarget:self action:@selector(DeleteButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)DeleteButtonTouchUpInside:(id)sender {
    [[TeoriusHelper instance] addWaitingViewWithText:@"Подождите" font:[SharedClass getRegularFontWithSize:14] toView:self.navigationController.view disablingInteractionEvents:YES];
    
    [[MainAPI defaultAPI] deleteAppointmentWithId:self.appointment.ID completion:^(BOOL success, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[TeoriusHelper instance] removeWaitingView];
            
            if (error) {
                [TeoriusHelper showStandartAlertInMainThreadWithTitle:@"Ошибка" body:error.localizedDescription];
            }
            else {
                [TeoriusHelper showStandartAlertInMainThreadWithTitle:@"Сообщение" body:@"Запись на прием отменена"];

                AppointmentsListViewController *vc = nil;
                for (UIViewController *vc1 in self.navigationController.viewControllers) {
                    if ([vc1 isKindOfClass:[AppointmentsListViewController class]]) {
                        vc = (AppointmentsListViewController *)vc1;
                    }
                }
                
                if (vc) {
                    [AppointmentHelper instance].needToUpdateList = YES;
                    [self.navigationController popToViewController:vc animated:YES];
                }
            }
        });
    }];
}

@end


