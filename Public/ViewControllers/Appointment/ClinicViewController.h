//
//  ClinicViewController.h
//  Public
//
//  Created by Arslan Zagidullin on 15/09/15.
//  Copyright (c) 2015 Teoria. All rights reserved.
//

#import "HeaderViewController.h"

@class Clinic;

@interface ClinicViewController : HeaderViewController

@property (nonatomic) Clinic *clinic;

@end
