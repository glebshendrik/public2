//
//  SpecialistViewController.h
//  Public
//
//  Created by Arslan Zagidullin on 17/09/15.
//  Copyright (c) 2015 Teoria. All rights reserved.
//

#import "HeaderViewController.h"
#import "SpecialistRLM.h"

@interface SpecialistViewController : HeaderViewController

@property (nonatomic, strong) SpecialistRLM* specialist;

@end
