//
//  AppointmentDateAndTimeViewController.h
//  Public
//
//  Created by Arslan Zagidullin on 15/09/15.
//  Copyright (c) 2015 Teoria. All rights reserved.
//

#import "HeaderViewController.h"
#import "Resource.h"

@interface AppointmentDateAndTimeViewController : HeaderViewController

@property (nonatomic) Resource *resource;

@end
