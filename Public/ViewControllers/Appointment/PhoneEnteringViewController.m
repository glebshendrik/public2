//
//  PhoneEnteringViewController.m
//  Public
//
//  Created by Arslan Zagidullin on 16/09/15.
//  Copyright (c) 2015 Teoria. All rights reserved.
//

#import "PhoneEnteringViewController.h"
#import "TeoriusHelper+Extension.h"
#import "SharedClass.h"
#import "ConfirmationViewController.h"
#import "StandardScrollView.h"
#import "AppointmentHelper.h"
#import "MainAPI.h"
#import "User.h"

@interface PhoneEnteringViewController () <UITextFieldDelegate>

@property (nonatomic, strong) NSString* simpleNumber;
@property (nonatomic) UITextField *textField;
@property (nonatomic) StandardScrollView *scrollView;

@end


@implementation PhoneEnteringViewController

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:nil
                                                                            action:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.simpleNumber = @"";
    [self configSubviews];
}

- (void)configSubviews {
    UIScrollView* mainScrollView = [[StandardScrollView alloc] initWithParentVeiwFrame:self.view.frame];
    [self.view addSubview:mainScrollView];
    self.scrollView = mainScrollView;
    
    CGRect labelFrame = [TeoriusHelper scaledRectFromCenter:CGPointMake(LOGICAL_WIDTH/2, 125) size:CGSizeMake(LOGICAL_WIDTH-80, 140)];

    NSMutableParagraphStyle *parStyle = [NSMutableParagraphStyle new];
    parStyle.lineHeightMultiple = 1.3f;
    
    NSAttributedString *attrText = [[NSAttributedString alloc] initWithString:@"Введите свой телефон, чтобы мы могли оповестить вас" attributes:@{NSParagraphStyleAttributeName: parStyle, NSFontAttributeName: [SharedClass getRegularFontWithSize:14.0f], NSForegroundColorAttributeName: COLOR_DARKGRAY}];
    
    UILabel* textLabel = [[UILabel alloc] initWithFrame:labelFrame];
    textLabel.attributedText = attrText;
    textLabel.numberOfLines = 2;
    textLabel.textAlignment = NSTextAlignmentCenter;
    //    [nameLabel sizeToFit];
    [mainScrollView addSubview:textLabel];
    
    CGRect textFieldFrame = [TeoriusHelper scaledRectFromCenter:CGPointMake(LOGICAL_WIDTH/2, 190) size:CGSizeMake(210, 40)];
    
    UITextField* textField = [[UITextField alloc] initWithFrame:textFieldFrame];
    UIFont* textFieldFont = [SharedClass getRegularFontWithSize:24];
    textField.font = textFieldFont;
    textField.keyboardType = UIKeyboardTypeNumberPad;
    textField.tintColor = COLOR_DARKGRAY;
    textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"+7(      )        -      -      " attributes:@{NSForegroundColorAttributeName: COLOR_LIGHTGRAY, NSFontAttributeName: textFieldFont}];
    //        textField.textColor = UIColorFromRGB(0x);
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(CGRectGetMinX(textField.frame), CGRectGetMaxY(textField.frame)+2, textField.bounds.size.width, 1.0f);
    bottomBorder.backgroundColor = COLOR_LIGHTGRAY.CGColor;
    [mainScrollView.layer addSublayer:bottomBorder];
    textField.delegate = self;
    [mainScrollView addSubview:textField];
    
    NSString *phone = [MainAPI defaultAPI].currentUser.phone;
    if ([TeoriusHelper stringIsValid:phone]) {
        phone = [phone stringByReplacingOccurrencesOfString:@"+7" withString:@""];
        [self textField:textField shouldChangeCharactersInRange:NSMakeRange(0, phone.length) replacementString:phone];
    }
    
    self.textField = textField;
    
    CGRect frame = [TeoriusHelper scaledRectFromCenter:CGPointMake(LOGICAL_WIDTH/2, 265) size:CGSizeMake(130, 40)];
    UIButton* gb = [SharedClass pinkBorderButtonWithFrame:frame text:@"Отправить"];
    [mainScrollView addSubview:gb];
    
    [gb addTarget:self action:@selector(buttonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (range.length == 1)
        // Delete button was hit.. so tell the method to delete the last char.
        textField.text = [self changePhoneNumber:string deleteLastChar:YES];
    else
        textField.text = [self changePhoneNumber:string deleteLastChar:NO ];
    return NO;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    self.scrollView.contentSize = CGSizeMake(self.scrollView.contentSize.width, self.scrollView.contentSize.height + 160.f);
    
    return YES;
}

- (NSString*)changePhoneNumber:(NSString*)replacementString deleteLastChar:(BOOL)deleteLastChar {
    if (deleteLastChar && [self.simpleNumber length]>=1)
        self.simpleNumber = [self.simpleNumber substringToIndex:[self.simpleNumber length]-1];
    else if (!deleteLastChar && [replacementString length]>=1)
        self.simpleNumber = [self.simpleNumber stringByAppendingString:replacementString];
    
    if (self.simpleNumber.length==0)
        return @"";
    
    if (self.simpleNumber.length>10)
        self.simpleNumber = [self.simpleNumber substringToIndex:10];
    
    NSString* resultString;
    
    if (self.simpleNumber.length<=3)
        resultString = [self.simpleNumber stringByReplacingOccurrencesOfString:@"(\\d+)"
                                                                    withString:@"+7 ($1"
                                                                       options:NSRegularExpressionSearch
                                                                         range:NSMakeRange(0, [self.simpleNumber length])];
    else if (self.simpleNumber.length<=6)
        resultString = [self.simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{3})(\\d+)"
                                                                    withString:@"+7 ($1) $2"
                                                                       options:NSRegularExpressionSearch
                                                                         range:NSMakeRange(0, [self.simpleNumber length])];
    else if (self.simpleNumber.length<=8)
        resultString = [self.simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{3})(\\d{3})(\\d+)"
                                                                    withString:@"+7 ($1) $2-$3"
                                                                       options:NSRegularExpressionSearch
                                                                         range:NSMakeRange(0, [self.simpleNumber length])];
    else
        resultString = [self.simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{3})(\\d{3})(\\d{2})(\\d+)"
                                                                    withString:@"+7 ($1) $2-$3-$4"
                                                                       options:NSRegularExpressionSearch
                                                                         range:NSMakeRange(0, [self.simpleNumber length])];
    return resultString;
}


#pragma mark - User actions

- (void)buttonTouchUpInside:(id)sender {
    if (self.textField.hasText && ![self phoneIsValid:self.textField.text]) {
        [TeoriusHelper showStandartAlertInMainThreadWithTitle:@"Некорректный номер" body:@"Пожалуйста, введите корректный номер телефона или оставьте поле пустым."];
        return;
    }
    
    NSString *phoneString = self.textField.text;
    phoneString = [phoneString stringByReplacingOccurrencesOfString:@" " withString:@""];
    phoneString = [phoneString stringByReplacingOccurrencesOfString:@"-" withString:@""];
    phoneString = [phoneString stringByReplacingOccurrencesOfString:@")" withString:@""];
    phoneString = [phoneString stringByReplacingOccurrencesOfString:@"(" withString:@""];
    [AppointmentHelper instance].phone = phoneString;
    
    UIViewController* vc = [[ConfirmationViewController alloc] initWithTitle:@"Подтверждение" menuButtonVisible:NO];
    [self.navigationController pushViewController:vc animated:YES];
}


#pragma mark - Helper

- (BOOL)phoneIsValid:(NSString *)phone {
    NSString *phoneRegEx = @"\\+7 \\(\\d\\d\\d\\) \\d\\d\\d-\\d\\d-\\d\\d";
    
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES[c] %@", phoneRegEx];
    
    return [emailTest evaluateWithObject:phone];
}


@end
