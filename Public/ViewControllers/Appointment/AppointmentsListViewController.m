//
//  PassportTableViewController.m
//  Public
//
//  Created by Teoria 5 on 10/09/15.
//  Copyright (c) 2015 Teoria. All rights reserved.
//

#import "AppointmentsListViewController.h"
#import "SharedClass.h"
#import "AppDelegate.h"
#import "AppointmentViewController.h"
#import "ClinicsAndResourcesViewController.h"
#import "MainAPI.h"
#import "NSObject+PerformBlockAfterDelay.h"
#import "Appointment.h"
#import "TwoLeftOneRightLabelsCell.h"
#import "AppointmentHelper.h"
#import "BaseButton.h"
#import "NSObject+PerformBlockAfterDelay.h"
#import "OneLongTwoShortLabelsCell.h"

@interface AppointmentsListViewController () {
    NSDateFormatter *formatter;
    CGFloat rowHeight;
    CGFloat heightSumm;
}

@property (nonatomic, strong) NSArray *appointmentsArray;

@end


@implementation AppointmentsListViewController

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:nil
                                                                            action:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if ([AppointmentHelper instance].needToUpdateList) {
        [AppointmentHelper instance].needToUpdateList = NO;
        
        [[TeoriusHelper instance] addWaitingViewWithText:@"Обновление записей..." font: [SharedClass getRegularFontWithSize:14] toView:self.navigationController.view disablingInteractionEvents: YES];

        [[MainAPI defaultAPI] getFutureAppointments: ^(NSArray *arr, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[TeoriusHelper instance] removeWaitingView];

                if (error) {
                    [TeoriusHelper showStandartAlertInMainThreadWithTitle:@"Ошибка" body:error.localizedDescription];
                }
                else {
                    _appointmentsArray = arr.copy;
                    [self.tableView reloadData];
                    [self performBlock:^{
                        [self reloadFooterView];
                    } afterDelay:.05f];
                }
            });
        }];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    rowHeight = 60.f;
    formatter = [TeoriusHelper dateFormatterUsingFormat:@"dd.MM.yyyy"];
    
    [self configSubviews];

    [[TeoriusHelper instance] addWaitingViewWithText:@"Обновление записей..." font: [SharedClass getRegularFontWithSize:14] toView:self.navigationController.view disablingInteractionEvents: YES];
    
    [[MainAPI defaultAPI] getFutureAppointments: ^(NSArray *arr, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[TeoriusHelper instance] removeWaitingView];

            if (error) {
                [TeoriusHelper showStandartAlertInMainThreadWithTitle:@"Ошибка" body:error.localizedDescription];
            }
            else {
                _appointmentsArray = arr.copy;
                [self.tableView reloadData];
                [self reloadFooterView];
            }
        });
    }];
}

- (void)configSubviews {
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"UserIcon"]
//                                                                              style:UIBarButtonItemStylePlain
//                                                                             target:self
//                                                                             action:@selector(userIconTapped:)];
    
    self.tableView.showsVerticalScrollIndicator = NO;
    
    [self reloadFooterView];
}

- (void)reloadFooterView {
//    if (self.appointmentsArray.count == 0) {
//        self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
//        return;
//    }
    
    BaseButton *plusButton = [BaseButton new];
    [plusButton addTarget:self action:@selector(ButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *plusImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"PlusIcon"]];
    [plusButton addSubview:plusImage];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, plusImage.maxY + 29.f, roundf(self.view.width / 3.f), 0)];
    label.font = [SharedClass getRegularFont];
    label.textColor = COLOR_LIGHTERGREEN;
    label.textAlignment = NSTextAlignmentCenter;
    label.numberOfLines = 0;
    label.text = @"Записаться на прием";
    [label sizeToFit];
    [plusButton addSubview:label];
    
    plusButton.size = CGSizeMake(MAX(plusImage.width, label.width), label.maxY);
    [TeoriusHelper placeViewsVertically:@[plusImage, label] inSuperview:plusButton];
    
    CGFloat footerHeight = roundf(plusButton.height * 1.2f);
    CGFloat availableSpace = self.view.height - heightSumm;
    footerHeight = MAX(availableSpace, footerHeight);
    
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, footerHeight)];
    footerView.backgroundColor = [UIColor clearColor];
    [footerView addSubview:plusButton];
    plusButton.center = CGPointMake(footerView.halfWidth, footerView.halfHeight);
    self.tableView.tableFooterView = footerView;
}

- (void)ButtonTouchUpInside:(id)sender {
    ClinicsAndResourcesViewController *vc = [[ClinicsAndResourcesViewController alloc] init];
    vc.currentMode = MODE_SEGMENTED_CONTROL;
    [self.navigationController pushViewController:vc animated:YES];
}


#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat width = self.view.width - 34.f; // accessory view width
    
    CGFloat h1 = [OneLongTwoShortLabelsCell heightOfLabelOneWithText:[self labelOneTextForIndexPath:indexPath] font:[SharedClass getRegularFontWithSize:14.0f] viewWidth:width];
    CGFloat h2 = [OneLongTwoShortLabelsCell heightOfLabelTwoWithText:[self labelTwoTextForIndexPath:indexPath] font:[SharedClass getRegularFontWithSize:14.0f] viewWidth:width];
    CGFloat h3 = [OneLongTwoShortLabelsCell heightOfLabelThreeWithText:[self labelThreeTextForIndexPath:indexPath] font:[SharedClass getRegularFontWithSize:11.0f] viewWidth:width];
    CGFloat h4 = [OneLongTwoShortLabelsCell additionalHeight];
    
    CGFloat height = h1 + h2 + h3 + h4;
    heightSumm += height;
    
    return height;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.appointmentsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    OneLongTwoShortLabelsCell *cell = [[OneLongTwoShortLabelsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    
    cell.labelOne.font = [SharedClass getRegularFontWithSize:14.0f];
    cell.labelOne.textColor = COLOR_GREEN1;
    cell.labelOne.text = [self labelOneTextForIndexPath:indexPath];
    
    cell.labelTwo.font = [SharedClass getRegularFontWithSize:14.0f];
    cell.labelTwo.textColor = COLOR_DARKGRAY;
    cell.labelTwo.text = [self labelTwoTextForIndexPath:indexPath];

    cell.labelThree.font = [SharedClass getRegularFontWithSize:11.0f];
    cell.labelThree.textColor = COLOR_DARKGRAY;
    cell.labelThree.text = [self labelThreeTextForIndexPath:indexPath];

    cell.backgroundColor = [UIColor whiteColor];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [TeoriusHelper tableViewDeselectSelectedCell:tableView];
    AppointmentViewController* vc = [[AppointmentViewController alloc] initWithTitle:@"Запись на прием" menuButtonVisible:NO];
    vc.appointment = self.appointmentsArray[indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)userIconTapped:(id)sender {
}

- (void)viewTapped:(id)sender {
    [self resignFirstResponder];
}


#pragma mark - Helper

- (NSString *)labelOneTextForIndexPath:(NSIndexPath *)indexPath {
    AppointmentRLM *appointment = self.appointmentsArray[indexPath.row];
    return [TeoriusHelper stringIsValid:appointment.resourceSpeciality] ? appointment.resourceSpeciality : appointment.serviceShortName;
}

- (NSString *)labelTwoTextForIndexPath:(NSIndexPath *)indexPath {
    AppointmentRLM *appointment = self.appointmentsArray[indexPath.row];
    return appointment.resourceFio;
}

- (NSString *)labelThreeTextForIndexPath:(NSIndexPath *)indexPath {
    AppointmentRLM *appointment = self.appointmentsArray[indexPath.row];
    return [formatter stringFromDate:appointment.date];
}

@end