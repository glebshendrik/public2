//
//  AppointmentDateAndTimeViewController.m
//  Public
//
//  Created by Arslan Zagidullin on 15/09/15.
//  Copyright (c) 2015 Teoria. All rights reserved.
//

#import "AppointmentDateAndTimeViewController.h"
#import "TeoriusHelper+Extension.h"
#import "SharedClass.h"
#import "PhoneEnteringViewController.h"
#import "MainAPI.h"
#import "AppointmentHelper.h"
#import "TimeInterval.h"


@interface AppointmentDateAndTimeViewController () <UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate>

@property (nonatomic, strong) UIPickerView *datePicker;
@property (nonatomic, strong) UIPickerView *timePicker;
@property (nonatomic) UITableView *tableView;
@property (nonatomic) NSUInteger selectedDateRow;

@property (nonatomic) NSDictionary *scheduleDict;
@property (nonatomic) NSArray *datesArray;
@property (nonatomic) TimeInterval *timeInterval;

@property (nonatomic) UITextField *dateTextField;
@property (nonatomic) UITextField *timeTextField;

@end


@implementation AppointmentDateAndTimeViewController

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:nil
                                                                            action:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.selectedDateRow = 0;
    
    [self configSubviews];
    
    [[TeoriusHelper instance] addWaitingViewWithText:@"Обновление" font:[SharedClass getRegularFont] toView:self.navigationController.view disablingInteractionEvents:YES];
    
    NSDateComponents *curDateComponents = [[NSCalendar currentCalendar] components:NSYearCalendarUnit|NSMonthCalendarUnit fromDate:[NSDate date]];
    
    [[MainAPI defaultAPI] getScheduleForClinicID:self.resource.clinicID serviceID:self.resource.serviceID resourceID:self.resource.ID year:(int)curDateComponents.year month:(int)curDateComponents.month completion:^(NSDictionary *dict, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[TeoriusHelper instance] removeWaitingView];
            
            if (error) {
                [TeoriusHelper showStandartAlertInMainThreadWithTitle:@"Ошибка" body:error.localizedDescription];
            }   
            else {
                self.scheduleDict = dict.copy;
                
                NSMutableDictionary *scheduleDictTemp = [[NSMutableDictionary alloc] initWithCapacity:dict.count];
                NSMutableArray *keysTempArray = dict.allKeys.mutableCopy;
                NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:YES];
                [keysTempArray sortUsingDescriptors:@[descriptor]];
                
                NSMutableArray *datesTempArray = [[NSMutableArray alloc] initWithCapacity:dict.count];
                
                for (NSNumber *num in keysTempArray) {
                    NSTimeInterval timeInterval = num.doubleValue;
                    
                    if (timeInterval > 999999999999) {
                        timeInterval /= 1000;
                    }
                    
                    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
                    NSString *dateString = [TeoriusHelper dateToString:date];
                    
                    [datesTempArray addObject:dateString];
                    scheduleDictTemp[dateString] = dict[num];
                }
                
                self.datesArray = datesTempArray.copy;
                self.scheduleDict = scheduleDictTemp.copy;
                
                [self.tableView reloadData];
            }
        });
    }];
}

- (void)configSubviews {
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Готово"
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(doneButtonTapped:)];
    const float kRowHeight = 50.0f;
    
    UITableView* tableView = [[UITableView alloc] initWithFrame:[TeoriusHelper scaledRect:CGRectMake(0, HEADER_HEIGHT, LOGICAL_WIDTH, LOGICAL_HEIGHT_SUB_HH)]];
    
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.rowHeight = [TeoriusHelper scaledFloat:kRowHeight];
    tableView.showsVerticalScrollIndicator = NO;
    tableView.tintColor = COLOR_PINK;
    tableView.backgroundColor = [UIColor clearColor];
    tableView.contentInset = UIEdgeInsetsMake(-HEADER_HEIGHT, 0, 0, 0);
    [self.view addSubview:tableView];
    
    UIView* greenButtonContainer = [[UIView alloc] initWithFrame:[TeoriusHelper scaledRect:CGRectMake(0, 0, SCREEN_WIDTH, 20)]];
    tableView.tableFooterView = greenButtonContainer;
    
    self.tableView = tableView;
}


#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger row = (NSUInteger)indexPath.row;
    NSArray *cellTitles = @[@"Дата", @"Время"];
    
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    cell.backgroundColor = [UIColor whiteColor];
    
    cell.textLabel.font = [SharedClass getRegularFont];
    cell.textLabel.textColor = COLOR_LIGHTERGREEN;
    cell.textLabel.text = cellTitles[row];
    
    UITextField *textField = [UITextField new];
    textField.allowsEditingTextAttributes = NO;
    textField.textAlignment = NSTextAlignmentRight;
    textField.font = [SharedClass getRegularFont];
    textField.tintColor = [UIColor clearColor];//UIColorFromRGB(0xEAE3E1);
    textField.delegate = self;
    textField.textColor = COLOR_DARKGRAY;
    textField.tag = row;
    
    CGSize size = [@"BLA-BLA" sizeWithAttributes:@{NSFontAttributeName: textField.font}];
    textField.frame = CGRectMake(0.f, 0.f, self.view.frame.size.width / 1.5f, size.height + 1.f);
    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentBottom;

    cell.accessoryView = textField;
    
    UIPickerView *pickerView = [UIPickerView new];
    pickerView.delegate = self;
    pickerView.dataSource = self;
    
    textField.inputView = pickerView;

    if (row == 0) {
        if (self.datesArray.count > self.selectedDateRow) {
            textField.text = self.datesArray[self.selectedDateRow];
        }
        else {
            textField.text = @"Нет свободных дней";
        }
        self.dateTextField = textField;
        self.datePicker = pickerView;
    }
    else {
        self.timeTextField = textField;
        self.timePicker = pickerView;
        
        [self changeTimeText];
    }
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = COLOR_LIGHTPINK;
    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
}

- (void)doneButtonTapped:(id)sender {
    NSDate *date = [TeoriusHelper stringToDate:self.dateTextField.text];//[TeoriusHelper stringToDate:[NSString  stringWithFormat:@"%@ 12:00", self.dateTextField.text] usingFormat:@"dd MM yyyy hh:mm"];
    
    if (!date || !self.timeInterval) {
        [TeoriusHelper showStandartAlertInMainThreadWithTitle:@"Некорректная дата" body:@"Выберите дату и время, чтобы продолжить"];
        return;
    }
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:date];
    NSString *dateString = [TeoriusHelper dateToString:date usingFormat:@"dd MMMM, yyyy"];
    NSString *dateAndTime = [NSString  stringWithFormat:@"%@ %@", dateString, self.timeInterval.time];
    
    [AppointmentHelper instance].year = components.year;
    [AppointmentHelper instance].month = components.month;
    [AppointmentHelper instance].day = components.day;
    [AppointmentHelper instance].timeInterval = self.timeInterval;
    [AppointmentHelper instance].dateAndTimeString = dateAndTime;
    
    UIViewController* vc = [[PhoneEnteringViewController alloc] initWithTitle:@"Телефон" menuButtonVisible:NO];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - Picker view

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (pickerView == self.datePicker) {
        return self.datesArray.count;
    }
    else {
        NSArray *times = self.scheduleDict[self.dateTextField.text];
        return times.count;
    }
}

- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (pickerView == self.datePicker) {
        return self.datesArray[row];
    }
    else {
        NSArray *times = self.scheduleDict[self.dateTextField.text];
        TimeInterval *interval = times[row];
        return interval.time;
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (pickerView == self.datePicker) {
        self.selectedDateRow = (NSUInteger)row;
        self.dateTextField.text = self.datesArray[row];
        
        [self.timePicker reloadAllComponents];
        [self.timePicker selectRow:0 inComponent:0 animated:NO];
        
        [self changeTimeText];
    }
    else {
        NSArray *times = self.scheduleDict[self.dateTextField.text];
        TimeInterval *interval = times[row];
        self.timeTextField.text = interval.time;
        self.timeInterval = interval;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger row = (NSUInteger)indexPath.row;
    
    if (row == 0) {
        [self.dateTextField becomeFirstResponder];
    }
    else {
        [self.timeTextField becomeFirstResponder];
    }
}


#pragma mark - Text field delegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:textField.tag inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
    
    return YES;
}


#pragma mark - Other

- (void)changeTimeText {
    NSArray *times = self.scheduleDict[self.dateTextField.text];
    BOOL hasTime = NO;
    
    if ([TeoriusHelper arrayIsValid:times]) {
        TimeInterval *interval = times[0];
        NSString *time = interval.time;
        
        if ([TeoriusHelper stringIsValid:time]) {
            self.timeInterval = interval;
            self.timeTextField.text = time;
            hasTime = YES;
        }
    }
    
    if (!hasTime) {
        self.timeTextField.text = @"Нет свободного времени";
        self.timeInterval = nil;
    }
}


@end
