//
//  SpecialitiesListViewController.h
//  Public
//
//  Created by Arslan Zagidullin on 28/09/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HeaderTableViewController.h"
#import "Clinic.h"

@interface ServicesListViewController : HeaderTableViewController

@property(nonatomic) Clinic *selectedClinic;

@end
