//
//  AppointmentViewController.h
//  Public
//
//  Created by Teoria 5 on 04/09/15.
//  Copyright (c) 2015 Teoria. All rights reserved.
//

#import "HeaderViewController.h"
#import "Appointment.h"

@interface AppointmentViewController : HeaderViewController

@property (nonatomic, strong) Appointment *appointment;

@end
