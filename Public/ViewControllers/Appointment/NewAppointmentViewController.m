//
//  AppointmentViewController.m
//  Public
//
//  Created by Teoria 5 on 04/09/15.
//  Copyright (c) 2015 Teoria. All rights reserved.
//

#import "NewAppointmentViewController.h"
#import "SharedClass.h"
#import "TeoriusHelper+Extension.h"
#import "PassportViewController.h"
#import "PolisViewController.h"
#import "MilitaryIDViewController.h"
#import "BirthCertificateViewController.h"
#import "StandardScrollView.h"
#import "MainAPI.h"
#import "User.h"

@interface NewAppointmentViewController ()

@end

@implementation NewAppointmentViewController

#pragma mark - View's lifecycle

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}

- (void)viewWillAppear:(BOOL)animated  {
    [super viewWillAppear:animated];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:nil
                                                                            action:nil];
    
    if (self.isMovingToParentViewController) {
        User *user = [MainAPI defaultAPI].currentUser;
        
        if ([TeoriusHelper stringIsValid:user.polisNumber] && [TeoriusHelper dateIsValid:user.birthDay]) {
            PolisViewController *vc = [[PolisViewController alloc] initWithTitle:@"Полис ОМС" menuButtonVisible:NO];
            vc.forceAuth = YES;
            [self.navigationController pushViewController:vc animated:NO];
        }
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
//    [[MainAPI defaultAPI] sendAnswerAndStateNotifications];
    
    [self configSubviews];
}

- (void)configSubviews {
    UIScrollView *mainScrollView = [[StandardScrollView alloc] initWithParentVeiwFrame:self.view.frame];
    [self.view addSubview:mainScrollView];
    
    NSArray* buttonTitles = @[@"Полис ОМС", @"Паспорт"/*, @"Военный билет", @"Свидетельство о рождении"*/];
    const int kButtonWidth = 220;
    const int kButtonHeight = 36;
    const int kButtonsVerticalSpace = 50;
    const int kButtonsBottomOffset = 50;
    
    int btCount = (int)buttonTitles.count;
    for (int i=btCount-1; i>=0; i--) {
        int buttonY = LOGICAL_HEIGHT - kButtonsBottomOffset - (btCount-i-1)*kButtonsVerticalSpace - HEADER_HEIGHT;
        CGRect frame = [TeoriusHelper scaledRectFromCenter:CGPointMake(LOGICAL_WIDTH/2, buttonY) size:CGSizeMake(kButtonWidth, kButtonHeight)];
        UIButton *gb = [SharedClass greenBorderButtonWithFrame:frame text:buttonTitles[i]];
        [mainScrollView addSubview:gb];

        switch (i) {
            case 0:
                [gb addTarget:self action:@selector(polisButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
                break;
                
            case 1:
                [gb addTarget:self action:@selector(passportButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
                break;
                
            case 2:
                [gb addTarget:self action:@selector(militaryIDButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
                break;
                
            case 3:
                [gb addTarget:self action:@selector(birthCertificateButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
                break;
                
            default:
                break;
        }
    }
    
    CGRect labelFrame = [TeoriusHelper scaledRectFromCenter:CGPointMake(LOGICAL_WIDTH/2, 135) size:CGSizeMake(LOGICAL_WIDTH-40, 140)];
//    NSLog(@"%@", NSStringFromCGRect(labelFrame));
    UILabel* nameLabel = [[UILabel alloc] initWithFrame:labelFrame];
    nameLabel.text = @"Выберите документ для записи";
    nameLabel.numberOfLines = 2;
    nameLabel.textAlignment = NSTextAlignmentCenter;
    nameLabel.font = [SharedClass getRegularFontWithSize:14.0f];
    nameLabel.textColor = COLOR_GRAY;
//    [nameLabel sizeToFit];
    [mainScrollView addSubview:nameLabel];
}

- (void)passportButtonTouchUpInside:(id)sender {
    UIViewController* vc = [[PassportViewController alloc] initWithTitle:@"Паспорт" menuButtonVisible:NO];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)polisButtonTouchUpInside:(id)sender {
    UIViewController* vc = [[PolisViewController alloc] initWithTitle:@"Полис ОМС" menuButtonVisible:NO];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)militaryIDButtonTouchUpInside:(id)sender {
    UIViewController* vc = [[MilitaryIDViewController alloc] initWithTitle:@"Военный билет" menuButtonVisible:NO];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)birthCertificateButtonTouchUpInside:(id)sender {
    UIViewController* vc = [[BirthCertificateViewController alloc] initWithTitle:@"Свидетельство о рождении" menuButtonVisible:NO];
    [self.navigationController pushViewController:vc animated:YES];
}

@end
