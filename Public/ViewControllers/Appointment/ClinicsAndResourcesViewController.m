//
//  ClinicsAndSpecialistsViewController.m
//  Public
//
//  Created by Teoria 5 on 21/09/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import "ClinicsAndResourcesViewController.h"
#import "TeoriusHelper+Extension.h"
#import "SharedClass.h"
#import "MainAPI.h"
#import "ClinicViewController.h"
#import "SpecialistViewController.h"
#import "AppointmentDateAndTimeViewController.h"
#import "ServicesListViewController.h"
#import "Clinic.h"
#import "Resource.h"
#import "Service.h"
#import "AppointmentHelper.h"
#import "OneLongTwoShortLabelsCell.h"

@interface ClinicsAndResourcesViewController () <UISearchBarDelegate, UISearchDisplayDelegate, UIActionSheetDelegate>

@property (nonatomic) NSArray *clinicsArr;
@property (nonatomic) NSMutableArray *filtedClinicsArr;
@property (nonatomic) NSArray *resourcesArr;
@property (nonatomic) NSMutableArray *filteredResourcesArr;

@property (nonatomic) NSArray *someProperty;

@property (nonatomic) UISearchDisplayController* searchDisplayController;

@end


@implementation ClinicsAndResourcesViewController

- (void)setCurrentMode:(int)currentMode {
    _currentMode = currentMode;
    if (self.currentMode == MODE_SPECIALISTS_OF_PARTICULAR_CLINIC)
        self.activeTable = TABLE_SPECIALISTS;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:nil
                                                                            action:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBar.barTintColor = UIColorFromRGB(0xE9E3E2);
    self.navigationController.navigationBar.translucent = NO;

    self.resourcesArr = self.selectedService.resources;
    
    [self configSubviews];
    
    if (self.currentMode == MODE_SEGMENTED_CONTROL) {
        [[TeoriusHelper instance] addWaitingViewWithText:@"Загрузка..." font:[SharedClass getRegularFontWithSize:14] toView:self.navigationController.view  disablingInteractionEvents: YES];
        
        [[MainAPI defaultAPI] getClinics:^(NSArray *arr, NSError *err) {
            self.clinicsArr = arr.copy;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tableView reloadData];
                
                [[TeoriusHelper instance] removeWaitingView];
            });
        }];
    }
}

- (void)configSubviews {
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"FilterIcon"]
//                                                                              style:UIBarButtonItemStylePlain
//                                                                             target:self
//                                                                            action:@selector(filterButtonTapped:)];
    if (self.currentMode == MODE_SEGMENTED_CONTROL) {
//        CGRect segmentedControlFrame = [TeoriusHelper scaledRectFromCenter:CGPointMake(LOGICAL_WIDTH/2, 0) size:CGSizeMake(140.0f, 26.0f)];
//        UISegmentedControl* segmentedControl = [[UISegmentedControl alloc] initWithItems:@[@"Клиники", @"Врачи"]];
//        [segmentedControl setFrame:segmentedControlFrame];
//        [segmentedControl setTintColor:COLOR_PINK];
//        int selectedSegmentIndex = self.activeTable;
//        [segmentedControl setSelectedSegmentIndex:selectedSegmentIndex];
//        [segmentedControl setTitleTextAttributes:@{NSFontAttributeName: [SharedClass getRegularFontWithSize:10.0f]} forState:UIControlStateNormal];
//        [segmentedControl addTarget:self action:@selector(segmentedControlValueChanged:) forControlEvents:UIControlEventValueChanged];
//        [self.navigationItem setTitleView:segmentedControl];
        self.title = @"Клиники";
    }
    else if (self.currentMode == MODE_SPECIALISTS_OF_PARTICULAR_CLINIC) {
        self.title = @"Врачи";
    }
    
    const float kRowHeight = 54.0f;
    
    UISearchBar *searchBar = [[UISearchBar alloc] initWithFrame:[TeoriusHelper scaledRect:CGRectMake(0, 70, LOGICAL_WIDTH, kRowHeight)]];
    [searchBar setSearchBarStyle:UISearchBarStyleMinimal];
    //    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setLeftViewMode:UITextFieldViewModeNever];
    searchBar.placeholder = @"Поиск";
    searchBar.delegate = self;
    //    [searchBar setTintColor:COLOR_LIGHTPINK];
    //    [searchBar setBackgroundColor:COLOR_LIGHTPINK];
    
    UISearchDisplayController *searchController = [[UISearchDisplayController alloc] initWithSearchBar:searchBar contentsController:self];
    searchController.delegate = self;
    searchController.searchResultsDelegate = self;
    searchController.searchResultsDataSource = self;
    self.searchDisplayController = searchController;
    
    self.tableView.rowHeight = kRowHeight;
    self.tableView.tableHeaderView = searchBar;
    self.tableView.tintColor = COLOR_LIGHTERGREEN;
}

- (void)segmentedControlValueChanged:(id)sender {
    UISegmentedControl* segmentedControl = (UISegmentedControl*)self.navigationItem.titleView;
    self.activeTable = (int)segmentedControl.selectedSegmentIndex;
    
    [self.tableView reloadData];
}

- (void)filterButtonTapped:(id)sender {
    NSString* actionSheetTitle = @"Сортировка";
    NSString* actionSheetCancelButtonTitle = @"Отмена";
    NSString* actionSheetButton1Title = @"По алфавиту";
    NSString* actionSheetButton2Title = @"По близости";
    NSString* actionSheetButton3Title = @"По рейтингу";
    
    UIActionSheet* actionSheet;
    if (self.activeTable == TABLE_SPECIALISTS)
        actionSheet = [[UIActionSheet alloc] initWithTitle:actionSheetTitle
                                                  delegate:self
                                         cancelButtonTitle:actionSheetCancelButtonTitle
                                    destructiveButtonTitle:nil
                                         otherButtonTitles:actionSheetButton1Title, actionSheetButton2Title, actionSheetButton3Title, nil];
    else
        actionSheet = [[UIActionSheet alloc] initWithTitle:actionSheetTitle
                                                  delegate:self
                                         cancelButtonTitle:actionSheetCancelButtonTitle
                                    destructiveButtonTitle:nil
                                         otherButtonTitles:actionSheetButton1Title, actionSheetButton2Title, nil];
    [actionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    switch (buttonIndex) {
        case 0:
            NSLog(@"Alphabet");
            break;
            
        case 1:
            NSLog(@"Distance");
            break;
            
        case 2:
            if (self.activeTable == TABLE_SPECIALISTS)
                NSLog(@"Rating");
            break;
            
        default:
            break;
    }
}


#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.activeTable == TABLE_CLINICS) {
        NSArray *array = tableView == self.tableView ? self.clinicsArr : self.filtedClinicsArr;
        Clinic *clinic = array[indexPath.row];

        CGFloat h1 = [OneLongTwoShortLabelsCell heightOfLabelOneWithText:clinic.name font:[SharedClass getRegularFontWithSize:15.0f] viewWidth:self.view.width - 48.f withDoubleOffset:!clinic.parent];
        
        CGFloat h2 = 0;
        CGFloat h3 = [OneLongTwoShortLabelsCell additionalHeight2];

        if (clinic.attached) {
            h2 = [OneLongTwoShortLabelsCell heightOfLabelTwoWithText:@"Вы прикреплены" font:[SharedClass getRegularFontWithSize:12.f] viewWidth:self.view.width - 48.f];
            h3 = [OneLongTwoShortLabelsCell additionalHeight1];
        }
        
        return h1 + h2 + h3;
    }
    else {
        return 44.f;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.tableView) {
        if (self.activeTable == TABLE_CLINICS) {
            return self.clinicsArr.count;
        }
        else {
            return self.resourcesArr.count;
        }
    }
    else {
        if (self.activeTable == TABLE_CLINICS) {
            return self.filtedClinicsArr.count;
        }
        else {
            return self.filteredResourcesArr.count;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.activeTable == TABLE_CLINICS) {
        return [self getCellForClinicAtIndexPath:indexPath forTableView:tableView];
    }
    else {
        return [self getCellForResourceAtIndexPath:indexPath forTableView:tableView];
    }
}

- (UITableViewCell *)getCellForClinicAtIndexPath:(NSIndexPath *)indexPath forTableView:(UITableView *)tableView {
    NSArray *array = tableView == self.tableView ? self.clinicsArr : self.filtedClinicsArr;
    OneLongTwoShortLabelsCell *cell = [[OneLongTwoShortLabelsCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"SpecialistCell"];
    Clinic *clinic = array[indexPath.row];
    
    cell.shortLabelsDoubleOffset = !clinic.parent;

    cell.labelOne.font =[ SharedClass getRegularFontWithSize:15.0f];
    cell.labelOne.textColor = COLOR_LIGHTERGREEN;
    cell.labelOne.text = clinic.name;
    
    if (clinic.attached) {
        cell.labelTwo.font = [SharedClass getRegularFontWithSize:12.f];
        cell.labelTwo.textColor = COLOR_PINK;
        cell.labelTwo.text = @"Вы прикреплены";
    }
    
//    CGRect detailButtonFrame = [TeoriusHelper scaledRectFromCenter:CGPointMake(300, 27) size:CGSizeMake(24, 24)];
//    UIButton* detailButton = [SharedClass detailButtonWithFrame:detailButtonFrame];
//    [detailButton addTarget:self action:@selector(detailButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
//    cell.accessoryView = detailButton;
    
    //    [cell setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0]];
    cell.accessoryType = UITableViewCellAccessoryDetailButton;
    
    return cell;
}

- (UITableViewCell *)getCellForResourceAtIndexPath:(NSIndexPath *)indexPath forTableView:(UITableView *)tableView {
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"SpecialistCell"];
    
    NSArray *array = tableView == self.tableView ? self.resourcesArr : self.filteredResourcesArr;
    
    Resource *resource = array[indexPath.row];
    
    cell.textLabel.text = [TeoriusHelper stringIsValid:resource.specialityName] ? resource.specialityName : resource.fullName;
    cell.textLabel.font = [SharedClass getRegularFontWithSize:14.0f];
    cell.textLabel.textColor = COLOR_LIGHTERGREEN;
    
    cell.detailTextLabel.text = resource.fio;
    cell.detailTextLabel.font = [SharedClass getRegularFontWithSize:11.0f];
    cell.detailTextLabel.textColor = COLOR_DARKGRAY;
    
    cell.imageView.image = [UIImage imageNamed:@"DefaultSpecialistPortrait"];
    
    cell.accessoryType = UITableViewCellAccessoryDetailButton;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.activeTable == TABLE_CLINICS) {
        [TeoriusHelper tableViewDeselectSelectedCell:tableView];
        ServicesListViewController* vc = [[ServicesListViewController alloc] initWithTitle:@"Услуги" menuButtonVisible:NO];
        
        NSArray *array = tableView == self.tableView ? self.clinicsArr : self.filtedClinicsArr;
        Clinic *clinic = array[indexPath.row];
        vc.selectedClinic = clinic;
        
        [AppointmentHelper instance].clinic = clinic;
        
        [self.navigationController pushViewController:vc animated:YES];
    }
    else if (self.activeTable == TABLE_SPECIALISTS) {
        NSArray *array = tableView == self.tableView ? self.resourcesArr : self.filteredResourcesArr;
        Resource *resource = array[indexPath.row];
        [TeoriusHelper tableViewDeselectSelectedCell:tableView];
        
        [AppointmentHelper instance].resource = resource;
        
        AppointmentDateAndTimeViewController *vc = [[AppointmentDateAndTimeViewController alloc] initWithTitle:@"Дата и время приема" menuButtonVisible:NO];
        vc.resource = resource;
//        vc.specialist = specialist;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
    if (self.activeTable == TABLE_CLINICS) {
        NSArray *array = tableView == self.tableView ? self.clinicsArr : self.filtedClinicsArr;
        Clinic *clinic = array[indexPath.row];
        ClinicViewController *cvc = [[ClinicViewController alloc] initWithTitle:clinic.name menuButtonVisible:NO];
        cvc.clinic = clinic;
        [self.navigationController pushViewController:cvc animated:YES];
    } else if (self.activeTable == TABLE_SPECIALISTS) {
//        NSArray *array = tableView == self.tableView ? self.resourcesArr : self.filteredResourcesArr;
//        SpecialistRLM *specialist = array[indexPath.row];
//        NSString *title = [TeoriusHelper shortName:specialist.name surname:specialist.surname patronymic:specialist.patronymic];
//        SpecialistViewController* vc = [[SpecialistViewController alloc] initWithTitle:title menuButtonVisible:NO];
//        vc.specialist = specialist;
//        [self.navigationController pushViewController:vc animated:YES];
    }
}


#pragma mark - Content Filtering

- (void)filterContentForSearchText:(NSString *)searchText scope:(NSString*)scope
{
//    [arrayFilteredProfiles removeAllObjects];
//    NSPredicate *predicate = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
//        Profile *profile = evaluatedObject;
//        return [profile.name.lowercaseString rangeOfString:searchText.lowercaseString].location != NSNotFound;
//    }];
//    
//    arrayFilteredProfiles = [NSMutableArray arrayWithArray:[arrayProfiles filteredArrayUsingPredicate:predicate]];
    if (self.activeTable == TABLE_CLINICS) {
        [self.filtedClinicsArr removeAllObjects];
        
        NSPredicate *predicate = [NSPredicate predicateWithBlock:^BOOL(id  _Nonnull evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
            Clinic *clinic = evaluatedObject;
            return [clinic.name.lowercaseString rangeOfString:searchText.lowercaseString].location != NSNotFound;
        }];
        
        self.filtedClinicsArr = [NSMutableArray arrayWithArray:[self.clinicsArr filteredArrayUsingPredicate:predicate]];
    }
    else {
        [self.filteredResourcesArr removeAllObjects];
        
        NSPredicate *predicate = [NSPredicate predicateWithBlock:^BOOL(id  _Nonnull evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
            Resource *resource = evaluatedObject;
            
            if ([TeoriusHelper stringIsValid:resource.specialityName]) {
                return [resource.fullName.lowercaseString rangeOfString:searchText.lowercaseString].location != NSNotFound || [resource.fio.lowercaseString rangeOfString:searchText.lowercaseString].location != NSNotFound || [resource.specialityName.lowercaseString rangeOfString:searchText.lowercaseString].location != NSNotFound;
            }
            else {
                return [resource.fullName.lowercaseString rangeOfString:searchText.lowercaseString].location != NSNotFound || [resource.fio.lowercaseString rangeOfString:searchText.lowercaseString].location != NSNotFound;
            }
        }];
        
        self.filteredResourcesArr = [NSMutableArray arrayWithArray:[self.resourcesArr filteredArrayUsingPredicate:predicate]];
    }
}


#pragma mark - UISearchDisplayController Delegate Methods

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString scope:[[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
    return YES;
}

- (void)searchDisplayController:(UISearchDisplayController *)controller didLoadSearchResultsTableView:(UITableView *)tableView
{
    tableView.rowHeight = 54.0f; // or some other height
}



@end
