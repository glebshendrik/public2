//
//  SpecialistViewController.m
//  Public
//
//  Created by Arslan Zagidullin on 17/09/15.
//  Copyright (c) 2015 Teoria. All rights reserved.
//

#import "SpecialistViewController.h"
#import "TeoriusHelper+Extension.h"
#import "SharedClass.h"
#import "AppointmentDateAndTimeViewController.h"
#import "StandardScrollView.h"


@interface SpecialistViewController ()

@end


@implementation SpecialistViewController

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:nil
                                                                            action:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.specialist == NULL)
        return;
    
    [self configSubviews];
}

- (void)configSubviews {
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"CheckboxIcon"]
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(checkboxIconTapped:)];
    
    UIScrollView* mainScrollView = [[StandardScrollView alloc] initWithParentVeiwFrame:self.view.frame];
    [self.view addSubview:mainScrollView];
    
    CGRect viewRect = [TeoriusHelper scaledRect:CGRectMake(0, 0, LOGICAL_WIDTH, 250.0f)];
    UIView* view = [[UIView alloc] initWithFrame:viewRect];
    
    view.backgroundColor = [UIColor whiteColor];
    [mainScrollView addSubview:view];
    
    NSArray* buttonTitles = @[@"ФИО", @"Клиника", @"Адрес", @"Телефон", @"Часы работы", @"Сайт"];
    
    float lastY = 95.0f - HEADER_HEIGHT;
    
    for (int row = 0; row<6; row++) {
        NSMutableParagraphStyle *parStyle = [NSMutableParagraphStyle new];
        parStyle.lineHeightMultiple = 1.3f;
        
        NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:buttonTitles[row] attributes:@{NSParagraphStyleAttributeName: parStyle, NSFontAttributeName: [SharedClass getRegularFontWithSize:14.0f], NSForegroundColorAttributeName: COLOR_LIGHTERGRAY}];
        
        UILabel* firstLabel = [[UILabel alloc] initWithFrame:[TeoriusHelper scaledRect:CGRectMake(20, lastY, 150, 0)]];
        //        nameLabel.numberOfLines = 0;
        firstLabel.attributedText = attrString;
        //        dateLabel.font = [SharedClass getRegularFontWithSize:14.0f];
        //        dateLabel.textColor = UIColorFromRGB(0xB3B3B3);
        [firstLabel sizeToFit];
        [mainScrollView addSubview:firstLabel];
        
        NSString* secondLabelText;
        switch (row) {
            case 0:
                secondLabelText = [NSString stringWithFormat:@"%@ %@ %@", self.specialist.surname, self.specialist.name, self.specialist.patronymic];
                break;
            case 1:
                secondLabelText = self.specialist.clinic.name;
                break;
            case 2:
                secondLabelText = self.specialist.clinic.address;
                break;
            case 3:
                secondLabelText = self.specialist.clinic.phone;
                break;
            case 4:
                secondLabelText = self.specialist.clinic.work_hours;
                break;
            case 5:
                secondLabelText = self.specialist.clinic.site;
                break;
        }
        
        attrString = [[NSAttributedString alloc] initWithString:secondLabelText attributes:@{NSParagraphStyleAttributeName: parStyle, NSFontAttributeName: [SharedClass getRegularFontWithSize:14.0f], NSForegroundColorAttributeName: COLOR_DARKGRAY}];
        
        UILabel* secondLabel = [[UILabel alloc] initWithFrame:[TeoriusHelper scaledRect:CGRectMake(LOGICAL_WIDTH-170-30, lastY, 170, 0)]];
        secondLabel.numberOfLines = 0;
        secondLabel.attributedText = attrString;
        //        nameLabel.font = [SharedClass getRegularFontWithSize:14.0f];
        //        nameLabel.textColor = UIColorFromRGB(0x666666);
        [secondLabel sizeToFit];
        [mainScrollView addSubview:secondLabel];
        
        lastY = (CGRectGetMaxY(secondLabel.frame) + 10.0f) / SCREEN_SCALE;
        
        viewRect.size.height = [TeoriusHelper scaledFloat:lastY + 30.0f];
        view.frame = viewRect;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)checkboxIconTapped:(id)sender {
    AppointmentDateAndTimeViewController* vc = [[AppointmentDateAndTimeViewController alloc] initWithTitle:@"Дата и время приема" menuButtonVisible:NO];
//    vc.specialist = self.specialist;
    [self.navigationController pushViewController:vc animated:YES];
}

@end