//
//  ClinicViewController.m
//  Public
//
//  Created by Arslan Zagidullin on 15/09/15.
//  Copyright (c) 2015 Teoria. All rights reserved.
//

#import "ClinicViewController.h"
#import "TeoriusHelper+Extension.h"
#import "SharedClass.h"
#import "ServicesListViewController.h"
#import "StandardScrollView.h"
#import "Clinic.h"
#import "AppointmentHelper.h"

@interface ClinicViewController ()

@end


@implementation ClinicViewController

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:nil
                                                                            action:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (!self.clinic)
        return;
    
    [self configSubviews];
}

- (void)configSubviews {
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"CheckboxIcon"]
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(checkboxIconTapped:)];
    
    UIScrollView *mainScrollView = [[StandardScrollView alloc] initWithParentVeiwFrame:self.view.bounds];
    [self.view addSubview:mainScrollView];
    
    CGRect viewRect = [TeoriusHelper scaledRect:CGRectMake(0, 0, LOGICAL_WIDTH, 250.0f)];
    UIView *view = [[UIView alloc] initWithFrame:viewRect];
    
    view.backgroundColor = [UIColor whiteColor];
    [mainScrollView addSubview:view];
    
    NSArray *buttonTitles = @[@"Название", @"Адрес", @"Телефон"/*, @"Часы работы", @"Сайт"*/];
    
    float lastY = 95.0f - HEADER_HEIGHT;
    
    for (int row = 0; row < buttonTitles.count; row++) {
        NSMutableParagraphStyle *parStyle = [NSMutableParagraphStyle new];
        parStyle.lineHeightMultiple = 1.3f;
        
        NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:buttonTitles[row] attributes:@{NSParagraphStyleAttributeName: parStyle, NSFontAttributeName: [SharedClass getRegularFontWithSize:14.0f], NSForegroundColorAttributeName: COLOR_LIGHTERGRAY}];
        
        UILabel *firstLabel = [[UILabel alloc] initWithFrame:[TeoriusHelper scaledRect:CGRectMake(20, lastY, 150, 0)]];
        firstLabel.attributedText = attrString;
        [firstLabel sizeToFit];
        [mainScrollView addSubview:firstLabel];
        
        NSString *secondLabelText;
        switch (row) {
            case 0:
                secondLabelText = self.clinic.name;
                break;
            case 1:
                secondLabelText = self.clinic.address;
                break;
            case 2:
                secondLabelText = self.clinic.phone;
                break;
            case 3:
                secondLabelText = self.clinic.work_hours;
                break;
            case 4:
                secondLabelText = self.clinic.site;
                break;
        }
        
        if (![TeoriusHelper stringIsValid:secondLabelText]) {
            secondLabelText = @"";
        }
        
        attrString = [[NSAttributedString alloc] initWithString:secondLabelText attributes:@{NSParagraphStyleAttributeName: parStyle, NSFontAttributeName: [SharedClass getRegularFontWithSize:14.0f], NSForegroundColorAttributeName: COLOR_DARKGRAY}];
        
        UILabel *secondLabel = [[UILabel alloc] initWithFrame:[TeoriusHelper scaledRect:CGRectMake(LOGICAL_WIDTH-170-30, lastY, 170, 0)]];
        secondLabel.numberOfLines = 0;
        secondLabel.attributedText = attrString;
        //        nameLabel.font = [SharedClass getRegularFontWithSize:14.0f];
        //        nameLabel.textColor = UIColorFromRGB(0x666666);
        [secondLabel sizeToFit];
        [mainScrollView addSubview:secondLabel];
        
        lastY = (MAX(firstLabel.maxY, secondLabel.maxY) + 10.0f) / SCREEN_SCALE;
        
        viewRect.size.height = [TeoriusHelper scaledFloat:lastY + 30.0f];
        view.frame = viewRect;
    }
}

- (void)checkboxIconTapped:(id)sender {
    [AppointmentHelper instance].clinic = self.clinic;

    ServicesListViewController* vc = [[ServicesListViewController alloc] initWithTitle:@"Услуги" menuButtonVisible:NO];
    vc.selectedClinic = self.clinic;
    [self.navigationController pushViewController:vc animated:YES];
}

@end
