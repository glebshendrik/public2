//
//  BaseViewController.m
//  Public
//
//  Created by Teoria 5 on 02/09/15.
//  Copyright (c) 2015 Teoria. All rights reserved.
//

#import "BaseViewController.h"
#import "AuthorizationViewController.h"
#import "MonitoringViewController.h"
#import "FamilyViewController.h"
#import "NewAppointmentViewController.h"
#import "RecommendationsListViewController.h"
#import "NotificationsListViewController.h"
#import "UseViewController.h"
#import "SharedClass.h"
#import "SettingsManager.h"
#import "User.h"
//
#import "MainAPI.h"
#import "AccountRLM.h"
#import "BaseView.h"

#define DURATION_TIME .3f
#define MINIMUM_VELOCITY_TO_CONTINUE_DRAGGING 600.f

typedef NS_ENUM(NSUInteger, MenuItem) {
    MenuItemProfile,
    MenuItemMonitoring,
    MenuItemAppointment,
    MenuItemRecommendation,
    MenuItemUse,
};

typedef NS_ENUM(int, MenuState) {
    MenuStateHidden,
    MenuStateVisible,
    MenuStateIntermediate
};


@interface BaseViewController () <UITableViewDelegate, UITableViewDataSource, MenuHeaderViewDelegate, FamilyViewControllerDelegate> {
    MenuItem selectedItem;
    
    CGFloat baseViewDragStartX;
    CGFloat baseViewDragMaxX;
    CGFloat baseViewDragMinX;
    
    CGFloat kRightSpace;
}


@property (nonatomic) BaseView *baseView;
@property (nonatomic) UITableView *menuTableView;
@property (nonatomic) UIView *contentView;
@property (nonatomic) MenuState menuState;
@property (nonatomic) UINavigationController *contentNavigationController;
@property (nonatomic) User *user;

@end


@implementation BaseViewController


#pragma mark - view's lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xE9E3E2);
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(usersDataChanged) name:notificationNameUsersDataChanged object:nil];
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    self.user  = [MainAPI defaultAPI].currentUser;
    
    [self.menuTableView reloadData];
}

- (void)loadView {
    [super loadView];
    
    UIImageView *backgroundView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"login_bg1.jpg"]];
    backgroundView.frame = self.view.bounds;
    [self.view addSubview:backgroundView];
    self.navigationController.navigationBarHidden = YES;
    
    [self configSubviews];

    self.user  = [MainAPI defaultAPI].currentUser;
    NSString *nameProfile = @"Вы";
    if ([TeoriusHelper stringIsValid:self.user.name]) {
        nameProfile = self.user.name;
    }
    
//    if (!self.user) {
//        [[TeoriusHelper instance] addWaitingViewWithText:@"Обновление" font:[SharedClass getRegularFontWithSize:14] toView:[UIApplication sharedApplication].keyWindow disablingInteractionEvents:YES];
//        [[MainAPI defaultAPI]getUserCitizen:^(NSArray *arr, NSError *err) {
//    
//            self.user = [MainAPI defaultAPI].currentUser;
//            [self.menuTableView reloadData];
//            
//            [[TeoriusHelper instance] removeWaitingView];
//            
//        }];
//    };
}


- (void)configSubviews {
    kRightSpace = roundf((267.f/1242.f) * self.view.bounds.size.width);
    
    self.baseView = [[BaseView alloc]initWithFrame:self.view.bounds];
    self.baseView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.baseView];
    
    self.menuTableView = [[UITableView alloc]initWithFrame:CGRectMake(-(self.view.frame.size.width - kRightSpace), 0, self.view.frame.size.width - kRightSpace, self.baseView.frame.size.height) style:UITableViewStylePlain];
    self.menuTableView.delegate = self;
    self.menuTableView.dataSource = self;
    self.menuTableView.showsVerticalScrollIndicator = NO;
    self.menuTableView.scrollEnabled = NO;
    self.menuState = MenuStateHidden;
    self.menuTableView.backgroundColor = [UIColor clearColor];
    self.menuTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.menuTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectZero];
    self.menuTableView.contentInset = UIEdgeInsetsMake(20, 10, 0, 10);
    [self.baseView addSubview:self.menuTableView];
    
    UIFont *font = [UIFont fontWithName:@"Roboto-Bold" size:16.f];
    
    UIButton *logoutButton = [[UIButton alloc]init];
    [logoutButton setTitle:@"Выйти" forState:UIControlStateNormal];
    [logoutButton.titleLabel setFont:font];
    [logoutButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [logoutButton sizeToFit];
    logoutButton.center = CGPointMake(self.menuTableView.frame.size.width / 2.f - 10, self.menuTableView.frame.size.height - 40 - logoutButton.frame.size.height / 2.f);
    [logoutButton addTarget:self action:@selector(logoutPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.menuTableView addSubview:logoutButton];
    
    self.contentView = [[UIView alloc]initWithFrame:self.view.bounds];
    self.contentView.backgroundColor = [UIColor whiteColor];
    [self.baseView addSubview:self.contentView];
    [self addShadowToContentView];
    baseViewDragMinX = self.baseView.center.x;
    baseViewDragMaxX = self.baseView.center.x + self.menuTableView.frame.size.width;
    
    selectedItem = MenuItemRecommendation;
    
    UIViewController *viewController = [self viewControllerForSelectedMenuItem];
    self.contentNavigationController = [[UINavigationController alloc]initWithRootViewController:viewController];
    
    if ([self.contentNavigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.contentNavigationController.interactivePopGestureRecognizer.enabled = NO;
    }

    [self addChildViewController:self.contentNavigationController];
    [self.contentView addSubview:self.contentNavigationController.view];
    [self.contentNavigationController didMoveToParentViewController:self];
    
    UIPanGestureRecognizer *gestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(contentViewDragged:)];
    [self.view addGestureRecognizer:gestureRecognizer];
}

- (void)addShadowToContentView {
    CALayer *sublayer = [CALayer layer];
    sublayer.bounds = self.contentView.bounds;
    sublayer.position = self.contentView.center;
    sublayer.shadowColor = [UIColor blackColor].CGColor;
    sublayer.shadowOffset = CGSizeMake(0, 0);
    sublayer.shadowRadius = 5;
    sublayer.shadowOpacity = 1.f;
    CGPathRef path = [UIBezierPath bezierPathWithRect:CGRectMake(0, 0, self.contentView.frame.size.width, self.contentView.frame.size.height)].CGPath;
    sublayer.shadowPath = path;
    sublayer.backgroundColor = UIColorFromRGB(0xEFF5F8).CGColor;
    sublayer.shouldRasterize = YES;
    [self.contentView.layer addSublayer:sublayer];
}


#pragma mark - tableView dataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UIFont *font = [SharedClass getBoldFontWithSize:16.0f];
    UIFont *font1 = [SharedClass getRegularFontWithSize:16.0f];

    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"Cell"];
    cell.textLabel.font = font;
    cell.textLabel.textColor = (selectedItem == indexPath.row) ? UIColorFromRGB(0xD67163) : [UIColor whiteColor];
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.imageView.contentMode = UIViewContentModeScaleToFill;
    
    if (indexPath.row == MenuItemProfile) {
        NSString *nameProfile = @"Вы";
        if ([TeoriusHelper stringIsValid:self.user.name]) {
            nameProfile = self.user.name;
        }

        UIImage *image = [UIImage imageWithData:self.user.image];
        cell.textLabel.font = font1;
        cell.imageView.image = image;
        cell.textLabel.text = nameProfile;
        
        CALayer *layer = cell.imageView.layer;
        layer.cornerRadius = 22.f;//cell.imageView.frame.size.width / 2.f;
        layer.masksToBounds = YES;
        layer.borderColor = [UIColor whiteColor].CGColor;
        layer.borderWidth = 1.f;

    }
    else if(indexPath.row == MenuItemMonitoring) {
        cell.textLabel.text = @"Мониторинг";
        UIImage *image = [UIImage imageNamed:(selectedItem == MenuItemMonitoring) ? @"monitoringIconActive" : @"monitoringIconInactive"];
        cell.imageView.image = image;
    }
    else if(indexPath.row == MenuItemAppointment) {
        cell.textLabel.text = @"Запись на прием";
        UIImage *image = [UIImage imageNamed:(selectedItem == MenuItemAppointment) ? @"appointmentIconActive" : @"appointmentIconInactive"];
        cell.imageView.image = image;
    }
    else if(indexPath.row == MenuItemRecommendation) {
        UIImage *image = [UIImage imageNamed:(selectedItem == MenuItemRecommendation) ? @"recommendationIconActive" : @"recommendationIconInactive"];
        cell.textLabel.text = @"Уведомления";
        cell.imageView.image = image;
    }
    else if (indexPath.row == MenuItemUse) {
        UIImage *image = [UIImage imageNamed:(selectedItem == MenuItemUse) ? @"useIconActive" : @"useIconInactive"];
        cell.imageView.image = image;
    }

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return 48;
    }
    return 75;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 20.f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.menuTableView.frame.size.width, 10.f)];
    view.backgroundColor = [UIColor clearColor];
    return view;
}


#pragma mark - tableView delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSArray *array = tableView.visibleCells;
    for (UITableViewCell *cell in array) {
        cell.textLabel.textColor = [UIColor whiteColor];
    }
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.textLabel.textColor = UIColorFromRGB(0xD67163);
    
    selectedItem = indexPath.row;
    
    UIViewController *viewController = [self viewControllerForSelectedMenuItem];
    self.contentNavigationController.viewControllers = @[viewController];

    [tableView reloadData];
    [self menuTapped];
}


#pragma mark - MenuHeaderViewDelegate

- (void)menuTapped {
    if (self.menuState == MenuStateHidden) {
        [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        self.menuState = MenuStateIntermediate;
        [UIView animateWithDuration:.3f animations:^{
            self.baseView.frame = CGRectOffset(self.baseView.frame, self.menuTableView.frame.size.width, 0);
        } completion:^(BOOL finished) {
            [[UIApplication sharedApplication] endIgnoringInteractionEvents];
            self.menuState = MenuStateVisible;
        }];
    }
    else if(self.menuState == MenuStateVisible) {
        [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        self.menuState = MenuStateIntermediate;
        [UIView animateWithDuration:.3f animations:^{
            self.baseView.frame = CGRectOffset(self.baseView.frame, -self.menuTableView.frame.size.width, 0);
        } completion:^(BOOL finished) {
            [[UIApplication sharedApplication] endIgnoringInteractionEvents];
            self.menuState = MenuStateHidden;
        }];
    }
}

- (void)openMonitoring {
    selectedItem = MenuItemMonitoring;
    
    UIViewController *viewController = [self viewControllerForSelectedMenuItem];
    self.contentNavigationController.viewControllers = @[viewController];
}


#pragma mark - user action

- (void)contentViewDragged:(UIPanGestureRecognizer *)gestureRecognizer {
    
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        baseViewDragStartX = self.baseView.center.x;
        self.menuState = MenuStateIntermediate;
    }
    else if(gestureRecognizer.state == UIGestureRecognizerStateChanged){
        CGFloat translationX = [gestureRecognizer translationInView:self.view].x;
        CGFloat newBaseViewX = baseViewDragStartX + translationX;
        newBaseViewX = MAX(baseViewDragMinX, newBaseViewX);
        newBaseViewX = MIN(baseViewDragMaxX, newBaseViewX);
        
        self.baseView.center = CGPointMake(newBaseViewX, self.baseView.center.y);
    }
    else if (gestureRecognizer.state == UIGestureRecognizerStateEnded || gestureRecognizer.state == UIGestureRecognizerStateCancelled) {
        
        CGPoint velocity = [gestureRecognizer velocityInView:self.view];
        BOOL dragToRight = velocity.x > 0;
        CGFloat newBaseViewX;
        CGFloat duration;
        MenuState newState = MenuStateIntermediate;
        
        if (fabsf(velocity.x) >= MINIMUM_VELOCITY_TO_CONTINUE_DRAGGING) {
            newBaseViewX = dragToRight ? baseViewDragMaxX : baseViewDragMinX;
            duration = fabsf((newBaseViewX - self.baseView.center.x) / (velocity.x * .5f));
            newState = dragToRight ? MenuStateVisible : MenuStateHidden;
        }
        else {
            newState = fabsf(baseViewDragMaxX - self.baseView.center.x) < fabsf(baseViewDragMinX - self.baseView.center.x) ? MenuStateVisible : MenuStateHidden;
            newBaseViewX = newState == MenuStateVisible ? baseViewDragMaxX : baseViewDragMinX;
            duration = fabsf((newBaseViewX - self.baseView.center.x) / 500);
        }
        
        [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        [UIView animateWithDuration:duration delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            self.baseView.center = CGPointMake(newBaseViewX, self.baseView.center.y);
        } completion:^(BOOL finished) {
            [[UIApplication sharedApplication] endIgnoringInteractionEvents];
            self.menuState = newState;
        }];
    }
}

- (void)logoutPressed {
    [SettingsManager sharedManager].loggedIn = NO;
    
    [[MainAPI defaultAPI] logout];
    
    UIViewController* vc = [AuthorizationViewController new];
    self.navigationController.viewControllers = @[vc];
}


#pragma mark - Helpers

- (UIViewController *)viewControllerForSelectedMenuItem {
    UIViewController *viewController;
    
    if (selectedItem == MenuItemProfile) {
        FamilyViewController *vc = [[FamilyViewController alloc]initWithTitle:@"Профиль" menuButtonVisible:YES];
        vc.delegate = self;
        vc.familyVCDelegate = self;
        viewController = vc;
    }
    else if (selectedItem == MenuItemMonitoring) {
        MonitoringViewController *vc = [[MonitoringViewController alloc]initWithTitle:@"Мониторинг" menuButtonVisible:YES];
        vc.delegate = self;
        viewController = vc;
    }
    else if (selectedItem == MenuItemAppointment) {
        NewAppointmentViewController *vc = [[NewAppointmentViewController alloc]initWithTitle:@"Запись на прием" menuButtonVisible:YES];
        vc.delegate = self;
        viewController = vc;
    }
    else if (selectedItem == MenuItemRecommendation) {
        NotificationsListViewController *vc = [[NotificationsListViewController alloc]initWithTitle:@"Уведомления" menuButtonVisible:YES]; // Бывшие рекомендации
        vc.delegate = self;
        viewController = vc;
    }
    else if (selectedItem == MenuItemUse) {
        UseViewController *vc = [[UseViewController alloc]initWithTitle:@"Назначения" menuButtonVisible:YES];
        vc.delegate = self;
        viewController = vc;
    }
    
    return viewController;
}


#pragma mark - Notifications

- (void)usersDataChanged {
    self.user = nil;
    [self.menuTableView reloadData];
}


#pragma mark - Dealloc

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark - Setters/Getters

- (User *)user {
    if (!_user) {
        _user = [MainAPI defaultAPI].currentUser;
    }
    
    return _user;
}

- (void)setMenuState:(MenuState)menuState {
    _menuState = menuState;
    
    if (_menuState == MenuStateHidden) {
        self.contentNavigationController.topViewController.view.userInteractionEnabled = YES;
    }
    else if (_menuState == MenuStateVisible) {
        self.contentNavigationController.topViewController.view.userInteractionEnabled = NO;
    }
}

@end
