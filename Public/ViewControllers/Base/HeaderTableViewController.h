//
//  HeaderTableViewController.h
//  Public
//
//  Created by Глеб on 23/10/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import <UIKit/UIKit.h>

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@protocol MenuHeaderTableViewDelegate <NSObject>

- (void)menuTapped;

@end

@interface HeaderTableViewController : UITableViewController

@property (nonatomic) BOOL isShowMenuButton;
@property (nonatomic,weak) id <MenuHeaderTableViewDelegate>delegate;

- (instancetype)initWithTitle:(NSString *)title menuButtonVisible:(BOOL)isVisible;

@end
