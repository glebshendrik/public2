//
//  ViewController.h
//  Public
//
//  Created by Teoria 5 on 02/09/15.
//  Copyright (c) 2015 Teoria. All rights reserved.
//

#import <UIKit/UIKit.h>

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@protocol MenuHeaderViewDelegate <NSObject>

- (void)menuTapped;

@end


@interface HeaderViewController : UIViewController

@property (nonatomic) BOOL isShowMenuButton;
@property (nonatomic,weak) id <MenuHeaderViewDelegate>delegate;

- (instancetype)initWithTitle:(NSString *)title menuButtonVisible:(BOOL)isVisible;

@end

