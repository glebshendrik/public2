//
//  ViewController.m
//  Public
//
//  Created by Teoria 5 on 02/09/15.
//  Copyright (c) 2015 Teoria. All rights reserved.
//

#import "HeaderViewController.h"
#import "SharedClass.h"

@interface HeaderViewController ()

@end

@implementation HeaderViewController {

    NSString *_title;
    
}

- (instancetype)initWithTitle:(NSString *)title menuButtonVisible:(BOOL)isVisible {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    _isShowMenuButton = isVisible;
    _title = title;
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = UIColorFromRGB(0xE9E3E2);
    [self addMenuButton];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
    self.navigationController.navigationBar.barTintColor = UIColorFromRGB(0xE9E3E2);
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.tintColor = UIColorFromRGB(0xD2695B);
    [self setTitle:_title];

    [self configViewController];
}

- (void)configViewController {
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
}

- (void)addMenuButton {
    
    if (self.isShowMenuButton) {
        UIBarButtonItem *barItem = [[UIBarButtonItem alloc]initWithImage:[[UIImage imageNamed:@"menuIcon"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStyleBordered target:self action:@selector(menuButtonTapped)];
        self.navigationItem.leftBarButtonItem = barItem;
    }
}

- (void)menuButtonTapped {
    [self.delegate menuTapped];
}


@end
