//
//  GraphViewController.m
//  Public
//
//  Created by Teoria 5 on 12/09/15.
//  Copyright (c) 2015 Teoria. All rights reserved.
//

#import "GraphViewController.h"
#import "GraphController.h"
#import "SharedClass.h"
#import "TeoriusHelper.h"
#import "StandardScrollView.h"
#import "NewReadingViewController.h"
#import "MainAPI.h"
#import "Reading.h"
#import "GraphPointInfoView.h"

#define NO_PICKER 0
#define DATE_FROM_PICKER 1
#define DATE_TO_PICKER 2

#define DATE_FORMAT_STRING @"dd.MM.yy"


@interface GraphViewController () <UIActionSheetDelegate, UIGestureRecognizerDelegate> {
    UIView *_graphLineView;
    UIView *_graphPointView;
    GraphPointInfoView *_graphPointInfoView;
}

@property (nonatomic) GraphController* graphController;

@property (nonatomic) NSArray<NSString*>* intervalNames;
@property (nonatomic) NSInteger selectedInterval;
@property (nonatomic) NSDate* dateFrom;
@property (nonatomic) NSDate* dateTo;
@property (nonatomic) NSArray<NSNumber*>* selectedPlots;

@property (nonatomic) DropDownLabel* intervalLabel;
@property (nonatomic) UILabel* dateFromLabel;
@property (nonatomic) UILabel* dateToLabel;

@property (nonatomic) UIDatePicker* dateFromPicker;
@property (nonatomic) UIDatePicker* dateToPicker;
@property int activePicker;

@property (nonatomic) NSArray<UIButton*>* plotButtons;
@property (nonatomic) Measurement* randomMeasurement;
@property (nonatomic) BOOL isBloodPressure;

@property (nonatomic) UIView* readingsView;
@property (nonatomic) UIScrollView* mainScrollView;
@property (nonatomic) UIView *graphView;
@property (nonatomic) UIView *graphHostingView;
@property (nonatomic) UIView *graphLineView;
@property (nonatomic) UIView *graphPointView;
@property (nonatomic) GraphPointInfoView *graphPointInfoView;

@end


@implementation GraphViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (self.isMovingToParentViewController) {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                                                                               target:self
                                                                                               action:@selector(newReadingButtonTapped:)];
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                                 style:UIBarButtonItemStylePlain
                                                                                target:nil
                                                                                action:nil];
    }
    else {
        self.readings = [[MainAPI defaultAPI] myReadingsArr];
        self.readings = [[MainAPI defaultAPI] getReadingsForMeasurements:self.measurements fromReadings:self.readings];
        
        self.graphController.readingsArray = self.readings;
        [self.graphController reloadData];
        [self printReadingsToView:self.readingsView parentScrollView:self.mainScrollView];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    [self configSubviews];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    
//    if (!self.isMovingToParentViewController) {
//        NSLog(@"%@", self.measurementsArray);
//        NSArray *measArr = [self getReadingsForGraphByName:self.title];
//        self.measurementsArray = measArr;
//        NSLog(@"Новый массив %@", measArr);
//        self.measurements = [[MainAPI defaultAPI]myMeasurementsByName:self.title];
//        self.readsArray = [self getReadingsForGraphByName:self.title];
        
//        dispatch_async(dispatch_get_main_queue(), ^{
//                    [[TeoriusHelper instance] removeWaitingView];
//            [self initData];
//            [self configSubviews];
//        });
//    }
}


- (void)initData {
    if (self.readings.count == 0) {
        NSLog(@"Measurements Array is empty");
        return;
    }
    self.intervalNames = @[@"За год", @"За 3 месяца", @"За месяц", @"За неделю", @"За 3 дня", @"За день"];
    self.selectedInterval = INTERVAL_1WEEK;
    
    // creating graph controller
    self.graphController = [[GraphController alloc] initWithMeasurementsArray:self.measurements readingsArray:self.readings dateFrom:self.dateFrom dateTo:self.dateTo type:GraphControllerTypeDefault];
    self.graphController.scaleX = 1.15f;
    self.graphController.scaleY = 1.15f;
    self.graphController.normalAreaIsVisible = NO;
}

- (void)configSubviews {
    StandardScrollView *mainScrollView = [[StandardScrollView alloc] initWithParentVeiwFrame:self.view.frame];

    [self.view addSubview:mainScrollView];
    
    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped)]];
    
    int plotButtonsCount = (int)self.measurements.count;
    
    const float kPlotButtonBarHeight = 45;
    float graphViewHeight = (plotButtonsCount >= 2 && !self.isBloodPressure) ? 320 : 320-kPlotButtonBarHeight;
    UIView* graphView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, graphViewHeight)];
    graphView.backgroundColor = [UIColor whiteColor];
    [mainScrollView addSubview:graphView];

    float lastY = 10;
    const float kLeftRightPadding = 20;
    
    if (plotButtonsCount >= 2 && !self.isBloodPressure) {
        NSMutableArray<UIButton*>* plotButtons = [NSMutableArray array];
        
        UIFont* font = [SharedClass getRegularFontWithSize:12];
        float lastX = 0;
        float buttonHeight = 0;
        for (int i=0; i<plotButtonsCount; i++) {
            Measurement *m = self.measurements[i];
            UIColor* normalColor = COLOR_LIGHTGRAY;
            UIColor* selectedColor = [SharedClass getColorOfPlotNumber:i];
            
            UIButton* plotButton = [UIButton buttonWithType:UIButtonTypeCustom];
            plotButton.titleLabel.font = font;
            [plotButton setTitle:m.shortName forState:UIControlStateNormal];
            [plotButton setTitleColor:normalColor forState:UIControlStateNormal];
            [plotButton setTitleColor:[normalColor colorWithAlphaComponent:0.3] forState:UIControlStateHighlighted];
            [plotButton setTitleColor:selectedColor forState:UIControlStateSelected];
            [plotButton sizeToFit];
            plotButton.x = lastX;
            
            [mainScrollView addSubview:plotButton];
            
            plotButton.tag = i;
            [plotButton addTarget:self action:@selector(plotButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
            
            [plotButtons addObject:plotButton];
            
            lastX = plotButton.maxX + SCALED_F(25.f);
            buttonHeight = plotButton.height;
        }
        
        [TeoriusHelper placeViewsHorizontally:plotButtons inSuperview:mainScrollView toCenterPoint:CGPointMake(mainScrollView.halfWidth, lastY + buttonHeight / 2.f)];
        
        lastY += kPlotButtonBarHeight;

        self.plotButtons = [plotButtons copy];
        self.plotButtons[0].selected = YES;
        self.selectedPlots = [self getSelectedPlots];
    }
    
    CGSize size = [@"BLA-BLA" sizeWithAttributes:@{NSFontAttributeName: [SharedClass getRegularFontWithSize:14]}];

    NSString* intervalLabelText = self.intervalNames[self.selectedInterval];
    CGRect intervalLabelRect = [TeoriusHelper scaledRect:CGRectMake(kLeftRightPadding, lastY, 100, size.height + 1)];
    DropDownLabel *intervalLabel = [SharedClass dropDownLabelWithFrame:intervalLabelRect text:intervalLabelText];
    intervalLabel.userInteractionEnabled = YES;
    UITapGestureRecognizer *intervalLabelTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(intervalLabelTapped:)];
    [intervalLabel addGestureRecognizer:intervalLabelTapGesture];
    [graphView addSubview:intervalLabel];
    self.intervalLabel = intervalLabel;

    CGRect dateFromLabelRect = [TeoriusHelper scaledRect:CGRectMake(155, lastY, 70, size.height+1)];
    UILabel* dateFromLabel = [[UILabel alloc]initWithFrame:dateFromLabelRect];
    dateFromLabel.text = [TeoriusHelper dateToString:self.dateFrom usingFormat:DATE_FORMAT_STRING];
    dateFromLabel.font = [SharedClass getRegularFontWithSize:14];
    dateFromLabel.textColor = COLOR_LIGHTERGREEN;
    dateFromLabel.textAlignment = NSTextAlignmentRight;
    dateFromLabel.userInteractionEnabled = YES;
    UITapGestureRecognizer *dateFromLabelTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dateFromLabelTapped:)];
    [dateFromLabel addGestureRecognizer:dateFromLabelTapGesture];
    [graphView addSubview:dateFromLabel];
    self.dateFromLabel = dateFromLabel;
    
    CGRect dashLabelRect = [TeoriusHelper scaledRect:CGRectMake(CGRectGetMaxX(dateFromLabel.frame)/SCREEN_SCALE, lastY, 20, size.height+1)];
    UILabel* dashLabel = [[UILabel alloc]initWithFrame:dashLabelRect];
    dashLabel.text = @"—"; // @arslan: alt + shift + '-'
    dashLabel.font = [SharedClass getRegularFontWithSize:14];
    dashLabel.textAlignment = NSTextAlignmentCenter;
    dashLabel.textColor = COLOR_LIGHTERGREEN;
    [graphView addSubview:dashLabel];
    
    CGRect dateToLabelRect = [TeoriusHelper scaledRect:CGRectMake(CGRectGetMaxX(dashLabel.frame)/SCREEN_SCALE, lastY, 70, size.height+1)];
    UILabel* dateToLabel = [[UILabel alloc]initWithFrame:dateToLabelRect];
    dateToLabel.text = [TeoriusHelper dateToString:self.dateTo usingFormat:DATE_FORMAT_STRING];
    dateToLabel.font = [SharedClass getRegularFontWithSize:14];
    dateToLabel.textColor = COLOR_LIGHTERGREEN;
    dateToLabel.textAlignment = NSTextAlignmentLeft;
    dateToLabel.userInteractionEnabled = YES;
    UITapGestureRecognizer *dateToLabelTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dateToLabelTapped:)];
    [dateToLabel addGestureRecognizer:dateToLabelTapGesture];
    [graphView addSubview:dateToLabel];
    self.dateToLabel = dateToLabel;
    
    CGFloat xForHostingView = SCALED_F(20.f);
    CPTGraphHostingView *hostingView = [self.graphController getHostingView:CGRectMake(xForHostingView, CGRectGetMaxY(intervalLabel.frame), self.view.width - xForHostingView, graphView.frame.size.height - CGRectGetMaxY(intervalLabel.frame))];
//    hostingView.backgroundColor = [UIColor redColor];
    
    UILongPressGestureRecognizer *touchGesRec = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(touch:)];
    touchGesRec.minimumPressDuration = .001;
    [hostingView addGestureRecognizer:touchGesRec];
    
    UIPanGestureRecognizer *panGesRec = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(pan:)];
    panGesRec.delegate = self;
    [hostingView addGestureRecognizer:panGesRec];

    [graphView addSubview:hostingView];
//    mainScrollView.hostingView = hostingView;
    
    // BOTTOM VIEW
    
    CGRect readingsViewFrame = [TeoriusHelper scaledRect:CGRectMake(0, CGRectGetMaxY(hostingView.frame)/SCREEN_SCALE + 1, LOGICAL_WIDTH, 100)];
    UIView* readingsView = [[UIView alloc] initWithFrame:readingsViewFrame];
    readingsView.backgroundColor = [UIColor whiteColor];
    [mainScrollView addSubview:readingsView];
    
    self.graphView = graphView;
    self.graphHostingView = hostingView;
    self.readingsView = readingsView;
    self.mainScrollView = mainScrollView;
    [self printReadingsToView:self.readingsView parentScrollView:self.mainScrollView];
}

- (void)printReadingsToView:(UIView *)view parentScrollView:(UIScrollView *)scrollView {
    [view.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    float lastY = 10;
    
    for (Measurement *m in self.measurements) {
        if (![self.graphController measurementIsSelected:m]) {
            continue;
        }
        
        NSArray *readings = [self getReadingsForGraphByName:m.name];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(date >= %@) AND (date <= %@)", self.dateFrom, self.dateTo];
        readings = [readings filteredArrayUsingPredicate:predicate];
        if (readings.count == 0) {
            continue;
        }
        
        NSMutableParagraphStyle *parStyle = [NSMutableParagraphStyle new];
        parStyle.lineHeightMultiple = 1.3f;
        
        NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:m.name attributes:@{NSParagraphStyleAttributeName: parStyle, NSFontAttributeName: [SharedClass getBoldFontWithSize:14.0f], NSForegroundColorAttributeName: COLOR_LIGHTERGREEN}];
        
        CGRect measurementNameLabelRect = [TeoriusHelper scaledRect:CGRectMake(30, lastY, LOGICAL_WIDTH-60, 0)];
        UILabel* measurementNameLabel = [[UILabel alloc] initWithFrame:measurementNameLabelRect];
        measurementNameLabel.attributedText = attrString;
        measurementNameLabel.numberOfLines = 0;
        [measurementNameLabel sizeToFit];
        [view addSubview:measurementNameLabel];
        
        lastY = CGRectGetMaxY(measurementNameLabel.frame);
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:NO];
        NSArray *readingsSorted = [readings sortedArrayUsingDescriptors:@[sortDescriptor]];

        for (Reading* r in readingsSorted) {
            NSString* firstLabelStringFormat = @"%.f";
            if (m.decimalDigits == 1)
                firstLabelStringFormat = @"%.1f";
            
            UILabel* firstLabel = [[UILabel alloc] initWithFrame:[TeoriusHelper scaledRect:CGRectMake(30, lastY, 150, 40)]];
            firstLabel.text = [NSString stringWithFormat:firstLabelStringFormat, r.value];
            firstLabel.font = [SharedClass getRegularFontWithSize:14.0f];
            firstLabel.textColor = COLOR_LIGHTERGREEN;
            [view addSubview:firstLabel];
            
            UILabel* secondLabel = [[UILabel alloc] initWithFrame:[TeoriusHelper scaledRect:CGRectMake(70, lastY, 170, 42)]];
            secondLabel.text = [TeoriusHelper dateToString:r.date];
            secondLabel.font = [SharedClass getRegularFontWithSize:10.0f];
            secondLabel.textColor = COLOR_LIGHTERGRAY;
            [view addSubview:secondLabel];
            
            UILabel* thirdLabel = [[UILabel alloc] initWithFrame:[TeoriusHelper scaledRect:CGRectMake(170, lastY, 170, 42)]];
            thirdLabel.text = [TeoriusHelper timeToString:r.date];
            thirdLabel.font = [SharedClass getRegularFontWithSize:10.0f];
            thirdLabel.textColor = COLOR_LIGHTERGRAY;
            [view addSubview:thirdLabel];
            
            lastY = CGRectGetMaxY(firstLabel.frame)-15;
        }
        lastY += 30;
    }
    
    CGRect viewFrame = view.frame;
    viewFrame.size.height = lastY + 10;
    view.frame = viewFrame;
    
    scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, CGRectGetMaxY(viewFrame));
}

#pragma mark - UIActionSheet delegate

- (void)willPresentActionSheet:(UIActionSheet *)actionSheet {
    for (UIView *subview in actionSheet.subviews) {
        if ([subview isKindOfClass:[UIButton class]]) {
            UIButton *button = (UIButton *)subview;
            [button setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        }
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex >= INTERVAL_1YEAR && buttonIndex <= INTERVAL_1DAY)
        self.selectedInterval = buttonIndex;
}

#pragma mark - user actions

- (void)touch:(UITapGestureRecognizer *)gesRec {
    if (gesRec.state == UIGestureRecognizerStateBegan) {
        CGPoint point = [gesRec locationInView:self.graphHostingView];
        float x = floorf(point.x);
        float y = floorf(point.y);
        [self handleGraphTouchInPoint:CGPointMake(x, y)];
    }
    else if (gesRec.state == UIGestureRecognizerStateEnded || gesRec.state == UIGestureRecognizerStateFailed || gesRec.state == UIGestureRecognizerStateCancelled) {
        self.graphLineView = nil;
        self.graphPointView = nil;
        self.graphPointInfoView = nil;
    }
}

- (void)pan:(UIPanGestureRecognizer *)gesRec {
    if (gesRec.state == UIGestureRecognizerStateBegan || gesRec.state == UIGestureRecognizerStateChanged) {
        CGPoint point = [gesRec locationInView:self.graphHostingView];
        float x = floorf(point.x);
        x = MAX(0, x);
        x = MIN(x, self.graphHostingView.width);
        float y = floorf(point.y);
        [self handleGraphTouchInPoint:CGPointMake(x, y)];
    }
    else if (gesRec.state == UIGestureRecognizerStateEnded || gesRec.state == UIGestureRecognizerStateFailed || gesRec.state == UIGestureRecognizerStateCancelled) {
        self.graphLineView = nil;
        self.graphPointView = nil;
        self.graphPointInfoView = nil;
    }
}

- (void)handleGraphTouchInPoint:(CGPoint)touchPoint {
    CGPoint convertedPoint = [self.graphView convertPoint:touchPoint fromView:self.graphHostingView];
    self.graphLineView.x = convertedPoint.x;

    NSDictionary *closestReadingDict = [self.graphController closestReadingForPoint:touchPoint];
    
    if (!closestReadingDict) {
        return;
    }
    
    CGPoint point = [closestReadingDict[@"point"] CGPointValue];
    point = CGPointMake([self floatByRounding:point.x], [self floatByRounding:point.y]);
    point.y = self.graphHostingView.height - point.y;
    point = [self.graphView convertPoint:point fromView:self.graphHostingView];
    
    Reading *reading = closestReadingDict[@"reading"];
    UIColor *color = closestReadingDict[@"color"];
    if (!color) {
        color = COLOR_GREEN1;
    }
    
    self.graphPointView.layer.borderColor = color.CGColor;
    self.graphPointView.center = point;
    
    self.graphPointInfoView.layer.borderColor = color.CGColor;
    self.graphPointInfoView.titleLabel.text = [TeoriusHelper stringFromFloatValue:reading.value];
    self.graphPointInfoView.titleLabel.textColor = color;
    self.graphPointInfoView.leftDetailLabel.text = [TeoriusHelper dateToString:reading.date usingFormat:@"dd.MM.yy"];
    self.graphPointInfoView.leftDetailLabel.textColor = color;
    self.graphPointInfoView.rightDetailLabel.text = [TeoriusHelper dateToString:reading.date usingFormat:@"kk:mm"];
    self.graphPointInfoView.rightDetailLabel.textColor = color;
    [self.graphPointInfoView sizeToFit];
    self.graphPointInfoView.y = self.graphPointView.y - SCALED_F(1.f) - self.graphPointInfoView.height;
    self.graphPointInfoView.centerX = self.graphPointView.centerX;
    
    if (self.graphPointInfoView.maxX > self.graphPointInfoView.superview.width) {
        self.graphPointInfoView.x = self.graphPointInfoView.superview.width - self.graphPointInfoView.width;
    }
    else if (self.graphPointInfoView.minX < 0) {
        self.graphPointInfoView.x = 0;
    }
}

- (void)intervalLabelTapped:(id)sender {
    [self viewTapped];
    UIActionSheet* actionSheet = [[UIActionSheet alloc] initWithTitle:@"Выберите временной промежуток"
                                                             delegate:self
                                                    cancelButtonTitle:@"Отмена"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Год", @"3 месяца", @"Месяц", @"Неделя", @"3 дня", @"День", nil];
    [actionSheet showInView:self.view];
}

- (void)dateFromLabelTapped:(UITapGestureRecognizer*)tapGestureRecognizer {
    if (self.activePicker == DATE_FROM_PICKER)
        return;
    [self viewTapped];
    
    self.dateFromPicker.frame = [TeoriusHelper scaledRect:CGRectMake(0, LOGICAL_HEIGHT, LOGICAL_WIDTH, DATEPICKER_HEIGHT)];
    self.dateFromPicker.alpha = 0;
    self.dateFromPicker.maximumDate = [SharedClass dateFromForInterval:INTERVAL_1DAY dateTo:self.dateTo];
    self.dateFromPicker.date = self.dateFrom;
    [UIView animateWithDuration:DATEPICKER_ANIM_DURATION animations:^{
        self.dateFromPicker.frame = [TeoriusHelper scaledRect:CGRectMake(0, LOGICAL_HEIGHT-DATEPICKER_HEIGHT, LOGICAL_WIDTH, DATEPICKER_HEIGHT)];
        self.dateFromPicker.alpha = 1;
    }];
    self.activePicker = DATE_FROM_PICKER;
}

- (void)dateToLabelTapped:(UITapGestureRecognizer*)tapGestureRecognizer {
    if (self.activePicker == DATE_TO_PICKER)
        return;
    [self viewTapped];
    
    self.dateToPicker.frame = [TeoriusHelper scaledRect:CGRectMake(0, LOGICAL_HEIGHT, LOGICAL_WIDTH, DATEPICKER_HEIGHT)];
    self.dateToPicker.alpha = 0;
    self.dateToPicker.minimumDate = [NSDate dateWithTimeInterval:1*24*60*60 sinceDate:self.dateFrom];
    self.dateToPicker.maximumDate = [NSDate date];
    self.dateToPicker.date = self.dateTo;
    [UIView animateWithDuration:DATEPICKER_ANIM_DURATION animations:^{
        self.dateToPicker.frame = [TeoriusHelper scaledRect:CGRectMake(0, LOGICAL_HEIGHT-DATEPICKER_HEIGHT, LOGICAL_WIDTH, DATEPICKER_HEIGHT)];
        self.dateToPicker.alpha = 1;
    }];
    self.activePicker = DATE_TO_PICKER;
}

- (void)viewTapped {
    if (self.activePicker != NO_PICKER) {
        UIDatePicker* datePicker = (self.activePicker == DATE_FROM_PICKER) ? self.dateFromPicker : self.dateToPicker;
        
        [UIView animateWithDuration:DATEPICKER_ANIM_DURATION animations:^{
            datePicker.frame = [TeoriusHelper scaledRect:CGRectMake(0, LOGICAL_HEIGHT, LOGICAL_WIDTH, DATEPICKER_HEIGHT)];
            datePicker.alpha = 0;
        }];
        self.activePicker = NO_PICKER;
    }
}

- (void)datePickerDateDidChanged:(UIDatePicker *)datePicker {
    
    NSDate *date = datePicker.date;
    
    if (self.activePicker == DATE_FROM_PICKER)
        self.dateFrom = date;
    else if (self.activePicker == DATE_TO_PICKER)
        self.dateTo = date;
    
    [self.graphController reloadData];

    [self.intervalLabel setNewText:@"Свой"];
}

- (void)newReadingButtonTapped:(id)sender {
    NewReadingViewController *vc = [[NewReadingViewController alloc]initWithTitle:self.title menuButtonVisible:NO];
    vc.readsArray = self.readsArray;
    vc.measurementsArray = self.measurements;
    vc.afterDone = ^(void) {
        [self.graphController reloadData];
        [self printReadingsToView:self.readingsView parentScrollView:self.mainScrollView];
    };
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)plotButtonPressed:(UIButton *)sender {
    UIButton* b = sender;
    b.selected = !b.selected;
    
    BOOL isSomethingSelected = NO;
    for (UIButton* b in self.plotButtons)
        isSomethingSelected = isSomethingSelected || b.selected;
    if (!isSomethingSelected)
        self.plotButtons.firstObject.selected = YES;
    
    self.selectedPlots = [self getSelectedPlots];
    [self printReadingsToView:self.readingsView parentScrollView:self.mainScrollView];
}

#pragma mark - properties

- (UIDatePicker*)dateFromPicker {
    if (_dateFromPicker == NULL) {
        CGRect pickerFrame = [TeoriusHelper scaledRect:CGRectMake(0, LOGICAL_HEIGHT, LOGICAL_WIDTH, DATEPICKER_HEIGHT)];
        _dateFromPicker = [[UIDatePicker alloc] initWithFrame:pickerFrame];
        _dateFromPicker.backgroundColor = [UIColor whiteColor];
        _dateFromPicker.datePickerMode = UIDatePickerModeDate;
        _dateFromPicker.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"ru_RU"];
        [_dateFromPicker addTarget:self action:@selector(datePickerDateDidChanged:) forControlEvents:UIControlEventValueChanged];
        [self.view addSubview:_dateFromPicker];
    }
    return _dateFromPicker;
}

- (UIDatePicker*)dateToPicker {
    if (_dateToPicker == NULL) {
        CGRect pickerFrame = [TeoriusHelper scaledRect:CGRectMake(0, LOGICAL_HEIGHT, LOGICAL_WIDTH, DATEPICKER_HEIGHT)];
        _dateToPicker = [[UIDatePicker alloc] initWithFrame:pickerFrame];
        _dateToPicker.backgroundColor = [UIColor whiteColor];
        _dateToPicker.datePickerMode = UIDatePickerModeDate;
        _dateToPicker.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"ru_RU"];
        [_dateToPicker addTarget:self action:@selector(datePickerDateDidChanged:) forControlEvents:UIControlEventValueChanged];
        
        [self.view addSubview:_dateToPicker];
    }
    return _dateToPicker;
}

- (void)setSelectedInterval:(NSInteger)selectedInterval {
    _selectedInterval = selectedInterval;
    [self.intervalLabel setNewText:self.intervalNames[_selectedInterval]];
    
    self.dateTo = [NSDate date];
    self.dateFrom = [SharedClass dateFromForInterval:_selectedInterval dateTo:self.dateTo];
    
    [self.graphController reloadData];

    [self printReadingsToView:self.readingsView parentScrollView:self.mainScrollView];
}

- (void)setDateFrom:(NSDate *)dateFrom {
    _dateFrom = dateFrom;
    self.dateFromLabel.text = [TeoriusHelper dateToString:self.dateFrom usingFormat:DATE_FORMAT_STRING];
    self.graphController.dateFrom = _dateFrom;
    
    [self printReadingsToView:self.readingsView parentScrollView:self.mainScrollView];
}

- (void)setDateTo:(NSDate *)dateTo {
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:dateTo];
    components.hour = 23;
    components.minute = 59;
    components.second = 59;
    _dateTo = [[NSCalendar currentCalendar] dateFromComponents:components];
    
    self.dateToLabel.text = [TeoriusHelper dateToString:self.dateTo usingFormat:DATE_FORMAT_STRING];
    self.graphController.dateTo = _dateTo;
    
    [self printReadingsToView:self.readingsView parentScrollView:self.mainScrollView];
}

- (void)setSelectedPlots:(NSArray<NSNumber*>*)selectedPlots {
    _selectedPlots = selectedPlots;
    self.graphController.selectedPlots = _selectedPlots;
    [self.graphController reloadData];
}

- (Measurement*)randomMeasurement {
    if (_randomMeasurement == NULL) {
        _randomMeasurement = [self.measurements firstObject];
    }
    return _randomMeasurement;
}

- (BOOL)isBloodPressure {
    return ([self.randomMeasurement.plotID isEqualToString:@"kSystolicBloodPressure"]) || ([self.randomMeasurement.plotID isEqualToString:@"kDiastolicBloodPressure"]);
}

- (UIView *)graphPointView {
    if (!_graphPointView) {
        _graphPointView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.graphController.symbolSize.width + 1.f + 4.f, self.graphController.symbolSize.width + 1.f + 4.f)];
        _graphPointView.backgroundColor = [UIColor clearColor];
        _graphPointView.layer.cornerRadius = _graphPointView.width / 2.f;
        _graphPointView.layer.borderWidth = 1.f;
        
        [self.graphView addSubview:_graphPointView];
    }
    
    return _graphPointView;
}

- (void)setGraphPointView:(UIView *)graphPointView {
    if (!graphPointView) {
        [_graphPointView removeFromSuperview];
    }
    
    _graphPointView = graphPointView;
}

- (UIView *)graphLineView {
    if (!_graphLineView) {
        CGPoint convertedPoint = [self.graphView convertPoint:CGPointZero fromView:self.graphHostingView];
        
        _graphLineView = [[UIView alloc] initWithFrame:CGRectMake(convertedPoint.x, convertedPoint.y, .5f, self.graphHostingView.height)];
        _graphLineView.backgroundColor = COLOR_GREEN1;
        _graphLineView.alpha = .4f;
        
        [self.graphView addSubview:_graphLineView];
    }
    
    return _graphLineView;
}

- (void)setGraphLineView:(UIView *)graphLineView {
    if (!graphLineView) {
        [_graphLineView removeFromSuperview];
    }
    
    _graphLineView = graphLineView;
}

- (GraphPointInfoView *)graphPointInfoView {
    if (!_graphPointInfoView) {
        _graphPointInfoView = [GraphPointInfoView new];
        _graphPointInfoView.titleLabel.font = [SharedClass getBoldFontWithSize:SCALED_F(12.f)];
        _graphPointInfoView.leftDetailLabel.font = [SharedClass getRegularFontWithSize:10.f];
        _graphPointInfoView.rightDetailLabel.font = [SharedClass getRegularFontWithSize:10.f];
        _graphPointInfoView.layer.borderColor = [UIColor blackColor].CGColor;
        _graphPointInfoView.layer.borderWidth = .5f;
        
        [self.graphView addSubview:_graphPointInfoView];
    }
    
    return _graphPointInfoView;
}

- (void)setGraphPointInfoView:(GraphPointInfoView *)graphPointInfoView {
    if (!graphPointInfoView) {
        [_graphPointInfoView removeFromSuperview];
    }
    
    _graphPointInfoView = graphPointInfoView;
}


#pragma mark - Gesture recognizer delegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    if (gestureRecognizer.view == self.graphHostingView && [gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]) {
        if ([otherGestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]) {
            return NO;
        }
    }
    
    return YES;
}

#pragma mark - helpers

- (NSArray<NSNumber*>*)getSelectedPlots {
    NSMutableArray<NSNumber*>* selectedPlots = [NSMutableArray array];
    for (UIButton* b in self.plotButtons)
        [selectedPlots addObject:[NSNumber numberWithBool:b.selected]];
    return [selectedPlots copy];
}

- (NSArray *)getReadingsForGraphByName:(NSString *)str {
    NSPredicate *predicate;
    
    predicate = [NSPredicate predicateWithBlock:^BOOL(id  _Nonnull evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
        Reading *reading = evaluatedObject;
        
        
        if ([reading.measurement.name containsString:str]) {
            return YES;
        }
        
        return NO;
    }];
    NSArray *arr = [self.readings filteredArrayUsingPredicate:predicate];
    return arr;
}

- (CGFloat)floatByRounding:(CGFloat)floatValue {
    CGFloat fraction = floatValue - floorf(floatValue);

    if (fraction == .5f || fraction == 0) {
        return floatValue;
    }
    else if (fraction < .5) {
        return floor(floatValue) + .5f;
    }
    else {
        return ceilf(floatValue);
    }
}


@end
