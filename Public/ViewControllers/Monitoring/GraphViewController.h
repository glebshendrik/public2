//
//  GraphViewController.h
//  Public
//
//  Created by Teoria 5 on 12/09/15.
//  Copyright (c) 2015 Teoria. All rights reserved.
//

#import "HeaderViewController.h"
#import "MeasurementRLM.h"


@interface GraphViewController : HeaderViewController

@property (nonatomic) NSArray *readings;
@property (nonatomic) NSArray *measurements;
@property (nonatomic) NSArray* readsArray;

@end
