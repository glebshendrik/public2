//
//  NewReadingViewController.m
//  Public
//
//  Created by Arslan Zagidullin on 24.09.15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import "NewReadingViewController.h"
#import "MonitoringViewController.h"
#import "GraphViewController.h"
#import "SharedClass.h"
#import "TeoriusHelper+Extension.h"
#import "StandardScrollView.h"
#import "Reading.h"
#import "Measurement.h"
#import "MainAPI.h"

#define NO_PICKER 0
#define DATE_PICKER 1
#define TIME_PICKER 2


@interface NewReadingViewController () <UIPickerViewDelegate, UIPickerViewDataSource>

@property (nonatomic) UIDatePicker* datePicker;
@property (nonatomic) UIDatePicker* timePicker;
@property int activePicker;

@property (nonatomic) UIPickerView* pickerView;

@property (nonatomic) DropDownLabel* dateLabel;
@property (nonatomic) UILabel* timeLabel;
@property (nonatomic) NSDate* selectedDate;
@property (nonatomic) NSDate* selectedTime;

@property (nonatomic) NSArray<NSNumber*>* pickerFirstColumnData;
@property (nonatomic) NSArray<NSNumber*>* pickerSeckondColumnData;
@property (nonatomic) int pickerFirstColumnDefaultRow;
@property (nonatomic) int pickerSecondColumnDefaultRow;

@property (nonatomic) NSArray<UIButton*>* plotButtons;
@property (nonatomic) int selectedPlot;
@property (nonatomic) Measurement* randomMeasurement;
@property (nonatomic) Measurement* selectedMeasurement;
@property (nonatomic) BOOL isBloodPressure;

@end


@implementation NewReadingViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Готово"
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(doneButtonTapped:)];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:nil
                                                                            action:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.selectedMeasurement = self.randomMeasurement;
    [self initPickerData];
    [self configSubviews];
}

- (void)initPickerData {
    if (!self.isBloodPressure) {
        int rangeOfValuesMin = floor(self.randomMeasurement.rangeOfValuesMin);
        int rangeOfValuesMax = floor(self.randomMeasurement.rangeOfValuesMax);
        int firstColumnDefaultValue = floor(self.randomMeasurement.defaultValue);
        NSMutableArray<NSNumber*>* pickerFirstColumnData = [NSMutableArray array];
        for (int i=rangeOfValuesMin; i<=rangeOfValuesMax; i++) {
            [pickerFirstColumnData addObject:[NSNumber numberWithInteger:i]];
            if (firstColumnDefaultValue == i)
                self.pickerFirstColumnDefaultRow = pickerFirstColumnData.count - 1;
        }
        self.pickerFirstColumnData = [pickerFirstColumnData copy];
        
        NSMutableArray<NSNumber*>* pickerSeckondColumnData = [NSMutableArray array];
        for (NSInteger i=0; i<=9; i++)
            [pickerSeckondColumnData addObject:[NSNumber numberWithInteger:i]];
        self.pickerSeckondColumnData = [pickerSeckondColumnData copy];
        self.pickerSecondColumnDefaultRow = 0;
        
        if ([self.randomMeasurement.name containsString:@"Температура"]) {
            self.pickerSecondColumnDefaultRow = 6;
        }
    } else {
        Measurement *systolicBloodPressure = [self measurementsArray][0];
        
        int rangeOfSystolicValuesMin = floor(systolicBloodPressure.rangeOfValuesMin);
        int rangeOfSystolicValuesMax = floor(systolicBloodPressure.rangeOfValuesMax);
        int systolicDefaultValue = floor(systolicBloodPressure.defaultValue);
        NSMutableArray<NSNumber*>* pickerFirstColumnData = [NSMutableArray array];
        for (int i=rangeOfSystolicValuesMin; i<=rangeOfSystolicValuesMax; i++) {
            [pickerFirstColumnData addObject:[NSNumber numberWithInteger:i]];
            if (systolicDefaultValue == i)
                self.pickerFirstColumnDefaultRow = pickerFirstColumnData.count - 1;
        }
        self.pickerFirstColumnData = [pickerFirstColumnData copy];
        
        Measurement *diastolicBloodPressure = [self measurementsArray][1];
        int rangeOfDiastolicValuesMin = floor(diastolicBloodPressure.rangeOfValuesMin);
        int rangeOfDiastolicValuesMax = floor(diastolicBloodPressure.rangeOfValuesMax);
        int diastolicDefaultValue = floor(diastolicBloodPressure.defaultValue);
        NSMutableArray<NSNumber*>* pickerSeckondColumnData = [NSMutableArray array];
        for (int i=rangeOfDiastolicValuesMin; i<=rangeOfDiastolicValuesMax; i++) {
            [pickerSeckondColumnData addObject:[NSNumber numberWithInteger:i]];
            if (diastolicDefaultValue == i)
                self.pickerSecondColumnDefaultRow = pickerSeckondColumnData.count - 1;
        }
        self.pickerSeckondColumnData = [pickerSeckondColumnData copy];
    }
}

- (void)configSubviews {
    UIScrollView* mainScrollView = [[StandardScrollView alloc] initWithParentVeiwFrame:self.view.frame];
    [self.view addSubview:mainScrollView];
    
    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped)]];
    
    int plotButtonsCount = (int)self.measurementsArray.count;
    
    const float kPlotButtonBarHeight = 45;
    float graphViewHeight = (plotButtonsCount >= 2 && !self.isBloodPressure) ? 340 : 340-kPlotButtonBarHeight;
    UIView* graphView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, graphViewHeight)];
    graphView.backgroundColor = COLOR_DARKGREENISHGRAY;
    [mainScrollView addSubview:graphView];
    
    float lastY = 10;
    
    if (plotButtonsCount >= 2 && !self.isBloodPressure) {
        NSMutableArray<UIButton*>* plotButtons = [NSMutableArray array];
        
        UIFont* font = [SharedClass getRegularFontWithSize:12];
        float lastX = 0;
        float buttonHeight = 0;
        
        for (int i=0; i<plotButtonsCount; i++) {
            Measurement *m = self.measurementsArray[i];
            UIColor* normalColor = COLOR_LIGHTGRAY;
            UIColor* selectedColor = [SharedClass getColorOfPlotNumber:i];
            
            UIButton* plotButton = [UIButton buttonWithType:UIButtonTypeCustom];
            plotButton.titleLabel.font = font;
            [plotButton setTitle:m.shortName forState:UIControlStateNormal];
            [plotButton setTitleColor:normalColor forState:UIControlStateNormal];
            [plotButton setTitleColor:[normalColor colorWithAlphaComponent:0.3] forState:UIControlStateHighlighted];
            [plotButton setTitleColor:selectedColor forState:UIControlStateSelected];
            [plotButton sizeToFit]; 
            plotButton.x = lastX;
            
            [mainScrollView addSubview:plotButton];
            
            plotButton.tag = i;
            [plotButton addTarget:self action:@selector(plotButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
            
            [plotButtons addObject:plotButton];
            
            lastX = plotButton.maxX + SCALED_F(25.f);
            buttonHeight = plotButton.height;
        }
        
        [TeoriusHelper placeViewsHorizontally:plotButtons inSuperview:mainScrollView toCenterPoint:CGPointMake(mainScrollView.halfWidth, lastY + buttonHeight / 2.f)];

        lastY += kPlotButtonBarHeight;
        
        self.plotButtons = [plotButtons copy];
        
        self.selectedPlot = 0;
        self.plotButtons[self.selectedPlot].selected = YES;
    }
    
    CGSize size = [@"BLA-BLA" sizeWithAttributes:@{NSFontAttributeName: [SharedClass getRegularFontWithSize:14]}];
    NSDate* now = [NSDate date];
    self.selectedDate = now;
    self.selectedTime = now;
    
    CGRect timeLabelRect = [TeoriusHelper scaledRect:CGRectMake(LOGICAL_WIDTH/2-8-70, lastY, 70, size.height + 1)];
    UILabel* timeLabel = [[UILabel alloc]initWithFrame:timeLabelRect];
    timeLabel.text = [TeoriusHelper dateToString:self.selectedTime usingFormat:@"H:mm"];
    timeLabel.font = [SharedClass getRegularFontWithSize:14];
    timeLabel.textColor = COLOR_LIGHTERGREEN;
    timeLabel.textAlignment = NSTextAlignmentCenter;
    timeLabel.userInteractionEnabled = YES;
    UITapGestureRecognizer *dateFromLabelTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(timeLabelTapped:)];
    [timeLabel addGestureRecognizer:dateFromLabelTapGesture];
    [graphView addSubview:timeLabel];
    self.timeLabel = timeLabel;
    
    UIImageView* dropDownArrowImgForDate = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"DropDownArrow"]];
    dropDownArrowImgForDate.frame = [TeoriusHelper scaledRect:CGRectMake(58, 7, 11, 11)];
    [timeLabel addSubview:dropDownArrowImgForDate];
    
    CGRect dateLabelRect = [TeoriusHelper scaledRect:CGRectMake(LOGICAL_WIDTH/2+8, lastY, 100, size.height+1)];
    NSString* dateLabelText = [TeoriusHelper dateToString:self.selectedDate usingFormat:@"dd.MM.yy"];
    DropDownLabel *dateLabel = [SharedClass dropDownLabelWithFrame:dateLabelRect text:dateLabelText];
    dateLabel.userInteractionEnabled = YES;
    UITapGestureRecognizer *intervalLabelTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dateLabelTapped:)];
    [dateLabel addGestureRecognizer:intervalLabelTapGesture];
    [graphView addSubview:dateLabel];
    self.dateLabel = dateLabel;
    
    CGRect pickerFrame = [TeoriusHelper scaledRect:CGRectMake(0, CGRectGetMaxY(dateLabel.frame)/SCREEN_SCALE + 5, LOGICAL_WIDTH, DATEPICKER_HEIGHT)];
    UIPickerView* pickerView = [[UIPickerView alloc] initWithFrame:pickerFrame];
    pickerView.backgroundColor = COLOR_DARKGREENISHGRAY;
    pickerView.delegate = self;
    pickerView.dataSource = self;
    [graphView addSubview:pickerView];
    self.pickerView = pickerView;
    [self setDefaultValuesOfPickerView:pickerView];
    
    [self setDefaultValuesOfPickerView:pickerView];
    [pickerView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped)]];
    
    NSString* unitString = self.randomMeasurement.unitName;
    
    CGRect unitLabelRect = [TeoriusHelper scaledRectFromCenter:CGPointMake(LOGICAL_WIDTH/2+4, CGRectGetMaxY(pickerFrame)/SCREEN_SCALE + 15) size:CGSizeMake(LOGICAL_WIDTH, 20)];
    UILabel *unitLabel = [[UILabel alloc]initWithFrame:unitLabelRect];
    unitLabel.text = unitString;
    unitLabel.font = [SharedClass getRegularFontWithSize:14];
    unitLabel.textColor = [UIColor whiteColor];
    unitLabel.textAlignment = NSTextAlignmentCenter;
    [graphView addSubview:unitLabel];
}

- (void)setDefaultValuesOfPickerView:(UIPickerView*)pickerView {
    [pickerView selectRow:self.pickerFirstColumnDefaultRow inComponent:0 animated:NO];
    if (pickerView.numberOfComponents > 1)
        [pickerView selectRow:self.pickerSecondColumnDefaultRow inComponent:1 animated:NO];
}

#pragma mark - pickerView dataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    if (self.isBloodPressure)
        return 2;
    return 1 + self.randomMeasurement.decimalDigits;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [[self getDataForColumn:component] count];
}

//- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
//    return self.pickerData[row];
//}

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSNumber* value = [[self getDataForColumn:component] objectAtIndex:row];
    NSString* title = [NSString stringWithFormat:@"%@", value];
    NSAttributedString *attString = [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    return attString;
}

- (NSArray<NSNumber*>*)getDataForColumn:(NSInteger)column {
    if (column == 0)
        return self.pickerFirstColumnData;
    return self.pickerSeckondColumnData;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    if (self.isBloodPressure)
        return 100.0f;
    return 55.0f;
}

#pragma mark - pickerView delegate

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
//    NSLog(@"selected %@", [self getDataForColumn:component][row]);
}

#pragma mark - user actions

- (void)timeLabelTapped:(UITapGestureRecognizer*)tapGestureRecognizer {
    if (self.activePicker == TIME_PICKER)
        return;
    [self viewTapped];
    
    self.timePicker.frame = [TeoriusHelper scaledRect:CGRectMake(0, LOGICAL_HEIGHT, LOGICAL_WIDTH, DATEPICKER_HEIGHT)];
    self.timePicker.alpha = 0;
//    self.timePicker.maximumDate = [NSDate date];
    self.timePicker.date = [NSDate date];
    [UIView animateWithDuration:DATEPICKER_ANIM_DURATION animations:^{
        self.timePicker.frame = [TeoriusHelper scaledRect:CGRectMake(0, LOGICAL_HEIGHT-DATEPICKER_HEIGHT, LOGICAL_WIDTH, DATEPICKER_HEIGHT)];
        self.timePicker.alpha = 1;
    }];
    self.activePicker = TIME_PICKER;
}

- (void)dateLabelTapped:(UITapGestureRecognizer*)tapGestureRecognizer {
    if (self.activePicker == DATE_PICKER)
        return;
    [self viewTapped];
    
    self.datePicker.frame = [TeoriusHelper scaledRect:CGRectMake(0, LOGICAL_HEIGHT, LOGICAL_WIDTH, DATEPICKER_HEIGHT)];
    self.datePicker.alpha = 0;
    self.datePicker.date = [NSDate date];
    self.datePicker.maximumDate = [NSDate date];
    [UIView animateWithDuration:DATEPICKER_ANIM_DURATION animations:^{
        self.datePicker.frame = [TeoriusHelper scaledRect:CGRectMake(0, LOGICAL_HEIGHT-DATEPICKER_HEIGHT, LOGICAL_WIDTH, DATEPICKER_HEIGHT)];
        self.datePicker.alpha = 1;
    }];
    self.activePicker = DATE_PICKER;
}

- (void)viewTapped {
    if (self.activePicker != NO_PICKER) {
        UIDatePicker* datePicker = (self.activePicker == TIME_PICKER) ? self.timePicker : self.datePicker;
        
        [UIView animateWithDuration:DATEPICKER_ANIM_DURATION animations:^{
            datePicker.frame = [TeoriusHelper scaledRect:CGRectMake(0, LOGICAL_HEIGHT, LOGICAL_WIDTH, DATEPICKER_HEIGHT)];
            datePicker.alpha = 0;
        }];
        self.activePicker = NO_PICKER;
    }
}

- (void)datePickerDateDidChanged:(UIDatePicker *)datePicker {    
    NSDate *date = datePicker.date;
    
    if (self.activePicker == TIME_PICKER) {
        self.selectedTime = date;
        self.timeLabel.text = [TeoriusHelper dateToString:self.selectedTime usingFormat:@"H:mm"];
    }
    else if (self.activePicker == DATE_PICKER) {
        self.selectedDate = date;
        [self.dateLabel setNewText:[TeoriusHelper dateToString:self.selectedDate usingFormat:@"dd.MM.yy"]];
    }
}

- (void)doneButtonTapped:(id)sender {
    
    [[TeoriusHelper instance]addWaitingViewWithText:@"Сохранение..." font:[SharedClass getRegularFontWithSize:14] toView:self.navigationController.view disablingInteractionEvents:YES];
    
    NSCalendar *calenderForTime = [NSCalendar currentCalendar];
    NSDateComponents *timeComponents = [calenderForTime components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:self.selectedTime];
    NSInteger hour = [timeComponents hour];
    NSInteger minute = [timeComponents minute];
    
    NSCalendar *calenderForDate = [NSCalendar currentCalendar];
    NSDateComponents *dateComponents = [calenderForDate components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:self.selectedDate];
    NSInteger year = [dateComponents year];
    NSInteger month = [dateComponents month];
    NSInteger day = [dateComponents day];
    
    NSString* readingDateString = [NSString stringWithFormat:@"%d-%d-%d %d:%d:00", year, month, day, hour, minute];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *readingDate = [dateFormatter dateFromString:readingDateString];
    
    float firstComponentValue = [self.pickerFirstColumnData[[self.pickerView selectedRowInComponent:0]] floatValue];
    float secondComponentValue = ([self.pickerView numberOfComponents] > 1) ? [self.pickerSeckondColumnData[[self.pickerView selectedRowInComponent:1]] floatValue] : 0;

    if (!self.isBloodPressure) {
        secondComponentValue /= 10;
        float value = firstComponentValue + secondComponentValue;
        NSString *val = [NSString stringWithFormat:@"%f", value];
        NSArray *valArr = @[val];
        
        [[MainAPI defaultAPI]newReading:self.selectedMeasurement date:readingDate value:valArr completion:^(BOOL success, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (error) {
                    [TeoriusHelper showStandartAlertInMainThreadWithTitle:@"Ошибка" body:error.localizedDescription];
                }
                [[TeoriusHelper instance]removeWaitingView];
                [self.navigationController popViewControllerAnimated:YES];
            });
        }];
    }
    else {
        
        float systolicBloodPressureValue = firstComponentValue;
//            readingDate
//            systolicBloodPressureValue
        
        float diastolicBloodPressureValue = secondComponentValue;
        
//            diastolicBloodPressureValue
//            readingDate
        NSArray *valArr = @[@(firstComponentValue), @(secondComponentValue)];

        
        [[MainAPI defaultAPI]newReading:self.selectedMeasurement date:readingDate value:valArr completion:^(BOOL success, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (error) {
                    [TeoriusHelper showStandartAlertInMainThreadWithTitle:@"Ошибка" body:error.localizedDescription];
                }
                
                [[TeoriusHelper instance]removeWaitingView];
                [self.navigationController popViewControllerAnimated:YES];
            });
        }];
        
    }
    
    
//    UIViewController* vc = [[MonitoringViewController alloc] initWithTitle:@"" menuButtonVisible:NO];
    
    
//    if (self.afterDone)
//        self.afterDone();
}

- (void)plotButtonPressed:(UIButton *)sender {
    UIButton* b = sender;
    for (UIButton* otherButton in self.plotButtons)
        otherButton.selected = NO;
    b.selected = YES;
    self.selectedPlot = (int)b.tag;
    self.selectedMeasurement = self.measurementsArray[b.tag];
}

#pragma mark - properties

- (UIDatePicker*)datePicker {
    if (_datePicker == NULL) {
        CGRect pickerFrame = [TeoriusHelper scaledRect:CGRectMake(0, LOGICAL_HEIGHT, LOGICAL_WIDTH, DATEPICKER_HEIGHT)];
        _datePicker = [[UIDatePicker alloc] initWithFrame:pickerFrame];
        _datePicker.datePickerMode = UIDatePickerModeDate;
        _datePicker.backgroundColor = [UIColor whiteColor];
        _datePicker.datePickerMode = UIDatePickerModeDate;
        _datePicker.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"ru_RU"];
        [_datePicker addTarget:self action:@selector(datePickerDateDidChanged:) forControlEvents:UIControlEventValueChanged];
        [self.view addSubview:_datePicker];
    }
    return _datePicker;
}

- (UIDatePicker*)timePicker {
    if (_timePicker == NULL) {
        CGRect pickerFrame = [TeoriusHelper scaledRect:CGRectMake(0, LOGICAL_HEIGHT, LOGICAL_WIDTH, DATEPICKER_HEIGHT)];
        _timePicker = [[UIDatePicker alloc] initWithFrame:pickerFrame];
        _timePicker.datePickerMode = UIDatePickerModeTime;
        _timePicker.backgroundColor = [UIColor whiteColor];
        _timePicker.datePickerMode = UIDatePickerModeTime;
        _timePicker.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"ru_RU"];
        [_timePicker addTarget:self action:@selector(datePickerDateDidChanged:) forControlEvents:UIControlEventValueChanged];
        
        [self.view addSubview:_timePicker];
    }
    return _timePicker;
}

- (Measurement*)randomMeasurement {
    if (_randomMeasurement == NULL) {
        _randomMeasurement = [self.measurementsArray firstObject];
    }
    return _randomMeasurement;
}

- (BOOL)isBloodPressure {
    return ([self.randomMeasurement.plotID isEqualToString:@"kSystolicBloodPressure"]) || ([self.randomMeasurement.plotID isEqualToString:@"kDiastolicBloodPressure"]);
}

@end
