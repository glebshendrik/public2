//
//  NewReadingViewController.h
//  Public
//
//  Created by Arslan Zagidullin on 24.09.15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import "HeaderViewController.h"
#import "MeasurementRLM.h"


typedef void (^AfterDoneBlock)(void);


@interface NewReadingViewController : HeaderViewController

@property (nonatomic) NSArray* measurementsArray;
@property (nonatomic) NSArray* readsArray;
@property (nonatomic, strong) AfterDoneBlock afterDone;
@property (nonatomic) NSString *plotID;

@end
