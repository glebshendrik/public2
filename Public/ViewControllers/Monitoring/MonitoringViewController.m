//
//  MonitoringViewController.m
//  Public
//
//  Created by Teoria 5 on 03/09/15.
//  Copyright (c) 2015 Teoria. All rights reserved.
//

#import "MonitoringViewController.h"
#import "NewReadingViewController.h"
#import "GraphViewController.h"
#import "SharedClass.h"
#import "StandardScrollView.h"
#import "GraphController.h"
#import "User.h"
#import "MainAPI.h"
#import "Reading.h"

#define GRAPH_PULSE 0
#define GRAPH_TEMPERATURE 1
#define GRAPH_PRESSURE 2
#define GRAPH_HEIGHT 3
#define GRAPH_WEIGHT 4
#define GRAPH_SUGAR 5
#define GRAPH_CHOLESTEROL 6
#define GRAPH_VOLUMES 7


@interface MonitoringViewController () {
    MainAPI *mainAPI;
}

@property (nonatomic) User *user;
@property (nonatomic) NSArray<NSString*>* graphNames;
@property (nonatomic) NSArray *arrRead;
@property (nonatomic) NSArray *arrMeas;
@end


@implementation MonitoringViewController

#pragma mark - View's lifecycle

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:nil
                                                                            action:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (!self.isMovingToParentViewController) {
        self.arrRead = [[MainAPI defaultAPI]myReadingsArr];
        [self reloadSubviews];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    mainAPI = [MainAPI defaultAPI];
    self.graphNames = @[@"Пульс", @"Температура", @"Давление", @"Рост", @"Вес", @"Сахар", @"Холестерин"];
    self.user = [MainAPI defaultAPI].currentUser;
    
    if (!self.user) {
        [[TeoriusHelper instance] addWaitingViewWithText:@"Обновление..." font: [SharedClass getRegularFontWithSize:14] toView:self.navigationController.view disablingInteractionEvents:YES];
        
        self.user = [MainAPI defaultAPI].currentUser;

        [self getAllUsers];
    }
    else {
        [[TeoriusHelper instance] addWaitingViewWithText:@"Обновление..." font: [SharedClass getRegularFontWithSize:14] toView:self.navigationController.view disablingInteractionEvents:YES];
        [self getAllMeasurements];
    }
}


#pragma mark - Updating data

- (void)getAllUsers {
    [[MainAPI defaultAPI]getUserCitizen:^(NSArray *arr, NSError *err) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (err) {
                [[TeoriusHelper instance] removeWaitingView];
                [TeoriusHelper showStandartAlertInMainThreadWithTitle:@"Ошибка" body:err.localizedDescription];
            }
            else {
                [self getAllMeasurements];
            }
        });
    }];
}

- (void)getAllMeasurements {
    [[MainAPI defaultAPI] getAllMeasure:self.user completion:^(NSDictionary *dic, NSError *err) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.arrRead = [[MainAPI defaultAPI]myReadingsArr];
//            for (int i =0; i<self.arrRead.count; i++) {
//                Reading *r = self.arrRead[i];
//                NSLog(@"measure: %@, readingID: %@, value: %f", r.measurement.plotID, r.ID, r.value);
//            }
            self.arrMeas = [MainAPI defaultAPI].measurementsArray;

            [[TeoriusHelper instance] removeWaitingView];
            
            if (err) {
                [TeoriusHelper showStandartAlertInMainThreadWithTitle:@"Ошибка" body:err.localizedDescription];
            }
            else {
                [self configSubviews];
            }
        });
    }];
}


#pragma mark - Subviews

- (void)configSubviews {
    UIScrollView* mainScrollView = [[StandardScrollView alloc] initWithParentVeiwFrame:self.view.frame];
    [self.view addSubview:mainScrollView];
    
    // @arslan: временная величина
    const BOOL isWidgetsOn = YES;
    
    if (isWidgetsOn) {
        CGRect rankLabelRect = [TeoriusHelper scaledRect:CGRectMake(LOGICAL_WIDTH/2, 0, 100, 40)];
        UILabel *rankLabel = [[UILabel alloc]initWithFrame:rankLabelRect];
        rankLabel.font = [SharedClass getBoldFontWithSize:14];
        rankLabel.textColor = COLOR_DARKGRAY;
        rankLabel.text = @"8/10";
        [mainScrollView addSubview:rankLabel];
        
        UIImage* greenCircle = [UIImage imageNamed:@"GreenCircle"];
        UIImageView* greenCircleImageView = [[UIImageView alloc] initWithImage:greenCircle];
        greenCircleImageView.frame = [TeoriusHelper scaledRect:CGRectMake(-35, 8, 24, 24)];
        [rankLabel addSubview:greenCircleImageView];
        
        CGRect statusTextLabel = [TeoriusHelper scaledRectFromCenter:CGPointMake(LOGICAL_WIDTH/2, 40) size:CGSizeMake(LOGICAL_WIDTH-35, 0)];
        
        NSMutableParagraphStyle *parStyle = [NSMutableParagraphStyle new];
        parStyle.alignment = NSTextAlignmentCenter;
        parStyle.lineHeightMultiple = 1.2f;
        
        NSString* text = @"В целом, все показатели в норме. Обратите внимание на давление, возможно, нужно обратиться к врачу.";
        NSAttributedString *attrText = [[NSAttributedString alloc] initWithString:text
                                                                         attributes:@{NSParagraphStyleAttributeName: parStyle,
                                                                                      NSFontAttributeName: [SharedClass getRegularFontWithSize:10.5],
                                                                                      NSForegroundColorAttributeName: COLOR_DARKGRAY}];
        UILabel *infoLabel = [[UILabel alloc]initWithFrame:statusTextLabel];
        infoLabel.attributedText = attrText;
        infoLabel.numberOfLines = 0;
        [infoLabel sizeToFit];
        [mainScrollView addSubview:infoLabel];
        
    } else {
        CGRect infoLabelRect = [TeoriusHelper scaledRect:CGRectMake(0, 20, 0, 0)];
        UILabel *infoLabel = [[UILabel alloc]initWithFrame:infoLabelRect];
        infoLabel.font = [SharedClass getRegularFont];
        infoLabel.textColor = [SharedClass getGrayColor];
        infoLabel.text = @"Добавьте свой первый виджет:";
        [infoLabel sizeToFit];
        infoLabel.center = CGPointMake(self.view.frame.size.width / 2.f, infoLabel.center.y);
        [mainScrollView addSubview:infoLabel];
    }
    
    CGFloat dy = [TeoriusHelper scaledFloat:15];
    CGFloat width = 120;
    CGFloat height = 40;
    
    CGFloat lastY = 95;

    for (int i = 0; i < self.graphNames.count; i++) {
        NSString *graphName = self.graphNames[i];
        NSArray *measurements = [[MainAPI defaultAPI] getMeasurementsByGraphName:graphName fromMeasurements:self.arrMeas];
        
        NSDictionary *readingsByMeasurements = [mainAPI groupReadings:self.arrRead byMeasurements:measurements];
        
        NSUInteger maxCount = 0;
        
        for (NSArray *readings in readingsByMeasurements.allValues) {
            maxCount = MAX(maxCount, readings.count);
        }

         if (maxCount > 0) {
            const float kGraphViewHeight = 78;
            CGRect graphViewRect = [TeoriusHelper scaledRect:CGRectMake(0, lastY, LOGICAL_WIDTH, kGraphViewHeight)];
            UIView* graphView = [[UIView alloc] initWithFrame:graphViewRect];
            graphView.tag = i;
            graphView.backgroundColor = [UIColor whiteColor];
            [mainScrollView addSubview:graphView];
            
            [graphView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped:)]];
            
            CGRect titleLabelRect = [TeoriusHelper scaledRect:CGRectMake(15, kGraphViewHeight/2-40/2-1, 120, 40)];
            UILabel *titleLabel = [[UILabel alloc]initWithFrame:titleLabelRect];
            titleLabel.font = [SharedClass getBoldFontWithSize:14];
            titleLabel.textColor = COLOR_LIGHTERGREEN;
            titleLabel.text = graphName;
            [graphView addSubview:titleLabel];
            
             if (maxCount > 1) {
                 NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:YES];
                 
                 // calculate minDate and maxDate
                 NSDate *maxDate, *minDate;
                 NSMutableArray *minDatesArray = [[NSMutableArray alloc] initWithCapacity:readingsByMeasurements.count];
                 NSMutableArray *allReadingsArray = [NSMutableArray new];
                 
                 for (NSString *plotID in readingsByMeasurements.allKeys) {
                     NSArray *readings = readingsByMeasurements[plotID];
                     NSArray *sortedReadings = [readings sortedArrayUsingDescriptors:@[descriptor]];
                     
                     if (sortedReadings.count > 10) {
                         sortedReadings = [sortedReadings subarrayWithRange:NSMakeRange(0, 10)];
                     }
                     
                     Reading *minReading = sortedReadings.firstObject;
                     Reading *maxReading = sortedReadings.lastObject;
                     
                     if (!maxDate) {
                         maxDate = maxReading.date;
                     }
                     else {
                         maxDate = [maxDate compare:maxReading.date] == NSOrderedAscending ? maxReading.date : maxDate;
                     }
                     
                     [minDatesArray addObject:minReading.date];
                     [allReadingsArray addObjectsFromArray:sortedReadings];
                 }
                 
                 [minDatesArray sortUsingDescriptors:@[[[NSSortDescriptor alloc] initWithKey:@"self" ascending:YES]]];
                 
                 for (NSInteger i = (minDatesArray.count - 2); i >= 0; i--) {
                     NSDate *earlierDate = minDatesArray[i];
                     NSDate *laterDate = minDatesArray[i + 1];
                     
                     NSDateComponents *components = [[NSCalendar currentCalendar] components:NSDayCalendarUnit fromDate:earlierDate toDate:laterDate options:0];
                     if (components.day > 7) {
                         minDate = laterDate;
                         break;
                     }
                 }
                 
                 if (!minDate) {
                     minDate = minDatesArray.firstObject;
                 }
                 
//                 // calculate min and max values on Y axis
//                 CGFloat minValue = CGFLOAT_MAX;
//                 CGFloat maxValue = CGFLOAT_MIN;
//                 
//                 for (Measurement *m in measurements) {
//                     if ([graphName isEqualToString:@"Вес"]) {
//                         minValue = [m.norm minNormForValue:151];
//                         maxValue = [m.norm maxNormForValue:196];
//                     }
//                     else {
//                         minValue = MIN(minValue, [m.norm minNormForValue:0]);
//                         maxValue = MAX(maxValue, [m.norm maxNormForValue:0]);
//                     }
//                     
//                     NSArray *readings = readingsByMeasurements[m.plotID];
//                     
//                     for (Reading *r in readings) {
//                         minValue = MIN(minValue, r.value);
//                         maxValue = MAX(maxValue, r.value);
//                     }
//                 }
                 
                 // creating graph controller
                 GraphController* graphController = [[GraphController alloc] initWithMeasurementsArray:measurements readingsArray:allReadingsArray dateFrom:minDate dateTo:maxDate type:GraphControllerTypeWidget];
                 graphController.axesAreVisible = NO;
                 graphController.normalAreaIsVisible = NO;
                 graphController.leftRightPadding = [TeoriusHelper scaledFloat:10];
                 graphController.topBottomPadding = [TeoriusHelper scaledFloat:10];
                 graphController.scaleX = 1.05;
                 graphController.scaleY = 1.15;
                 graphController.symbolSize = [TeoriusHelper scaledSizeWithWidth:4.3 height:4.3];
                 graphController.isInteractive = NO;
                 const float kHostingViewWidth = 210;
                 CGRect hostingViewRect = [TeoriusHelper scaledRect:CGRectMake(LOGICAL_WIDTH-kHostingViewWidth, 0, kHostingViewWidth, kGraphViewHeight)];
                 CPTGraphHostingView* hostingView = [graphController getHostingView:hostingViewRect];
                 [graphView addSubview:hostingView];
             }
             else {
                 UILabel *messageLabel = [UILabel new];
                 messageLabel.font = [SharedClass getRegularFontWithSize:14.f];
                 messageLabel.textColor = COLOR_LIGHTGRAY;
                 messageLabel.text = @"Недостаточно измерений";
                 [messageLabel sizeToFit];
                 messageLabel.center = CGPointMake(graphView.width - SCALED_F(15.f) - messageLabel.halfWidth, titleLabel.center.y);
                 [graphView addSubview:messageLabel];
             }
             
             lastY = CGRectGetMaxY(graphView.frame)/SCREEN_SCALE + 1;
        }
    }
    for (int i = 0; i < self.graphNames.count; i++) {
        if ([self getReadingsForGraphByName:self.graphNames[i]].count < 1) {
            CGRect monitoringButtonFrame = [TeoriusHelper scaledRectFromCenter:CGPointMake(LOGICAL_WIDTH/2.f, lastY+height) size:CGSizeMake(width, height)];
            UIButton* monitoringButton = [SharedClass monitoringButtonWithFrame:monitoringButtonFrame text:self.graphNames[i]];
            monitoringButton.tag = i;
            [monitoringButton addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
            [mainScrollView addSubview:monitoringButton];
            lastY += (dy + height)/SCREEN_SCALE;
        }
    }
    
    mainScrollView.contentSize = [TeoriusHelper scaledSizeWithWidth:LOGICAL_WIDTH height:lastY+20];
}

- (void)reloadSubviews {
    [self.view.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    [self configSubviews];
}

#pragma mark - user actions 

- (void)buttonPressed:(UIButton *)sender {
    NSString *name = self.graphNames[sender.tag];
    
    NewReadingViewController *vc = [[NewReadingViewController alloc]initWithTitle:self.title menuButtonVisible:NO];
    NSArray *measArr = [[MainAPI defaultAPI] getMeasurementsByGraphName:name fromMeasurements:self.arrMeas];
    vc.measurementsArray = measArr;
    vc.plotID = name;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)viewTapped:(UITapGestureRecognizer*)gestrueRecognizer {
    UIView *graphView = gestrueRecognizer.view;
    [self goToGraphViewController:graphView.tag];
}

- (void)goToGraphViewController:(NSInteger)graph {
    NSString *name = self.graphNames[graph];
    
    GraphViewController *vc = [[GraphViewController alloc]initWithTitle:name menuButtonVisible:NO];
    NSArray *measArr = [self getReadingsForGraphByName:name];
    vc.readings = measArr;
    vc.measurements = [[MainAPI defaultAPI] getMeasurementsByGraphName:name fromMeasurements:self.arrMeas];
    vc.readsArray = [self getReadingsForGraphByName:name];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - helpers

- (NSArray *)getReadingsForGraphByName:(NSString *)str {
    return [[MainAPI defaultAPI] getReadingsForGraphByName:str fromReadings:self.arrRead];
}

@end
