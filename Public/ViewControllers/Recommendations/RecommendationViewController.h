//
//  RecommendationViewController.h
//  Public
//
//  Created by Teoria 5 on 10/09/15.
//  Copyright (c) 2015 Teoria. All rights reserved.
//

#import "HeaderViewController.h"
#import "Recommendation.h"

@interface RecommendationViewController : HeaderViewController

- (instancetype)initWithTitle:(NSString *)title menuButtonVisible:(BOOL)isVisible withRecommendation:(Recommendation *)recommend;

@end
