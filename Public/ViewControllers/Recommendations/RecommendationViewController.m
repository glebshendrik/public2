//
//  RecommendationViewController.m
//  Public
//
//  Created by Teoria 5 on 10/09/15.
//  Copyright (c) 2015 Teoria. All rights reserved.
//

#import "RecommendationViewController.h"
#import "Recommendation.h"
#import "SharedClass.h"

@interface RecommendationViewController () <UIActionSheetDelegate> {
    UIScrollView *scrollView;
    Recommendation *_recommend;
}

@end

@implementation RecommendationViewController

#pragma mark - init

- (instancetype)initWithTitle:(NSString *)title menuButtonVisible:(BOOL)isVisible withRecommendation:(Recommendation *)recommend {
    
    self = [super initWithTitle:recommend.title menuButtonVisible:isVisible];
    
    if (!self) {
        return nil;
    }
    
    _recommend = recommend;
    
    return self;
}

#pragma mark - View's lifecycle

- (void)loadView {
    [super loadView];
    
    [self configSubviews];
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)configSubviews {
    UITextView *textView = [[UITextView alloc]initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height - 64)];
    textView.backgroundColor = [UIColor whiteColor];
    CGFloat inset = [TeoriusHelper scaledFloat:20];
    textView.textContainerInset = UIEdgeInsetsMake(inset, inset, inset, inset);
    textView.contentInset = UIEdgeInsetsMake(-64, 0, 0, 0);
    textView.showsVerticalScrollIndicator = NO;
    UIFont *font = [SharedClass getRegularFontWithSize:14.0f];
    textView.font = font;
    textView.editable = NO;
    textView.text = _recommend.fullText;
    textView.textColor = [UIColor blackColor];
    [self.view addSubview:textView];
    
//    scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height)];
//    scrollView.backgroundColor = [UIColor whiteColor];
//    scrollView.showsVerticalScrollIndicator = NO;
//    [self.view addSubview:scrollView];
//    
//    CGFloat dx = 15.f;
//    CGFloat dy = 20.f;
//    
//    CGRect rect = CGRectMake(dx, dy, self.view.frame.size.width - 2 * dx, 0);
//
//    UILabel *textLabel = [[UILabel alloc]initWithFrame:rect];
//    textLabel.numberOfLines = 0;
//    textLabel.font = font;
//    textLabel.text = _recommend.fullText;
//    textLabel.textColor = [UIColor blackColor];//UIColorFromRGB(0x98999A);
//    [textLabel sizeToFit];
//    [scrollView addSubview:textLabel];
//    
//    scrollView.contentSize = CGSizeMake(self.view.frame.size.width, CGRectGetMaxY(textLabel.frame) + dy + 64);
//    scrollView.contentInset = UIEdgeInsetsMake(-64, 0, 0, 0);
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"redArrow"] style:UIBarButtonItemStyleBordered target:self action:@selector(showOptions)];
    self.navigationItem.rightBarButtonItem = rightItem;
}

#pragma mark - user actions

- (void)showOptions {
    UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"Отменить" destructiveButtonTitle:nil otherButtonTitles:@"Записаться к врачу",@"Не показывать", nil];
    
    [actionSheet showInView:self.view];
}
@end
