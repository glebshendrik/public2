//
//  RecommendationViewController.m
//  Public
//
//  Created by Teoria 5 on 04/09/15.
//  Copyright (c) 2015 Teoria. All rights reserved.
//

#import "RecommendationsListViewController.h"
#import "RecommendationView.h"
#import "RecommendationViewController.h"
#import "SharedClass.h"
#import "Recommendation.h"
#import "StandardScrollView.h"
#import "RecommendationActionSheet.h"
#import "MainAPI.h"
#import "NotificationArticleView.h"

#define RECOMMENDATIONS_LIST_ALL 0
#define RECOMMENDATIONS_LIST_POSTPONED 1
#define RECOMMENDATIONS_LIST_CLOSED 2


@interface RecommendationsListViewController () <RecommendationViewDelegate, UIActionSheetDelegate>

@property (nonatomic) UIScrollView* mainScrollView;
@property (nonatomic) NSArray<UIView*>* recommendationViews;

@property (nonatomic) int selectedRecommendationList;
@property (nonatomic) int scrollViewTop;
@property (nonatomic) NSArray *recommendationsArray;

@end


@implementation RecommendationsListViewController


#pragma mark - View's lifecycle

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:nil
                                                                            action:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configSubviews];
    
}

- (void)configSubviewWithoutRecommendations {
    
    
    
}

- (void)configSubviews {
    const float kSegmentedControlHeight = 26;
    CGRect segmentedControlFrame = [TeoriusHelper scaledRectFromCenter:CGPointMake(LOGICAL_WIDTH/2, HEADER_HEIGHT+20)
                                                                  size:CGSizeMake(260, kSegmentedControlHeight)];
    UISegmentedControl* segmentedControl = [[UISegmentedControl alloc] initWithItems:@[@"Все", @"Отложенные", @"Удаленные"]];
    segmentedControl.frame = segmentedControlFrame;
    segmentedControl.tintColor = COLOR_LIGHTERGREEN;
    segmentedControl.selectedSegmentIndex = RECOMMENDATIONS_LIST_ALL;
    [segmentedControl setTitleTextAttributes:@{NSFontAttributeName: [SharedClass getRegularFontWithSize:10.0f]} forState:UIControlStateNormal];
    [segmentedControl addTarget:self action:@selector(segmentedControlValueChanged:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:segmentedControl];
    
    self.scrollViewTop = HEADER_HEIGHT+kSegmentedControlHeight+20;
    CGRect mainScrollViewFrame = [TeoriusHelper scaledRect:CGRectMake(0, self.scrollViewTop, LOGICAL_WIDTH, SCREEN_HEIGHT-self.scrollViewTop)];
    UIScrollView* mainScrollView = [[StandardScrollView alloc] initWithFrame:mainScrollViewFrame];
    [self.view addSubview:mainScrollView];
    
    self.mainScrollView = mainScrollView;
    [self showRecommendationList:RECOMMENDATIONS_LIST_ALL inParentScrollView:self.mainScrollView];
}

- (void)showRecommendationList:(int)recommendationList inParentScrollView:(UIScrollView*)scrollView {
    [scrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    NSArray* recommendationsArray = [self rcommendataionsArrayForList:recommendationList];
    
    CGFloat dY = self.scrollViewTop;
    CGFloat dX = 10.f;
    
    NSMutableArray<UIView*>* recommendationViews = [NSMutableArray array];
    if (recommendationsArray.count > 0)
        for (int i = 0; i < recommendationsArray.count; i++) {
            Recommendation* r = recommendationsArray[i];
            CGRect recommendationViewFrame = [TeoriusHelper scaledRect:CGRectMake(dX, dY, LOGICAL_WIDTH-2*dX, 0)];
            RecommendationView *recommendationView = [[RecommendationView alloc]initWithFrame:recommendationViewFrame withRecommendation:r];
            recommendationView.delegate = self;
            [scrollView addSubview:recommendationView];
            [recommendationViews addObject:recommendationView];
            
            dY = CGRectGetMaxY(recommendationView.frame)/SCREEN_SCALE + 5;
        }
    else {
        NSString* emptyLabelText;
        switch (recommendationList) {
            case RECOMMENDATIONS_LIST_POSTPONED:
                emptyLabelText = @"У Вас нет отложенных уведомлений";
                break;
            case RECOMMENDATIONS_LIST_CLOSED:
                emptyLabelText = @"У Вас нет удаленных уведомлений";
                break;
            default: //RECOMMENDATIONS_LIST_ALL
                emptyLabelText = @"Пока нет ни одного уведомления\n\nВозможно, вы не ввели достаточно информации в разделе \"Мониторинг\" или личной информации";
                break;
        }
        UIFont *emptyLabelFont = [SharedClass getRegularFontWithSize:14.0f];
        CGRect emptyLabelFrame = CGRectMake(0, dY+10, LOGICAL_WIDTH - 2*dX, 0);
        UILabel *emptyLabel = [[UILabel alloc]initWithFrame:[TeoriusHelper scaledRect:emptyLabelFrame]];
        emptyLabel.text = emptyLabelText;
        emptyLabel.numberOfLines = 0;
        emptyLabel.textAlignment = NSTextAlignmentCenter;
        emptyLabel.textColor = UIColorFromRGB(0x98999A);
        emptyLabel.font = emptyLabelFont;
        [emptyLabel sizeToFit];
        [scrollView addSubview:emptyLabel];
        emptyLabel.center = CGPointMake(scrollView.frame.size.width/2, emptyLabel.center.y);
        
        dY = CGRectGetMaxY(emptyLabel.frame)/SCREEN_SCALE + 5;
    }
    self.recommendationViews = [recommendationViews copy];
    
    scrollView.contentSize = [TeoriusHelper scaledSize:CGSizeMake(LOGICAL_WIDTH, dY)];
}

#pragma mark - RecommendationViewDelegate 

- (void)showPlainRecommendationOptionsForView:(UIView*)recommendationView {
    RecommendationActionSheet *actionSheet = [[RecommendationActionSheet alloc]initWithTitle:nil
                                                                                    delegate:self
                                                                           cancelButtonTitle:@"Отменить"
                                                                      destructiveButtonTitle:nil
                                                                           otherButtonTitles:@"Записаться к врачу", @"Не показывать", nil];
    actionSheet.recommendationView = recommendationView;
    [actionSheet showInView:self.view];
}

- (void)showPollOptionsForView:(UIView *)recommendationView {
    RecommendationActionSheet *actionSheet = [[RecommendationActionSheet alloc]initWithTitle:nil
                                                                                    delegate:self
                                                                           cancelButtonTitle:@"Отменить"
                                                                      destructiveButtonTitle:nil
                                                                           otherButtonTitles:@"Отложить", @"Зачем нам это знать?", nil];
    actionSheet.recommendationView = recommendationView;
    [actionSheet showInView:self.view];
}

- (void)viewTapped:(UIGestureRecognizer *)gestureRecognizer {
    RecommendationView* rv = (RecommendationView*)gestureRecognizer.view;
    Recommendation *rec = rv.recommendation;
    RecommendationViewController *vc = [[RecommendationViewController alloc]initWithTitle:rec.title menuButtonVisible:NO withRecommendation:rec];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)reactToRecommendationClosing:(UIView *)recommendationView {
    if (self.selectedRecommendationList == RECOMMENDATIONS_LIST_CLOSED)
        return;
    
    int index = (int)[self.recommendationViews indexOfObject:recommendationView];
    [UIView animateWithDuration:1 animations:^{
        CGRect recommendationViewFrame = recommendationView.frame;
        int emptySpaceHeight = recommendationViewFrame.size.height + 5;
        recommendationViewFrame.size.height = 0;
        recommendationView.frame = recommendationViewFrame;
        for (int i=index+1; i<self.recommendationViews.count; i++) {
            UIView* underView = self.recommendationViews[i];
            CGRect underViewFrame = underView.frame;
            underViewFrame.origin.y -= emptySpaceHeight;
            underView.frame = underViewFrame;
        }
        if (self.recommendationViews.count > 1)
            self.mainScrollView.contentSize = CGSizeMake(SCREEN_WIDTH, CGRectGetMaxY(self.recommendationViews.lastObject.frame));
    }];
}

#pragma mark - UIActionSheet delegate

- (void)actionSheet:(UIActionSheet*)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    RecommendationActionSheet* ras = (RecommendationActionSheet*)actionSheet;
    RecommendationView* rv = (RecommendationView*)ras.recommendationView;
    Recommendation* r = rv.recommendation;
    
    if (buttonIndex == 0) {
        if (r.type == RECOMMENDATION_TYPE_PLAIN)
            NSLog(@"Записаться к врачу");
        else if (r.type == RECOMMENDATION_TYPE_BIRTHDAY || r.type == RECOMMENDATION_TYPE_BAD_HABITS) {
            RLMRealm* realm = [RLMRealm defaultRealm];
            [realm beginWriteTransaction]; {
                r.isNew = NO;
                r.isPostponed = YES;
            }
            [realm commitWriteTransaction];
            [self reactToRecommendationPostponing:rv];
        }
    }
    else if(buttonIndex == 1) {
        if (r.type == RECOMMENDATION_TYPE_PLAIN) {
            RLMRealm* realm = [RLMRealm defaultRealm];
            [realm beginWriteTransaction]; {
                r.isNew = NO;
                r.isClosed = YES;
            }
            [realm commitWriteTransaction];
            [self reactToRecommendationClosing:rv];
        }
        else if (r.type == RECOMMENDATION_TYPE_BIRTHDAY || r.type == RECOMMENDATION_TYPE_BAD_HABITS)
            NSLog(@"Зачем нам это знать?");
    }
}

#pragma mark - user actions

- (void)segmentedControlValueChanged:(UISegmentedControl*)segmentedControl {
    [self showRecommendationList:(int)segmentedControl.selectedSegmentIndex inParentScrollView:self.mainScrollView];
}

#pragma mark - helpers

- (NSArray*)rcommendataionsArrayForList:(int)recommendationsList {
    self.selectedRecommendationList = recommendationsList;
//    NSPredicate *predicate;
    NSArray *filteredRecommendArr;
    
//    switch (recommendationsList) {
//        case RECOMMENDATIONS_LIST_POSTPONED:
//            self.recommendationsArray = [Recommendation objectsWithPredicate:[NSPredicate predicateWithFormat:@"isPostponed = 1 && isPassed = 0"]];
//            break;
//        case RECOMMENDATIONS_LIST_CLOSED:
//            self.recommendationsArray = [Recommendation objectsWithPredicate:[NSPredicate predicateWithFormat:@"isClosed = 1"]];
//            break;
//        default: //RECOMMENDATIONS_LIST_ALL
//            predicate = [NSPredicate predicateWithBlock:^BOOL(id  _Nonnull evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
//                Reading *reading = evaluatedObject;
//                
//                if ([reading.measurement.name containsString:@""]) {
//                    return YES;
//                }
//                
//                return NO;
//                return YES;
//            }];
//            filteredRecommendArr = [self.recommendationsArray filteredArrayUsingPredicate:predicate].copy;
//            break;
//    }
    
    
    filteredRecommendArr = self.recommendationsArray.copy;
    return filteredRecommendArr;
}

- (void)reactToRecommendationPostponing:(UIView *)recommendationView {
    if (self.selectedRecommendationList == RECOMMENDATIONS_LIST_POSTPONED)
        return;
    
    RecommendationView* rv = (RecommendationView*)recommendationView;
    [rv reactToRecommendationPostponing];
}

@end
