//
//  FamilyViewController.h
//  Public
//
//  Created by Arslan Zagidullin on 06/10/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import "HeaderViewController.h"
#import "FamilyRLM.h"

@protocol FamilyViewControllerDelegate <NSObject>

- (void)openMonitoring;

@end

@interface FamilyViewController : HeaderViewController

@property (nonatomic) FamilyRLM* family;
@property (nonatomic, weak) id <FamilyViewControllerDelegate> familyVCDelegate;

@end
