//
//  RegistrationViewController.m
//  Public
//
//  Created by Teoria 5 on 02/09/15.
//  Copyright (c) 2015 Teoria. All rights reserved.
//

#import "RegistrationViewController.h"
#import "AuthorizationViewController.h"
#import "StandardScrollView.h"
#import "TeoriusHelper+Extension.h"
#import "SharedClass.h"
#import "BaseViewController.h"
#import "RESTAPIManager.h"
#import "MainAPI.h"

@interface RegistrationViewController () <UITextFieldDelegate> {
    UITextField* loginField;
    UITextField* passField;
    BOOL isLogin;
    MainAPI *mainAPI;
}

@end

@implementation RegistrationViewController

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configSubviews];
    
    mainAPI = [MainAPI defaultAPI];

}

- (void)configSubviews {
    UIImageView *backgroundImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    backgroundImage.image = [UIImage imageNamed:@"background"];
    
    [self.view addSubview:backgroundImage];
    
    UIScrollView* mainScrollView = [[StandardScrollView alloc] initWithFrame:self.view.frame];
    [self.view addSubview:mainScrollView];
    
    UIImageView* logoImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"PublicLogo"]];
    logoImageView.center = [TeoriusHelper scaledPoint:CGPointMake(LOGICAL_WIDTH/2, 60)];
    [mainScrollView addSubview:logoImageView];
    
    CGRect blackViewFrame = [TeoriusHelper scaledRect:CGRectMake(0, 100, LOGICAL_WIDTH, 210)];
    UIView* blackView = [[UIView alloc] initWithFrame:blackViewFrame];
    blackView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    [mainScrollView addSubview:blackView];
    
    UIFont* font = [SharedClass getRegularFontWithSize:14];
    const float kTextFieldWidth = 280;
    
    CGRect loginFieldFrame = [TeoriusHelper scaledRectFromCenter:CGPointMake(LOGICAL_WIDTH/2, 55) size:CGSizeMake(kTextFieldWidth, 25)];
    loginField = [[UITextField alloc] initWithFrame:loginFieldFrame];
    loginField.delegate = self;
    loginField.keyboardType = UIKeyboardTypeEmailAddress;
    loginField.returnKeyType = UIReturnKeyNext;
    loginField.textAlignment = NSTextAlignmentCenter;
    loginField.font = font;
    loginField.textColor = [UIColor whiteColor];
    loginField.tintColor = [UIColor whiteColor];
    loginField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    loginField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor], NSFontAttributeName: font}];
    CALayer *loginFieldBottomBorder = [CALayer layer];
    loginFieldBottomBorder.frame = CGRectMake(CGRectGetMinX(loginField.frame), CGRectGetMaxY(loginField.frame)+2, loginField.bounds.size.width, 1.0f);
    loginFieldBottomBorder.backgroundColor = [[UIColor whiteColor] CGColor];
    [blackView.layer addSublayer:loginFieldBottomBorder];
    [blackView addSubview:loginField];
    
    CGRect passFieldFrame = [TeoriusHelper scaledRectFromCenter:CGPointMake(LOGICAL_WIDTH/2, 110) size:CGSizeMake(kTextFieldWidth, 25)];
    passField = [[UITextField alloc] initWithFrame:passFieldFrame];
    passField.delegate = self;
//    passField.keyboardType = UIKeyboardTypeEmailAddress;
    passField.returnKeyType = UIReturnKeyGo;
    passField.textAlignment = NSTextAlignmentCenter;
    passField.font = font;
    passField.textColor = [UIColor whiteColor];
    passField.tintColor = [UIColor whiteColor];
    passField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Пароль" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor], NSFontAttributeName: font}];
    passField.secureTextEntry = YES;
    CALayer *passFieldBottomBorder = [CALayer layer];
    passFieldBottomBorder.frame = CGRectMake(CGRectGetMinX(passField.frame), CGRectGetMaxY(passField.frame)+2, passField.bounds.size.width, 1.0f);
    passFieldBottomBorder.backgroundColor = [[UIColor whiteColor] CGColor];
    [blackView.layer addSublayer:passFieldBottomBorder];
    [blackView addSubview:passField];
    
    CGRect pinkButtonFrame = [TeoriusHelper scaledRect:CGRectMake(0, 150, 150, 40)];
    UIButton* pinkButton = [SharedClass pinkButtonWithFrame:pinkButtonFrame text:@"Зарегистрироваться"];
    pinkButton.centerX = self.view.halfWidth;
    [pinkButton addTarget:self action:@selector(signUpButtonTouchUpInside) forControlEvents:UIControlEventTouchUpInside];
    [blackView addSubview:pinkButton];
    
//    const float kRegisterButtonWidth = 140;
//    CGRect registerButtonFrame = [TeoriusHelper scaledRect:CGRectMake(LOGICAL_WIDTH - leftRightPadding - kRegisterButtonWidth, 150, kRegisterButtonWidth, 40)];
//    UIButton* registerButton = [SharedClass underlineButtonWithFrame:registerButtonFrame text:@"Зарегистрироваться"];
//    [registerButton addTarget:self action:@selector(registerButtonTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
//    [blackView addSubview:registerButton];
    
    const float kForgotThePasswordButtonWidth = 140;
    CGRect alreadyHaveLoginButtonFrame = [TeoriusHelper scaledRectFromCenter:CGPointMake(LOGICAL_WIDTH/2, CGRectGetMaxY(blackView.frame)+40)
                                                                        size:CGSizeMake(kForgotThePasswordButtonWidth, 40)];
    UIButton* alreadyHaveLogin = [SharedClass underlineButtonWithFrame:alreadyHaveLoginButtonFrame text:@"Логин"];
    [alreadyHaveLogin addTarget:self action:@selector(alreadyHaveLoginTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
    [mainScrollView addSubview:alreadyHaveLogin];
}

-(void)alreadyHaveLoginTouchUpInside:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

//signUpButtonTouchUpInside
-(void)signUpButtonTouchUpInside {
    if (![self validateUserInfoWithPasswordValidation:YES]) {
        return;
    }
    else {
        [[TeoriusHelper instance] addWaitingViewWithText:@"Регистрация..." font: [SharedClass getRegularFontWithSize:14] toView:self.view disablingInteractionEvents: YES];
        
        [[MainAPI defaultAPI]registerWithLogin:loginField.text password:passField.text completion:^(BOOL success, NSError *error) {
            [self afterRegistrationWithSuccess:success error:error];
        }];
    }
}

- (void)afterRegistrationWithSuccess:(BOOL)success error:(NSError *)error {
    dispatch_async(dispatch_get_main_queue(), ^{
        [[TeoriusHelper instance] removeWaitingView];
        
        if (success) {
            // notifications
            if ([UIApplication instancesRespondToSelector:@selector(registerUserNotificationSettings:)]) {
                [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:nil]];
                
                [[UIApplication sharedApplication] registerForRemoteNotifications];
            }
            else {
                [[UIApplication sharedApplication] registerForRemoteNotificationTypes: (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
            }

            UIViewController* vc = [BaseViewController new];
            self.navigationController.viewControllers = @[vc];
        }
        else {
            [TeoriusHelper showStandartAlertInMainThreadWithTitle:@"Ошибка" body:error.localizedDescription];
        }
    });
}

- (BOOL)validateUserInfoWithPasswordValidation:(BOOL)withPasswordValidation {
    //    [UsualTasksHelper sharedInstance]
    if (withPasswordValidation) {
        if (!loginField.hasText || !passField.hasText) {
            [TeoriusHelper showStandartAlertInMainThreadWithTitle:@"Ошибка" body:@"Введите email и пароль"];
            return NO;
        }
    }
    
    if (![TeoriusHelper validateEmail:loginField.text])
    {
        //        [self rightPositionOfAlertLabelWithText:@"Неверный формат почты"];
        [TeoriusHelper showStandartAlertInMainThreadWithTitle:@"Ошибка" body:@"Неверный формат почты"];
        return NO;
    }
    
    //    if (withPasswordValidation) {
    //        if (![[UsualTasksHelper sharedInstance] validatePassword:passwordTextField.text])
    //        {
    //            [self rightPositionOfAlertLabelWithText:@"Неверный формат пароля"];
    //            return NO;
    //        }
    //        else if (passwordTextField.text.length<3)
    //        {
    //            [self rightPositionOfAlertLabelWithText:@"Длина пароля меньше 3"];
    //            return NO;
    //        }
    //    }
    
    return YES;
}



#pragma mark - Text field delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == loginField) {
        [passField becomeFirstResponder];
    }
    else {
        [self signUpButtonTouchUpInside];
    }
    
    return YES;
}

@end
