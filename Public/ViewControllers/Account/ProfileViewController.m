//
//  ProfileViewController.m
//  Public
//
//  Created by Teoria 5 on 04/09/15.
//  Copyright (c) 2015 Teoria. All rights reserved.
//

#import "ProfileViewController.h"
#import "FamilyViewController.h"
#import "TeoriusHelper+Extension.h"
#import "SharedClass.h"
#import "FamilyRLM.h"
#import "User.h"
#import "ProfilePortrait.h"
#import "PortraitImageView.h"
#import "MainAPI.h"

typedef NS_ENUM(int, RowType) {
    RowTypeRelationship,
    RowTypeSurname,
    RowTypeName,
    RowTypePatronimyc,
    RowTypePassSeries,
    RowTypePassNumber,
    RowTypeOolisNumber,
    RowTypeBirthCert,
    RowTypeRegion
};

typedef NS_ENUM(int, TextViewType) {
    TextViewTypeTextField,
    TextViewTypeLabel
};


#define PROPERTY_INDEX_BASE 101
#define PROPERTY_INDEX_RELATIONSHIP 101
#define PROPERTY_INDEX_SURNAME 102
#define PROPERTY_INDEX_NAME 103
#define PROPERTY_INDEX_PATRONYMIC 104
#define PROPERTY_INDEX_GENDER 105
#define PROPERTY_INDEX_BIRTHDAY 106
#define PROPERTY_INDEX_PASSPORTSERIES 107
#define PROPERTY_INDEX_PASSPORTNUMBER 108
#define PROPERTY_INDEX_POLISNUMBER 109
#define PROPERTY_INDEX_BIRTHDAYCERTIFICATE 110
#define PROPERTY_INDEX_REGION 111


@interface ProfileViewController () <UITextFieldDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate> {
    BOOL wasChanged;
    NSArray *textViewTypes;
    NSDateFormatter *formatter;
}

@property (nonatomic) NSArray<NSString*>* labelTitles;

@property (nonatomic) Relationship* selectedRelationship;
@property (nonatomic) UIActionSheet* relationshipActionSheet;
@property (nonatomic) UILabel* relationshipLabel;

@property (nonatomic) Gender* selectedGender;
@property (nonatomic) UIActionSheet* genderActionSheet;
@property (nonatomic) UIActionSheet* photoActionSheet;

@property (nonatomic) UILabel* genderLabel;

//
@property (nonatomic) UIImage *avatar;

@property (nonatomic) NSString *genderSelected;
@property (nonatomic) NSString *kinshipSelected;
@property (nonatomic) UIDatePicker *datePicker;

@property (nonatomic, strong) UITextField* dateTextField;
@property (nonatomic, strong) UITextField* firstName;
@property (nonatomic, strong) UITextField* secondName;
@property (nonatomic, strong) UITextField* patronymic;
@property (nonatomic, strong) UITextField* passportSeries;
@property (nonatomic, strong) UITextField* passportNumber;
@property (nonatomic, strong) UITextField* polisNumber;
@property (nonatomic, strong) UITextField* birthdayCertificate;

@end


@implementation ProfileViewController

- (instancetype)initWithTitle:(NSString *)title subtitle:(NSString *)subtitle menuButtonVisible:(BOOL)menuButtonVisible avatarImage:(UIImage *)avatar {
//    self = [super initWithTitle:title subtitle:subtitle menuButtonVisible:menuButtonVisible];
    
    if (!self) {
        return nil;
    }
    
    _avatar = avatar;
    
    
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (self.isMovingToParentViewController) {
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                                 style:UIBarButtonItemStylePlain
                                                                                target:nil
                                                                                action:nil];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Готово"
                                                                                  style:UIBarButtonItemStylePlain
                                                                                 target:self
                                                                                 action:@selector(doneButtonTapped:)];
        self.navigationController.navigationBar.barTintColor = UIColorFromRGB(0xE9E3E2);
        self.navigationController.navigationBar.translucent = NO;
//        self.title = @"Профиль";
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.labelTitles = @[@"Степень родства", @"Фамилия", @"Имя", @"Отчество", @"Пол", @"День рождения", @"Серия паспорта", @"Номер паспорта", @"Полис ОМС", @"Свидетельство о рождении", @"Регион"];
    [self configSubviews];
    
    self.kinshipSelected = self.user.relationship;
    formatter = [TeoriusHelper dateFormatterUsingFormat:@"yyyy-MM-dd"];
    
}

- (void)reload {
//    [self removeAllSubviews];
    [self prepareModel];
//    [self addAllSubviews];
    [self configSubviews];
    
}

- (void)prepareModel {
    NSData *imageData = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:[NSString  stringWithFormat:@"avatar"] ofType:@"png"]];
    UIImage *avatarImage = [UIImage imageWithData:imageData];//imageNamed:[NSString  stringWithFormat:@"avatar_%d", i]];
    
    //    AvatarView *avatarView = [[AvatarView alloc] initWithImage:self.avatar diametr:imageViewWidth borderWidth:[SharedConstants avatarBorderWidth] * 2.f borderColor:[SharedConstants turquoiseColor]];
    //    [temp replaceObjectAtIndex:currentAvatarIndex withObject:[[UsualTasksHelper sharedInstance] imageFromView:avatarView]];
    
}

- (void)configSubviews {
    const float kRowHeight = 50.0f;
    
//    UITableView* tableView = [[UITableView alloc] initWithFrame:[TeoriusHelper scaledRect:CGRectMake(0, HEADER_HEIGHT, LOGICAL_WIDTH, LOGICAL_HEIGHT_SUB_HH)]];
    
    UITableView* tableView = self.tableView;
    tableView.rowHeight = [TeoriusHelper scaledFloat:kRowHeight];
    tableView.showsVerticalScrollIndicator = NO; // @arslan: если включить индиктор, то в он не доходит до верха
    tableView.separatorColor = UIColorFromRGB(0xEEEEEE);
    tableView.allowsSelection = NO;

    UIView* portraitContainter = [[UIView alloc] initWithFrame:[TeoriusHelper scaledRect:CGRectMake(0, 0, LOGICAL_WIDTH, 145)]];
    portraitContainter.backgroundColor = [UIColor whiteColor];
    tableView.tableHeaderView = portraitContainter;
    
    CGRect portraitFrame = [TeoriusHelper scaledRectFromCenter:CGPointMake(LOGICAL_WIDTH/2, 70) size:CGSizeMake(PORTRAIT_BIG_DIMENSION, PORTRAIT_BIG_DIMENSION)];
    
    if (self.user.image != nil) {
        PortraitImageView *profilePortrait = [[PortraitImageView alloc] initWithFrame:portraitFrame imageData:self.user.image borderWidth:2];
//        ProfilePortrait* profilePortrait = [[ProfilePortrait alloc] initWithFrame:portraitFrame imageName: _avatarData borderWidth:2];
        [portraitContainter addSubview:profilePortrait];
    }
    else {
        ProfilePortrait *profilePortrait = [[ProfilePortrait alloc] initWithFrame:portraitFrame imageName:_avatar borderWidth:2];
        [portraitContainter addSubview:profilePortrait];
    }
    portraitContainter.userInteractionEnabled = YES;
    UITapGestureRecognizer *viewTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takeOrSetUserPhoto)];
    [portraitContainter addGestureRecognizer:viewTapGesture];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.showRelationship)
        return 11;
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"Cell"];
    
    NSMutableParagraphStyle *parStyle = [NSMutableParagraphStyle new];
    parStyle.lineHeightMultiple = 0.8f;
    
    NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:[self getLabelTitleForRow:indexPath.row] attributes:@{NSParagraphStyleAttributeName: parStyle, NSFontAttributeName: [SharedClass getRegularFontWithSize:14.0f], NSForegroundColorAttributeName: COLOR_LIGHTGRAY}];
    
    float leftOffset = 30;
    UILabel* firstLabel = [[UILabel alloc] initWithFrame:[TeoriusHelper scaledRect:CGRectMake(leftOffset, 18, 120, 0)]];
    firstLabel.numberOfLines = 0;
    firstLabel.attributedText = attrString;
    //        dateLabel.font = [SharedClass getRegularFontWithSize:14.0f];
    //        dateLabel.textColor = UIColorFromRGB(0xB3B3B3);
    [firstLabel sizeToFit];
    [cell addSubview:firstLabel];
    
    if (PROPERTY_INDEX_RELATIONSHIP != [self getPropertyIndexFromRow:indexPath.row] &&
        PROPERTY_INDEX_GENDER != [self getPropertyIndexFromRow:indexPath.row] &&
        PROPERTY_INDEX_REGION != [self getPropertyIndexFromRow:indexPath.row]) {
        
        UITextField* textField = [[UITextField alloc] initWithFrame:[TeoriusHelper scaledRect:CGRectMake(160, 5, 170, 40)]];
        textField.font = [SharedClass getRegularFont];
        textField.textColor = COLOR_LIGHTERGREEN;
        
        
        if (PROPERTY_INDEX_SURNAME == [self getPropertyIndexFromRow:indexPath.row])
        {
            self.secondName = textField;
            textField.text = self.user.surname;
            textField.tag = PROPERTY_INDEX_SURNAME;
        }
        else if (PROPERTY_INDEX_NAME == [self getPropertyIndexFromRow:indexPath.row])
        {

            self.firstName = textField;
            textField.text = self.user.name;
            textField.tag = PROPERTY_INDEX_NAME;
        }
        else if (PROPERTY_INDEX_PATRONYMIC == [self getPropertyIndexFromRow:indexPath.row])
        {
            self.patronymic = textField;
            textField.text = self.user.patronymic;
            textField.tag = PROPERTY_INDEX_PATRONYMIC;
        }
        else if (PROPERTY_INDEX_BIRTHDAY == [self getPropertyIndexFromRow:indexPath.row])
        {
            
            self.dateTextField = textField;
            
            self.datePicker = [UIDatePicker new];
            [self.datePicker setMaximumDate:[NSDate date]];
            self.datePicker.datePickerMode = UIDatePickerModeDate;
            self.datePicker.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"ru_RU"];
            [self.datePicker addTarget:self action:@selector(pickerChanged:) forControlEvents:UIControlEventValueChanged];
            
            if ([TeoriusHelper dateIsValid:self.user.birthDay]) {
                self.datePicker.date = self.user.birthDay;
            }
            else {
                [self.datePicker setDate:[[NSDate date] dateByAddingTimeInterval:86400]];
            }
            
            self.dateTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[TeoriusHelper dateToString:self.datePicker.date] attributes:@{NSForegroundColorAttributeName: COLOR_LIGHTGRAY, NSFontAttributeName: [SharedClass getRegularFont]}];
            self.dateTextField.inputView = self.datePicker;
            self.dateTextField.text = [TeoriusHelper dateToString:self.datePicker.date];
            self.dateTextField.tag = PROPERTY_INDEX_BIRTHDAY;

            [cell addSubview:self.dateTextField];
            
            
//            textField.keyboardType = UIKeyboardTypeNumberPad;
        }
        else if (PROPERTY_INDEX_PASSPORTSERIES == [self getPropertyIndexFromRow:indexPath.row])
        {
            self.passportSeries = textField;
            textField.keyboardType = UIKeyboardTypeNumberPad;
            textField.tag = PROPERTY_INDEX_PASSPORTSERIES;
            textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"" attributes:@{NSForegroundColorAttributeName: COLOR_LIGHTGRAY, NSFontAttributeName: [SharedClass getRegularFont]}];
            textField.delegate = self;
            textField.text = self.user.passportSeries;
            
        }
        else if (PROPERTY_INDEX_PASSPORTNUMBER == [self getPropertyIndexFromRow:indexPath.row])
        {
            self.passportNumber = textField;
            textField.keyboardType = UIKeyboardTypeNumberPad;
            textField.tag = PROPERTY_INDEX_PASSPORTNUMBER;
            textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"●●●●●●" attributes:@{NSForegroundColorAttributeName: COLOR_LIGHTGRAY, NSFontAttributeName: [SharedClass getRegularFont]}];
            textField.secureTextEntry = YES;
            textField.delegate = self;
            textField.text = self.user.passportNumber;
            
        }
        else if (PROPERTY_INDEX_POLISNUMBER == [self getPropertyIndexFromRow:(int)indexPath.row])
        {
            self.polisNumber = textField;
            textField.keyboardType = UIKeyboardTypeNumberPad;
            textField.tag = PROPERTY_INDEX_POLISNUMBER;
            textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"" attributes:@{NSForegroundColorAttributeName: COLOR_LIGHTGRAY, NSFontAttributeName: [SharedClass getRegularFont]}];
            textField.delegate = self;
            textField.text = self.user.polisNumber;
        }
        else if (PROPERTY_INDEX_BIRTHDAYCERTIFICATE == [self getPropertyIndexFromRow:indexPath.row])
        {
            self.birthdayCertificate = textField;
            textField.text = self.user.birthdayCertificate;
            textField.keyboardType = UIKeyboardTypeNumberPad;
            firstLabel.center = CGPointMake(firstLabel.center.x, 25);
            textField.tag = PROPERTY_INDEX_BIRTHDAYCERTIFICATE;
        }
        [textField addTarget:self action:@selector(textFieldTextDidChanged:) forControlEvents:UIControlEventEditingChanged];
        
        [cell addSubview:textField];
    }
    else if (PROPERTY_INDEX_RELATIONSHIP == [self getPropertyIndexFromRow:indexPath.row] ||
               PROPERTY_INDEX_GENDER == [self getPropertyIndexFromRow:indexPath.row] ||
               PROPERTY_INDEX_REGION == [self getPropertyIndexFromRow:indexPath.row]) {
        
        NSString* secondLabelText;
        UILabel* secondLabel = [[UILabel alloc] initWithFrame:[TeoriusHelper scaledRect:CGRectMake(160, 5, 170, 40)]];
        if (PROPERTY_INDEX_RELATIONSHIP == [self getPropertyIndexFromRow:indexPath.row])
        {
            
            if ([TeoriusHelper stringIsValid:self.user.relationship]) {
                self.selectedRelationship = self.user.relationship;
                secondLabelText = [self relationshipList:[self.user.relationship integerValue]];
            }
            
            secondLabel.userInteractionEnabled = YES;
            UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(relationshiopLabelTapped)];
            [secondLabel addGestureRecognizer:tapGesture];
            
            self.relationshipLabel = secondLabel;
        }
        else if (PROPERTY_INDEX_GENDER == [self getPropertyIndexFromRow:indexPath.row])
        {
            
//            [self relationshipList:[self.user.gender integerValue]];
            self.selectedGender = [self genderList:[self.user.gender integerValue]];

            secondLabelText = self.selectedGender;

            secondLabel.userInteractionEnabled = YES;
            UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(genderLabelTapped)];
            [secondLabel addGestureRecognizer:tapGesture];
            
            self.genderLabel = secondLabel;
        }
        else if (PROPERTY_INDEX_REGION == [self getPropertyIndexFromRow:indexPath.row])
        {
            secondLabelText = @"Республика Татарстан";
        }
        secondLabel.text = secondLabelText;
        secondLabel.font = [SharedClass getRegularFontWithSize:14.0f];
        secondLabel.textColor = COLOR_LIGHTERGREEN;
        [cell addSubview:secondLabel];
    }
    
    return cell;
}

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    [TeoriusHelper tableViewDeselectSelectedCell:tableView];
//    ProfileViewController* vc = [[ProfileViewController alloc] initWithTitle:@"Профиль" menuButtonVisible:NO];
//    vc.user = self.family.members[indexPath.row];
//    [self.navigationController pushViewController:vc animated:YES];
//}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsMake(0, 30, 0, 0)];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsMake(0, 30, 0, 0)];
    }
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
}

#pragma mark - actions

- (void)doneButtonTapped:(id)sender {

    if (![self validateCreateUser:YES]) {
        return;
    }
    else {
        [[TeoriusHelper instance] addWaitingViewWithText:@"Подождите" font:[SharedClass getRegularFontWithSize:14] toView:self.navigationController.view disablingInteractionEvents:YES];
        
        self.user.birthDay = self.datePicker.date;
        self.user.image = self.user.image;
        if (self.avatar != nil) {
            self.user.image = UIImagePNGRepresentation(self.avatar);
        }
        
        
        self.user.name = [self getPropertyValueIndex:PROPERTY_INDEX_NAME];
        self.user.surname = [self getPropertyValueIndex:PROPERTY_INDEX_SURNAME];
        self.user.patronymic = [self getPropertyValueIndex:PROPERTY_INDEX_PATRONYMIC];
        self.user.relationship = self.kinshipSelected;
        self.user.gender = self.genderSelected;
        self.user.passportSeries = [self getPropertyValueIndex:PROPERTY_INDEX_PASSPORTSERIES];
        self.user.passportNumber = [self getPropertyValueIndex:PROPERTY_INDEX_PASSPORTNUMBER];
        self.user.polisNumber = [self getPropertyValueIndex:PROPERTY_INDEX_POLISNUMBER];
        self.user.region = [self getPropertyValueIndex:PROPERTY_INDEX_REGION];
        //    self.user.image = self.user.image;
        
        if (self.user.ID) {
            [[MainAPI defaultAPI] updateUser:self.user completion:^(NSDictionary *dic, NSError *err) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[TeoriusHelper instance] removeWaitingView];

                    if (err) {
                        [TeoriusHelper showStandartAlertInMainThreadWithTitle:@"Ошибка" body:err.localizedDescription];
                    }
                    else {
                        [SharedClass sharedInstance].needToUpdateFamily = YES;
                        [self.navigationController popViewControllerAnimated:YES];
                    }
                });
            }];
        }
        else {
            [[MainAPI defaultAPI] createNewUser:self.user completion:^(NSDictionary *arr, NSError *err) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[TeoriusHelper instance] removeWaitingView];
                    
                    if (err) {
                        [TeoriusHelper showStandartAlertInMainThreadWithTitle:@"Ошибка" body:err.localizedDescription];
                    }
                    else {
                        [SharedClass sharedInstance].needToUpdateFamily = YES;
                        [self.navigationController popViewControllerAnimated:YES];
                    }
                });
            }];
        }
        
    }
    
}

- (void)relationshiopLabelTapped {
    [self resignAllTextFieldsFirstResponder];
    [self.relationshipActionSheet showInView:self.view];
}

- (void)textFieldTextDidChanged:(UITextField *)textField {
    
    if (self.firstName == textField) {
        self.user.name = textField.text;
    }
    if (self.secondName == textField) {
        self.user.surname = textField.text;
    }
    if (self.patronymic == textField) {
        self.user.patronymic = textField.text;
    }
    if (self.passportSeries == textField) {
        self.user.passportSeries = textField.text;
    }
    if (self.passportNumber == textField) {
        self.user.passportNumber = textField.text;
    }
    if (self.polisNumber == textField) {
        self.user.polisNumber = textField.text;
    }
    if (self.birthdayCertificate == textField) {
        self.user.birthdayCertificate = textField.text;
    }
}



- (void)genderLabelTapped {
    [self resignAllTextFieldsFirstResponder];
    [self.genderActionSheet showInView:self.view];
}

- (void)viewTapGesture {
    [self resignAllTextFieldsFirstResponder];
}

#pragma mark - UIActionSheet delegate

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (actionSheet == self.relationshipActionSheet) {
        if (buttonIndex >= 0 && buttonIndex <= 7) {
            switch (buttonIndex) {
              case 0:
                self.relationshipLabel.text = @"Мать";
                self.kinshipSelected = @"1";
                    self.user.relationship = @"1";
                break;
                    
                case 1:
                    self.relationshipLabel.text = @"Отец";
                    self.kinshipSelected = @"2";
                    self.user.relationship = @"2";
                    break;
                case 2:
                    self.relationshipLabel.text = @"Дочь";
                    self.kinshipSelected = @"3";
                    self.user.relationship = @"3";
                    break;
                case 3:
                    self.relationshipLabel.text = @"Сын";
                    self.kinshipSelected = @"4";
                    self.user.relationship = @"4";
                    break;
                case 4:
                    self.relationshipLabel.text = @"Муж";
                    self.kinshipSelected = @"5";
                    self.user.relationship = @"5";
                    break;
                case 5:
                    self.relationshipLabel.text = @"Жена";
                    self.kinshipSelected = @"6";
                    self.user.relationship = @"6";
                    break;

              default:
                break;
            }
        }
    }
    else if (actionSheet == self.genderActionSheet) {
        if (buttonIndex >= 0 && buttonIndex <= 1) {
            switch (buttonIndex) {
                case 0:
                    self.genderLabel.text = @"Женский";
                    self.genderSelected = @"2";
                    break;
                    
                case 1:
                    self.genderLabel.text = @"Мужской";
                    self.genderSelected = @"1";
                    break;
                default:
                    break;
            }
        }
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (actionSheet == self.photoActionSheet) {
        if (buttonIndex == 0) {
            [self takePhoto];
        }
        else if (buttonIndex == 1) {
            [self choosePhoto];
        }
    }
}

#pragma mark - properties

- (UIActionSheet*)relationshipActionSheet {
    if (_relationshipActionSheet == NULL) {
        _relationshipActionSheet = [[UIActionSheet alloc] initWithTitle:@"Выберите степень родства"
                                                               delegate:self
                                                      cancelButtonTitle:@"Отмена"
                                                 destructiveButtonTitle:nil
                                                      otherButtonTitles:@"Мать", @"Отец", @"Дочь", @"Сын", @"Муж", @"Жена", nil];
    }
    return _relationshipActionSheet;
}

- (UIActionSheet*)genderActionSheet {
    if (_genderActionSheet == NULL) {
        _genderActionSheet = [[UIActionSheet alloc] initWithTitle:@"Выберите пол"
                                                         delegate:self
                                                cancelButtonTitle:@"Отмена"
                                           destructiveButtonTitle:nil
                                                otherButtonTitles:@"Женский", @"Мужской", nil];
    }
    return _genderActionSheet;
}

- (void)takeOrSetUserPhoto {
    [self.photoActionSheet showInView:self.view];
}

- (void)takePhoto {
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        [TeoriusHelper showStandartAlertInMainThreadWithTitle:nil body:@"Камера недоступна"];
        return;
    }
    UIImagePickerController *imagePickerController = [UIImagePickerController new];
    imagePickerController.sourceType =  UIImagePickerControllerSourceTypeCamera;
    imagePickerController.allowsEditing = YES;
    imagePickerController.delegate = self;
    [self presentViewController:imagePickerController animated:YES completion:nil];
}

- (void)choosePhoto {
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        [TeoriusHelper showStandartAlertInMainThreadWithTitle:nil body:@"Галерея недоступна"];
        return;
    }
    UIImagePickerController *imagePickerController = [UIImagePickerController new];
    imagePickerController.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
    imagePickerController.allowsEditing = YES;
    imagePickerController.delegate = self;
    
    [self presentViewController:imagePickerController animated:YES completion:nil];
}


- (UIActionSheet*)photoActionSheet {
    if (_photoActionSheet == NULL) {
        _photoActionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                         delegate:self
                                                cancelButtonTitle:@"Отмена"
                                           destructiveButtonTitle:nil
                                                otherButtonTitles:@"Сделать фотографию", @"Выбрать из галереи", nil];
    }
    return _photoActionSheet;
}

#pragma mark - helpers

#define ACCEPTABLE_CHARECTERS @"0123456789"
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    int maxLength = 40;
    
    if (textField == self.passportNumber)
        maxLength = 6;
    
    if (textField == self.passportSeries)
        maxLength = 4;
    
    if (textField == self.polisNumber)
        maxLength = 16;
    
    if(range.length + range.location > textField.text.length)
        return NO;
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    if (maxLength < newLength)
        return NO;
    
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARECTERS] invertedSet];
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return [string isEqualToString:filtered];
}

- (BOOL)validateCreateUser:(BOOL)withPasswordValidation {
    if (withPasswordValidation) {
        if (self.showRelationship) {
            if (![TeoriusHelper stringIsValid:self.kinshipSelected ]) {
                [TeoriusHelper showStandartAlertInMainThreadWithTitle:@"Ошибка" body:@"Выберите вид родства"];
                return NO;
            }
        }
    
        
        if (![TeoriusHelper stringIsValid:self.firstName.text] || ![TeoriusHelper stringIsValid:self.secondName.text]) {
            [TeoriusHelper showStandartAlertInMainThreadWithTitle:@"Ошибка" body:@"Введите Имя и фамилию"];
            return NO;
        }
        
        NSUInteger seriesCount = [self.user.passportSeries length];
        
        if ((seriesCount >= 1 && seriesCount < 4) || (seriesCount > 4)) {
            [TeoriusHelper showStandartAlertInMainThreadWithTitle:@"Ошибка" body:@"Серия паспорта должна иметь 4 цифры"];
            return NO;
        }
        
        NSUInteger passportNumberCount = [self.user.passportNumber length];
        
        if ((passportNumberCount >= 1 && passportNumberCount < 6) || (passportNumberCount > 6)) {
            [TeoriusHelper showStandartAlertInMainThreadWithTitle:@"Ошибка" body:@"Номер паспорта должен иметь 6 цифр"];
            return NO;
        }
        
        if ((seriesCount >= 1 && passportNumberCount <= 1) || (seriesCount <= 1 && passportNumberCount > 1)) {
            [TeoriusHelper showStandartAlertInMainThreadWithTitle:@"Ошибка" body:@"Проверьте заполнение полей паспорта"];
            return NO;
        }
        
        NSUInteger polisCount = [self.user.polisNumber length];
        
        if (polisCount >= 1 && polisCount < 16) {
            [TeoriusHelper showStandartAlertInMainThreadWithTitle:@"Ошибка" body:@"Номер полиса должен иметь 16 цифр"];
            return NO;
        }
        
//        self.user.polisNumber
    }
    
    
    return YES;
}


- (int)getPropertyIndexFromRow:(int)row {
    row += PROPERTY_INDEX_BASE;
    if (self.showRelationship)
        return row;
    return row+1;
}

- (NSString*)getLabelTitleForRow:(int)row {
    int index = [self getPropertyIndexFromRow:row] - PROPERTY_INDEX_BASE;
    return self.labelTitles[index];
}

- (UITextField*)getPropertyTextField:(int)propertyIndex {
    return (UITextField *)[self.view viewWithTag:propertyIndex];
}

- (NSString*)getPropertyValueIndex:(int)propertyIndex {
    UITextField *tf = [self getPropertyTextField:propertyIndex];
    if (tf != NULL)
        return tf.text;
    return @"";
}
- (UIImage *)croppIngimageByImageName:(UIImage *)imageToCrop toRect:(CGRect)rect
{
    //CGRect CropRect = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, rect.size.height+15);
    
    CGImageRef imageRef = CGImageCreateWithImageInRect([imageToCrop CGImage], rect);
    UIImage *cropped = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    return cropped;
}
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    wasChanged = YES;
    
    _avatar = info[UIImagePickerControllerEditedImage];
    
    if (_avatar.size.width != _avatar.size.height) {
        UIImage *croppedImg = nil;
        CGRect cropRect = CGRectMake(400/2, 400/2, 400, 400); //set your rect size.
        croppedImg = [self croppIngimageByImageName:_avatar toRect:cropRect];
        _avatar = croppedImg;
//        NSLog(@"%@", croppedImg);
    };
    
    
    
    
    self.user.image = UIImagePNGRepresentation(self.avatar);
    
    
    
//    UIImage *myImage = [info objectForKey:UIImagePickerControllerEditedImage];
    
    [self reload];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (NSString*)getNameRelationship:(int)row {
    int index = [self getPropertyIndexFromRow:row] - PROPERTY_INDEX_BASE;
    return self.labelTitles[index];
}

- (NSString*)getNumberRelationship:(int)row {
    int index = [self getPropertyIndexFromRow:row] - PROPERTY_INDEX_BASE;
    return self.labelTitles[index];
}

- (NSString*) relationshipList:(int)row {
    
    
    if (!row == 0) {
        row = row - 1;
    }
    
    NSArray *listRelationship = @[@"Мать", @"Отец", @"Дочь", @"Сын", @"Муж", @"Жена"];
    
    return listRelationship[row];
}

- (NSString*) genderList:(int)row {
    
    
    if (!row == 0) {
        row = row - 1;
    }
    
    NSArray *listRelationship = @[@"Мужской", @"Женский"];
    
    return listRelationship[row];
}

- (void)pickerChanged:(id)sender {
    self.dateTextField.text = [TeoriusHelper dateToString:[sender date]];
    
    self.dateTextField.selectedTextRange = nil;
    
}

- (void)resignAllTextFieldsFirstResponder {
    [[self getPropertyTextField:PROPERTY_INDEX_SURNAME] resignFirstResponder];
    [[self getPropertyTextField:PROPERTY_INDEX_NAME] resignFirstResponder];
    [[self getPropertyTextField:PROPERTY_INDEX_PATRONYMIC] resignFirstResponder];
    [[self getPropertyTextField:PROPERTY_INDEX_BIRTHDAY] resignFirstResponder];
    [[self getPropertyTextField:PROPERTY_INDEX_PASSPORTSERIES] resignFirstResponder];
    [[self getPropertyTextField:PROPERTY_INDEX_PASSPORTNUMBER] resignFirstResponder];
    [[self getPropertyTextField:PROPERTY_INDEX_POLISNUMBER] resignFirstResponder];
    [[self getPropertyTextField:PROPERTY_INDEX_BIRTHDAYCERTIFICATE] resignFirstResponder];
}

- (User *)user {
    if (!_user) {
        _user = [User new];
    }
 
    return _user;
}

@end