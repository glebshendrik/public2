//
//  FamilyViewController.m
//  Public
//
//  Created by Arslan Zagidullin on 06/10/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import "MainAPI.h"
#import "FamilyViewController.h"
#import "TeoriusHelper+Extension.h"
#import "SharedClass.h"
#import "ProfilePortrait.h"
#import "PortraitImageView.h"
#import "ProfileViewController.h"
#import "MonitoringViewController.h"
#import "User.h"
#import "Family.h"

@interface FamilyViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic) UITableView* tableView;
@property (nonatomic) NSMutableArray *users;
@end


@implementation FamilyViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (self.isMovingToParentViewController) {
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@""
                                                                                 style:UIBarButtonItemStylePlain
                                                                                target:nil
                                                                                action:nil];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                                                                               target:self
                                                                                               action:@selector(addButtonTapped:)];
    }
    else {
        if ([SharedClass sharedInstance].needToUpdateFamily) {
            [SharedClass sharedInstance].needToUpdateFamily = NO;
            
            [self updateFamilyInfo];
        }
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSArray *familyMembers = [MainAPI defaultAPI].familyMembers;
    self.users = [[NSMutableArray alloc]init];
    
    if (familyMembers) {
        [self.users addObjectsFromArray:familyMembers];
    }

    [self configSubviews];
    
    [self updateFamilyInfo];
}

- (void)updateFamilyInfo {
    [[TeoriusHelper instance] addWaitingViewWithText:@"Обновление..." font: [SharedClass getRegularFontWithSize:14] toView:self.view disablingInteractionEvents: YES];
    
    [[MainAPI defaultAPI]getUserCitizen:^(NSArray *arr, NSError *err) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[TeoriusHelper instance] removeWaitingView];
            
            if (err) {
                [TeoriusHelper showStandartAlertInMainThreadWithTitle:@"Ошибка" body:err.localizedDescription];
            }
            else {
                [self.users removeAllObjects];
                [self.users addObjectsFromArray:[MainAPI defaultAPI].familyMembers];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:notificationNameUsersDataChanged object:nil];
                
                [self.tableView reloadData];
            }
        });
        
    }];
}

- (void)configSubviews {
    const float kRowHeight = 50.0f;
    
    UITableView* tableView = [[UITableView alloc] initWithFrame:[TeoriusHelper scaledRect:CGRectMake(0, HEADER_HEIGHT, LOGICAL_WIDTH, LOGICAL_HEIGHT_SUB_HH)]];
    
    tableView.tintColor = COLOR_LIGHTERGREEN;
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.rowHeight = [TeoriusHelper scaledFloat:kRowHeight];
    tableView.showsVerticalScrollIndicator = NO; // @arslan: если включить индиктор, то в он не доходит до верха
    tableView.backgroundColor = [UIColor clearColor];
    tableView.contentInset = UIEdgeInsetsMake(-HEADER_HEIGHT, 0, 0, 0);
    tableView.separatorColor = UIColorFromRGB(0xEEEEEE);
    [self.view addSubview:tableView];
    self.tableView = tableView;
    
    float tableCellMaxY = self.family.members.count * tableView.rowHeight;
    float plusButtonContainerHeight = LOGICAL_HEIGHT_SUB_HH-tableCellMaxY;
    
    const float kMinPlusButtonContainerHeight = 150;
    if (plusButtonContainerHeight < kMinPlusButtonContainerHeight)
        plusButtonContainerHeight = kMinPlusButtonContainerHeight;
    
    UIView* plusButtonContainer = [[UIView alloc] initWithFrame:[TeoriusHelper scaledRect:CGRectMake(0, 0, LOGICAL_WIDTH, plusButtonContainerHeight)]];
    plusButtonContainer.backgroundColor = [UIColor whiteColor];
    tableView.tableFooterView = plusButtonContainer;
    
    CGRect plusButtonFrame = [TeoriusHelper scaledRectFromCenter:CGPointMake(LOGICAL_WIDTH/2, plusButtonContainer.bounds.size.height/2) size:CGSizeMake(120, 120)];
    UIButton* greenButton = [SharedClass bigPlusButtonWithFrame:plusButtonFrame text:@"Добавить родственника"];
    [plusButtonContainer addSubview:greenButton];
    
    [greenButton addTarget:self action:@selector(addButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.users count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    UserRLM *user = self.family.currentMember;
    User* user = _users[indexPath.row];
    
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"Cell"];
    
    CGRect portraitFrame = [TeoriusHelper scaledRectFromCenter:CGPointMake(30, 25) size:CGSizeMake(PORTRAIT_SMALL_DIMENSION, PORTRAIT_SMALL_DIMENSION)];
//    ProfilePortrait* profilePortrait = [[ProfilePortrait alloc] initWithFrame:portraitFrame imageName:user.portraitFilename borderWidth:0];
    
    PortraitImageView *profilePortrait = [[PortraitImageView alloc] initWithFrame:portraitFrame imageData:user.image borderWidth:2];
    
    
    UIButton *infoButton = [UIButton buttonWithType: UIButtonTypeInfoLight];
    infoButton.tag = indexPath.row;
    [infoButton addTarget:self action:@selector(infoButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.accessoryType = UITableViewCellAccessoryDetailButton;
    cell.accessoryView = infoButton;
    
    
    [cell addSubview:profilePortrait];
    
    
    NSString* fullName = [NSString stringWithFormat:@"Ваш профиль"];

    if ([TeoriusHelper stringIsValid:user.name] || [TeoriusHelper stringIsValid:user.surname] || [TeoriusHelper stringIsValid:user.patronymic]) {
        fullName = [NSString stringWithFormat:@"%@ %@ %@", user.surname, user.name, user.patronymic];
    }
    
    UILabel* nameLabel = [[UILabel alloc] initWithFrame:[TeoriusHelper scaledRect:CGRectMake(65, 5, LOGICAL_WIDTH, 40)]];
    nameLabel.text = fullName;
    nameLabel.font = [SharedClass getRegularFontWithSize:14.0f];
    nameLabel.textColor = COLOR_LIGHTERGREEN;
    [cell addSubview:nameLabel];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [MainAPI defaultAPI].currentUser = self.users[indexPath.row];
    
//    MonitoringViewController* vc = [[MonitoringViewController alloc] initWithTitle:@"Мониторинг" menuButtonVisible:YES];
//    vc.delegate = self;
//    self.navigationController.viewControllers = @[vc];
    [self.familyVCDelegate openMonitoring];
    
//    [[NSNotificationCenter defaultCenter] postNotificationName:notificationNameUsersDataChanged object:nil];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsMake(0, 60, 0, 0)];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsMake(0, 60, 0, 0)];
    }
}

#pragma mark - actions

- (void)addButtonTapped:(id)sender {
    ProfileViewController* vc = [[ProfileViewController alloc] initWithTitle:@"Добавить родственника" menuButtonVisible:NO];
    
    
    vc.showRelationship = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)infoButtonTapped:(UIButton *)sender {
    ProfileViewController* vc = [[ProfileViewController alloc] initWithTitle:@"Профиль" menuButtonVisible:NO];
    vc.user = _users[sender.tag];
    
    if ([TeoriusHelper stringIsValid:vc.user.relationship])
        vc.showRelationship = YES;
    
    [self.navigationController pushViewController:vc animated:YES];
}

@end
