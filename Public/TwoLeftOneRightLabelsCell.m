//
//  TwoLeftOneRightLabelsCell.m
//  Public
//
//  Created by Tagi on 16.11.15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import "TwoLeftOneRightLabelsCell.h"
#import "TeoriusHelper.h"

@implementation TwoLeftOneRightLabelsCell

- (instancetype)init {
    self = [super initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:nil];
    
    if (!self) {
        return nil;
    }
    
    [self baseInit];
    
    return self;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseIdentifier];
    
    if (!self) {
        return nil;
    }
    
    [self baseInit];
    
    return self;
}

- (void)baseInit {
    _rightLabel = [UILabel new];
    [self.contentView addSubview:_rightLabel];
    
    _rightLabel = [UILabel new];
    [self.contentView addSubview:_rightLabel];
}


#pragma mark - View's lifecycle

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat addition = self.accessoryType != UITableViewCellAccessoryNone ? roundf(self.contentView.width / 15.f) : 0;
    
    [self.textLabel sizeToFit];
    [self.detailTextLabel sizeToFit];
    [self.textLabel cutWidthTo:(self.contentView.width - self.textLabel.minX * 2.f)];
    [self.detailTextLabel cutWidthTo:(self.contentView.width - self.textLabel.minX * 2.f)];

    [self.rightLabel sizeToFit];
    CGFloat maxWidth = self.contentView.width - MAX(self.textLabel.maxX, self.detailTextLabel.maxX) - self.textLabel.minX * 2.f + addition;
    if (maxWidth <= 5.f) {
        self.rightLabel.size = CGSizeMake(0, 0);
        return;
    }
    
    [self.rightLabel cutWidthTo:maxWidth];
    self.rightLabel.centerY = self.contentView.halfHeight;
    
    CGFloat rightEdge = self.contentView.width - self.textLabel.minX + addition;
    
    self.rightLabel.x = rightEdge - self.rightLabel.width;
}

@end
