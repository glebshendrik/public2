//
//  SettingsManager.h
//  Public
//
//  Created by Глеб on 13/10/15.
//  Copyright © 2015 Teoria. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface SettingsManager : NSObject

@property (nonatomic, getter=isLoggedIn) BOOL loggedIn;
@property (nonatomic, copy) NSString *apnsToken;

+ (SettingsManager *)sharedManager;

@end
