//
//  NotificationInterView.h
//  Public
//
//  Created by Глеб on 25/11/15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import "NotificationView.h"

@interface NotificationInterView : NotificationView

@property (nonatomic, readonly) UILabel *textLabel;
@property (nonatomic) NSMutableArray *buttonsArr;

@end
