//
//  TeoriusHelper.h
//  Energy_infoshell
//
//  Created by Tagi on 04.08.15.
//  Copyright (c) 2015 teourius. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

// macros

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
#define UIColorFromRGBAlpha(rgbValue, alpha) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:alpha]

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#define CGRectMakeCenterSize(center, size) CGRectMake(center.x-size.width/2, center.y-size.height/2, size.width, size.height)

typedef NS_ENUM(int, ViewAlignment) {
    ViewAlignmentNone,
    ViewAlignmentLeft,
    ViewAlignmentCenter,
    ViewAlignmentRight,
    ViewAlignmentTop,
    ViewAlignmentBottom
};

typedef NS_ENUM(int, ViewsPositioning) {
    ViewsPositioningVertical,
    ViewsPositioningHorizontal
};

// other interfaces

@interface NSObject(AdditionalMethods)

- (void)performBlockInMainThread:(void(^)(void))block;

@end


@interface NSString(AdditionalMethods)

- (BOOL)isValid;
- (id)jsonObject;

@end


@interface NSDictionary(AdditionalMethods)

- (NSString *)jsonString;

@end


@interface UIView(AdditionalMethods)

- (NSData *)pdf;
- (UIImage *)image;
- (UIView *)subviewWithTag:(NSInteger)tag;
- (void)fitToSize:(CGSize)size;
- (void)cutWidthTo:(CGFloat)width;
- (void)makeFrameIntegral;
- (void)setIntegralCenter:(CGPoint)center;

@property (nonatomic, readonly) CGPoint localCenter;
@property (nonatomic) CGFloat width;
@property (nonatomic) CGFloat height;
@property (nonatomic, readonly) CGFloat halfWidth;
@property (nonatomic, readonly) CGFloat halfHeight;
@property (nonatomic, readonly) CGFloat maxX;
@property (nonatomic, readonly) CGFloat maxY;
@property (nonatomic, readonly) CGFloat minX;
@property (nonatomic, readonly) CGFloat minY;
@property (nonatomic) CGSize size;
@property (nonatomic) CGPoint origin;
@property (nonatomic) CGFloat x;
@property (nonatomic) CGFloat y;
@property (nonatomic) CGFloat centerX;
@property (nonatomic) CGFloat centerY;

@end


@interface UIImage(AdditionalMethods)

- (UIImage *)imageScaledToSize:(CGSize)newSize;
- (BOOL)compareWithImage:(UIImage *)image;

@end


@interface NSMutableArray(AdditionalMethods)

- (void)reverse;

@end


@interface NSDate(AdditionalMethods)

- (NSDate *)beginningOfMonthDate;
- (NSUInteger)dayFromNil;
- (NSUInteger)dayFromOne;

@end


// teorius helper

@interface TeoriusHelper : NSObject

+ (TeoriusHelper *)instance;

// string
+ (NSString *)stringFromFloatValue:(CGFloat)floatValue;

// Usual common operations
+ (NSString *)getUUID;
+ (void)printAllFontNames;
+ (void)showStandartAlertInMainThreadWithTitle:(NSString *)title body:(NSString *)body;
+ (void)tableViewDeselectSelectedCell:(UITableView *)tableView afterDelay:(CGFloat)delay;
+ (void)tableViewDeselectSelectedCell:(UITableView *)tableView;

// Convertion and calculation
+ (double)radiansToDegrees:(double)radians;
+ (double)degreesToRadians:(double)degrees;

// Validation
+ (BOOL)validateEmail:(NSString *)candidate;
+ (BOOL)validatePassword:(NSString *)candidate;
+ (BOOL)stringIsValid:(NSString *)string;
+ (BOOL)dataIsValid:(NSData *)data;
+ (BOOL)arrayIsValid:(NSArray *)array;
+ (BOOL)dictIsValid:(NSDictionary *)dict;
+ (BOOL)numberIsValid:(NSNumber *)number;
+ (BOOL)dateIsValid:(NSDate *)date;

// pdf
+ (void)createPDFfromUIView:(UIView *)aView saveToDocumentsWithFileName:(NSString*)aFilename;

// waiting view
- (void)addWaitingViewWithKey:(NSString *)key text:(NSString *)text font:(UIFont *)font toView:(UIView *)view disablingInteractionEvents:(BOOL)disablingInteractionEvents;
- (void)addWaitingViewWithText:(NSString *)text font:(UIFont *)font toView:(UIView *)view disablingInteractionEvents:(BOOL)disablingInteractionEvents;
- (void)changeWaitingViewAfterSuccessWithImage:(NSString *)imageName text:(NSString *)text completion:(void(^)(void))block;
- (void)changeWaitingViewWithKey:(NSString *)key afterSuccessWithImage:(NSString *)imageName text:(NSString *)text completion:(void(^)(void))block;
- (void)removeWaitingView;
- (void)removeWaitingViewWithKey:(NSString *)key;
- (void)removeWaitingViewAnimatedWithCompletion:(void(^)(void))block;
- (void)removeWaitingViewWithKey:(NSString *)key animatedWithCompletion:(void(^)(void))block;
- (BOOL)isShowingWaitingView;
+ (UIView *)addDarkViewWithColorAlpha:(CGFloat)alphaColor alpha:(CGFloat)alpha toView:(UIView *)view withY:(CGFloat)y;
+ (void)removeDarkViewWithAnimation:(UIView *)darkView1;

// objects
+ (void)setValuesForObject:(id)object fromDict:(NSDictionary *)dict withLinkingDict:(NSDictionary *)linkingDict;

// dictionary
+ (NSDictionary *)dictionaryByRemovingWrongValues:(NSDictionary *)dictionary withValueClasses:(NSDictionary *)classesDict;

// views
+ (void)placeViewsHorizontally:(NSArray *)views inSuperview:(UIView *)superview toCenterPoint:(CGPoint)centerPoint;
+ (void)addAndPlaceViewsHorizontally:(NSArray *)views inSuperview:(UIView *)superview toCenterPoint:(CGPoint)centerPoint;
+ (void)placeViewsHorizontally:(NSArray *)views inSuperview:(UIView *)superview;
+ (void)addAndPlaceViewsHorizontally:(NSArray *)views inSuperview:(UIView *)superview;

+ (void)placeViewsVertically:(NSArray *)views inSuperview:(UIView *)superview toCenterPoint:(CGPoint)centerPoint;
+ (void)addAndPlaceViewsVertically:(NSArray *)views inSuperview:(UIView *)superview toCenterPoint:(CGPoint)centerPoint;
+ (void)placeViewsVertically:(NSArray *)views inSuperview:(UIView *)superview;
+ (void)addAndPlaceViewsVertically:(NSArray *)views inSuperview:(UIView *)superview;

+ (void)placeSubviews:(NSArray *)subviews withPositioning:(ViewsPositioning)positioning alignment:(ViewAlignment)alignment;
+ (void)placeSubviews:(NSArray *)subviews withPositioning:(ViewsPositioning)positioning alignment:(ViewAlignment)alignment toPoint:(CGPoint)point;

// time noting
- (void)setTimeNoteForKey:(NSString *)key;
- (CGFloat)getTimeDifferenceForKey:(NSString *)key withLogging:(BOOL)logging loggingInfo:(NSString *)loggingInfo replacingPreviousNote:(BOOL)replacing;
- (void)clearTimeNoteInfoForKey:(NSString *)key;

@end
