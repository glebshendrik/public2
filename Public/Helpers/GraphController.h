//
//  GraphFactory.h
//  Public
//
//  Created by Arslan Zagidullin on 24.09.15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CorePlot/ios/CorePlot-CocoaTouch.h>

@class Measurement;

typedef NS_ENUM(int, GraphControllerType) {
    GraphControllerTypeDefault,
    GraphControllerTypeWidget
};

@interface GraphController : NSObject

- (id)initWithMeasurementsArray:(NSArray *)measurementsArray readingsArray:(NSArray *)readingsArray dateFrom:(NSDate*)dateFrom dateTo:(NSDate*)dateTo type:(GraphControllerType)type;

@property (nonatomic) NSArray *measurementsArray;
@property (nonatomic) NSArray *readingsArray;

@property (nonatomic) NSDate* dateFrom;
@property (nonatomic) NSDate* dateTo;
@property (nonatomic) NSArray<NSNumber*>* selectedPlots;

@property (nonatomic) BOOL axesAreVisible;
@property (nonatomic) BOOL normalAreaIsVisible;
@property (nonatomic) float leftRightPadding;
@property (nonatomic) float topBottomPadding;
@property (nonatomic) float scaleX;
@property (nonatomic) float scaleY;
@property (nonatomic) CGSize symbolSize;
@property (nonatomic) BOOL isInteractive;
@property (nonatomic) GraphControllerType type;

- (CPTGraphHostingView*)getHostingView:(CGRect)tmpFrame;

- (void)reloadData;
- (BOOL)measurementIsSelected:(Measurement *)m;
- (NSDictionary *)closestReadingForX:(CGFloat)x;
- (NSDictionary *)closestReadingForPoint:(CGPoint)touchPoint;

@end
