//
//  TeoriusHelper+Extension.h
//  Public
//
//  Created by Teoria 5 on 09/09/15.
//  Copyright (c) 2015 Teoria. All rights reserved.
//

#import "TeoriusHelper.h"

#define LOGICAL_WIDTH 320.0f
#define LOGICAL_HEIGHT ( IS_IPHONE_4_OR_LESS ? 480.0f : 568.0f )
#define HEADER_HEIGHT 64
#define LOGICAL_HEIGHT_SUB_HH LOGICAL_HEIGHT - HEADER_HEIGHT
#define SCREEN_SCALE (SCREEN_WIDTH/LOGICAL_WIDTH)
#define SCALED_F(f) (roundf(f * SCREEN_SCALE))

@interface TeoriusHelper (Extension)

+ (CGRect)rectFromCenter:(CGPoint)center size:(CGSize)size;
+ (CGRect)scaledRectFromCenter:(CGPoint)center size:(CGSize)size;
+ (CGRect)scaledRect:(CGRect)rect;
+ (CGFloat)scaledFloat:(CGFloat)f;
+ (CGPoint)scaledPointWithX:(CGFloat)x Y:(CGFloat)y;
+ (CGPoint)scaledPoint:(CGPoint)point;
+ (CGSize)scaledSizeWithWidth:(CGFloat)width height:(CGFloat)height;
+ (CGSize)scaledSize:(CGSize)size;
+ (NSString*)documentsPath;
+ (NSString*)applicationDocumentsDirectory;

+ (NSString *)randomString;
+ (NSDate *)randomDate;

+ (NSString*)dateToString:(NSDate*)date;
+ (NSString*)timeToString:(NSDate*)date;
+ (NSString *)birthdayToStringWithDefis:(NSDate *)date;
+ (NSString*)dateAndTimeToString:(NSDate*)date;
+ (NSString*)dateToString:(NSDate*)date usingFormat:(NSString*)format;
+ (NSDate *)stringToDate:(NSString *)string;
+ (NSDate *)birthdayToDateWithDefis:(NSString *)string;
+ (NSDate *)stringToDate:(NSString *)string usingFormat:(NSString*)format;
+ (NSDateFormatter*)dateFormatterUsingFormat:(NSString*)format;
+ (NSString*)shortName:(NSString*)name surname:(NSString*)surname patronymic:(NSString*)patronymic;

@end
