//
//  TeoriusHelper+Arslan.m
//  Public
//
//  Created by Teoria 5 on 09/09/15.
//  Copyright (c) 2015 Teoria. All rights reserved.
//

#import "TeoriusHelper+Extension.h"

@implementation TeoriusHelper (Extension)

+ (CGRect)rectFromCenter:(CGPoint)center size:(CGSize)size {
    return CGRectMake(center.x-size.width/2, center.y-size.height/2, size.width, size.height);
}

+ (CGRect)scaledRectFromCenter:(CGPoint)center size:(CGSize)size {
    CGRect rect = [self rectFromCenter:center size:size];
    return [self scaledRect:rect];
}

+ (CGRect)scaledRect:(CGRect)rect {
    return CGRectMake(rect.origin.x * SCREEN_SCALE, rect.origin.y * SCREEN_SCALE, rect.size.width * SCREEN_SCALE, rect.size.height * SCREEN_SCALE);
}

+ (CGFloat)scaledFloat:(CGFloat)f {
    return roundf(f * SCREEN_SCALE);
}

+ (CGPoint)scaledPointWithX:(CGFloat)x Y:(CGFloat)y {
    CGPoint point = CGPointMake(x, y);
    return [self scaledPoint:point];
}

+ (CGPoint)scaledPoint:(CGPoint)point {
    return CGPointMake(point.x * SCREEN_SCALE, point.y * SCREEN_SCALE);
}

+ (CGSize)scaledSizeWithWidth:(CGFloat)width height:(CGFloat)height {
    CGSize size = CGSizeMake(width, height);
    return [self scaledSize:size];
}

+ (CGSize)scaledSize:(CGSize)size {
    return CGSizeMake(size.width * SCREEN_SCALE, size.height * SCREEN_SCALE);
}

/// Возвращает путь к директории Documents на устройстве (полезно при отладке на симуляторе)
+ (NSString*)documentsPath {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

/// Возвращает путь к директории Documents на устройстве (полезно при отладке на симуляторе)
+ (NSString *)applicationDocumentsDirectory {
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}


+ (NSString *)randomString {
    return [NSString stringWithFormat:@"Name %d", arc4random()];
}

+ (NSDate *)randomDate {
    return [NSDate dateWithTimeIntervalSince1970:arc4random()];
}

+ (NSString*)dateToString:(NSDate *)date {
    return [self dateToString:date usingFormat:@"d MMMM y"];
}

+ (NSString*)timeToString:(NSDate*)date {
    return [self dateToString:date usingFormat:@"H:mm"];
}

+ (NSString*)dateAndTimeToString:(NSDate*)date {
    return [self dateToString:date usingFormat:@"d MMMM y  H:m"];
}

+ (NSString *)birthdayToStringWithDefis:(NSDate *)date {
    return [self dateToString:date usingFormat:@"yyyy-MM-dd"];
}

+ (NSString*)dateToString:(NSDate*)date usingFormat:(NSString*)format {
    NSDateFormatter* formatter = [self dateFormatterUsingFormat:format];
    return [formatter stringFromDate:date];
}

+ (NSDate *)stringToDate:(NSString *)string {
    return [self stringToDate:string usingFormat:@"d MMMM y"];
}

+ (NSDate *)birthdayToDateWithDefis:(NSString *)string {
    return [self stringToDate:string usingFormat:@"yyyy-MM-dd"];
}

+ (NSDate *)stringToDate:(NSString *)string usingFormat:(NSString *)format {
    NSDateFormatter* formatter = [self dateFormatterUsingFormat:format];
    return [formatter dateFromString:string];
}

+ (NSDateFormatter*)dateFormatterUsingFormat:(NSString*)format {
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:format];
    [formatter setTimeZone:[NSTimeZone localTimeZone]];
    NSLocale* locale = [[NSLocale alloc] initWithLocaleIdentifier:@"ru_RU"];
    [formatter setLocale:locale];
    return formatter;
}

+ (NSString*)shortName:(NSString*)name surname:(NSString*)surname patronymic:(NSString*)patronymic {
    NSString* result = surname;
    if (name.length > 1)
        result = [result stringByAppendingFormat:@" %@.", [[name substringToIndex:1] uppercaseString]];
    if (patronymic.length > 1)
        result = [result stringByAppendingFormat:@" %@.", [[patronymic substringToIndex:1] uppercaseString]];
    return result;
}

@end
