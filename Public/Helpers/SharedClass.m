//
//  SharedClass.m
//  Public
//
//  Created by Teoria 5 on 08/09/15.
//  Copyright (c) 2015 Teoria. All rights reserved.
//

#import "SharedClass.h"
#import "TeoriusHelper+Extension.h"

#import "ThinBorderButton.h"
#import "PinkBorderButton.h"
#import "BorderlessButton.h"
#import "BigPlusButton.h"
#import "DetailButton.h"
#import "MonitoringButton.h"
#import "PinkButton.h"
#import "UnderlineButton.h"

NSString *const notificationNameUsersDataChanged = @"notificationNameUsersDataChanged";
NSString *const notificationNameNeedToUpdateAppointmentsList = @"notificationNameNeedToUpdateAppointmentsList";

@implementation SharedClass

+ (SharedClass *)sharedInstance {
    static SharedClass *_sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [self new];
    });
    
    return _sharedInstance;
}


+ (UIFont *)getRegularFont {
    return [self getRegularFontWithSize:14.0f];
}

+ (UIFont *)getRegularFontWithSize:(CGFloat)fontSize {
    UIFont *font = [UIFont fontWithName:@"Roboto-Regular" size:[TeoriusHelper scaledFloat:fontSize]];
    return font;
}

+ (UIFont *)getBoldFont {
    return [self getBoldFontWithSize:13.0f];
}

+ (UIFont *)getBoldFontWithSize:(CGFloat)fontSize {
    UIFont *font = [UIFont fontWithName:@"Roboto-Bold" size:[TeoriusHelper scaledFloat:fontSize]];
    return font;
}

+ (UIColor *)getGrayColor {
    return UIColorFromRGB(0x98999A);
}

+ (UIColor *)getRedColor {
    return UIColorFromRGB(0xD2695B);
}

+ (UIColor *)getGreenColor {
    return UIColorFromRGB(0x00A092);
}

+ (UIButton*)greenBorderButtonWithFrame:(CGRect)frame text:(NSString*)text {
    return [[ThinBorderButton alloc] initWithFrame:frame text:text color:COLOR_GREEN1];
}

+ (UIButton*)pinkBorderButtonWithFrame:(CGRect)frame text:(NSString*)text {
    return [[PinkBorderButton alloc] initWithFrame:frame text:text];
}

+ (UIButton*)borderlessButtonWithFrame:(CGRect)frame text:(NSString*)text {
    return [[BorderlessButton alloc] initWithFrame:frame text:text color:COLOR_GREEN1];
}

+ (UIButton*)bigPlusButtonWithFrame:(CGRect)frame text:(NSString*)text {
    return [[BigPlusButton alloc] initWithFrame:frame text:text];
}

+ (UIButton*)detailButtonWithFrame:(CGRect)frame {
    return [DetailButton buttonWithFrame:frame];
}

+ (UIButton*)monitoringButtonWithFrame:(CGRect)frame text:(NSString*)text {
    return [[MonitoringButton alloc] initWithFrame:frame text:text];
}

+ (UIButton*)pinkButtonWithFrame:(CGRect)frame text:(NSString *)text {
    return [[PinkButton alloc] initWithFrame:frame text:text];
}

+ (UIButton*)underlineButtonWithFrame:(CGRect)frame text:(NSString *)text {
    return [[UnderlineButton alloc] initWithFrame:frame text:text];
}

+ (DropDownLabel*)dropDownLabelWithFrame:(CGRect)frame text:(NSString*)text {
    return [[DropDownLabel alloc] initWithFrame:frame text:text];
}

+ (NSDate*)dateFromForInterval:(NSInteger)interval dateTo:(NSDate*)dateTo {
    NSCalendar *calendar = [[NSCalendar alloc]initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *dateComponents = [calendar components:NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear fromDate:dateTo];
    switch (interval) {
        case INTERVAL_1YEAR:
            dateComponents.year -= 1;
            break;
        case INTERVAL_3MONTHS:
            dateComponents.month -= 3;
            break;
        case INTERVAL_1MONTH:
            dateComponents.month -= 1;
            break;
        case INTERVAL_1WEEK:
            dateComponents.day -= 6;
            break;
        case INTERVAL_3DAYS:
            dateComponents.day -= 2;
            break;
        case INTERVAL_1DAY:
            dateComponents.day -= 0;
            break;
            
        default:
            break;
    }
    return [calendar dateFromComponents:dateComponents];
}

+ (UIColor*)getColorOfPlotNumber:(NSInteger)graphIndex {
    switch (graphIndex) {
        case 0:
            return UIColorFromRGB(0x6ACFF6);
            break;
            
        case 1:
            return UIColorFromRGB(0xFBAD18);
            break;
            
        case 2:
            return UIColorFromRGB(0xB57AB4);
            break;
            
        case 3:
            return UIColorFromRGB(0x4EBFAE);
            break;
            
        default:
            break;
    }
    return UIColorFromRGB(0xFF0000);
}

@end
