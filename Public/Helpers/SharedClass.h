//
//  SharedClass.h
//  Public
//
//  Created by Teoria 5 on 08/09/15.
//  Copyright (c) 2015 Teoria. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "TeoriusHelper+Extension.h"
#import "DropDownLabel.h"

#define COLOR_GREEN1 UIColorFromRGB(0x00A092)
#define COLOR_LIGHTERGREEN UIColorFromRGB(0x4FBFAE)
#define COLOR_PINK UIColorFromRGB(0xD76F62)
#define COLOR_LIGHTPINK UIColorFromRGB(0xEFC5C0)
#define COLOR_DARKGRAY UIColorFromRGB(0x666666)
#define COLOR_GRAY UIColorFromRGB(0x999999)
#define COLOR_LIGHTERGRAY UIColorFromRGB(0xB3B3B3)
#define COLOR_LIGHTGRAY UIColorFromRGB(0xCCCCCC)
#define COLOR_DARKGREENISHGRAY UIColorFromRGB(0x404447)

#define BUTTON_ANIM_DURATION 0.05f
#define DATEPICKER_ANIM_DURATION 0.2f
#define DATEPICKER_HEIGHT 216.0f

#define INTERVAL_1YEAR   0
#define INTERVAL_3MONTHS 1
#define INTERVAL_1MONTH  2
#define INTERVAL_1WEEK   3
#define INTERVAL_3DAYS   4
#define INTERVAL_1DAY    5

extern NSString *const notificationNameUsersDataChanged;
extern NSString *const notificationNameNeedToUpdateAppointmentsList;

@interface SharedClass : NSObject

+ (SharedClass *)sharedInstance;

+ (UIFont *)getRegularFont;
+ (UIFont *)getRegularFontWithSize:(CGFloat)fontSize;
+ (UIFont *)getBoldFont;
+ (UIFont *)getBoldFontWithSize:(CGFloat)fontSize;

+ (UIColor *)getGrayColor;
+ (UIColor *)getRedColor;
+ (UIColor *)getGreenColor;

+ (UIButton*)greenBorderButtonWithFrame:(CGRect)frame text:(NSString*)text;
+ (UIButton*)pinkBorderButtonWithFrame:(CGRect)frame text:(NSString*)text;
+ (UIButton*)borderlessButtonWithFrame:(CGRect)frame text:(NSString*)text;
+ (UIButton*)bigPlusButtonWithFrame:(CGRect)frame text:(NSString*)text;
+ (UIButton*)detailButtonWithFrame:(CGRect)frame;
+ (UIButton*)monitoringButtonWithFrame:(CGRect)frame text:(NSString*)text;
+ (UIButton*)pinkButtonWithFrame:(CGRect)frame text:(NSString*)text;
+ (UIButton*)underlineButtonWithFrame:(CGRect)frame text:(NSString*)text;
+ (DropDownLabel*)dropDownLabelWithFrame:(CGRect)frame text:(NSString*)text;

+ (NSDate*)dateFromForInterval:(NSInteger)interval dateTo:(NSDate*)dateTo;

+ (UIColor*)getColorOfPlotNumber:(NSInteger)graphIndex;

@property (nonatomic) BOOL needToUpdateFamily;

@end
