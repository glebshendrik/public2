//
//  TeoriusHelper.m
//  Energy_infoshell
//
//  Created by Tagi on 04.08.15.
//  Copyright (c) 2015 teourius. All rights reserved.
//

#import "TeoriusHelper.h"
#import <CommonCrypto/CommonDigest.h>
#include <mach/mach_time.h>
#import "objc/runtime.h"

typedef NS_ENUM(int, WaitingViewSubview) {
    WaitingViewSubviewBase = 100,
    WaitingViewSubviewLabel = 101,
    WaitingViewSubviewLoadingIndicator = 102,
    WaitingViewSubviewImage = 103
};


@implementation NSObject(AdditionalMethods)

- (void)performBlockInMainThread:(void (^)(void))block {
    dispatch_async(dispatch_get_main_queue(), block);
}

@end


@implementation NSString(AdditionalMethods)

- (BOOL)isValid {
    if (self.length == 0) {
        return NO;
    }
    
    return YES;
}

- (id)jsonObject {
    NSData *data = [self dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    return json;
}

@end


@implementation NSDictionary(AdditionalMethods)

- (NSString *)jsonString {
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self
                                                       options:kNilOptions
                                                         error:&error];
    
    if (!jsonData) {
        return nil;
    }
    else {
        return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
}

@end


@implementation UIView(AdditionalMethods)

- (NSData *)pdf {
    NSMutableData *pdfData = [NSMutableData data];
    
    // Points the pdf converter to the mutable data object and to the UIView to be converted
    UIGraphicsBeginPDFContextToData(pdfData, self.bounds, nil);
    UIGraphicsBeginPDFPage();
    CGContextRef pdfContext = UIGraphicsGetCurrentContext();
    
    
    // draws rect to the view and thus this is captured by UIGraphicsBeginPDFContextToData
    
    [self.layer renderInContext:pdfContext];
    
    // remove PDF rendering context
    UIGraphicsEndPDFContext();
    
    return pdfData;
}

- (UIImage *)image {
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, self.opaque, [[UIScreen mainScreen] scale]);
    [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}

- (UIView *)subviewWithTag:(NSInteger)tag {
    for (UIView *subview in self.subviews) {
        if (subview.tag == tag) {
            return subview;
        }
    }
    
    return nil;
}

- (void)fitToSize:(CGSize)size {
    CGSize actualSize = self.frame.size;
    
    if (actualSize.width > size.width || actualSize.height > size.height) {
        CGFloat widthFactor = actualSize.width / size.width;
        CGFloat heightFactor = actualSize.height / size.height;
        CGFloat factor = widthFactor > heightFactor ? widthFactor : heightFactor;
        
        self.frame = (CGRect){self.frame.origin, CGSizeMake(roundf(actualSize.width / factor), roundf(actualSize.height / factor))};
    }
}

- (void)cutWidthTo:(CGFloat)width {
    if (self.width > width) {
        self.frame = (CGRect){self.origin, CGSizeMake(width, self.height)};
    }
}

- (void)makeFrameIntegral {
    self.frame = CGRectIntegral(self.frame);
}

- (void)setIntegralCenter:(CGPoint)center {
    self.center = center;
    [self makeFrameIntegral];
}

- (CGPoint)localCenter {
    return CGPointMake(self.frame.size.width / 2.f, self.frame.size.height / 2.f);
}

- (CGSize)size {
    return self.frame.size;
}

- (void)setSize:(CGSize)size {
    self.frame = (CGRect){self.origin, size};
}

- (CGPoint)origin {
    return self.frame.origin;
}

- (void)setOrigin:(CGPoint)origin {
    self.frame = (CGRect){origin, self.size};
}

- (CGFloat)x {
    return self.origin.x;
}

- (void)setX:(CGFloat)x {
    self.frame = (CGRect){CGPointMake(x, self.y), self.size};
}

- (CGFloat)y {
    return self.origin.y;
}

- (void)setY:(CGFloat)y {
    self.frame = (CGRect){CGPointMake(self.x, y), self.size};
}

- (CGFloat)width {
    return self.frame.size.width;
}

- (void)setWidth:(CGFloat)width {
    self.frame = (CGRect){self.origin, CGSizeMake(width, self.height)};
}

- (CGFloat)height {
    return self.frame.size.height;
}

- (void)setHeight:(CGFloat)height {
    self.frame = (CGRect){self.origin, CGSizeMake(self.width, height)};
}

- (CGFloat)halfWidth {
    return self.frame.size.width / 2.f;
}

- (CGFloat)halfHeight {
    return self.frame.size.height / 2.f;
}

- (CGFloat)maxX {
    return CGRectGetMaxX(self.frame);
}

- (CGFloat)maxY {
    return CGRectGetMaxY(self.frame);
}

- (CGFloat)minX {
    return CGRectGetMinX(self.frame);
}

- (CGFloat)minY {
    return CGRectGetMinY(self.frame);
}

- (CGFloat)centerX {
    return self.center.x;
}

- (void)setCenterX:(CGFloat)centerX {
    self.center = CGPointMake(centerX, self.centerY);
}

- (CGFloat)centerY {
    return self.center.y;
}

- (void)setCenterY:(CGFloat)centerY {
    self.center = CGPointMake(self.centerX, centerY);
}

@end


@implementation UIImage(AdditionalMethods)

- (UIImage *)imageScaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [self drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (BOOL)compareWithImage:(UIImage *)image {
    unsigned char resultA[CC_MD5_DIGEST_LENGTH];
    NSData *imageDataA = [NSData dataWithData:UIImagePNGRepresentation(self)];
    CC_MD5([imageDataA bytes], (CC_LONG)[imageDataA length], resultA);
    NSString *imageHashA = [NSString stringWithFormat:
                            @"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
                            resultA[0], resultA[1], resultA[2], resultA[3],
                            resultA[4], resultA[5], resultA[6], resultA[7],
                            resultA[8], resultA[9], resultA[10], resultA[11],
                            resultA[12], resultA[13], resultA[14], resultA[15]
                            ];
    
    unsigned char resultB[CC_MD5_DIGEST_LENGTH];
    NSData *imageDataB = [NSData dataWithData:UIImagePNGRepresentation(image)];
    CC_MD5([imageDataB bytes], (CC_LONG)[imageDataB length], resultB);
    NSString *imageHashB = [NSString stringWithFormat:
                            @"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
                            resultB[0], resultB[1], resultB[2], resultB[3],
                            resultB[4], resultB[5], resultB[6], resultB[7],
                            resultB[8], resultB[9], resultB[10], resultB[11],
                            resultB[12], resultB[13], resultB[14], resultB[15]
                            ];
    
    return [imageHashA isEqualToString:imageHashB];
}

@end


@implementation NSMutableArray(AdditionalMethods)

- (void)reverse {
    if (self.count == 0) {
        return;
    }
    
    NSUInteger i = 0;
    NSUInteger j = self.count - 1;
    
    while (i < j) {
        [self exchangeObjectAtIndex:i withObjectAtIndex:j];
        
        i++;
        j--;
    }
}

@end


@implementation NSDate(AdditionalMethods)

- (NSDate *)beginningOfMonthDate {
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSYearCalendarUnit | NSMonthCalendarUnit fromDate:self];
    components.day = 1;
    components.hour = components.minute = components.second = 0;
    
    return [[NSCalendar currentCalendar] dateFromComponents:components];
}

- (NSUInteger)dayFromNil {
    return [self dayFromOne] - 1;
}

- (NSUInteger)dayFromOne {
    NSDateComponents *comps = [[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] components:NSDayCalendarUnit fromDate:self];
    return comps.day;
}

@end


@interface TeoriusHelper() {
    UIView *darkView;
    UILabel *waitingLabel;
    UIView *waitingBaseView;
    UIActivityIndicatorView *indicatorView;
    UIImageView *checkImage;
}

@property (nonatomic) NSMutableDictionary *dictWaitingViews;
@property (nonatomic) NSMutableDictionary *dictTimeNoting;

@end


@implementation TeoriusHelper


#pragma mark - Shared instance

+ (TeoriusHelper *)instance {
    
    static TeoriusHelper *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [TeoriusHelper new];
    });
    
    return sharedInstance;
}


#pragma mark - String

+ (NSString *)stringFromFloatValue:(CGFloat)floatValue {
    NSString *valueString = floatValue == floorf(floatValue) ? [NSString  stringWithFormat:@"%.0f", floatValue] : [NSString  stringWithFormat:@"%.1f", floatValue];
    return valueString;
}


#pragma mark - Usual common operations

+ (NSString *)getUUID
{
    CFUUIDRef uuidRef = CFUUIDCreate(NULL);
    CFStringRef uuidStringRef = CFUUIDCreateString(NULL, uuidRef);
    CFRelease(uuidRef);
    return (__bridge_transfer NSString *)uuidStringRef;
}

+ (void)printAllFontNames {
    for (NSString *familyName in [UIFont familyNames]) {
        
        for (NSString *fontName in [UIFont fontNamesForFamilyName:familyName]) {
            
            NSLog(@"%@", fontName);
        }
    }
}

+ (void)showStandartAlertInMainThreadWithTitle:(NSString *)title body:(NSString *)body {
    dispatch_async(dispatch_get_main_queue(), ^{
        [[[UIAlertView alloc] initWithTitle:title message:body delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    });
}

+ (void)tableViewDeselectSelectedCell:(UITableView *)tableView afterDelay:(CGFloat)delay {
    [self performSelector:@selector(tableViewDeselectSelectedCell:) withObject:tableView afterDelay:delay];
}

+ (void)tableViewDeselectSelectedCell:(UITableView *)tableView {
    [tableView deselectRowAtIndexPath:tableView.indexPathForSelectedRow animated:YES];
}


#pragma mark - Convertion and calculation

+ (double)radiansToDegrees:(double)radians {
    
    return (radians * (180 / M_PI));
}

+ (double)degreesToRadians:(double)degrees {
    
    return (degrees / 180 * M_PI);
}


#pragma mark - Validation

+ (BOOL)validateEmail:(NSString *) candidate {
    NSString *emailRegEx = @"^[^@]+@[^\\.]+\\..+$";
    /*@"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
     @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
     @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
     @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
     @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
     @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
     @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";*/
    
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES[c] %@", emailRegEx];
    
    return [emailTest evaluateWithObject:candidate];
}

+ (BOOL)validatePassword:(NSString *) candidate {
    NSString *regEx = @"([A-Z]|[a-z]|[0-9]|[!\"#\\$%&'\\(\\)\\*\\+,\\-\\.\\/:;<=>\\?@\\[\\\\\\]\\^_`\\{\\|\\}~])+";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES[c] %@", regEx];
    
    return [test evaluateWithObject:candidate];
}

+ (BOOL)stringIsValid:(NSString *)string {
    if (!string) {
        return NO;
    }
    if (![string isKindOfClass:[NSString class]]) {
        return NO;
    }
    if (string.length == 0) {
        return NO;
    }
    
    return YES;
}

+ (BOOL)dataIsValid:(NSData *)data {
    if (!data) {
        return NO;
    }
    if (![data isKindOfClass:[NSData class]]) {
        return NO;
    }
    if (data.length == 0) {
        return NO;
    }
    
    return YES;
}

+ (BOOL)arrayIsValid:(NSArray *)array {
    if (!array) {
        return NO;
    }
    if (![array isKindOfClass:[NSArray class]]) {
        return NO;
    }
    if (array.count == 0) {
        return NO;
    }
    
    return YES;
}

+ (BOOL)dictIsValid:(NSDictionary *)dict {
    if (!dict) {
        return NO;
    }
    if (![dict isKindOfClass:[NSDictionary class]]) {
        return NO;
    }
    if (dict.count == 0) {
        return NO;
    }
    
    return YES;
}

+ (BOOL)numberIsValid:(NSNumber *)number {
    if (!number) {
        return NO;
    }
    if (![number isKindOfClass:[NSNumber class]]) {
        return NO;
    }
    
    return YES;
}

+ (BOOL)dateIsValid:(NSDate *)date {
    if (!date) {
        return NO;
    }
    if (![date isKindOfClass:[NSDate class]]) {
        return NO;
    }
    
    return YES;
}


#pragma mark - PDF

+ (void)createPDFfromUIView:(UIView*)aView saveToDocumentsWithFileName:(NSString*)aFilename
{
    NSData *pdfData = [aView pdf];
    
    NSString *pathString = @"/Users/dns/Desktop";
    pathString = [pathString stringByAppendingPathComponent:aFilename];
    
    // instructs the mutable data object to write its context to a file on disk
    [pdfData writeToFile:pathString atomically:YES];
    
    //    UIGraphicsBeginImageContext(aView.bounds.size);
    //
    //    [aView.layer renderInContext:UIGraphicsGetCurrentContext()];
    //
    //    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    //
    //    UIGraphicsEndImageContext();
    //
    //    NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
    //    NSString *pathString = @"/Users/dns/Desktop";
    //    pathString = [pathString stringByAppendingPathComponent:aFilename];
    //
    //    [imageData writeToFile:pathString atomically:YES];
    
}


#pragma mark - Waiting view

- (void)addWaitingViewWithText:(NSString *)text font:(UIFont *)font toView:(UIView *)view disablingInteractionEvents:(BOOL)disablingInteractionEvents {
    if (disablingInteractionEvents) {
        [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    }
    
    darkView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, view.frame.size.width, view.frame.size.height)];
    darkView.backgroundColor = [UIColor colorWithRed:0.f/255.f green:0.f/255.f blue:0.f/255.f alpha:.6f];
    [view addSubview:darkView];
    
    indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    waitingLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(indicatorView.frame) + 10.f, darkView.frame.size.width - 20.f, 0)];
    waitingLabel.backgroundColor = [UIColor clearColor];
    waitingLabel.textColor = [UIColor whiteColor];
    waitingLabel.numberOfLines = 0;
    waitingLabel.textAlignment = NSTextAlignmentCenter;
    waitingLabel.font = font;
    waitingLabel.text = text;
    [waitingLabel sizeToFit];
    
    waitingBaseView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MAX(waitingLabel.frame.size.width, indicatorView.frame.size.width), CGRectGetMaxY(waitingLabel.frame))];
    waitingBaseView.backgroundColor = [UIColor clearColor];
    [waitingBaseView addSubview:indicatorView];
    [waitingBaseView addSubview:waitingLabel];
    [darkView addSubview:waitingBaseView];
    waitingBaseView.center = CGPointMake(darkView.frame.size.width / 2.f, darkView.frame.size.height / 2.f);
    
    indicatorView.center = CGPointMake(waitingBaseView.frame.size.width / 2.f, indicatorView.center.y);
    waitingLabel.center = CGPointMake(waitingBaseView.frame.size.width / 2.f, waitingLabel.center.y);
    [indicatorView startAnimating];
}

- (void)addWaitingViewWithKey:(NSString *)key text:(NSString *)text font:(UIFont *)font toView:(UIView *)view disablingInteractionEvents:(BOOL)disablingInteractionEvents {
    if (disablingInteractionEvents) {
        [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    }
    
    UIView *waitingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, view.frame.size.width, view.frame.size.height)];
    waitingView.backgroundColor = [UIColor colorWithRed:0.f/255.f green:0.f/255.f blue:0.f/255.f alpha:.6f];
    [view addSubview:waitingView];
    self.dictWaitingViews[key] = waitingView;
    
    UIActivityIndicatorView *indicatorViewA = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    indicatorViewA.tag = WaitingViewSubviewLoadingIndicator;
    
    UILabel *waitingLabelA = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(indicatorViewA.frame) + 10.f, waitingView.frame.size.width - 20.f, 0)];
    waitingLabelA.backgroundColor = [UIColor clearColor];
    waitingLabelA.textColor = [UIColor whiteColor];
    waitingLabelA.numberOfLines = 0;
    waitingLabelA.textAlignment = NSTextAlignmentCenter;
    waitingLabelA.font = font;
    waitingLabelA.text = text;
    waitingLabelA.tag = WaitingViewSubviewLabel;
    [waitingLabelA sizeToFit];
    
    UIView *waitingBaseViewA = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MAX(waitingLabelA.frame.size.width, indicatorViewA.frame.size.width), CGRectGetMaxY(waitingLabelA.frame))];
    waitingBaseViewA.backgroundColor = [UIColor clearColor];
    [waitingBaseViewA addSubview:indicatorViewA];
    [waitingBaseViewA addSubview:waitingLabelA];
    waitingBaseViewA.tag = WaitingViewSubviewBase;
    [waitingView addSubview:waitingBaseViewA];
    waitingBaseViewA.center = CGPointMake(waitingView.frame.size.width / 2.f, waitingView.frame.size.height / 2.f);
    
    indicatorViewA.center = CGPointMake(waitingBaseViewA.frame.size.width / 2.f, indicatorViewA.center.y);
    waitingLabelA.center = CGPointMake(waitingBaseViewA.frame.size.width / 2.f, waitingLabelA.center.y);
    [indicatorViewA startAnimating];
}

- (void)changeWaitingViewAfterSuccessWithImage:(NSString *)imageName text:(NSString *)text completion:(void (^)(void))block {
    
    checkImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
    [checkImage sizeToFit];
    checkImage.center = indicatorView.center;
    [waitingBaseView addSubview:checkImage];
    
    [indicatorView stopAnimating];
    [indicatorView removeFromSuperview];
    indicatorView = nil;
    
    waitingLabel.frame = CGRectMake(0, CGRectGetMaxY(checkImage.frame) + 10.f, darkView.frame.size.width - 20.f, 0);
    waitingLabel.text = text;
    [waitingLabel sizeToFit];
    waitingLabel.center = CGPointMake(waitingBaseView.frame.size.width / 2.f, waitingLabel.center.y);
    
    CGRect fr = waitingBaseView.frame;
    fr.size.height = CGRectGetMaxY(waitingLabel.frame);
    waitingBaseView.frame = fr;
    
    block();
}

- (void)changeWaitingViewWithKey:(NSString *)key afterSuccessWithImage:(NSString *)imageName text:(NSString *)text completion:(void (^)(void))block {
    UIView *waitingView = self.dictWaitingViews[key];
    
    if (!waitingView) {
        return;
    }
    
    UIView *waitingBaseViewA = [waitingView subviewWithTag:WaitingViewSubviewBase];
    
    if (!waitingBaseViewA) {
        return;
    }
    
    UIActivityIndicatorView *indicatorViewA = (UIActivityIndicatorView *)[waitingBaseViewA subviewWithTag:WaitingViewSubviewLoadingIndicator];
    
    UIImageView *checkImageA = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
    [checkImageA sizeToFit];
    checkImageA.tag = WaitingViewSubviewImage;
    checkImageA.center = indicatorViewA.center;
    [waitingBaseViewA addSubview:checkImageA];
    
    [indicatorViewA stopAnimating];
    [indicatorViewA removeFromSuperview];
    
    UILabel *waitingLabelA = (UILabel *)[waitingBaseViewA subviewWithTag:WaitingViewSubviewLabel];
    waitingLabelA.frame = CGRectMake(0, CGRectGetMaxY(checkImageA.frame) + 10.f, waitingView.frame.size.width - 20.f, 0);
    waitingLabelA.text = text;
    [waitingLabelA sizeToFit];
    waitingLabelA.center = CGPointMake(waitingBaseViewA.frame.size.width / 2.f, waitingLabelA.center.y);
    
    CGRect fr = waitingBaseViewA.frame;
    fr.size.height = CGRectGetMaxY(waitingLabelA.frame);
    waitingBaseViewA.frame = fr;
    
    block();
}

- (void)removeWaitingView {
    [darkView removeFromSuperview];
    darkView = nil;
    if ([UIApplication sharedApplication].isIgnoringInteractionEvents) {
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    }
}

- (void)removeWaitingViewWithKey:(NSString *)key {
    UIView *waitingView = self.dictWaitingViews[key];
    
    if (!waitingView) {
        return;
    }
    
    [waitingView removeFromSuperview];
    [self.dictWaitingViews removeObjectForKey:key];
    if ([UIApplication sharedApplication].isIgnoringInteractionEvents) {
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    }
}

- (void)removeWaitingViewAnimatedWithCompletion:(void (^)(void))block {
    [UIView animateWithDuration:.4
                     animations:^{
                         darkView.alpha = 0;
                     }
                     completion:^(BOOL finished) {
                         [darkView removeFromSuperview];
                         darkView = nil;
                         
                         if ([UIApplication sharedApplication].isIgnoringInteractionEvents) {
                             [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                         }
                         
                         if (block) {
                             block();
                         }
                     }];
}

- (void)removeWaitingViewWithKey:(NSString *)key animatedWithCompletion:(void (^)(void))block {
    UIView *waitingView = self.dictWaitingViews[key];
    
    if (!waitingView) {
        return;
    }
    
    [UIView animateWithDuration:.4
                     animations:^{
                         waitingView.alpha = 0;
                     }
                     completion:^(BOOL finished) {
                         [waitingView removeFromSuperview];
                         [self.dictWaitingViews removeObjectForKey:key];
                         
                         if ([UIApplication sharedApplication].isIgnoringInteractionEvents) {
                             [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                         }
                         
                         if (block) {
                             block();
                         }
                     }];
}

- (BOOL)isShowingWaitingView {
    return (darkView != nil || self.dictWaitingViews.count != 0);
}

+ (UIView *)addDarkViewWithColorAlpha:(CGFloat)alphaColor alpha:(CGFloat)alpha toView:(UIView *)view withY:(CGFloat)y {
    UIView *darkView1 = [[UIView alloc] initWithFrame:CGRectMake(0, y, view.frame.size.width, view.frame.size.height - y)];
    darkView1.backgroundColor = [UIColor colorWithRed:0.f / 255.f green:0.f / 255.f blue:0.f / 255.f alpha:alphaColor];
    darkView1.alpha = alpha;
    [view addSubview:darkView1];
    
    return darkView1;
}

+ (void)removeDarkViewWithAnimation:(UIView *)darkView1 {
    
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    
    [UIView animateWithDuration:.3 animations:^{
        darkView1.alpha = 0;
    } completion:^(BOOL finished) {
        [darkView1 removeFromSuperview];
        if ([UIApplication sharedApplication].isIgnoringInteractionEvents) {
            [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        }
    }];
}


#pragma mark - Objects

+ (void)setValuesForObject:(id)object fromDict:(NSDictionary *)dict withLinkingDict:(NSDictionary *)linkingDict {
    if (!object || ![TeoriusHelper dictIsValid:dict] || ![TeoriusHelper dictIsValid:linkingDict]) {
        return;
    }

    NSDictionary *namesAndTypesDict = [self propertiesDictFromObject:object];
    
    for (NSString *key in linkingDict.allKeys) {
        NSString *propertyName = linkingDict[key];
        if (![self stringIsValid:propertyName]) {
            propertyName = key;
        }
        
        if ([object respondsToSelector:NSSelectorFromString(propertyName)]) {
            NSString *type = namesAndTypesDict[propertyName];
            id value = dict[key];
            
            if (!value) {
                continue;
            }
            
            Class class = NSClassFromString(type);
            if (!class) {
                [object setValue:value forKey:propertyName];
                continue;
            }
            
            if (![value isKindOfClass:class]) {
                continue;
            }
            
            if ([value isKindOfClass:[NSString class]] || [value isKindOfClass:[NSData class]]) {
                if ([[value valueForKey:@"length"] intValue] == 0) {
                    continue;
                }
            }
//          !!!
//            else if ([value isKindOfClass:[NSArray class]] || [value isKindOfClass:[NSDictionary class]]) {
//                if ([[value valueForKey:@"count"] intValue] == 0) {
//                    continue;
//                }
//            }
            
            [object setValue:value forKey:propertyName];
        }
    }
}

+ (NSDictionary *)propertiesDictFromObject:(id)object {
    NSMutableDictionary *propertiesDict = [NSMutableDictionary new];
    
    unsigned int outCount, i;
    objc_property_t *properties = class_copyPropertyList([object class], &outCount);
    for(i = 0; i < outCount; i++) {
        objc_property_t property = properties[i];
        const char *propName = property_getName(property);
        if (propName) {
            const char *propType = getPropertyType(property);
            NSString *propertyName = [NSString stringWithCString:propName
                                                        encoding:[NSString defaultCStringEncoding]];
            NSString *propertyType = [NSString stringWithCString:propType
                                                        encoding:[NSString defaultCStringEncoding]];
            
            propertiesDict[propertyName] = propertyType;
        }
    }
    free(properties);
    
    return propertiesDict.copy;
}

static const char *getPropertyType(objc_property_t property) {
    const char *attributes = property_getAttributes(property);
    char buffer[1 + strlen(attributes)];
    strcpy(buffer, attributes);
    char *state = buffer, *attribute;
    while ((attribute = strsep(&state, ",")) != NULL) {
        if (attribute[0] == 'T') {
            if (strlen(attribute) <= 4) {
                break;
            }
            return (const char *)[[NSData dataWithBytes:(attribute + 3) length:strlen(attribute) - 4] bytes];
        }
    }
    return "@";
}


#pragma mark - Dictionary

+ (NSDictionary *)dictionaryByRemovingWrongValues:(NSDictionary *)dictionary withValueClasses:(NSDictionary *)classesDict {
    NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] initWithCapacity:dictionary.count];
    
    for (NSString *keyString in classesDict.allKeys) {
        
        NSString *classString = classesDict[keyString];
        id value = dictionary[keyString];
        
        if (!value || !classString)
            continue;
        
        if ([value isKindOfClass:NSClassFromString(classString)]) {
            
            tempDict[keyString] = value;
        }
        else if ([classString isEqualToString:NSStringFromClass([NSTimeZone class])]) {
            
            NSString *timeZoneString = value;
            NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:timeZoneString];
            
            if (timeZone)
                tempDict[keyString] = timeZone;
        }
    }
    
    return tempDict.copy;
}


#pragma mark - Views

+ (void)placeViewsHorizontally:(NSArray *)views inSuperview:(UIView *)superview toCenterPoint:(CGPoint)centerPoint {
    UIView *firstView = views.firstObject;
    UIView *lastView = views.lastObject;
    
    CGFloat middleX = (lastView.maxX - firstView.minX) / 2.f + firstView.minX;
    
    for (UIView *view in views) {
        [view setIntegralCenter:CGPointMake(centerPoint.x - (middleX - view.center.x), centerPoint.y)];
    }
}

+ (void)addAndPlaceViewsHorizontally:(NSArray *)views inSuperview:(UIView *)superview toCenterPoint:(CGPoint)centerPoint {
    for (UIView *view in views) {
        [superview addSubview:view];
    }
    
    [self placeViewsHorizontally:views inSuperview:superview toCenterPoint:centerPoint];
}

+ (void)placeViewsHorizontally:(NSArray *)views inSuperview:(UIView *)superview {
    [self placeViewsHorizontally:views inSuperview:superview toCenterPoint:superview.localCenter];
}

+ (void)addAndPlaceViewsHorizontally:(NSArray *)views inSuperview:(UIView *)superview {
    [self addAndPlaceViewsHorizontally:views inSuperview:superview toCenterPoint:superview.localCenter];
}


+ (void)placeViewsVertically:(NSArray *)views inSuperview:(UIView *)superview toCenterPoint:(CGPoint)centerPoint {
    UIView *firstView = views.firstObject;
    UIView *lastView = views.lastObject;
    
    CGFloat middleY = (lastView.maxY - firstView.minY) / 2.f + firstView.minY;
    
    for (UIView *view in views) {
        [view setIntegralCenter:CGPointMake(centerPoint.x, centerPoint.y - (middleY - view.center.y))];
    }
}

+ (void)addAndPlaceViewsVertically:(NSArray *)views inSuperview:(UIView *)superview toCenterPoint:(CGPoint)centerPoint {
    for (UIView *view in views) {
        [superview addSubview:view];
    }
    
    [self placeViewsVertically:views inSuperview:superview toCenterPoint:centerPoint];
}

+ (void)placeViewsVertically:(NSArray *)views inSuperview:(UIView *)superview {
    [self placeViewsVertically:views inSuperview:superview toCenterPoint:superview.localCenter];
}

+ (void)addAndPlaceViewsVertically:(NSArray *)views inSuperview:(UIView *)superview {
    [self addAndPlaceViewsVertically:views inSuperview:superview toCenterPoint:superview.localCenter];
}

+ (void)placeSubviews:(NSArray *)subviews withPositioning:(ViewsPositioning)positioning alignment:(ViewAlignment)alignment toPoint:(CGPoint)point {
    UIView *firstView = subviews.firstObject;
    UIView *lastView = subviews.lastObject;
    
    CGFloat middleValue;
    
    if (positioning == ViewsPositioningVertical) {
        middleValue = (lastView.maxY - firstView.minY) / 2.f + firstView.minY;
    }
    else {
        middleValue = (lastView.maxX - firstView.minX) / 2.f + firstView.minX;
    }
    
    for (UIView *view in subviews) {
        if (positioning == ViewsPositioningHorizontal) {
            if (alignment == ViewAlignmentCenter) {
                [view setIntegralCenter:CGPointMake(point.x - (middleValue - view.center.x), point.y)];
            }
            else if (alignment == ViewAlignmentTop) {
                [view setIntegralCenter:CGPointMake(point.x - (middleValue - view.center.x), point.y + view.halfHeight)];
            }
            else if (alignment == ViewAlignmentBottom) {
                [view setIntegralCenter:CGPointMake(point.x - (middleValue - view.center.x), point.y - view.halfHeight)];
            }
            else if (alignment == ViewAlignmentNone) {
                [view setIntegralCenter:CGPointMake(point.x - (middleValue - view.center.x), view.centerY)];
            }
        }
        else {
            if (alignment == ViewAlignmentCenter) {
                [view setIntegralCenter:CGPointMake(point.x, point.y - (middleValue - view.center.y))];
            }
            else if (alignment == ViewAlignmentLeft) {
                [view setIntegralCenter:CGPointMake(point.x + view.halfWidth, point.y - (middleValue - view.center.y))];
            }
            else if (alignment == ViewAlignmentRight) {
                [view setIntegralCenter:CGPointMake(point.x - view.halfWidth, point.y - (middleValue - view.center.y))];
            }
            else if (alignment == ViewAlignmentNone) {
                [view setIntegralCenter:CGPointMake(view.centerX, point.y - (middleValue - view.center.y))];
            }
        }
    }
}

+ (void)placeSubviews:(NSArray *)subviews withPositioning:(ViewsPositioning)positioning alignment:(ViewAlignment)alignment {
    UIView *subview = subviews.firstObject;
    if (!subviews) {
        return;
    }
    
    [self placeSubviews:subviews withPositioning:positioning alignment:alignment toPoint:subview.superview.localCenter];
}


#pragma mark - Time noting

- (int)getCurrentTimeInMilliseconds {
    const int64_t kOneMillion = 1000 * 1000;
    static mach_timebase_info_data_t s_timebase_info;
    
    if (s_timebase_info.denom == 0) {
        (void) mach_timebase_info(&s_timebase_info);
    }
    
    return (int)((mach_absolute_time() * s_timebase_info.numer) / (kOneMillion * s_timebase_info.denom));
}

- (void)setTimeNoteForKey:(NSString *)key {
    int time = [self getCurrentTimeInMilliseconds];
    self.dictTimeNoting[key] = @(time);
}

- (CGFloat)getTimeDifferenceForKey:(NSString *)key withLogging:(BOOL)logging loggingInfo:(NSString *)loggingInfo replacingPreviousNote:(BOOL)replacing {
    NSNumber *prevTime = self.dictTimeNoting[key];
    if (!prevTime) {
        if (logging) {
            NSLog(@"No previous time note for key: %@", key);
        }
        
        return 0;
    }
    
    int time = [self getCurrentTimeInMilliseconds];
    
    CGFloat difference = (CGFloat)(time - prevTime.intValue) / 1000.f;
    
    if (logging) {
        NSLog(@"Time difference for key: \"%@\" with info: \"%@\" is %f", key, loggingInfo, difference);
    }
    if (replacing) {
        self.dictTimeNoting[key] = @(time);
    }
    
    return difference;
}

- (void)clearTimeNoteInfoForKey:(NSString *)key {
    [self.dictTimeNoting removeObjectForKey:key];
}


#pragma mark - Setters/Getters

- (NSMutableDictionary *)dictWaitingViews {
    if (!_dictWaitingViews) {
        _dictWaitingViews = [NSMutableDictionary new];
    }
    
    return _dictWaitingViews;
}

- (NSMutableDictionary *)dictTimeNoting {
    if (!_dictTimeNoting) {
        _dictTimeNoting = [NSMutableDictionary new];
    }
    
    return _dictTimeNoting;
}


@end
