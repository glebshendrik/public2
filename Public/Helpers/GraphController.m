//
//  GraphFactory.m
//  Public
//
//  Created by Arslan Zagidullin on 24.09.15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import "GraphController.h"
#import "SharedClass.h"
#import "MainAPI.h"
#import "Reading.h"
#import "Measurement.h"
#import <objc/runtime.h>


@interface CPTScatterPlot (Public)

@property (nonatomic) CPTGradient* savedGradient;
@property (nonatomic) CPTColor* savedColor;

@end


static void * SavedGradientPropertyKey = &SavedGradientPropertyKey;
static void * SavedColorPropertyKey = &SavedColorPropertyKey;

@implementation CPTScatterPlot (Public)

- (CPTGradient *)savedGradient {
    return objc_getAssociatedObject(self, SavedGradientPropertyKey);
}

- (void)setSavedGradient:(CPTGradient *)savedGradient {
    objc_setAssociatedObject(self, SavedGradientPropertyKey, savedGradient, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (CPTColor *)savedColor {
    return objc_getAssociatedObject(self, SavedColorPropertyKey);
}

- (void)setSavedColor:(CPTColor *)savedColor {
    objc_setAssociatedObject(self, SavedColorPropertyKey, savedColor, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

@end


@interface GraphController () <CPTScatterPlotDataSource, CPTScatterPlotDelegate, CPTPlotSpaceDelegate>

@property (nonatomic) CPTGraphHostingView *hostingView;

@property (nonatomic) CPTColor* defaultPlotColor;
@property (nonatomic) CPTColor* abnormalPlotColor;

@property (nonatomic) CPTPlotRange *plotsXRange;
@property (nonatomic) CPTPlotRange *plotsYRange;

@property (nonatomic) Measurement* randomMeasurement;
@property (nonatomic) BOOL isBloodPressure;
@property (nonatomic) BOOL isHeight;
@property (nonatomic) NSInteger selectedPlotsCount;

@property (nonatomic) NSDictionary *readingsDict;
@property (nonatomic) NSMutableDictionary *colorsDict;

@property (nonatomic) float minValue;
@property (nonatomic) float maxValue;
@property (nonatomic) float minNorm;
@property (nonatomic) float maxNorm;

@end


@implementation GraphController

- (id)initWithMeasurementsArray:(NSArray *)measurementsArray readingsArray:(NSArray *)readingsArray dateFrom:(NSDate *)dateFrom dateTo:(NSDate *)dateTo type:(GraphControllerType)type {
    if (self = [super init]) {
        self.measurementsArray = measurementsArray;
        self.readingsArray = readingsArray;
        self.dateFrom = dateFrom;
        self.dateTo = dateTo;
        self.axesAreVisible = YES;
        self.normalAreaIsVisible = YES;
        self.leftRightPadding = 30;
        self.topBottomPadding = 20;
        self.scaleX = 1.3f;
        self.scaleY = 1.3f;
        self.symbolSize = CGSizeMake(6, 6);
        self.isInteractive = YES;
        self.type = type;
        
        NSMutableArray* selectedPlots = [NSMutableArray array];
        if (self.type == GraphControllerTypeDefault) {
            if (!self.isBloodPressure) {
                [selectedPlots addObject:@YES];
                for (int i = 1; i < self.measurementsArray.count; i++) {
                    [selectedPlots addObject:@NO];
                }
            }
            else {
                [selectedPlots addObject:@YES];
                [selectedPlots addObject:@YES];
            }
        }
        else {
            for (int i = 0; i < self.measurementsArray.count; i++) {
                Measurement *m = self.measurementsArray[i];
                NSArray *readings = self.readingsDict[m.plotID];
                
                if (readings.count > 0) {
                    [selectedPlots addObject:@YES];
                }
                else {
                    [selectedPlots addObject:@NO];
                }
            }
        }
        
        self.selectedPlots = [selectedPlots copy];
    }
    
    for (Measurement *m in self.measurementsArray) {
        NSLog(@"measurement: %@, min: %f, max: %f", m.name, m.norm.min, m.norm.max);
    }
    
    return self;
}

- (CPTGraphHostingView*)getHostingView:(CGRect)tmpFrame {
    CPTGraphHostingView* hostingView = [[CPTGraphHostingView alloc] initWithFrame:tmpFrame];
    hostingView.backgroundColor = [UIColor whiteColor];
    hostingView.allowPinchScaling = NO;
    self.hostingView = hostingView;
    
    [self configureGraph];
    [self configurePlots];
    [self configurePlotsScaleAndColor];
    [self configureAxes];
    if (self.normalAreaIsVisible)
        [self addNormalArea];
    
    return self.hostingView;
}

- (void)reloadData {
    if (self.hostingView == NULL || self.hostingView.hostedGraph == NULL)
        return;
    
    self.readingsDict = nil;
    
    CPTGraph* graph = self.hostingView.hostedGraph;
    [graph reloadData];
    
    [self configurePlots];
    [self configurePlotsScaleAndColor];
    [self configureAxes];
    
    for (CPTPlot* p in [graph allPlots])
        [p reloadData];
}

- (void)configureGraph {
    CPTTheme *theme = [CPTTheme new];
    CPTGraph *graph = [[CPTXYGraph alloc] initWithFrame:self.hostingView.frame];
    [graph applyTheme:theme];
    self.hostingView.hostedGraph = graph;
    
//    graph.defaultPlotSpace.allowsUserInteraction = YES;
//    graph.defaultPlotSpace.delegate = self;
    
    graph.paddingLeft = 15.f;//self.leftRightPadding;
    graph.paddingRight = 20.f;//self.type == GraphControllerTypeWidget ? 20.f : 0;//self.leftRightPadding;
    graph.paddingTop = self.topBottomPadding;
    graph.paddingBottom = self.topBottomPadding;
    
    graph.plotAreaFrame.masksToBorder = NO;
}

- (void)configurePlots {
    // calculate min and max values on Y axis
    CGFloat minValue = CGFLOAT_MAX;
    CGFloat maxValue = CGFLOAT_MIN;
    CGFloat minNorm = CGFLOAT_MAX;
    CGFloat maxNorm = CGFLOAT_MIN;
    
    for (Measurement *m in self.measurementsArray) {
        if (![self measurementIsSelected:m]) {
            continue;
        }
        
        minValue = MIN(minValue, m.norm.min);
        maxValue = MAX(maxValue, m.norm.max);
        minNorm = MIN(minValue, m.norm.min);
        maxNorm = MAX(maxValue, m.norm.max);
        
        NSArray *readings = self.readingsDict[m.plotID];
        
        for (Reading *r in readings) {
            minValue = MIN(minValue, r.value);
            maxValue = MAX(maxValue, r.value);
        }
    }

    // Config plots
    CPTGraph *graph = self.hostingView.hostedGraph;
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *) graph.defaultPlotSpace;
    NSTimeInterval dateFrom = [self.dateFrom timeIntervalSince1970];
    NSTimeInterval dateTo = [self.dateTo timeIntervalSince1970];
    
    self.minValue = minValue;
    self.maxValue = maxValue;
    self.minNorm = minNorm;
    self.maxNorm = maxNorm;

    if (self.type == GraphControllerTypeDefault) {
        float difference = maxValue - minValue;
        float addition = difference / 5;
        minValue -= addition;
        minValue = MAX(minValue, 0);
        maxValue += addition;
    }
    
    plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromDouble(dateFrom) length:CPTDecimalFromDouble(dateTo - dateFrom)];
    plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(minValue) length:CPTDecimalFromFloat(maxValue - minValue)];

    for (CPTPlot* p in [graph allPlots])
        [graph removePlot:p];
    
    for (int i=0; i<self.measurementsArray.count; i++) {
        BOOL isPlotSelected = [self.selectedPlots[i] boolValue];
        if (!isPlotSelected)
            continue;
        
        Measurement* m = self.measurementsArray[i];
        CPTScatterPlot* mainPlot = [self getPlotWithID:m.plotID];
        mainPlot.savedColor = [CPTColor colorWithCGColor: [[SharedClass getColorOfPlotNumber:i] CGColor]];
        [graph addPlot:mainPlot toPlotSpace:plotSpace];
    }
}

- (void)configurePlotsScaleAndColor {

    // Set axis ranges
    CPTGraph *graph = self.hostingView.hostedGraph;
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *) graph.defaultPlotSpace;
//
    if (self.type == GraphControllerTypeWidget) {
        [plotSpace scaleToFitPlots:[graph allPlots]];
    }
//
    self.plotsYRange = plotSpace.yRange;
    CPTMutablePlotRange *yRange = [self.plotsYRange mutableCopy];
//    yRange.location = CPTDecimalFromCGFloat(minValue);
//    yRange.length = CPTDecimalFromCGFloat(maxValue - minValue);
    [yRange expandRangeByFactor:CPTDecimalFromCGFloat(self.scaleY)];
    plotSpace.yRange = yRange;
//
//    float xRangeLocation = [self.dateFrom timeIntervalSince1970];
//    float xRangeLength = [self.dateTo timeIntervalSince1970] - xRangeLocation;
//    self.plotsXRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(xRangeLocation) length:CPTDecimalFromFloat(xRangeLength)];
    self.plotsXRange = plotSpace.xRange;
    if (self.type == GraphControllerTypeWidget) {
        CPTMutablePlotRange *xRange = [self.plotsXRange mutableCopy];
        [xRange expandRangeByFactor:CPTDecimalFromCGFloat(self.scaleX)];
        plotSpace.xRange = xRange;
    }
    
    // PLOT COLOR
    
    if ((self.selectedPlotsCount <= 1 && !self.isHeight) || self.isBloodPressure)
        for (CPTScatterPlot* plot in [graph allPlots]) {
            CPTPlotRange* range = [plot plotRangeForCoordinate:CPTCoordinateY];
            
            float yRangeLocation = [[NSDecimalNumber decimalNumberWithDecimal:range.location] floatValue];
            float yRangeLength = [[NSDecimalNumber decimalNumberWithDecimal:range.length] floatValue];
            float normalValueLow = (self.minNorm-yRangeLocation) / yRangeLength;
            float normalValueHigh = (self.maxNorm-yRangeLocation) / yRangeLength;
            CPTColor* normalColor = self.defaultPlotColor;
            CPTColor* abnormalColor = self.abnormalPlotColor;
            
            CPTGradient *gradient = [[CPTGradient alloc] init];
            
            if (normalValueLow<=0 && normalValueHigh>=0)
                gradient = [gradient addColorStop:normalColor atPosition:0];
            else
                gradient = [gradient addColorStop:abnormalColor atPosition:0];
            
            if (normalValueLow>0 && normalValueLow<1)
                gradient = [self gradient:gradient addTransitionFrom:abnormalColor to:normalColor atPositon:normalValueLow];
            if (normalValueHigh>0 && normalValueHigh<1)
                gradient = [self gradient:gradient addTransitionFrom:normalColor to:abnormalColor atPositon:normalValueHigh];
            
            if (normalValueLow<=1 && normalValueHigh>=1)
                gradient = [gradient addColorStop:normalColor atPosition:1];
            else
                gradient = [gradient addColorStop:abnormalColor atPosition:1];
            gradient.angle = 90;
            
            CPTFill *areaGradientFill = [CPTFill fillWithGradient:gradient];
            
            CPTMutableLineStyle *lineStyle = [plot.dataLineStyle mutableCopy];
            lineStyle.lineFill = areaGradientFill;
            lineStyle.lineWidth = 1;
            plot.dataLineStyle = lineStyle;
            
            plot.savedGradient = gradient;
            
            // TODO: @arslan: двойной рассчёт графика. можно ли обойтись одинарным?
            [plot reloadData];
        }
    else if (self.isHeight) {
        for (CPTScatterPlot* plot in [graph allPlots]) {
            CPTMutableLineStyle *lineStyle = [plot.dataLineStyle mutableCopy];
            lineStyle.lineFill = [CPTFill fillWithColor:self.defaultPlotColor];
            lineStyle.lineWidth = 1;
            plot.dataLineStyle = lineStyle;
        }
    }
    else if (self.selectedPlotsCount > 1 && !self.isBloodPressure && !self.isHeight) {
        for (CPTScatterPlot* plot in [graph allPlots]) {
            if (plot.savedColor != NULL) {
                CPTMutableLineStyle *lineStyle = [plot.dataLineStyle mutableCopy];
                lineStyle.lineFill = [CPTFill fillWithColor:plot.savedColor];
                lineStyle.lineWidth = 1;
                plot.dataLineStyle = lineStyle;
            }
        }
    }
}

- (void)configureAxes {
    CPTMutableTextStyle *axisTextStyle = [CPTMutableTextStyle textStyle];
    axisTextStyle.color = [CPTColor lightGrayColor];
    axisTextStyle.fontName = @"Roboto-Regular";
    axisTextStyle.fontSize = 11.0f;
    
    CPTMutableLineStyle *axisLineStyle = [CPTMutableLineStyle lineStyle];
    axisLineStyle.lineWidth = 0.0f;
    axisLineStyle.lineColor = [CPTColor blueColor];
    
    CPTXYAxisSet *axisSet = (CPTXYAxisSet *) self.hostingView.hostedGraph.axisSet;
    
    CPTXYAxis *x = axisSet.xAxis;
    CPTXYAxis *y = axisSet.yAxis;
    
    if (self.axesAreVisible) {
        NSString *dateFormat;
        NSCalendarUnit calendarUnit;
        NSTimeInterval datesDifference = [self.dateTo timeIntervalSinceDate:self.dateFrom];
        if (datesDifference <= 86400 * 2) {
            dateFormat = @"kk:mm";
            calendarUnit = NSCalendarUnitHour;
        }
        else if (datesDifference <= 5184000) {
            dateFormat = @"d.MM";
            calendarUnit = NSCalendarUnitDay;
        }
        else {
            dateFormat = @"MMM";
            calendarUnit = NSCalendarUnitMonth;
        }
        
        CPTCalendarFormatter* xAxisLabelFormatter = [[CPTCalendarFormatter alloc] initWithDateFormatter:[TeoriusHelper dateFormatterUsingFormat:dateFormat]];
        xAxisLabelFormatter.locale = [NSLocale localeWithLocaleIdentifier:@"ru_RU"];
        xAxisLabelFormatter.referenceCalendarUnit = NSSecondCalendarUnit;
        
        float xAxisMajorIntervalLength = [self calculateXAxisMajorIntervalLength:self.plotsXRange];
//        float xAxisLabelOrigin = [self calculateLabelOrigin:self.plotsXRange majorIntervalLength:xAxisMajorIntervalLength];
//        CPTPlotRange* xAxisExclusionRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0) length:CPTDecimalFromFloat(xAxisLabelOrigin)];
        
        x.axisLineStyle = axisLineStyle;
        x.labelingPolicy = CPTAxisLabelingPolicyFixedInterval;
        x.labelTextStyle = axisTextStyle;
        //        x.labelOffset = 0;
        x.labelFormatter = xAxisLabelFormatter;
//        x.labelExclusionRanges = @[xAxisExclusionRange];
        x.majorIntervalLength = CPTDecimalFromFloat(xAxisMajorIntervalLength);
        x.majorTickLineStyle = axisLineStyle;
        x.majorTickLength = 3.0f;
        x.minorTicksPerInterval = 3;
        x.minorTickLineStyle = axisLineStyle;
        x.minorTickLength = 1.0f;
        x.tickDirection = CPTSignNegative;
        x.tickLabelDirection = CPTSignNegative;
        
        x.orthogonalCoordinateDecimal = self.plotsYRange.location;
        
        Measurement* firstMeasurement = self.measurementsArray[0];
        NSNumberFormatter* labelFormatter = [[NSNumberFormatter alloc] init];
        [labelFormatter setNumberStyle: NSNumberFormatterDecimalStyle];
        labelFormatter.maximumFractionDigits = firstMeasurement.decimalDigits;
        labelFormatter.minimumFractionDigits = firstMeasurement.decimalDigits;
        
        float yAxisMajorIntervalLength = [self calculateYAxisMajorIntervalLength:self.plotsYRange];
//        float yAxisLabelOrigin = [self calculateLabelOrigin:self.plotsYRange majorIntervalLength:yAxisMajorIntervalLength];
//        CPTPlotRange* yAxisExclusionRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0) length:CPTDecimalFromFloat(yAxisLabelOrigin)];
        
        y.axisLineStyle = axisLineStyle;
        y.labelingPolicy = CPTAxisLabelingPolicyFixedInterval;
        y.labelTextStyle = axisTextStyle;
//        y.labelOffset = 35;
        y.labelFormatter = labelFormatter;
//        y.labelExclusionRanges = @[yAxisExclusionRange];
        y.majorIntervalLength = CPTDecimalFromFloat(yAxisMajorIntervalLength);
        y.majorTickLineStyle = axisLineStyle;
        y.majorTickLength = 3.0f;
        y.minorTicksPerInterval = 3;
        y.minorTickLineStyle = axisLineStyle;
        y.minorTickLength = 1.0f;
        y.tickDirection = CPTSignPositive;
        y.tickLabelDirection = CPTSignNegative;
        y.orthogonalCoordinateDecimal = self.plotsXRange.location;
    } else {
        x.labelingPolicy = CPTAxisLabelingPolicyNone;
        y.labelingPolicy = CPTAxisLabelingPolicyNone;
    }
}

- (CPTGradient*)gradient:(CPTGradient*)gradient addTransitionFrom:(CPTColor*)colorA to:(CPTColor*)colorB atPositon:(float)position {
    float transitionWidth = 0.1f;
    float positionA = position-transitionWidth/2;
    if (positionA<=0)
        positionA = 0;
    float positionB = position+transitionWidth/2;
    if (positionB>=1)
        positionB = 1;
    gradient = [gradient addColorStop:colorA atPosition:positionA];
    gradient = [gradient addColorStop:colorB atPosition:positionB];
    return gradient;
}

- (CPTScatterPlot*)getPlotWithID:(NSString*)plotID {
    
    CPTScatterPlot *plot = [[CPTScatterPlot alloc]init];
    plot.dataSource = self;
    if (self.isInteractive) {
        plot.plotSymbolMarginForHitDetection = 100;
        plot.delegate = self;
    }
//    plot.interpolation = CPTScatterPlotInterpolationCurved;
    plot.identifier = plotID;
    
    return plot;
}

- (float)calculateYAxisMajorIntervalLength:(CPTPlotRange*)range {
    float rangeLength = [[NSDecimalNumber decimalNumberWithDecimal:range.length] floatValue];
    float intervalLength = 50.0f;
    if (rangeLength<=1)
        intervalLength = 0.1f;
    else if (rangeLength<=3)
        intervalLength = 0.5f;
    else if (rangeLength<=10)
        intervalLength = 1.0f;
    else if (rangeLength<=30)
        intervalLength = 5.0f;
    else if (rangeLength<=100)
        intervalLength = 10.0f;
    return intervalLength;
}

- (float)calculateXAxisMajorIntervalLength:(CPTPlotRange*)range {
    const float kHour = 60*60;
    const float kDay = 24*kHour;

    NSTimeInterval datesDifference = [self.dateTo timeIntervalSinceDate:self.dateFrom];
//    int days = datesDifference / 86400;
//    float interval;
//
//    return interval;
    float intervalLength = 62*kDay;
    if (datesDifference <= 1*kDay)
        intervalLength = 4*kHour;
    else if (datesDifference <= 2*kDay)
        intervalLength = 8*kHour;
    else if (datesDifference <= 7*kDay)
        intervalLength = kDay;
    else if (datesDifference <= 8*kDay)
        intervalLength = 2*kDay;
    else if (datesDifference <= 16*kDay)
        intervalLength = 4*kDay;
    else if (datesDifference <= 32*kDay)
        intervalLength = 6*kDay;
    else if (datesDifference <= 60*kDay)
        intervalLength = 10*kDay;
    else if (datesDifference <= 95*kDay)
        intervalLength = 30*kDay;
    return intervalLength;
}

- (float)calculateLabelOrigin:(CPTPlotRange*)range majorIntervalLength:(float)majorIntervalLength {
    float rangeLocation = [[NSDecimalNumber decimalNumberWithDecimal:range.location] floatValue];
    float base = floorf(rangeLocation/majorIntervalLength);
    float labelOriginLower = (base-1) * majorIntervalLength;
    float labelOriginHigher = (base+0) * majorIntervalLength;
    return ((labelOriginLower-rangeLocation) > 0.5f*majorIntervalLength) ? labelOriginLower : labelOriginHigher;
}

- (void)addNormalArea {
    CPTXYAxisSet *axisSet = (CPTXYAxisSet *) self.hostingView.hostedGraph.axisSet;
    CPTXYAxis *y = axisSet.yAxis;
    
    for (Measurement* m in self.measurementsArray) {
        NSDecimal normalValueLowDecimal = [NSNumber numberWithFloat:m.normalValueLow].decimalValue;
        NSDecimal normalValueHighDecimal = [NSNumber numberWithFloat:m.normalValueHigh].decimalValue;
        CGPoint normalValueLowPoint  = [y viewPointForCoordinateDecimalNumber:normalValueLowDecimal];
        CGPoint normalValueHighPoint = [y viewPointForCoordinateDecimalNumber:normalValueHighDecimal];
        
        [self createBackgroundViewWithFrame:CGRectMake(0, normalValueLowPoint.y, SCREEN_WIDTH, normalValueHighPoint.y-normalValueLowPoint.y)];
    }
}

- (void)createBackgroundViewWithFrame:(CGRect)frame{
    UIView *view = [[UIView alloc]initWithFrame:frame];
    view.backgroundColor = UIColorFromRGB(0x53BEAF);
    view.alpha = .1f;
    [self.hostingView addSubview:view];
}


#pragma mark - CPTScatterPlotDataSource

- (NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot {
    Measurement *firstMeasurement = [self measurementForPlotID:plot.identifier];
    return [self readingsForMeasurementWithPlotID:firstMeasurement.plotID].count;
}

- (NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)idx {
    //    Measurement* firstMeasurement = self.measurementsArray[0];
    //    NSInteger valueCount = firstMeasurement.readings.count;
    
    Measurement* m = [self measurementForPlotID:plot.identifier];
    NSArray *readings = [self readingsForMeasurementWithPlotID:m.plotID];
    NSSortDescriptor *sorter = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:YES];
    readings = [readings sortedArrayUsingDescriptors:@[sorter]];
    Reading* r = readings[idx];
    if (fieldEnum == CPTScatterPlotFieldX)
        return [NSNumber numberWithDouble:[r.date timeIntervalSince1970]];
    else if (fieldEnum == CPTScatterPlotFieldY)
        return @(r.value);
    
    return NULL;
}

- (CPTPlotSymbol *)symbolForScatterPlot:(CPTScatterPlot *)plot recordIndex:(NSUInteger)idx {
    Measurement* m = [self measurementForPlotID:plot.identifier];
    NSArray *readings = [self readingsForMeasurementWithPlotID:m.plotID];
    NSSortDescriptor *sorter = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:YES];
    readings = [readings sortedArrayUsingDescriptors:@[sorter]];
    Reading* r = readings[idx];
    
    float value = r.value;
    
    CPTPlotRange* range = [plot plotRangeForCoordinate:CPTCoordinateY];
    float yRangeLocation = [[NSDecimalNumber decimalNumberWithDecimal:range.location] floatValue];
    float yRangeLength = [[NSDecimalNumber decimalNumberWithDecimal:range.length] floatValue];
    float normalizedValue = (value-yRangeLocation) / yRangeLength;
    
    CPTPlotSymbol* symbol = [CPTPlotSymbol ellipsePlotSymbol];
    symbol.size = self.symbolSize;
    CPTMutableLineStyle* lineStyle = [CPTMutableLineStyle lineStyle];
    symbol.lineStyle = lineStyle;
    
    
    if (((self.selectedPlotsCount <= 1 && !self.isHeight) || self.isBloodPressure) && plot.savedGradient != NULL) {
        CPTColor* symbolColor = [CPTColor colorWithCGColor:[plot.savedGradient newColorAtPosition:normalizedValue]];
        symbol.fill = [CPTFill fillWithColor:symbolColor];
        lineStyle.lineColor = symbolColor;
    } else if (self.isHeight) {
        symbol.fill = [CPTFill fillWithColor:self.defaultPlotColor];
        lineStyle.lineColor = self.defaultPlotColor;
    } else if ((self.selectedPlotsCount > 1 && !self.isBloodPressure && !self.isHeight) && plot.savedColor != NULL) {
        symbol.fill = [CPTFill fillWithColor:plot.savedColor];
        lineStyle.lineColor = plot.savedColor;
    }
    
    self.colorsDict[r.ID] = [UIColor colorWithCGColor:symbol.fill.cgColor];
    
    return symbol;
}


//#pragma mark - CPTScatterPlot delegate
//
//- (void)scatterPlot:(CPTScatterPlot *)plot plotSymbolWasSelectedAtRecordIndex:(NSUInteger)idx {
//    NSLog(@"idx = %d", (int)idx);
//}
//
//- (void)plot:(CPTPlot *)plot dataLabelTouchDownAtRecordIndex:(NSUInteger)idx {
//    NSLog(@"label = %d", (int)idx);
//}
//
//- (BOOL)plotSpace:(CPTPlotSpace *)space shouldHandlePointingDeviceDraggedEvent:(CPTNativeEvent *)event atPoint:(CGPoint)point {
//    return YES;
//}
//
//- (BOOL)plotSpace:(CPTPlotSpace *)space shouldHandlePointingDeviceDownEvent:(CPTNativeEvent *)event atPoint:(CGPoint)point {
//    return YES;
//}
//
//- (BOOL)plotSpace:(CPTPlotSpace *)space shouldHandlePointingDeviceUpEvent:(CPTNativeEvent *)event atPoint:(CGPoint)point {
//    return YES;
//}


#pragma mark - Interface

- (NSDictionary *)closestReadingForX:(CGFloat)x {
    CPTGraph *graph = self.hostingView.hostedGraph;
    
    x -= graph.paddingLeft;

    NSDecimal plotPoint[2];
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *) graph.defaultPlotSpace;
    [plotSpace plotPoint:plotPoint numberOfCoordinates:2 forPlotAreaViewPoint:CGPointMake(x, 0)];

    double dateInterval = [[NSDecimalNumber decimalNumberWithDecimal:plotPoint[CPTCoordinateX]] doubleValue];
    NSLog(@"%@", [NSDate dateWithTimeIntervalSince1970:dateInterval]);
    
    Reading *closestReading;
    NSTimeInterval shortestInterval = 0;
    
    for (Measurement *m in self.measurementsArray) {
        if (![self measurementIsSelected:m]) {
            continue;
        }
        
        NSArray *readings = self.readingsDict[m.plotID];
        
        for (Reading *r in readings) {
            if (![self readingIsInSelectedDatesInterval:r]) {
                continue;
            }
            
            NSTimeInterval interval = [r.date timeIntervalSince1970];
            double difference = ABS(interval - dateInterval);

            if (!closestReading || difference < shortestInterval) {
                closestReading = r;
                shortestInterval = difference;
            }
        }
    }
    
    if (!closestReading) {
        return nil;
    }
    
    NSLog(@"%@", closestReading);
    
    dateInterval = [closestReading.date timeIntervalSince1970];
    
    CPTXYAxisSet *axisSet = (CPTXYAxisSet *) self.hostingView.hostedGraph.axisSet;
    CPTXYAxis *xAxis = axisSet.xAxis;
    CPTXYAxis *yAxis = axisSet.yAxis;
    
    NSDecimal dateDecimal = [NSNumber numberWithDouble:dateInterval].decimalValue;
    CGPoint datePoint = [xAxis viewPointForCoordinateDecimalNumber:dateDecimal];
    
    NSDecimal valueDecimal = [NSNumber numberWithFloat:closestReading.value].decimalValue;
    CGPoint valuePoint = [yAxis viewPointForCoordinateDecimalNumber:valueDecimal];
    
    CGPoint point = CGPointMake(datePoint.x + graph.paddingLeft, valuePoint.y + graph.paddingTop);
    
    NSMutableDictionary *resultDict = [NSMutableDictionary dictionaryWithDictionary:@{@"reading": closestReading, @"point": [NSValue valueWithCGPoint:point]}];
    
    UIColor *color = self.colorsDict[closestReading.ID];
    if (color) {
        resultDict[@"color"] = color;
    }
    
    return resultDict.copy;
}

- (NSDictionary *)closestReadingForPoint:(CGPoint)touchPoint {
    CPTGraph *graph = self.hostingView.hostedGraph;
    
    CGFloat x = touchPoint.x - graph.paddingLeft;
//    CGFloat y = touchPoint.y - graph.paddingBottom;
    
    NSDecimal plotPoint[2];
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)graph.defaultPlotSpace;
    [plotSpace plotPoint:plotPoint numberOfCoordinates:2 forPlotAreaViewPoint:CGPointMake(x, 0)];
    
    double dateInterval = [[NSDecimalNumber decimalNumberWithDecimal:plotPoint[CPTCoordinateX]] doubleValue];
    NSLog(@"%@", [NSDate dateWithTimeIntervalSince1970:dateInterval]);
    
    Reading *closestReading;
    NSTimeInterval shortestInterval = 0;
    
    for (Measurement *m in self.measurementsArray) {
        if (![self measurementIsSelected:m]) {
            continue;
        }
        
        NSArray *readings = self.readingsDict[m.plotID];
        
        for (Reading *r in readings) {
            if (![self readingIsInSelectedDatesInterval:r]) {
                continue;
            }
            
            NSTimeInterval interval = [r.date timeIntervalSince1970];
            double difference = ABS(interval - dateInterval);
            
            if (!closestReading || difference < shortestInterval) {
                closestReading = r;
                shortestInterval = difference;
            }
        }
    }
    
    if (!closestReading) {
        return nil;
    }
    
    NSLog(@"%@", closestReading);
    
    dateInterval = [closestReading.date timeIntervalSince1970];
    
    CPTXYAxisSet *axisSet = (CPTXYAxisSet *) self.hostingView.hostedGraph.axisSet;
    CPTXYAxis *xAxis = axisSet.xAxis;
    CPTXYAxis *yAxis = axisSet.yAxis;
    
    NSDecimal dateDecimal = [NSNumber numberWithDouble:dateInterval].decimalValue;
    CGPoint datePoint = [xAxis viewPointForCoordinateDecimalNumber:dateDecimal];
    
    NSDecimal valueDecimal = [NSNumber numberWithFloat:closestReading.value].decimalValue;
    CGPoint valuePoint = [yAxis viewPointForCoordinateDecimalNumber:valueDecimal];
    
    CGPoint point = CGPointMake(datePoint.x + graph.paddingLeft, valuePoint.y + graph.paddingTop);
    
    NSMutableDictionary *resultDict = [NSMutableDictionary dictionaryWithDictionary:@{@"reading": closestReading, @"point": [NSValue valueWithCGPoint:point]}];
    
    UIColor *color = self.colorsDict[closestReading.ID];
    if (color) {
        resultDict[@"color"] = color;
    }
    
    return resultDict.copy;
}


#pragma mark - Helper

- (Measurement *)measurementForPlotID:(id)plotID {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"plotID = %@", plotID];
    NSArray *arr = [self.measurementsArray filteredArrayUsingPredicate:predicate];
    if (arr.count > 0) {
        return arr[0];
    }
    else {
        return nil;
    }
}

- (NSArray *)readingsForMeasurementWithPlotID:(NSString *)plotID {
    return self.readingsDict[plotID];
}

- (BOOL)measurementIsSelected:(Measurement *)m {
    NSUInteger index = [self.measurementsArray indexOfObject:m];
    
    if (index == NSNotFound) {
        return NO;
    }
    
    return [self.selectedPlots[index] boolValue];
}

- (BOOL)readingIsInSelectedDatesInterval:(Reading *)reading {
    return ([reading.date compare:self.dateFrom] != NSOrderedAscending && [reading.date compare:self.dateTo] != NSOrderedDescending);
}


#pragma mark - properties

- (CPTColor*)defaultPlotColor {
    if (_defaultPlotColor == NULL) {
        _defaultPlotColor = [CPTColor colorWithComponentRed:.33 green:.75 blue:.69 alpha:1];
    }
    return _defaultPlotColor;
}

- (CPTColor*)abnormalPlotColor {
    if (_abnormalPlotColor == NULL) {
        _abnormalPlotColor = [CPTColor colorWithComponentRed:.84 green:.44 blue:.39 alpha:1];
    }
    return _abnormalPlotColor;
}

- (Measurement*)randomMeasurement {
    if (_randomMeasurement == NULL) {
        _randomMeasurement = [self.measurementsArray firstObject];
    }
    return _randomMeasurement;
}

- (BOOL)isBloodPressure {
    return ([self.randomMeasurement.plotID isEqualToString:@"kSystolicBloodPressure"]) || ([self.randomMeasurement.plotID isEqualToString:@"kDiastolicBloodPressure"]);
}

- (BOOL)isHeight {
    return [self.randomMeasurement.plotID isEqualToString:@"kHeight"];
}

- (NSInteger)selectedPlotsCount {
    NSInteger count = 0;
    for (NSNumber* b in self.selectedPlots)
        if ([b boolValue] == YES)
            count++;
    return count;
}

- (NSDictionary *)readingsDict {
    if (!_readingsDict) {
        NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] initWithCapacity:self.measurementsArray.count];
        
        for (Measurement *measurement in self.measurementsArray) {
            NSPredicate *predicate = [NSPredicate predicateWithBlock:^BOOL(id  _Nonnull evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
                Reading *reading = evaluatedObject;
                return [reading.measurement.plotID isEqualToString:measurement.plotID];
            }];
            
            NSArray *filteredReadings = [self.readingsArray filteredArrayUsingPredicate:predicate];
            if (filteredReadings) {
                tempDict[measurement.plotID] = filteredReadings;
            }
        }
        
        _readingsDict = tempDict.copy;
    }
    
    return _readingsDict;
}

- (NSMutableDictionary *)colorsDict {
    if (!_colorsDict) {
        _colorsDict = [NSMutableDictionary new];
    }
    
    return _colorsDict;
}

- (void)setDateTo:(NSDate *)dateTo {
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:dateTo];
    components.hour = 00;
    components.minute = 00;
    _dateTo = [[NSCalendar currentCalendar] dateFromComponents:components];
    _dateTo = [_dateTo dateByAddingTimeInterval:86400];
}

@end
