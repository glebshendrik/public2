//
//  OneLongLabelCell.h
//  Public
//
//  Created by Tagi on 03.12.15.
//  Copyright © 2015 Teoria. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OneLongLabelCell : UITableViewCell

@property (nonatomic, readonly) UILabel *longTextLabel;

+ (CGFloat)heightOfLabelWithText:(NSString *)text font:(UIFont *)font viewWidth:(CGFloat)viewWidth;
+ (CGFloat)additionalHeight;

@end
