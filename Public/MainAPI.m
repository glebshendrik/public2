//
//  MainAPI.m
//  Electro_nrj
//
//  Created by Tagi on 26.05.15.
//  Copyright (c) 2015 Pavel Zarudnev. All rights reserved.
//
#import "MainAPI.h"
#import "RESTAPIManager.h"
#import "TeoriusHelper+Extension.h"
#import "SettingsManager.h"
#import "PersistencyManager.h"
#import "Account.h"
#import "Clinic.h"
#import "User.h"
#import "Resource.h"
#import "Service.h"
#import "SharedClass.h"
#import "Pulse.h"
#import "Reading.h"
#import "Measurement.h"
#import "DummyDataLight.h"
#import "TimeInterval.h"
#import "Interview.h"
#import "Article.h"
#import "Message.h"
#import "Question.h"
#import "NSObject+PerformBlockAfterDelay.h"


NSString *const kMainAPIErrorUnknown = @"Неизвестная ошибка";
NSString *const kMainAPIErrorMergingAccountsRequest = @"Эта почта уже использовалась Вами при регистрации. На указанную почту выслано письмо. Если Вы хотите объединить эти учетные записи, перейдите по ссылке в письме, а затем повторите попытку авторизации.";
NSString *const kMainAPIErrorWrongPassword = @"Неверный пароль.";
NSString *const kMainAPIErrorUserAlreadyExists = @"Такой пользователь уже существует.";
NSString *const kMainAPIErrorLoginNotExists = @"Пользователь с таким email не зарегистрирован.";
NSString *const kMainAPIErrorUnauthorized = @"Пользователь не авторизован.";
NSString *const kMainAPIErrorPermissionDenied = @"Отказано в доступе.";
NSString *const kMainAPIErrorRequestTimeout = @"Истекло время ожидания ответа на запрос, попробуйте повторить.";
//NSString *const kMainAPIErrorWrongAPIVersion = @"Неправильная версия АПИ";
//NSString *const kMainAPIErrorUnknownMethod = @"Неизвестный метод";
NSString *const kMainAPIErrorRegionNotFound = @"Регион не найден.";
NSString *const kMainAPIErrorProviderNotFound = @"Такая сбытовая организайия не найдена.";
NSString *const kMainAPIErrorRoomNotFound = @"Такое помещение не найдено.";
NSString *const kMainAPIErrorRoomNotUnique = @"Такое помещение уже существует.";
NSString *const kMainAPIErrorPaymentNotStarted = @"Платеж не стартовал.";
NSString *const kMainAPIErrorEmailNotSet = @"Email не настроен.";
NSString *const kMainAPIErrorPaymentNotFound = @"Такой платеж не найден.";
NSString *const kMainAPIErrorPaymentNotFinished = @"Платеж не завершен.";
NSString *const kMainAPIErrorCardNotFound = @"Карта не найдена.";
NSString *const kMainAPIErrorUserDeleted = @"Пользователь удален.";
NSString *const kMainAPIErrorProviderBlockedForPay = @"Данная энергосбытовая компания на текущий момент не доступна для оплаты.";
NSString *const kMainAPIErrorRegionBlockedForPay = @"Данный регион на текущий момент не доступен для оплаты.";

NSString *const kMainAPIErrorNoInternet = @"Отсутствует соединение с интернетом. Попробуйте установить соединение и повторить запрос.";

NSString *const kNotificationPersonalDataWasChanged = @"kNotificationPersonalDataWasChanged";
NSString *const kNotificationPaymentWasSent = @"kNotificationPaymentWasSent";
NSString *const kNotificationLogoutMade = @"kNotificationLogoutMade";
NSString *const kPreviousCardID = @"kPreviousCardID";
NSString *const kDeviceTokensDictionary = @"kDeviceTokensDictionary";

@interface MainAPI() {
    PersistencyManager *_persistencyManager;
    RESTAPIManager *_RESTAPIManager;
    User *_currentUser;
    NSString *jsessionID;
    NSMutableArray *_familyMembers;
    NSString *_previousCardID;
    NSDictionary *_deviceTokensDictionary;
    NSDateFormatter *formatter;
    NSDateFormatter *formatter2;
    NSArray *_measurementsArray;
}

@property (nonatomic, readonly) PersistencyManager *persistencyManager;
@property (nonatomic, readonly) RESTAPIManager *RESTAPIManager;
@property (nonatomic) NotificationType notificationType;
@property (nonatomic) StatusType typeStatus;
@property (nonatomic) UserRLM *currentUserRLM;
@property (nonatomic) UserRLM *defaultUserRLM;

@end

@implementation MainAPI

#pragma mark - Class methods

+ (MainAPI *)defaultAPI {
    static MainAPI *defaultAPI = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        defaultAPI = [self new];
    });
    
    return defaultAPI;
}


#pragma mark - Init

- (instancetype)init {
    self = [super init];
    
    if (!self) {
        return nil;
    }
    
    _persistencyManager = [PersistencyManager new];
    _RESTAPIManager = [RESTAPIManager new];
    
    return self;
}


- (void)registerDeviceToken {
    if ([TeoriusHelper stringIsValid:[SettingsManager sharedManager].apnsToken]) {
        [self.RESTAPIManager registerDeviceToken:[SettingsManager sharedManager].apnsToken];
    }
}

- (void)loginWithLogin:(NSString *)login password:(NSString *)password completion:(void (^)(BOOL success, NSError *error))completion {
    [self.RESTAPIManager loginWithLogin:login password:password completion:^(NSDictionary *responseDict, NSError *error) {
        NSError *error1 = [self errorFromResponseWithError:error andDict:responseDict];

        if (error1) {
            completion(NO, error1);
            return;
        }
        
        NSNumber *success = responseDict[@"success"];
        if (!success.boolValue) {
            completion(NO, responseDict[@"message"]);
            return;
        }
        
        NSString *idNumber = responseDict[@"id"];
        
        if (![TeoriusHelper stringIsValid:idNumber]) {
            completion(NO, error1);
            return;
        }
        
        NSString *uid = responseDict[@"uid"];
        if (![TeoriusHelper stringIsValid:uid]) {
            completion(NO, error1);
            return;
        }

        Account *account = [self objectFromRealmObject:self.persistencyManager.account];
        if (!account) {
            account = [Account new];
        }
        if (!account.family) {
            account.family = [Family new];
            account.family.ID = [TeoriusHelper getUUID];
        }
        account.login = login;
        account.password = password;
        
//        FamilyRLM *family = [FamilyRLM new];
        
//        UserRLM *user = [UserRLM new];
        account.ID = idNumber;
        account.uuid = uid;
        
//        family.defaultMember = user;
//        family.currentMember = user;
        

//        [account.family.members addObject:user];
  
//        account.family = family;
        
        [self.persistencyManager addOrUpdateObject:[self realmObjectFromObject:account]];
        
//        if (responseDict) {
//            completion(NO, nil);
//            return;
//        }
        
        [SettingsManager sharedManager].loggedIn = YES;
        completion(YES, nil);
    }];
}

- (void)registerWithLogin:(NSString *)login password:(NSString *)password completion:(void (^)(BOOL, NSError *))completion {
    [self.RESTAPIManager registerWithLogin:login password:password completion:^(NSDictionary *responseDict, NSError *error) {

        NSError *error1 = [self errorFromResponseWithError:error andDict:responseDict];
        
        
        if (error1) {
            completion(NO, error1);
            return;
        }
        
        NSNumber *success = responseDict[@"success"];
        if (!success.boolValue) {
            completion(NO, responseDict[@"message"]);
            return;
        }
        
        NSString *idNumber = responseDict[@"id"];
        
        if (![TeoriusHelper stringIsValid:idNumber]) {
            completion(NO, error1);
            return;
        }
        
        NSString *uid = responseDict[@"uid"];
        if (![TeoriusHelper stringIsValid:uid]) {
            completion(NO, error1);
            return;
        }
        
        AccountRLM *account = [AccountRLM new];
        account.login = login;
        account.password = password;
        account.ID = idNumber;
        account.uuid = uid;
        
//        if (!account.family) {
//            FamilyRLM *family = [FamilyRLM new];
        
//            UserRLM *user = [UserRLM new];
//            user.ID = idNumber;
//            user.uuid = uid;
            
//            family.defaultMember = user;
//            family.currentMember = user;
//            [account.family.members addObject:user];
//            account.family = family;
//        }
        [self.persistencyManager addOrUpdateObject:account];
        [SettingsManager sharedManager].loggedIn = YES;
        completion(YES, nil);
    }];
}

- (void)authWithDocument:(NSString *)typeDoc docNumber:(NSString *)docNumber docSeries:(NSString *)docSeries birthDate:(NSString *)birthDate completion:(void (^)(BOOL, NSError *))completion {
    [self.RESTAPIManager authWithDocument:typeDoc docNumber:docNumber docSeries:(NSString *)docSeries birthDate:birthDate completion:^(NSDictionary *responseDict, NSError *error) {
        
        NSError *error1 = [self errorFromAppointmentsResponseWithError:error andDict:responseDict];
        if (error1) {
            completion(NO, error1);
            return;
        }
//
//        NSString *idNumber = responseDict[@"id"];
//        if (![TeoriusHelper stringIsValid:idNumber]) {
//            completion(NO, error1);
//            return;
//        }
        completion(YES, nil);
    }];
}

- (void)authWithPassportNumber:(NSString *)number passportSeries:(NSString *)series birthDate:(NSString *)birthDate completion:(void (^)(BOOL, NSError *))completion {
    [self authWithDocument:@"PASSPORT_RUSSIAN_FEDERATION" docNumber:number docSeries:series birthDate:birthDate completion:completion];
}

- (void)authWithPolisNumber:(NSString *)number birthDate:(NSString *)birthDate completion:(void (^)(BOOL, NSError *))completion {
    [self authWithDocument:@"MHI_UNIFORM" docNumber:number docSeries:@"" birthDate:birthDate completion:completion];
}

- (void)logout {
    [[self persistencyManager] deleteAccount];
    [self.RESTAPIManager logoutWithCompletion:^(NSDictionary *responseDict, NSError *error) {
            NSError *error1 = [self errorFromResponseWithError:error andDict:responseDict];
        if (error1) {
            
            return;
        }
    }];
}

- (void)getTotalAppointments:(void (^)(BOOL, NSError *))completion {
    [self.RESTAPIManager getTotalAppointments:^(NSDictionary *responseDict, NSError *error) {
        
        NSError *error1 = [self errorFromAppointmentsResponseWithError:error andDict:responseDict];
        if (error1) {
            completion(NO, error1);
            return;
        }
        //
        completion(YES, nil);
        
    }];
}

- (void)getFutureAppointments:(void (^)(NSArray *, NSError *))completion {
    [self.RESTAPIManager getFutureAppointments:^(NSDictionary *responseDict, NSError *error) {
        NSError *error1 = [self errorFromAppointmentsResponseWithError:error andDict:responseDict];
        if (error1) {
            completion(@[], error1);
            return;
        }
        
        NSArray *list = responseDict[@"list"];
        
        NSMutableArray *appArr = [[NSMutableArray alloc] initWithCapacity:list.count];

        if (responseDict[@"count"] == 0) {
            
            completion(@[], nil);
            return;
        }
        
        for (NSDictionary *dic in list) {
            Appointment *app = [Appointment new];
            
            NSNumber *ID = dic[@"id"];
            if ([TeoriusHelper numberIsValid:ID]) {
                app.ID = [NSString  stringWithFormat:@"%d", ID.intValue];
            }
            else {
                continue;
            }
            
            NSString *ticketNumber = dic[@"ticketNumber"];
            if ([TeoriusHelper stringIsValid:ticketNumber]){
                app.ticketNumber = ticketNumber;
            }
            else {
                continue;
            }

            NSNumber *unixDate = dic[@"date"];
            NSString *stringUnix = [unixDate stringValue];
            stringUnix = [stringUnix stringByReplacingCharactersInRange:NSMakeRange(stringUnix.length - 3, 3) withString:@""];
            NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
            f.numberStyle = NSNumberFormatterDecimalStyle;
            unixDate = [f numberFromString:stringUnix];
            
            NSDate *dateApp = [NSDate dateWithTimeIntervalSince1970:[unixDate doubleValue]];
            if ([TeoriusHelper numberIsValid:unixDate]) {
                app.date = dateApp;
            }
            
            [TeoriusHelper setValuesForObject:app fromDict:dic withLinkingDict:@{@"clinicFullName":@"", @"clinicShortName":@"", @"name":@"", @"resourceId": @"", @"resourceFullName": @"", @"resourceShortName": @"", @"serviceShortName": @"", @"resourceFio": @"", @"resourceSpeciality": @"", @"room": @""}];
            [TeoriusHelper setValuesForObject:app fromDict:dic[@"resourceSpeciality"] withLinkingDict:@{@"name": @"resourceSpeciality"}];
            
            [appArr addObject:app];
        }
        
        completion(appArr.copy, nil);
    }];
}

- (void)getUserInfoForUserID:(NSString *)userID completion:(void (^)(BOOL, NSError *))completion {
    [self.RESTAPIManager getUserInfoForUserID:userID completion:^(NSDictionary *responseDict, NSError *error) {
        NSError *error1 = [self errorFromAppointmentsResponseWithError:error andDict:responseDict];
        if (error.code == 401) {
            [self loginWithLogin:self.persistencyManager.account.login password:self.persistencyManager.account.password completion:^(BOOL success, NSError *error) {
                [self getUserInfoForUserID:userID completion:completion];
            }];
            
            return;
        }
        if (error1) {
            completion(NO, error1);
        }
        
        UserRLM *userRLM = [UserRLM new];
        
        
        NSString *idNumber = responseDict[@"id"];
        if (![TeoriusHelper stringIsValid:idNumber]) {
            completion(NO, error1);
            return;
        }
        
        NSString *uid = responseDict[@"uid"];
        if (![TeoriusHelper stringIsValid:uid]) {
            completion(NO, error1);
            return;
        }
        
        NSString *firstName = responseDict[@"firstName"];
        if ([TeoriusHelper stringIsValid:firstName]) {
            userRLM.name = firstName;
        }
       
        NSString *lastName = responseDict[@"lastName"];
        if ([TeoriusHelper stringIsValid:lastName]) {
            userRLM.surname = lastName;
        }
        NSString *patronymic = responseDict[@"patronymic"];
        if ([TeoriusHelper stringIsValid:patronymic]) {
            userRLM.patronymic = patronymic;
        }
        NSString *gender = responseDict[@"gender"];
        if ([TeoriusHelper stringIsValid:gender]) {
            userRLM.gender = gender;
        }
        NSString *birthayString = responseDict[@"birthDay"];
        NSDate *birthday = [TeoriusHelper birthdayToDateWithDefis:birthayString];
        if ([TeoriusHelper dateIsValid:birthday]) {
            userRLM.birthDay = birthday;
        }
        NSString *kinship = responseDict[@"kinship"];
        if ([TeoriusHelper stringIsValid:kinship]) {
            userRLM.relationship = kinship;
        }
        NSString *russiaRegion = responseDict[@"russiaRegion"];
        if ([TeoriusHelper stringIsValid:russiaRegion]) {
            userRLM.region = russiaRegion;
        }
        
        userRLM.uuid = uid;
        userRLM.ID = idNumber;
        
        User *user = [self objectFromRealmObject:userRLM];
        AccountRLM *accRLM =  self.persistencyManager.account;
        Account *acc = [self objectFromRealmObject:accRLM];

        if ([acc.family.defaultMember.ID isEqualToString:idNumber]) {
            acc.family.defaultMember = user;
        }
        else {
            if (![self.persistencyManager accountContainsUserWithID:user.ID]) {
                [acc.family.members addObject:user];
            }
        }
        
        accRLM = [self realmObjectFromObject:acc];
        
        [self.persistencyManager addOrUpdateObject:accRLM];
        
    }];
}

- (void)getClinics:(void (^)(NSArray *, NSError *))completion {
    [self.RESTAPIManager getClinics:^(NSDictionary *responseDict, NSError *error) {
        if (error.code == 401) {
            [self loginWithLogin:self.persistencyManager.account.login password:self.persistencyManager.account.password completion:^(BOOL success, NSError *error) {
                [self getClinics:completion];
            }];
            
            return;
        }
        
        NSError *error1 = [self errorFromAppointmentsResponseWithError:error andDict:responseDict];
        if (error1) {
            completion(@[], error1);
            return;
        }
    
        NSArray *list = responseDict[@"list"];
        NSMutableDictionary *clinicsByParents = [NSMutableDictionary new];
        NSMutableArray *parentsArray = [NSMutableArray new];
        
        for (NSDictionary *dict in list) {
            Clinic *clinic = [self clinicFromDict:dict isParentClinic:YES];
            
            [parentsArray addObject:clinic];
            
            NSDictionary *childrenDict = dict[@"children"];
            if (![TeoriusHelper dictIsValid:childrenDict]) {
                continue;
            }

            NSArray *childrenArray = childrenDict[@"list"];
            for (NSDictionary *childDict in childrenArray) {
                Clinic *childClinic = [self clinicFromDict:childDict isParentClinic:NO];
                
                if (childClinic.attached) {
                    clinic.oneOfChildrenAttached = YES;
                }
                
                NSMutableArray *arr = clinicsByParents[clinic.ID];
                
                if (!arr) {
                    arr = [NSMutableArray new];
                    clinicsByParents[clinic.ID] = arr;
                }
                
                [arr addObject:childClinic];
            }

//            NSString *address = dic[@"address"];
//            if ([TeoriusHelper stringIsValid:address]){
//                clinic.address = address;
//            }
//            NSString *name = dic[@"name"];
//            if ([TeoriusHelper stringIsValid:name]){
//                clinic.name = name;
//            }
//            NSString *phone = dic[@"regPhone"];
//            if ([TeoriusHelper stringIsValid:phone]){
//                clinic.phone = phone;
//            }
//            NSString *work_hours = dic[@"note"];
//            if ([TeoriusHelper stringIsValid:work_hours]){
//                clinic.work_hours = work_hours;
//            }
//            NSString *ID = dic[@"id"];
//            if ([TeoriusHelper stringIsValid:ID]){
//                clinic.ID = ID;
//            }
        }
        
        [parentsArray sortUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
            Clinic *clinic1 = obj1;
            Clinic *clinic2 = obj2;
            
            if ((clinic1.attached || clinic1.oneOfChildrenAttached) && !clinic2.attached && !clinic2.oneOfChildrenAttached) {
                return NSOrderedAscending;
            }
            else if ((clinic1.attached || clinic1.oneOfChildrenAttached) == (clinic2.attached || clinic2.oneOfChildrenAttached)) {
                return [clinic1.name compare:clinic2.name];
            }
            else {
                return NSOrderedDescending;
            }
        }];
        
        NSMutableArray *allClinicsArray = [NSMutableArray new];
        
        for (Clinic *parentClinic in parentsArray) {
            [allClinicsArray addObject:parentClinic];
            
            NSMutableArray *childClinics = clinicsByParents[parentClinic.ID];
            [childClinics sortUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
                Clinic *clinic1 = obj1;
                Clinic *clinic2 = obj2;
                
                if (clinic1.attached && !clinic2.attached) {
                    return NSOrderedAscending;
                }
                else if (clinic1.attached == clinic2.attached) {
                    return [clinic1.name compare:clinic2.name];
                }
                else {
                    return NSOrderedDescending;
                }
            }];
            
            [allClinicsArray addObjectsFromArray:childClinics];
        }
        
        completion(allClinicsArray.copy, nil);
    }];
}

- (void)getResourcesForClinicID:(NSString *)clinicID completion:(void (^)(NSArray *, NSError *))completion {
    [self.RESTAPIManager getResourcesForClinicID:clinicID completion:^(NSDictionary *responseDict, NSError *error) {
        NSError *error1 = [self errorFromAppointmentsResponseWithError:error andDict:responseDict];
        if (error1) {
            completion(@[], error1);
            return;
        }
        
        NSMutableArray *servicesArray = [NSMutableArray new];
        NSArray *servicesList = responseDict[@"list"];
        
        if (![servicesList isKindOfClass:[NSArray class]]) {
            completion(@[], nil);
            return;
        }
        
        for (NSDictionary *serviceDict in servicesList) {
            Service *service = [Service new];
            [TeoriusHelper setValuesForObject:service fromDict:serviceDict withLinkingDict:@{@"id":@"ID", @"name":@"name"}];
            service.clinicID = clinicID;
            
            NSDictionary *resourcesDict = serviceDict[@"resources"];
            NSArray *resourcesList = resourcesDict[@"list"];
            
            if (![TeoriusHelper arrayIsValid:resourcesList]) {
                continue;
            }
            
            for (NSDictionary *resourceDict in resourcesList) {
                Resource *resource = [Resource new];
                [TeoriusHelper setValuesForObject:resource fromDict:resourceDict withLinkingDict:@{@"id":@"ID", @"fullName":@"fullName", @"room":@"room", @"serviceId":@"serviceID", @"fio":@"fio"}];
                resource.clinicID = clinicID;
                
                NSDictionary *specialityDict = resourceDict[@"speciality"];
                [TeoriusHelper setValuesForObject:resource fromDict:specialityDict withLinkingDict:@{@"id":@"specialityID", @"name":@"specialityName"}];
                
                [service.resources addObject:resource];
            }
            
            NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"fio" ascending:YES];
            [service.resources sortUsingDescriptors:@[descriptor]];
            
            [servicesArray addObject:service];
        }
        
        NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
        [servicesArray sortUsingDescriptors:@[descriptor]];
        
        completion(servicesArray, nil);
    }];
}

- (void)getScheduleForClinicID:(NSString *)clinicID serviceID:(long)serviceID resourceID:(long)resourceID year:(int)year month:(int)month completion:(void (^)(NSDictionary *, NSError *))completion {
    [self.RESTAPIManager getScheduleForClinicID:clinicID serviceID:[NSString  stringWithFormat:@"%ld", serviceID] resourceID:[NSString  stringWithFormat:@"%ld", resourceID] year:year month:month completion:^(NSDictionary *responseDict, NSError *error) {
        NSError *error1 = [self errorFromAppointmentsResponseWithError:error andDict:responseDict];
        if (error1) {
            completion(nil, error1);
            return;
        }
        
        NSArray *daysArray = responseDict[@"planning"];
        NSMutableDictionary *daysDict = [[NSMutableDictionary alloc] initWithCapacity:daysArray.count];
        
        for (NSDictionary *dayDict in daysArray) {
            NSArray *intervals = dayDict[@"intervals"];
            
            if (![TeoriusHelper arrayIsValid:intervals]) {
                continue;
            }
            
            NSTimeInterval dateLong = [dayDict[@"jobPeriodStart"] doubleValue];
            
            for (NSDictionary *intervalDict in intervals) {
                BOOL free = [intervalDict[@"free"] boolValue];
                if (!free) {
                    continue;
                }
                
                NSString *time = intervalDict[@"formattedDate"];
                NSNumber *ID = intervalDict[@"id"];
                if (![TeoriusHelper stringIsValid:time] || ![TeoriusHelper numberIsValid:ID]) {
                    continue;
                }
                
                NSMutableArray *timesArray = daysDict[@(dateLong)];
                if (!timesArray) {
                    timesArray = [NSMutableArray new];
                    daysDict[@(dateLong)] = timesArray;
                }
                
                TimeInterval *interval = [[TimeInterval alloc] initWithID:ID.longValue time:time];
                [timesArray addObject:interval];
            }
        }
        
        completion(daysDict, nil);
    }];
}

- (void)createAppointmentForClinicId:(NSString *)clinicId ServiceId:(long)serviceId resourceId:(long)resourceId year:(NSUInteger)year month:(NSUInteger)month day:(NSUInteger)day timeIntervalId:(long)timeIntervalId completion:(void (^)(BOOL, NSError *))completion {
    [self.RESTAPIManager createAppointmentForClinicId:clinicId ServiceId:serviceId resourceId:resourceId year:year month:month day:day timeIntervalId:timeIntervalId completion:^(NSDictionary *responseDict, NSError *error) {
        NSError *error1 = [self errorFromAppointmentsResponseWithError:error andDict:responseDict];
        if (error1) {
            completion(NO, error1);
            return;
        }
        
        completion(YES, nil);
    }];
}

- (void)createAppointmentForClinicId:(NSString *)clinicId ServiceId:(long)serviceId resourceId:(long)resourceId year:(NSUInteger)year month:(NSUInteger)month day:(NSUInteger)day timeIntervalId:(long)timeIntervalId phone:(NSString *)phone completion:(void (^)(BOOL, NSError *))completion {
    [self.RESTAPIManager createAppointmentForClinicId:clinicId ServiceId:serviceId resourceId:resourceId year:year month:month day:day timeIntervalId:timeIntervalId phone:phone completion:^(NSDictionary *responseDict, NSError *error) {
        NSError *error1 = [self errorFromAppointmentsResponseWithError:error andDict:responseDict];
        if (error1) {
            completion(NO, error1);
            return;
        }
        
        NSString *ticketNumber = responseDict[@"ticketNumber"];
        long ID = [responseDict[@"id"] longValue];
        long patientId = [responseDict[@"patientId"] longValue];
        NSString *clinicShortName = responseDict[@"clinicShortName"];
        NSString *serviceShortName = responseDict[@"serviceShortName"];
        NSString *resourceShortName = responseDict[@"resourceShortName"];
        NSString *room = responseDict[@"room"];
        NSTimeInterval timeInterval = [responseDict[@"timeStart"] doubleValue];
        NSDate *timeStart = [NSDate dateWithTimeIntervalSince1970:(timeInterval / 1000)];
        NSString *timeString = [TeoriusHelper dateToString:timeStart usingFormat:@"yyyy-MM-dd hh:mm:ss"];
        timeString = [timeString stringByReplacingOccurrencesOfString:@" " withString:@"T"];
        
        NSString *userCitizenId;
        
        UserRLM *currentUserRLM = self.currentUserRLM;
        if (![currentUserRLM.ID isEqualToString:self.defaultUserRLM.ID]) {
            userCitizenId = currentUserRLM.ID;
        }
        
        [self sendAppointmentInfoWithId:ID ticketNumber:ticketNumber patientId:patientId clinicName:clinicShortName serviceName:serviceShortName resourceId:resourceId resourceName:resourceShortName room:room time:timeString userCitizenId:userCitizenId];
        
        completion(YES, nil);
    }];
    
    [[TeoriusHelper instance] setTimeNoteForKey:NSStringFromClass(self.class)];
    UserRLM *userRLM = self.currentUserRLM;
    [[TeoriusHelper instance] getTimeDifferenceForKey:NSStringFromClass(self.class) withLogging:YES loggingInfo:@"Getting user from RLM" replacingPreviousNote:NO];
    if (/*![self.currentUserRLM.phone isEqualToString:phone]*/YES) {
        User *user = [self objectFromRealmObject:userRLM];
        
        [[TeoriusHelper instance] getTimeDifferenceForKey:NSStringFromClass(self.class) withLogging:YES loggingInfo:@"Convert to usual object" replacingPreviousNote:NO];
        
        user.phone = phone;
        
        userRLM = [self realmObjectFromObject:user];
        
        [[TeoriusHelper instance] getTimeDifferenceForKey:NSStringFromClass(self.class) withLogging:YES loggingInfo:@"Convert to rlm object" replacingPreviousNote:NO];

        [self.persistencyManager addOrUpdateObject:userRLM];
        
        [[TeoriusHelper instance] getTimeDifferenceForKey:NSStringFromClass(self.class) withLogging:YES loggingInfo:@"Update in DB" replacingPreviousNote:YES];

        [[TeoriusHelper instance] clearTimeNoteInfoForKey:NSStringFromClass(self.class)];

        [self updateUser:user completion:^(NSDictionary *dic, NSError *err) {
            NSLog(@"%@", dic);
            NSLog(@"%@", err);
        }];
    }
}

- (void)deleteAppointmentWithId:(NSString *)appointmentId completion:(void (^)(BOOL, NSError *))completion {
    [self.RESTAPIManager deleteAppointmentWithId:appointmentId completion:^(NSDictionary *responseDict, NSError *error) {
        NSError *error1 = [self errorFromAppointmentsResponseWithError:error andDict:responseDict];
        if (error1) {
            completion(NO, error1);
            return;
        }
        
        completion(YES, nil);
        
        [self sendAppointmentCancellingInfoWithId:appointmentId];
    }];
}

- (void)sendAppointmentInfoWithId:(long)Id ticketNumber:(NSString *)ticketNumber patientId:(long)patientId clinicName:(NSString *)clinicName serviceName:(NSString *)serviceName resourceId:(long)resourceId resourceName:(NSString *)resourceName room:(NSString *)room time:(NSString *)time userCitizenId:(NSString *)userCitizenId {
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithCapacity:9];
    
    NSString *idString = [NSString  stringWithFormat:@"%ld", Id];
    if (![TeoriusHelper stringIsValid:idString]) {
        return;
    }
    
    NSString *rmisPatiendId = [NSString  stringWithFormat:@"%ld", patientId];
    if (![TeoriusHelper stringIsValid:rmisPatiendId]) {
        return;
    }
    
    dict[@"rmisId"] = idString;
    dict[@"rmisPatientId"] = rmisPatiendId;
    
    if ([TeoriusHelper stringIsValid:ticketNumber]) {
        dict[@"ticketNumber"] = ticketNumber;
    }
    if ([TeoriusHelper stringIsValid:clinicName]) {
        dict[@"clinicShortName"] = clinicName;
    }
    if ([TeoriusHelper stringIsValid:serviceName]) {
        dict[@"serviceShortName"] = serviceName;
    }
    
    NSString *rmisResourceId = [NSString  stringWithFormat:@"%ld", resourceId];
    
    if ([TeoriusHelper stringIsValid:rmisResourceId]) {
        dict[@"rmisResourceId"] = rmisResourceId;
    }
    if ([TeoriusHelper stringIsValid:resourceName]) {
        dict[@"resourceShortName"] = resourceName;
    }
    if ([TeoriusHelper stringIsValid:room]) {
        dict[@"room"] = room;
    }
    if ([TeoriusHelper stringIsValid:time]) {
        dict[@"timeStart"] = time;
    }
    if ([TeoriusHelper stringIsValid:userCitizenId]) {
        dict[@"userCitizen"] = userCitizenId;
    }
    
    [self.RESTAPIManager sendAppointmentInfoWithDict:dict.copy completion:^(NSDictionary *responseDict, NSError *error) {
        if (error.code == 401) {
            [self loginWithLogin:self.persistencyManager.account.login password:self.persistencyManager.account.password completion:^(BOOL success, NSError *error) {
                if (success) {
                    [self sendAppointmentInfoWithId:Id ticketNumber:ticketNumber patientId:patientId clinicName:clinicName serviceName:serviceName resourceId:resourceId resourceName:resourceName room:room time:time userCitizenId:userCitizenId];
                }
            }];
            
            return;
        }

        NSLog(@"%@", error);
        NSLog(@"%@", responseDict);
    }];
}

- (void)sendAppointmentCancellingInfoWithId:(NSString *)ID {
    [self.RESTAPIManager sendAppointmentCancellingInfoWithId:ID completion:^(NSDictionary *responseDict, NSError *error) {
        if (error.code == 401) {
            [self loginWithLogin:self.persistencyManager.account.login password:self.persistencyManager.account.password completion:^(BOOL success, NSError *error) {
                if (success) {
                    [self sendAppointmentCancellingInfoWithId:ID];
                }
            }];
            
            return;
        }
        
        NSLog(@"%@", error);
        NSLog(@"%@", responseDict);
    }];
}



- (void)getUserCitizen: (void (^)(NSArray *arr, NSError *err))completion {
    [self.RESTAPIManager getUserCitizen:^(NSArray *responseArr, NSError *error) {
        if ([responseArr isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dict = (NSDictionary *)responseArr;
            
            NSError *error1 = [self errorFromResponseWithError:error andDict:dict];
            if (error1) {
                completion(@[], error1);
                return;
            }
            
            completion(@[], [self errorWithText:@"Неизвестная ошибка"]);
        }
        
        if (error.code == 401) {
            [self loginWithLogin:self.persistencyManager.account.login password:self.persistencyManager.account.password completion:^(BOOL success, NSError *error) {
                [self getUserCitizen:completion];
            }];
            
            return;
        }
        
        Account *acc = [self objectFromRealmObject:self.persistencyManager.account];
        if (!acc.family) {
            acc.family = [Family new];
            acc.family.ID = [TeoriusHelper getUUID];
        }
        
        for (NSDictionary *dic in responseArr) {
            User *newUser = [User new];
            [TeoriusHelper setValuesForObject:newUser fromDict:dic withLinkingDict:@{@"documNum":@"passportNumber", @"documSer":@"passportSeries", @"firstName":@"name", @"id":@"ID", @"lastName":@"surname", @"patronymic":@"", @"uid":@"uuid", @"gender":@"", @"policyNumber":@"polisNumber", @"kinship": @"relationship", @"phone":@""}];
            
            NSString *birthdayString = dic[@"birthDay"];
            NSDate *birthday = [TeoriusHelper birthdayToDateWithDefis:birthdayString];
            if ([TeoriusHelper dateIsValid:birthday]) {
                newUser.birthDay = birthday;
            }
            
            UserRLM *userRLM = [self realmObjectFromObject:newUser];
            
//            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ID = %@", user.ID];
//            NSArray *filteredUsers = [acc.family.members filteredArrayUsingPredicate:predicate];

            if ([UserRLM objectForPrimaryKey:userRLM.ID]) {
                [self.persistencyManager addOrUpdateObject:userRLM];
            }
            else {
                if (![TeoriusHelper stringIsValid:newUser.relationship]) {
                    acc.family.defaultMember = newUser;
                    acc.family.currentMember = newUser;
                }
                
                [acc.family.members addObject:newUser];
                
                AccountRLM *accRLM = [self realmObjectFromObject:acc];
                [self.persistencyManager addOrUpdateObject:accRLM];
            }            
        }
        
        completion([self familyMembers], nil);
    }];
}

- (void)createNewUser: (User *)user completion: (void (^)(NSDictionary *arr, NSError *err))completion {
    NSMutableDictionary *userDic = [NSMutableDictionary dictionaryWithDictionary:@{
                                                                                   @"isAuth": @NO,
                                                                                   @"firstName": [NSNull null],
                                                                                   @"lastName": [NSNull null],
                                                                                   @"patronymic": [NSNull null],
                                                                                   @"gender": [NSNull null],
                                                                                   @"birthDay": [NSNull null],
                                                                                   @"russiaRegion": [NSNull null],
                                                                                   @"kinship": [NSNull null],
                                                                                   @"policyNumber": [NSNull null],
                                                                                   @"documType": [NSNull null],
                                                                                   @"documSer": [NSNull null],
                                                                                   @"documNum": [NSNull null],
                                                                                   @"phone": [NSNull null]
                                                                                   }];
    if (user.name) {
        userDic[@"firstName"] = user.name;
    }
    if (user.surname) {
        userDic[@"lastName"] = user.surname;
    }
    if (user.patronymic) {
        userDic[@"patronymic"] = user.patronymic;
    }
    if (user.gender) {
        userDic[@"gender"] = user.gender;
    }
    if ([TeoriusHelper dateIsValid:user.birthDay]) {
        userDic[@"birthDay"] = [TeoriusHelper birthdayToStringWithDefis:user.birthDay];
    }
    if (user.region) {
//        userDic[@"russiaRegion"] = user.region;
        userDic[@"russiaRegion"] = @"16";
    }
    if (user.relationship) {
        userDic[@"kinship"] = user.relationship;
    }

    if (user.polisNumber && [TeoriusHelper stringIsValid:user.polisNumber]) {
        userDic[@"policyNumber"] = user.polisNumber;
    }
    if (user.passportSeries && [TeoriusHelper stringIsValid:user.passportSeries]) {
        userDic[@"documSer"] = user.passportSeries;
        userDic[@"documType"] = @"14";
    }
    if (user.passportNumber && [TeoriusHelper stringIsValid:user.passportNumber]) {
        userDic[@"documNum"] = user.passportNumber;
    }
    if ([TeoriusHelper stringIsValid:user.phone]) {
        userDic[@"phone"] = user.phone;
    }
    if (user.image) {
//        userDic[@"documNum"] = user.passportNumber;
    }
//    userDic[@"documType"] = @"14";
    
//    NSLog(@"%@", userDic);
    
    [self.RESTAPIManager createNewUser:userDic completion:^(NSDictionary *dic, NSError *error) {
        if (error.code == 401) {
            [self loginWithLogin:self.persistencyManager.account.login password:self.persistencyManager.account.password completion:^(BOOL success, NSError *error) {
                [self createNewUser:user completion:completion];
            }];
            
            return;
        }
        
        NSError *error1 = [self errorFromResponseWithError:error andDict:dic];
        if (error1) {
            completion(@{}, error1);
            return;
        }
        
        User *newUser = [User new];
        [TeoriusHelper setValuesForObject:newUser fromDict:dic withLinkingDict:@{@"documNum":@"passportNumber", @"documSer":@"passportSeries", @"firstName":@"name", @"id":@"ID", @"lastName":@"surname", @"patronymic":@"", @"uid":@"uuid", @"gender":@"", @"policyNumber":@"polisNumber", @"kinship": @"relationship", @"phone":@""}];

        NSString *birthdayString = dic[@"birthDay"];
        NSDate *birthday = [TeoriusHelper birthdayToDateWithDefis:birthdayString];
        if ([TeoriusHelper dateIsValid:birthday]) {
            newUser.birthDay = birthday;
        }

        Account *acc = [self objectFromRealmObject:self.persistencyManager.account];
        if (!acc.family) {
            acc.family = [Family new];
            acc.family.ID = [TeoriusHelper getUUID];
        }
        [acc.family.members addObject:newUser];
        AccountRLM *accRLM = [self realmObjectFromObject:acc];
        [self.persistencyManager addOrUpdateObject:accRLM];
        
        //        TODO: Сохранить пользователя в реалм с картинкой
        completion(@{}, nil);
        
    }];
}

- (void)updateUser: (User *)user completion: (void (^)(NSDictionary *dic, NSError *err))completion{
    NSMutableDictionary *userDic = [NSMutableDictionary dictionaryWithDictionary:@{
                                                                                   @"isAuth": @NO,
                                                                                   @"firstName": [NSNull null],
                                                                                   @"lastName": [NSNull null],
                                                                                   @"patronymic": [NSNull null],
                                                                                   @"gender": [NSNull null],
                                                                                   @"birthDay": [NSNull null],
                                                                                   @"russiaRegion": [NSNull null],
                                                                                   @"kinship": [NSNull null],
                                                                                   @"policyNumber": [NSNull null],
                                                                                   @"documType": [NSNull null],
                                                                                   @"documSer": [NSNull null],
                                                                                   @"documNum": [NSNull null],
                                                                                   @"phone": [NSNull null]
                                                                                   }];

    
    if (user.ID) {
        userDic[@"id"] = user.ID;
    }
    if (user.name) {
        userDic[@"firstName"] = user.name;
    }
    if (user.surname) {
        userDic[@"lastName"] = user.surname;
    }
    if (user.patronymic) {
        userDic[@"patronymic"] = user.patronymic;
    }
    if (user.gender) {
        userDic[@"gender"] = user.gender;
    }
    if ([TeoriusHelper dateIsValid:user.birthDay]) {
        userDic[@"birthDay"] = [TeoriusHelper birthdayToStringWithDefis:user.birthDay];
    }
    if (user.region) {
        //        userDic[@"russiaRegion"] = user.region;
        userDic[@"russiaRegion"] = @"16";
    }
    if (user.relationship) {
        if ([user.relationship isEqualToString:@""]) {
            userDic[@"isAuth"] = @YES;
        }
        else {
            userDic[@"kinship"] = user.relationship;
        }
    }
    
    if (user.polisNumber && [TeoriusHelper stringIsValid:user.polisNumber]) {
        userDic[@"policyNumber"] = user.polisNumber;
    }
    if (user.passportSeries && [TeoriusHelper stringIsValid:user.passportSeries]) {
        userDic[@"documSer"] = user.passportSeries;
        userDic[@"documType"] = @"14";
    }
    if (user.passportNumber && [TeoriusHelper stringIsValid:user.passportNumber]) {
        userDic[@"documNum"] = user.passportNumber;
    }
    if ([TeoriusHelper stringIsValid:user.phone]) {
        userDic[@"phone"] = user.phone;
    }
    if (user.image) {
        //        userDic[@"documNum"] = user.passportNumber;
    }

    
    NSString *idUser = user.ID;
    
    [[self RESTAPIManager]updateUser:userDic idUser:idUser completion:^(NSDictionary *dic, NSError *err) {

        if (err.code == 401) {
            [self loginWithLogin:self.persistencyManager.account.login password:self.persistencyManager.account.password completion:^(BOOL success, NSError *error) {
                [self updateUser:user completion:completion];
            }];
            
            return;
        }
        
        NSError *error1 = [self errorFromResponseWithError:err andDict:dic];
        if (error1) {
            completion(@{}, error1);
            return;
        }
        
        User *newUser = [User new];
        [TeoriusHelper setValuesForObject:newUser fromDict:dic withLinkingDict:@{@"documNum":@"passportNumber", @"documSer":@"passportSeries", @"firstName":@"name", @"id":@"ID", @"lastName":@"surname", @"patronymic":@"", @"uid":@"uuid", @"gender":@"", @"policyNumber":@"polisNumber", @"kinship": @"relationship", @"phone":@""}];
        
        NSString *birthdayString = dic[@"birthDay"];
        NSDate *birthday = [TeoriusHelper birthdayToDateWithDefis:birthdayString];
        if ([TeoriusHelper dateIsValid:birthday]) {
            newUser.birthDay = birthday;
        }

        Account *acc = [self objectFromRealmObject:self.persistencyManager.account];
        if (!acc.family) {
            acc.family = [Family new];
            acc.family.ID = [TeoriusHelper getUUID];
        }
        
        UserRLM *userRLM = [self realmObjectFromObject:newUser];
        
        if ([UserRLM objectForPrimaryKey:userRLM.ID]) {
            [self.persistencyManager addOrUpdateObject:userRLM];
        }
        else {
            if (![TeoriusHelper stringIsValid:user.relationship]) {
                acc.family.defaultMember = newUser;
            }
            
            [acc.family.members addObject:newUser];
            
            AccountRLM *accRLM = [self realmObjectFromObject:acc];
            [self.persistencyManager addOrUpdateObject:accRLM];
        }
        completion(@{}, nil);
    }];
}


#pragma mark - Measure

- (void)getAllActiveMeasure:(User *)user completion: (void (^)(NSDictionary *dic, NSError *err))completion {
    NSString *idUser = user.ID;
    
    [self.RESTAPIManager getAllActiveMeasure:idUser completion:^(NSDictionary *dict, NSError *error) {
        
        if (error.code == 401) {
            [self loginWithLogin:self.persistencyManager.account.login password:self.persistencyManager.account.password completion:^(BOOL success, NSError *error) {
                
                [self getAllActiveMeasure:user completion:completion];
            }];
            
            return;
        }
        
        completion(@{}, nil);
    }];
}

- (void)getAllMeasure:(User *)user completion:(void (^)(NSDictionary *, NSError *))completion {
    NSString *idUser = user.ID;
    NSDate *const dateToday = NSDate.date;
    NSTimeInterval year = 365 * 24 * 60 * 60;
    NSTimeInterval day =  24 * 60 * 60;
    NSDate *earlyYear = [NSDate dateWithTimeInterval:-year
                                          sinceDate:dateToday];
    NSDate *laterDay = [NSDate dateWithTimeInterval:+day
                                           sinceDate:dateToday];
    NSDate *const dateEarly = [dateToday earlierDate:earlyYear];
    NSDate *const dateLater = [dateToday laterDate:laterDay];
    formatter = [TeoriusHelper dateFormatterUsingFormat:@"yyyy-MM-dd"];
    NSString *dateFrom = [formatter stringFromDate:dateEarly];
    NSString *dateTo = [formatter stringFromDate:dateLater];

    
    NSString *date = [NSString stringWithFormat:@"%@..%@", dateFrom, dateTo];
    
    if (![TeoriusHelper stringIsValid:idUser] || ![TeoriusHelper stringIsValid:date]) {
        idUser = @"";
//        completion(@{}, nil);
//        return;
    }
    
    RLMResults *readings = [ReadingRLM allObjects];
    
    [[self persistencyManager]deleteObjectsFromResults:readings completion:^(BOOL success) {
        
    }];
    
    NSNumber *limit = @20;
    
    [self.RESTAPIManager getAllMeasure:idUser date:date limit:limit completion:^(NSDictionary *responseDict, NSError *err) {
        if (err.code == 401) {
            [self loginWithLogin:self.persistencyManager.account.login password:self.persistencyManager.account.password completion:^(BOOL success, NSError *error) {
                [self getAllMeasure:user completion:completion];
            }];
            
            return;
        }
        
        NSArray *measurements = responseDict[@"items"];
        
        NSString *plotID;
        for (NSDictionary *dic in measurements) {
            if ([dic[@"measure"] isEqualToString:@"1001"]) {
                plotID = @"kPulseCalm";
                if ([dic[@"measurePulseStatus"] isEqualToString:@"2"])
                    plotID = @"kPulseActive";
            }
            else if ([dic[@"measure"] isEqualToString:@"1002"]) {
                plotID = @"kTemperature";
            }
            else if ([dic[@"measure"] isEqualToString:@"1003"]) {
                plotID = @"kSystolicBloodPressure";
            }
            else if ([dic[@"measure"] isEqualToString:@"1004"]) {
                plotID = @"kHeight";
            }
            else if ([dic[@"measure"] isEqualToString:@"1005"]) {
                plotID = @"kWeight";
            }
            else if ([dic[@"measure"] isEqualToString:@"1006"]) {
                plotID = @"kSugar";
                if ([dic[@"measureSugarWhen"] isEqualToString:@"1"]) {
                    plotID = @"kSugarOnAnEmptyStomach";
                }
                else if ([dic[@"measureSugarWhen"] isEqualToString:@"2"]) {
                    plotID = @"kSugarBeforeMeals";
                }
                else if ([dic[@"measureSugarWhen"] isEqualToString:@"3"]) {
                    plotID = @"kSugarAfterMeals";
                }
                else if ([dic[@"measureSugarWhen"] isEqualToString:@"4"]) {
                    plotID = @"kSugarAtNight";
                }
            }
            else if ([dic[@"measure"] isEqualToString:@"1007"]) {
                plotID = @"kCholesterol";
                if ([dic[@"measureCholesterinType"] isEqualToString:@"1"]) {
                    plotID = @"kCholesterolGeneral";
                }
                else if ([dic[@"measureCholesterinType"] isEqualToString:@"2"]) {
                    plotID = @"kCholesterolLPNP";
                }
                else if ([dic[@"measureCholesterinType"] isEqualToString:@"3"]) {
                    plotID = @"kCholesterolLPVP";
                }
                else if ([dic[@"measureCholesterinType"] isEqualToString:@"4"]) {
                    plotID = @"kCholesterolTG";
                }
            }
//
            [self saveReading:dic plotID:plotID];
        }
        
        NSArray *norms = responseDict[@"norms"];
        
        for (NSDictionary *normDict in norms) {
            if (![TeoriusHelper dictIsValid:normDict]) {
                continue;
            }
            
            NSString *measure = normDict[@"measure"];
            NSString *minStr = normDict[@"min"];
            NSString *maxStr = normDict[@"max"];
            float min = minStr.floatValue;
            float max = maxStr.floatValue;

            if ([measure isEqualToString:@"1001"]) {
                NSArray *localNorms = normDict[@"norms"];
                for (NSDictionary *localNormDict in localNorms) {
                    NSString *measurePulseStatus = localNormDict[@"measurePulseStatus"];
                    
                    if ([measurePulseStatus isEqualToString:@"1"]) {
                        Measurement *pulse = [self getMeasurementByPlotID:@"kPulseCalm"];
                        pulse.norm = [[Norm alloc] initWithMin:[localNormDict[@"min"] floatValue] max:[localNormDict[@"max"] floatValue]];
                    }
                    else if ([measurePulseStatus isEqualToString:@"2"]) {
                        Measurement *pulse = [self getMeasurementByPlotID:@"kPulseActive"];
                        pulse.norm = [[Norm alloc] initWithMin:[localNormDict[@"min"] floatValue] max:[localNormDict[@"max"] floatValue]];
                    }
                }
            }
            else if ([measure isEqualToString:@"1002"]) {
                Measurement *temperature = [self getMeasurementByPlotID:@"kTemperature"];
                temperature.norm = [[Norm alloc] initWithMin:min max:max];
            }
            else if ([measure isEqualToString:@"1003"]) {
                NSDictionary *systole = normDict[@"systole"];
                Measurement *sysPressure = [self getMeasurementByPlotID:@"kSystolicBloodPressure"];
                sysPressure.norm = [[Norm alloc] initWithMin:[systole[@"min"] floatValue] max:[systole[@"max"] floatValue]];
                
                NSDictionary *diastole = normDict[@"diastole"];
                Measurement *dyasPressure = [self getMeasurementByPlotID:@"kDiastolicBloodPressure"];
                dyasPressure.norm = [[Norm alloc] initWithMin:[diastole[@"min"] floatValue] max:[diastole[@"max"] floatValue]];
            }
            else if ([measure isEqualToString:@"1005"]) {
                Measurement *weight = [self getMeasurementByPlotID:@"kWeight"];
                weight.norm = [[Norm alloc] initWithMin:min max:max];
            }
            else if ([measure isEqualToString:@"1006"]) {
                NSArray *localNorms = normDict[@"norms"];
                for (NSDictionary *localNormDict in localNorms) {
                    NSString *measureSugarWhen = localNormDict[@"measureSugarWhen"];
                    
                    if ([measureSugarWhen isEqualToString:@"1"]) {
                        Measurement *sugar = [self getMeasurementByPlotID:@"kSugarOnAnEmptyStomach"];
                        sugar.norm = [[Norm alloc] initWithMin:[localNormDict[@"min"] floatValue] max:[localNormDict[@"max"] floatValue]];
                    }
                    else if ([measureSugarWhen isEqualToString:@"2"]) {
                        Measurement *sugar = [self getMeasurementByPlotID:@"kSugarBeforeMeals"];
                        sugar.norm = [[Norm alloc] initWithMin:[localNormDict[@"min"] floatValue] max:[localNormDict[@"max"] floatValue]];
                    }
                    else if ([measureSugarWhen isEqualToString:@"3"]) {
                        Measurement *sugar = [self getMeasurementByPlotID:@"kSugarAfterMeals"];
                        sugar.norm = [[Norm alloc] initWithMin:[localNormDict[@"min"] floatValue] max:[localNormDict[@"max"] floatValue]];
                    }
                    else if ([measureSugarWhen isEqualToString:@"4"]) {
                        Measurement *sugar = [self getMeasurementByPlotID:@"kSugarAtNight"];
                        sugar.norm = [[Norm alloc] initWithMin:[localNormDict[@"min"] floatValue] max:[localNormDict[@"max"] floatValue]];
                    }
                }
            }
            else if ([measure isEqualToString:@"1007"]) {
                NSArray *localNorms = normDict[@"norms"];
                for (NSDictionary *localNormDict in localNorms) {
                    NSString *measureCholesterinType = localNormDict[@"measureCholesterinType"];
                    
                    if ([measureCholesterinType isEqualToString:@"1"]) {
                        Measurement *sugar = [self getMeasurementByPlotID:@"kCholesterolGeneral"];
                        sugar.norm = [[Norm alloc] initWithMin:[localNormDict[@"min"] floatValue] max:[localNormDict[@"max"] floatValue]];
                    }
                    else if ([measureCholesterinType isEqualToString:@"2"]) {
                        Measurement *sugar = [self getMeasurementByPlotID:@"kCholesterolLPNP"];
                        sugar.norm = [[Norm alloc] initWithMin:[localNormDict[@"min"] floatValue] max:[localNormDict[@"max"] floatValue]];
                    }
                    else if ([measureCholesterinType isEqualToString:@"3"]) {
                        Measurement *sugar = [self getMeasurementByPlotID:@"kCholesterolLPVP"];
                        sugar.norm = [[Norm alloc] initWithMin:[localNormDict[@"min"] floatValue] max:[localNormDict[@"max"] floatValue]];
                    }
                    else if ([measureCholesterinType isEqualToString:@"4"]) {
                        Measurement *sugar = [self getMeasurementByPlotID:@"kCholesterolTG"];
                        sugar.norm = [[Norm alloc] initWithMin:[localNormDict[@"min"] floatValue] max:[localNormDict[@"max"] floatValue]];
                    }
                }
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self performBlock:^{
                NSLog(@"%@", [ReadingRLM allObjects]);
                completion(@{}, nil);
            } afterDelay:.5];
        });
    }];
}

- (void)createPulse:(NSString *)fixTime status:(NSString *)status beat:(NSString *)beat completion:(void(^)(NSDictionary *, NSError *))completion {
    NSString *plotID = @"kPulseCalm";
    if ([status isEqualToString:@"2"]) {
        plotID = @"kPulseActive";
    }
    NSDictionary *dic = [NSDictionary new];
    float f = [beat floatValue];
    NSString *b = [NSString stringWithFormat:@"%.0f", f];
    
    dic = @{@"measurePulseStatus" : status, @"beat" : b, @"fixedTime" : fixTime, @"userCitizen" : self.currentUser.ID};
    NSMutableArray *arrayOfDicts=[[NSMutableArray alloc] init];
    [arrayOfDicts addObject:dic];
    NSArray *info = [NSArray arrayWithArray:arrayOfDicts];
    [self.RESTAPIManager createReading:info plotID:plotID completion:^(NSArray *arr, NSError *err) {
        if (err.code == 401) {
            [self loginWithLogin:self.persistencyManager.account.login password:self.persistencyManager.account.password completion:^(BOOL success, NSError *error) {
                [self createPulse:fixTime status:status beat:beat completion:completion];
            }];
            
            return;
        }
        
        if ([arr isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dict = (NSDictionary *)arr;
            
            NSError *error1 = [self errorFromResponseWithError:err andDict:dict];
            if (error1) {
                completion(@{}, error1);
                return;
            }
            
            completion(@{}, [self errorWithText:@"Неизвестная ошибка"]);
        }
        completion(arr[0], nil);
    }];
}

- (void)createTemperature:(NSString *)fixTime celsius:(NSString *)celsius completion:(void(^)(NSDictionary *, NSError *))completion {
    
    NSDictionary *dic = [NSDictionary new];
    dic = @{
        @"fixedTime":fixTime,
        @"celsius":celsius,
        @"userCitizen" : self.currentUser.ID
    };
    
    NSString *plotID = @"kTemperature";
    NSMutableArray *arrayOfDicts=[[NSMutableArray alloc] init];
    [arrayOfDicts addObject:dic];
    NSArray *info = [NSArray arrayWithArray:arrayOfDicts];
    [self.RESTAPIManager createReading:info plotID:plotID completion:^(NSArray *arr, NSError *err) {
        if (err.code == 401) {
            [self loginWithLogin:self.persistencyManager.account.login password:self.persistencyManager.account.password completion:^(BOOL success, NSError *error) {
                [self createTemperature:fixTime celsius:celsius completion:completion];
            }];
            
            return;
        }
        if ([arr isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dict = (NSDictionary *)arr;
            
            NSError *error1 = [self errorFromResponseWithError:err andDict:dict];
            if (error1) {
                completion(@{}, error1);
                return;
            }
            
            completion(@{}, [self errorWithText:@"Неизвестная ошибка"]);
        }
        completion(arr[0], nil);
    }];
}

- (void)createPressure:(NSString *)fixTime systole:(NSString *)systole diastole:(NSString *)diastole completion:(void(^)(NSDictionary *, NSError *))completion {
    NSDictionary *dic = [NSDictionary new];
    
    dic = @{
            @"fixedTime":fixTime,
            @"systole":systole,
            @"diastole":diastole,
            @"userCitizen" : self.currentUser.ID
            };
    
    NSString *plotID = @"kSystolicBloodPressure";
    
    NSMutableArray *arrayOfDicts=[[NSMutableArray alloc] init];
    [arrayOfDicts addObject:dic];
    NSArray *info = [NSArray arrayWithArray:arrayOfDicts];
    
    [self.RESTAPIManager createReading:info plotID:plotID completion:^(NSArray *arr, NSError *err) {
        if (err.code == 401) {
            [self loginWithLogin:self.persistencyManager.account.login password:self.persistencyManager.account.password completion:^(BOOL success, NSError *error) {
                [self createPressure:fixTime systole:systole diastole:diastole completion:completion];
            }];
            
            return;
        }
        if ([arr isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dict = (NSDictionary *)arr;
            
            NSError *error1 = [self errorFromResponseWithError:err andDict:dict];
            if (error1) {
                completion(@{}, error1);
                return;
            }
            
            completion(@{}, [self errorWithText:@"Неизвестная ошибка"]);
        }
        completion(arr[0], nil);
    }];
}

- (void)createStature:(NSString *)fixTime centimeter:(NSString *)centimeter completion:(void(^)(NSDictionary *, NSError *))completion {
//    {"fixedTime":"2014-08-01T09:24:24", "centimeter":"165"},
    NSDictionary *dic = [NSDictionary new];
    
    float centimeterFloat = [centimeter floatValue];
    NSString *newCentimeter = [NSString stringWithFormat:@"%.0f", centimeterFloat];
    
    
    dic = @{
            @"fixedTime":fixTime,
            @"centimeter":newCentimeter,
            @"userCitizen" : self.currentUser.ID
            };
    
    NSString *plotID = @"kHeight";
    NSMutableArray *arrayOfDicts=[[NSMutableArray alloc] init];
    [arrayOfDicts addObject:dic];
    NSArray *info = [NSArray arrayWithArray:arrayOfDicts];
    [self.RESTAPIManager createReading:info plotID:plotID completion:^(NSArray *arr, NSError *err) {
        if (err.code == 401) {
            [self loginWithLogin:self.persistencyManager.account.login password:self.persistencyManager.account.password completion:^(BOOL success, NSError *error) {
                [self createStature:fixTime centimeter:centimeter completion:completion];
            }];
            
            return;
        }
        if ([arr isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dict = (NSDictionary *)arr;
            
            NSError *error1 = [self errorFromResponseWithError:err andDict:dict];
            if (error1) {
                completion(@{}, error1);
                return;
            }
            
            completion(@{}, [self errorWithText:@"Неизвестная ошибка"]);
        }
        completion(arr[0], nil);
    }];
}

- (void)createWeight:(NSString *)fixTime kilogram:(NSString *)kilogram completion:(void(^)(NSDictionary *, NSError *))completion {
    NSDictionary *dic = [NSDictionary new];
    
    float f = [kilogram floatValue];
    NSString *b = [NSString stringWithFormat:@"%.1f", f];
    
    dic = @{
            @"fixedTime":fixTime,
            @"kilogram":b,
            @"userCitizen" : self.currentUser.ID
            };
    
    NSString *plotID = @"kWeight";
    NSMutableArray *arrayOfDicts=[[NSMutableArray alloc] init];
    [arrayOfDicts addObject:dic];
    NSArray *info = [NSArray arrayWithArray:arrayOfDicts];
    [self.RESTAPIManager createReading:info plotID:plotID completion:^(NSArray *arr, NSError *err) {
        if (err.code == 401) {
            [self loginWithLogin:self.persistencyManager.account.login password:self.persistencyManager.account.password completion:^(BOOL success, NSError *error) {
                [self createWeight:fixTime kilogram:kilogram completion:completion];
            }];
            
            return;
        }
        if ([arr isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dict = (NSDictionary *)arr;
            
            NSError *error1 = [self errorFromResponseWithError:err andDict:dict];
            if (error1) {
                completion(@{}, error1);
                return;
            }
            
            completion(@{}, [self errorWithText:@"Неизвестная ошибка"]);
        }
        completion(arr[0], nil);
    }];
}

- (void)createSugar:(NSString *)fixTime status:(NSString *)status mmol:(NSString *)mmol completion:(void(^)(NSDictionary *, NSError *))completion {
    NSDictionary *dic = [NSDictionary new];
    
    
    if ([status isEqualToString:@"kSugarOnAnEmptyStomach"]) {
        status = @"1";
    }
    else if ([status isEqualToString:@"kSugarBeforeMeals"]) {
        status = @"2";
    }
    else if ([status isEqualToString:@"kSugarAfterMeals"]) {
        status = @"3";
    }
    else if ([status isEqualToString:@"kSugarAtNight"]) {
        status = @"4";
    }
    
    
    dic = @{
            @"fixedTime":fixTime,
            @"measureSugarWhen":status,
            @"mmol":mmol,
            @"userCitizen" : self.currentUser.ID
            };
    
    NSString *plotID = @"kSugarOnAnEmptyStomach";
    
    NSMutableArray *arrayOfDicts=[[NSMutableArray alloc] init];
    [arrayOfDicts addObject:dic];
    NSArray *info = [NSArray arrayWithArray:arrayOfDicts];
    [self.RESTAPIManager createReading:info plotID:plotID completion:^(NSArray *arr, NSError *err) {
        if (err.code == 401) {
            [self loginWithLogin:self.persistencyManager.account.login password:self.persistencyManager.account.password completion:^(BOOL success, NSError *error) {
                [self createSugar:fixTime status:status mmol:mmol completion:completion];
            }];
            
            return;
        }
        if ([arr isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dict = (NSDictionary *)arr;
            
            NSError *error1 = [self errorFromResponseWithError:err andDict:dict];
            if (error1) {
                completion(@{}, error1);
                return;
            }
            
            completion(@{}, [self errorWithText:@"Неизвестная ошибка"]);
        }
        completion(arr[0], nil);
    }];
}

- (void)createCholesterin:(NSString *)fixTime status:(NSString *)status mmol:(NSString *)mmol completion:(void(^)(NSDictionary *, NSError *))completion {
    NSDictionary *dic = [NSDictionary new];
    
    if ([status isEqualToString:@"kCholesterolGeneral"]) {
        status = @"1";
    }
    else if ([status isEqualToString:@"kCholesterolLPNP"]) {
        status = @"2";
    }
    else if ([status isEqualToString:@"kCholesterolLPVP"]) {
        status = @"3";
    }
    else if ([status isEqualToString:@"kCholesterolTG"]) {
        status = @"4";
    }
    
    dic = @{
            @"fixedTime":fixTime,
            @"measureCholesterinType":status,
            @"mmol":mmol,
            @"userCitizen" : self.currentUser.ID
            };
    
    NSString *plotID = @"kCholesterolGeneral";
    NSMutableArray *arrayOfDicts=[[NSMutableArray alloc] init];
    [arrayOfDicts addObject:dic];
    NSArray *info = [NSArray arrayWithArray:arrayOfDicts];
    [self.RESTAPIManager createReading:info plotID:plotID completion:^(NSArray *arr, NSError *err) {
        if (err.code == 401) {
            [self loginWithLogin:self.persistencyManager.account.login password:self.persistencyManager.account.password completion:^(BOOL success, NSError *error) {
                [self createCholesterin:fixTime status:status mmol:mmol completion:completion];
            }];
            
            return;
        }
        if ([arr isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dict = (NSDictionary *)arr;
            
            NSError *error1 = [self errorFromResponseWithError:err andDict:dict];
            if (error1) {
                completion(@{}, error1);
                return;
            }
            
            completion(@{}, [self errorWithText:@"Неизвестная ошибка"]);
        }
        completion(arr[0], nil);
    }];
}

- (void)saveReading:(NSDictionary *)dic plotID:(NSString *)plotID {
    formatter = [TeoriusHelper dateFormatterUsingFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSError *error;
    NSPredicate *pred;
    NSString *newString;
    NSString *newDate = dic[@"fixedTime"];
    if ([TeoriusHelper stringIsValid:newDate]) {
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"T" options:NSRegularExpressionCaseInsensitive error:&error];
        newString = [regex stringByReplacingMatchesInString:newDate options:0 range:NSMakeRange(0, [newDate length]) withTemplate:@" "];
    }

    Reading *reading = [Reading new];
    User *currentUser = self.currentUser;

    pred = [NSPredicate predicateWithFormat:@"plotID = %@", plotID];
    
    RLMResults *measRLM = [MeasurementRLM objectsWithPredicate:pred];
    Measurement *meas = [self objectFromRealmObject:measRLM.firstObject];
    
    pred = [NSPredicate predicateWithFormat:@"plotID = %@", @"kDiastolicBloodPressure"];
    
    RLMResults *measRLM2 = [MeasurementRLM objectsWithPredicate:pred];
    Measurement *meas2 = [self objectFromRealmObject:measRLM2.firstObject];
    
    NSString *measurementId = dic[@"measurementId"];
    NSString *measure = dic[@"measure"];
    if ([TeoriusHelper stringIsValid:measurementId] && [TeoriusHelper stringIsValid:measure]) {
        reading.ID = [NSString  stringWithFormat:@"%@_%@", measure, measurementId];
    }
    else {
        reading.ID = dic[@"id"];
    }
    
    reading.user = currentUser;
    reading.measurement = meas;
    reading.date = [formatter dateFromString:newString];
    
    if ([TeoriusHelper stringIsValid:dic[@"beat"]]) {
        reading.value = [dic[@"beat"] floatValue];
        ReadingRLM *readingRLM = [self realmObjectFromObject:reading];
        [self.persistencyManager addOrUpdateObject:readingRLM];
    }
    else if ([TeoriusHelper stringIsValid:dic[@"celsius"]]){
        NSLog(@"%@", [ReadingRLM allObjects]);
        
        reading.value = [dic[@"celsius"] floatValue];
        ReadingRLM *readingRLM = [self realmObjectFromObject:reading];
        [self.persistencyManager addOrUpdateObject:readingRLM];
    }
    else if ([TeoriusHelper stringIsValid:dic[@"centimeter"]]){
     reading.value = [dic[@"centimeter"] floatValue];
        ReadingRLM *readingRLM = [self realmObjectFromObject:reading];
        [self.persistencyManager addOrUpdateObject:readingRLM];
    }
    else if ([TeoriusHelper stringIsValid:dic[@"kilogram"]]){
        reading.value = [dic[@"kilogram"] floatValue];
        ReadingRLM *readingRLM = [self realmObjectFromObject:reading];
        [self.persistencyManager addOrUpdateObject:readingRLM];
    }
    else if ([TeoriusHelper stringIsValid:dic[@"mmol"]]) {
     reading.value = [dic[@"mmol"] floatValue];
        ReadingRLM *readingRLM = [self realmObjectFromObject:reading];
        [self.persistencyManager addOrUpdateObject:readingRLM];
    }
    else if ([TeoriusHelper stringIsValid:dic[@"mmol"]]) {
        reading.value = [dic[@"mmol"] floatValue];
        ReadingRLM *readingRLM = [self realmObjectFromObject:reading];
        [self.persistencyManager addOrUpdateObject:readingRLM];
    }
    else if ([TeoriusHelper stringIsValid:dic[@"systole"]]) {
        NSString *idSystole = [NSString stringWithFormat:@"%@_%@", reading.ID, @1];
        NSString *idDiastole = [NSString stringWithFormat:@"%@_%@", reading.ID, @2];
        
        reading.value = [dic[@"systole"] floatValue];
        reading.ID = idSystole;
        ReadingRLM *readingRLM = [self realmObjectFromObject:reading];
        [self.persistencyManager addOrUpdateObject:readingRLM];
        
        reading.ID = idDiastole;
        reading.value = [dic[@"diastole"] floatValue];
        reading.measurement = meas2;
        ReadingRLM *readingRLM2 = [self realmObjectFromObject:reading];
        [self.persistencyManager addOrUpdateObject:readingRLM2];
    }
}

- (void)newReading:(Measurement *)meas date:(NSDate *)date value:(NSArray *)value completion:(void(^)(BOOL success, NSError *error))completion {
    formatter = [TeoriusHelper dateFormatterUsingFormat:@"yyyy-MM-dd"];
    formatter2 = [TeoriusHelper dateFormatterUsingFormat:@"HH:mm:ss"];
    
    NSString *dateStr = [formatter stringFromDate:date];
    NSString *timeStr = [formatter2 stringFromDate:date];
    NSString *fullDateStr = [NSString stringWithFormat:@"%@T%@", dateStr, timeStr];
    
    if ([meas.plotID containsString:@"kPulse"]) {
        NSString *status = @"1";
        if ([meas.plotID isEqualToString:@"kPulseActive"])
            status = @"2";
            [self createPulse:fullDateStr status:status beat:value[0] completion:^(NSDictionary *dic, NSError *err) {
                [self saveReading:dic plotID: meas.plotID];
                completion(YES, err);
        }];
    }
    else if ([meas.plotID containsString:@"kTemperature"]) {
        [self createTemperature:fullDateStr celsius:value[0] completion:^(NSDictionary *dic, NSError *err) {
            [self saveReading:dic plotID: meas.plotID];
            completion(YES, err);
        }];
    }
    else if ([meas.plotID containsString:@"BloodPressure"]) {
        NSString *systole = [value[0] stringValue];
        NSString *diastole = [value[1] stringValue];
        
        [self createPressure:fullDateStr systole:systole diastole:diastole completion:^(NSDictionary *dic, NSError *err) {
            [self saveReading:dic plotID: meas.plotID];
            completion(YES, err);
        }];
    }
    else if ([meas.plotID containsString:@"kHeight"]) {
        [self createStature:fullDateStr centimeter:value[0] completion:^(NSDictionary *dic, NSError *err) {
            [self saveReading:dic plotID: meas.plotID];
            completion(YES, err);
        }];
    }
    else if ([meas.plotID containsString:@"kWeight"]) {
        [self createWeight:fullDateStr kilogram:value[0] completion:^(NSDictionary *dic, NSError *err) {
            [self saveReading:dic plotID: meas.plotID];
            completion(YES, err);
        }];
    }
    else if ([meas.plotID containsString:@"kSugar"]) {
        [self createSugar:fullDateStr status:meas.plotID mmol:value[0] completion:^(NSDictionary *dic, NSError *err) {
            [self saveReading:dic plotID: meas.plotID];
            completion(YES, err);
        }];
    }
    else if ([meas.plotID containsString:@"kCholesterol"]) {
        [self createCholesterin:fullDateStr status:meas.plotID mmol:value[0] completion:^(NSDictionary *dic, NSError *err) {
            [self saveReading:dic plotID: meas.plotID];
            completion(YES, err);
        }];
    }
}

- (NSArray *)myReadingsArr {
    RLMResults *rlmRes = [self.persistencyManager myReadingsResult];
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:rlmRes.count];
    for (RLMObject *object in rlmRes) {
        [array addObject:[self objectFromRealmObject:object]];
    }
    return array;
}

- (NSArray *)getReadingsForGraphByName:(NSString *)str fromReadings:(NSArray *)readings {
    NSPredicate *predicate;
    
    predicate = [NSPredicate predicateWithBlock:^BOOL(id  _Nonnull evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
        Reading *reading = evaluatedObject;
        
        
        if ([reading.measurement.name containsString:str]) {
            return YES;
        }
        
        return NO;
    }];
    NSArray *arr = [readings filteredArrayUsingPredicate:predicate];
    return arr;
}

- (NSArray *)getReadingsByCorePlotName:(NSString *)str fromReadings:(NSArray *)readings {
    NSPredicate *predicate;
    
    predicate = [NSPredicate predicateWithBlock:^BOOL(id  _Nonnull evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
        Reading *reading = evaluatedObject;
        return [reading.measurement.plotID containsString:str];
    }];
    NSArray *arr = [readings filteredArrayUsingPredicate:predicate];
    return arr;
}

- (NSArray *)getReadingsForMeasurements:(NSArray *)measurements fromReadings:(NSArray *)readings {
    NSPredicate *predicate;
    
    predicate = [NSPredicate predicateWithBlock:^BOOL(id  _Nonnull evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
        Reading *reading = evaluatedObject;
        
        BOOL found = NO;
        for (Measurement *m in measurements) {
            if ([m.plotID isEqualToString:reading.measurement.plotID]) {
                found = YES;
                break;
            }
        }
        
        return found;
    }];
    
    NSArray *arr = [readings filteredArrayUsingPredicate:predicate];
    return arr;
}

- (NSArray *)getMeasurementsByGraphName:(NSString *)graphName fromMeasurements:(NSArray *)measurements {
    NSPredicate *predicate = [NSPredicate predicateWithBlock:^BOOL(id  _Nonnull evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
        Measurement *m = evaluatedObject;
        return [m.name containsString:graphName];
    }];
    
    return [measurements filteredArrayUsingPredicate:predicate];
}

- (NSDictionary *)groupReadings:(NSArray *)readings byMeasurements:(NSArray *)measurements {
    NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] initWithCapacity:measurements.count];
    
    for (Measurement *measurement in measurements) {
        NSPredicate *predicate = [NSPredicate predicateWithBlock:^BOOL(id  _Nonnull evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
            Reading *reading = evaluatedObject;
            return [reading.measurement.plotID isEqualToString:measurement.plotID];
        }];
        
        NSArray *filteredReadings = [readings filteredArrayUsingPredicate:predicate];
        if (filteredReadings.count > 0) {
            tempDict[measurement.plotID] = filteredReadings;
        }
    }
    
    return tempDict.copy;
}

- (Measurement *)getMeasurementByPlotID:(NSString *)plotID {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"plotID = %@", plotID];
    NSArray *filtered = [self.measurementsArray filteredArrayUsingPredicate:predicate];
    return filtered.firstObject;
}

#pragma mark - Notifications

- (void)getAllNotificationsAfterTapped:(void(^)(NSMutableArray *, NSMutableArray *, NSError *))completion {
    [[self RESTAPIManager] getAllNotificationsAfterTapped:@"" completion:^(NSDictionary *dic, NSError *error) {
        
        NSString *demoDic = @"{\"uri\":\"notification\",\"items\":[{\"title\":\"\u041F\u043E\u043C\u043E\u0433\u0438\u0442\u0435 \u043D\u0430\u043C \u0443\u0437\u043D\u0430\u0442\u044C \u043E \u0412\u0430\u0441 \u0431\u043E\u043B\u044C\u0448\u0435\",\"type\":\"1\",\"status\":\"6\",\"code\":\"10001\",\"list\":[{\"status\":\"5\",\"control\":\"2\",\"answer\":\"1\",\"label\":\"\u0423\u043A\u0430\u0436\u0438\u0442\u0435 \u0444\u0430\u043C\u0438\u043B\u0438\u044E\",\"constraint\":{\"maxSize\":\"70\",\"minSize\":\"2\"},\"value\":\"\u0418\u0432\u0430\u043D\u043E\u0432\"},{\"status\":\"5\",\"control\":\"2\",\"answer\":\"2\",\"label\":\"\u0423\u043A\u0430\u0436\u0438\u0442\u0435 \u0438\u043C\u044F\",\"constraint\":{\"maxSize\":\"70\",\"minSize\":\"2\"},\"value\":\"\u0418\u0432\u0430\u043D\"},{\"status\":\"5\",\"control\":\"2\",\"answer\":\"3\",\"label\":\"\u0423\u043A\u0430\u0436\u0438\u0442\u0435 \u043E\u0442\u0447\u0435\u0441\u0442\u0432\u043E\",\"constraint\":{\"maxSize\":\"70\",\"minSize\":\"2\"},\"value\":\"\u0418\u0432\u0430\u043D\u043E\u0432\u0438\u0447\"},{\"status\":\"1\",\"control\":\"2\",\"answer\":\"4\",\"label\":\"\u0423\u043A\u0430\u0436\u0438\u0442\u0435 \u0434\u0430\u0442\u0443 \u0440\u043E\u0436\u0434\u0435\u043D\u0438\u044F\",\"constraint\":{\"min\":\"1895-01-01\"}},{\"status\":\"5\",\"control\":\"2\",\"answer\":\"5\",\"label\":\"\u0423\u043A\u0430\u0436\u0438\u0442\u0435 \u0412\u0430\u0448 \u043F\u043E\u043B\",\"options\":[{\"id\":\"1\",\"name\":\"\u041C\u0443\u0436\u0441\u043A\u043E\u0439\"},{\"id\":\"2\",\"name\":\"\u0416\u0435\u043D\u0441\u043A\u0438\u0439\"}],\"value\":\"1\"}]},{\"title\":\"\u041F\u0440\u043E\u0445\u043E\u0434\u0438\u043B\u0438 \u043B\u0438 \u0412\u044B \u0434\u0438\u0441\u043F\u0430\u043D\u0441\u0435\u0440\u0438\u0437\u0430\u0446\u0438\u044E \u0432 \u044D\u0442\u043E\u043C \u0433\u043E\u0434\u0443?\",\"type\":\"1\",\"status\":\"5\",\"code\":\"10003\",\"buttons\":[{\"answer\":\"7\",\"label\":\"\u0414\u0430\",\"value\":{\"code\":\"24001\",\"type\":\"3\"}},{\"answer\":\"8\",\"label\":\"\u041D\u0435\u0442\",\"value\":{\"code\":\"24002\",\"type\":\"3\"}},{\"answer\":\"9\",\"label\":\"\u041D\u0435 \u0437\u043D\u0430\u044E, \u0447\u0442\u043E \u044D\u0442\u043E \u0442\u0430\u043A\u043E\u0435\",\"value\":{\"code\":\"24002\",\"type\":\"3\"}}]},{\"title\":\"\u0425\u043E\u0442\u0435\u043B\u0438 \u0431\u044B \u0412\u044B \u043E\u0446\u0435\u043D\u0438\u0442\u044C \u0440\u0438\u0441\u043A \u0440\u0430\u0437\u0432\u0438\u0442\u0438\u044F \u0441\u0435\u0440\u0434\u0435\u0447\u043D\u043E-\u0441\u043E\u0441\u0443\u0434\u0438\u0441\u0442\u044B\u0445 \u0437\u0430\u0431\u043E\u043B\u0435\u0432\u0430\u043D\u0438\u0439 \u0441\u043E \u0441\u043C\u0435\u0440\u0442\u0435\u043B\u044C\u043D\u044B\u043C \u0438\u0441\u0445\u043E\u0434\u043E\u043C \u0432 \u0431\u043B\u0438\u0436\u0430\u0439\u0448\u0438\u0435 10 \u043B\u0435\u0442?\",\"type\":\"1\",\"status\":\"5\",\"code\":\"10008\",\"list\":[{\"status\":\"5\",\"control\":\"2\",\"answer\":\"100555292\",\"label\":\"\u0423\u043A\u0430\u0436\u0438\u0442\u0435 \u0412\u0430\u0448\u0435 \u0434\u0430\u0432\u043B\u0435\u043D\u0438\u0435(\u0441\u0438\u0441\u0442\u043E\u043B\u0438\u0447\u0435\u0441\u043A\u043E\u0435)\",\"value\":\"70\",\"constraint\":{\"minSize\":\"60\",\"maxSize\":\"280\"}},{\"status\":\"5\",\"control\":\"2\",\"answer\":\"100555293\",\"label\":\"\u0423\u043A\u0430\u0436\u0438\u0442\u0435 \u0443\u0440\u043E\u0432\u0435\u043D\u044C \u0412\u0430\u0448\u0435\u0433\u043E \u0445\u043E\u043B\u0435\u0441\u0442\u0435\u0440\u0438\u043D\u0430 (\u043E\u0431\u0449\u0438\u0439)\",\"value\":\"12.8\",\"constraint\":{\"minSize\":\"0\",\"maxSize\":\"15.9\"}},{\"status\":\"5\",\"type\":\"1\",\"code\":\"20008\"}],\"buttons\":[{\"answer\":\"100555300\",\"label\":\"\u0414\u0430\"},{\"answer\":\"100555301\",\"label\":\"\u041D\u0435\u0442\",\"value\":\"0\"}]},{\"title\":\"\u041A\u0443\u0440\u0438\u0442\u0435 \u043B\u0438 \u0432\u044B?\",\"type\":\"1\",\"status\":\"1\",\"code\":\"10004\",\"buttons\":[{\"answer\":\"11\",\"label\":\"\u0414\u0430\",\"value\":{\"code\":\"21002\",\"type\":\"2\"}},{\"answer\":\"12\",\"label\":\"\u041D\u0435\u0442\",\"value\":{\"code\":\"24003\",\"type\":\"3\"}}]}],\"hidden\":[{\"title\":\"\u0411\u043B\u0430\u0433\u043E\u0434\u0430\u0440\u0438\u043C \u0412\u0430\u0441 \u0437\u0430 \u043E\u0442\u0432\u0435\u0442. \u041C\u044B \u0440\u0430\u0434\u044B, \u0447\u0442\u043E \u0412\u044B \u0437\u0430\u0431\u043E\u0442\u0438\u0442\u0435\u0441\u044C \u043E \u0441\u0432\u043E\u0435\u043C \u0437\u0434\u043E\u0440\u043E\u0432\u044C\u0435. \u042D\u0442\u043E \u043F\u043E\u043C\u043E\u0436\u0435\u0442 \u0432\u0430\u043C \u043D\u0435 \u0437\u0430\u043F\u0443\u0441\u0442\u0438\u0442\u044C \u0431\u043E\u043B\u0435\u0437\u043D\u0438, \u0438 \u043F\u0440\u0435\u0434\u0443\u043F\u0440\u0435\u0434\u0438\u0442\u044C \u0432\u043E\u0437\u043C\u043E\u0436\u043D\u044B\u0435 \u0437\u0430\u0431\u043E\u043B\u0435\u0432\u0430\u043D\u0438\u044F \u0435\u0449\u0435 \u043D\u0430 \u0440\u0430\u043D\u043D\u0438\u0445 \u0441\u0442\u0430\u0434\u0438\u044F\u0445.\",\"type\":\"3\",\"status\":\"1\",\"code\":\"24001\"},{\"title\":\"\u0414\u043B\u044F \u043D\u0430\u0447\u0430\u043B\u0430 \u043E\u0431\u0441\u043B\u0435\u0434\u043E\u0432\u0430\u043D\u0438\u044F \u0432 \u0440\u0430\u043C\u043A\u0430\u0445 \u0434\u0438\u0441\u043F\u0430\u043D\u0441\u0435\u0440\u0438\u0437\u0430\u0446\u0438\u0438 \u043D\u0435\u043E\u0431\u0445\u043E\u0434\u0438\u043C\u043E \u0437\u0430\u043F\u0438\u0441\u0430\u0442\u044C\u0441\u044F \u043D\u0430 \u043F\u0440\u0438\u0435\u043C \u043A \u0432\u0440\u0430\u0447\u0443-\u0442\u0435\u0440\u0430\u043F\u0435\u0432\u0442\u0443 \u0432 \u0412\u0430\u0448\u0443 \u043F\u043E\u043B\u0438\u043A\u043B\u0438\u043D\u0438\u043A\u0443\",\"type\":\"3\",\"status\":\"5\",\"code\":\"24002\",\"buttons\":[{\"answer\":\"10\",\"label\":\"\u0417\u0430\u043F\u0438\u0441\u044C \u043D\u0430 \u043F\u0440\u0438\u0435\u043C\",\"value\":{\"userInterface\":\"103\"}}]},{\"title\":\"\u041A\u0443\u0440\u0438\u0442\u0435 \u043B\u0438 \u0432\u044B?\",\"type\":\"1\",\"status\":\"5\",\"code\":\"20008\",\"buttons\":[{\"label\":\"\u0414\u0430\",\"answer\":\"100555302\",\"status\":\"5\",\"type\":\"3\",\"code\":\"77777777\"},{\"label\":\"\u041D\u0435\u0442\",\"answer\":\"100555303\",\"status\":\"1\",\"type\":\"3\",\"code\":\"44444444\"}]},{\"title\":\"\u0420\u0438\u0441\u043A \u0440\u0430\u0437\u0432\u0438\u0442\u0438\u044F \u0441\u0435\u0440\u0434\u0435\u0447\u043D\u043E-\u0441\u043E\u0441\u0443\u0434\u0438\u0441\u0442\u044B\u0445 \u0437\u0430\u0431\u043E\u043B\u0435\u0432\u0430\u043D\u0438\u0439 \u0441\u043E \u0441\u043C\u0435\u0440\u0442\u0435\u043B\u044C\u043D\u044B\u043C \u0438\u0441\u0445\u043E\u0434\u043E\u043C \u0432 \u0431\u043B\u0438\u0436\u0430\u0439\u0448\u0438\u0435 10 \u043B\u0435\u0442 \u0434\u043B\u044F \u0412\u0430\u0441 \u0441\u043E\u0441\u0442\u0430\u0432\u0438\u0442 30%\",\"type\":\"3\",\"status\":\"1\",\"code\":\"77777777\"},{\"title\":\"\u0420\u0438\u0441\u043A \u0440\u0430\u0437\u0432\u0438\u0442\u0438\u044F \u0441\u0435\u0440\u0434\u0435\u0447\u043D\u043E-\u0441\u043E\u0441\u0443\u0434\u0438\u0441\u0442\u044B\u0445 \u0437\u0430\u0431\u043E\u043B\u0435\u0432\u0430\u043D\u0438\u0439 \u0441\u043E \u0441\u043C\u0435\u0440\u0442\u0435\u043B\u044C\u043D\u044B\u043C \u0438\u0441\u0445\u043E\u0434\u043E\u043C \u0432 \u0431\u043B\u0438\u0436\u0430\u0439\u0448\u0438\u0435 10 \u043B\u0435\u0442 \u0434\u043B\u044F \u0412\u0430\u0441 \u0441\u043E\u0441\u0442\u0430\u0432\u0438\u0442 15%\",\"type\":\"3\",\"status\":\"1\",\"code\":\"44444444\"},{\"title\":\"\u0421\u0442\u0430\u0442\u044C\u044F \u043E \u0432\u0440\u0435\u0434\u0435 \u043A\u0443\u0440\u0435\u043D\u0438\u044F\",\"type\":\"2\",\"status\":\"1\",\"code\":\"21002\",\"cmsArticleId\":\"27\"},{\"title\":\"\u0411\u043B\u0430\u0433\u043E\u0434\u0430\u0440\u0438\u043C \u0412\u0430\u0441 \u0437\u0430 \u043E\u0442\u0432\u0435\u0442. \u041D\u0430\u0434\u0435\u0435\u043C\u0441\u044F, \u0447\u0442\u043E \u0438 \u0432\u043F\u0440\u0435\u0434\u044C \u0412\u044B \u0431\u0443\u0434\u0435\u0442\u0435 \u043E\u0431\u0445\u043E\u0434\u0438\u0442\u044C \u0441\u0442\u043E\u0440\u043E\u043D\u043E\u0439 \u044D\u0442\u0443 \u043F\u0430\u0433\u0443\u0431\u043D\u0443\u044E \u043F\u0440\u0438\u0432\u044B\u0447\u043A\u0443.\",\"type\":\"3\",\"status\":\"1\",\"code\":\"24003\"}]}";
        NSDictionary *newDic = [demoDic jsonObject];
        
        if (error.code == 401) {
            [self loginWithLogin:self.persistencyManager.account.login password:self.persistencyManager.account.password completion:^(BOOL success, NSError *error) {
                
                [self getAllNotificationsAfterTapped:completion];
            }];
            
            return;
        }
        
        NSMutableArray *arr = newDic[@"items"];
        NSMutableArray *arrHidden = newDic[@"hidden"];
        NSMutableArray *notificationsArr = [NSMutableArray new];
        NSMutableArray *hiddenArr = [NSMutableArray new];
        
        
        [self notificationsArrayToArray:arr outArr:notificationsArr];
        [self notificationsArrayToArray:arrHidden outArr:hiddenArr];
        
        completion(notificationsArr, hiddenArr, nil);
        
    }];
}

- (void)sendAnswerAndStateNotifications {
    [[self RESTAPIManager] sendAnswerAndStateNotifications:@[] completion:^(NSArray *arr, NSError *err) {
        
    }];
}

- (void)notificationsArrayToArray:(NSMutableArray *)inArr outArr:(NSMutableArray *)outArr {
    NSMutableArray *questionsArr = [NSMutableArray new];
    NSMutableArray *answersArr = [NSMutableArray new];
    for (NSDictionary *notif in inArr) {
        NSString *typeStr = notif[@"type"];
        if([TeoriusHelper stringIsValid:typeStr]) {
            int type = [typeStr intValue];
            switch (type) {
                case NotificationTypeInterview: {
                    Interview *interview = [Interview new];
                    [TeoriusHelper setValuesForObject:interview fromDict:notif withLinkingDict:@{@"title":@"", @"status":@"", @"code":@"", @"type":@"typeNotification"}];
                    if ([TeoriusHelper arrayIsValid:notif[@"list"]]) {
                        NSArray *qArr = notif[@"list"];
                        for (NSDictionary *q in qArr) {
                            Question *question = [Question new];
                            int status = [q[@"status"] intValue];
                            int control = [q[@"control"] intValue];
                            question.status = status;
                            question.control = control;
                            
                            [TeoriusHelper setValuesForObject:question fromDict:q withLinkingDict:@{@"label":@"", @"value":@"", @"options": @"", @"answer":@"answerID"}];
                            
                            [questionsArr addObject:question];
                        }
                    }
                    if ([TeoriusHelper arrayIsValid:notif[@"buttons"]]) {
                        NSArray *bArr = notif[@"buttons"];
                        for (NSDictionary *b in bArr) {
                            Answer *answer = [Answer new];
                            [TeoriusHelper setValuesForObject:answer fromDict:b withLinkingDict:@{@"answer":@"answerID", @"label":@"", @"value":@""}];
                            
                            [answersArr addObject:answer];
                        }
                    }
                    
                    interview.questions = questionsArr.copy;
                    interview.answers = answersArr.copy;
                    [questionsArr removeAllObjects];
                    [answersArr removeAllObjects];
                    [outArr addObject:interview];
                    break;
                }
                case NotificationTypeArticle: {
                    Article *article = [Article new];
                    [TeoriusHelper setValuesForObject:article fromDict:notif withLinkingDict:@{@"title":@"", @"status":@"", @"code":@"", @"hide":@"", @"type":@"", @"cmsArticleId":@""}];
                    
                    [outArr addObject:article];
                    break;
                }
                case NotificationTypeMessage: {
                    Message *message = [Message new];
                    [TeoriusHelper setValuesForObject:message fromDict:notif withLinkingDict:@{@"title":@"", @"status":@"", @"code":@"", @"hide":@"", @"type":@"typeNotification", @"buttons":@"buttons"}];
                    
                    [outArr addObject:message];
                    break;
                }
                default: {
                    break;
                }
            }
        }
    }

}

#pragma mark - Error
//
- (NSError *)errorFromResponseWithError:(NSError *)error andDict:(NSDictionary *)dict {
    if (error) {
        return [self errorWithLocalizedTextFromError:error];
    }
    
//    NSDictionary *errorDict = dict[kRESTAPIError];
//    
//    if (!errorDict) {
//        return nil;
//    }
//
    NSNumber *successNumber = dict[@"success"];
    
    if (successNumber) {
        BOOL success = [dict[@"success"] boolValue];
        
        if (!success) {
            NSError *error = [self errorWithText:dict.jsonString];
            return error;
        }
    }
    
//    NSDictionary *extraDict = errorDict[kRESTAPIExtra];
//    
//    if (!extraDict) {
//        NSError *error = [self errorWithText:kMainAPIErrorUnknown];
//        return error;
//    }
//    
//    NSString *errorMessageCode = extraDict[kRESTAPIErrorMessageCode];
//    NSString *errorDescription = [self errorDescriptionForErrorMessageCode:errorMessageCode];
//    if (errorDescription) {
//        NSError *error = [self errorWithText:errorDescription];
//        return error;
//    }
//    else {
//        NSError *error = [self errorWithText:kMainAPIErrorUnknown];
//        return error;
//    }
    
    return nil;
}

- (NSError *)errorFromAppointmentsResponseWithError:(NSError *)error andDict:(NSDictionary *)dict {
    NSString *message = dict[@"message"];
    
    if ([TeoriusHelper stringIsValid:message]) {
        NSError *error = [self errorWithText:message];
        return error;
    }

    if (error) {
        return error;
    }
    
    return nil;
}

- (NSError *)errorWithLocalizedTextFromError:(NSError *)error {
    NSString *description = nil;
    
    switch (error.code) {
        case 401:
            description = kMainAPIErrorUnauthorized;
            break;
        case 302:
            description = @"Была потеряна связь с сервером. Попробуйте еще раз.";
            break;
        case -1001:
            description = kMainAPIErrorRequestTimeout;
            break;
        case -1005:
            description = kMainAPIErrorNoInternet;
            break;
        case -1009:
            description = kMainAPIErrorNoInternet;
            break;
        case 500:
            description = @"Внутренняя ошибка сервера.";
            break;
            
        default:
            description = kMainAPIErrorUnknown;
            break;
    }
    
    NSMutableDictionary *details = [NSMutableDictionary dictionary];
    details[NSLocalizedDescriptionKey] = description;
    
    return [NSError errorWithDomain:@"world" code:error.code userInfo:details];
}

- (NSError *)errorWithText:(NSString *)text {
    NSMutableDictionary *details = [NSMutableDictionary dictionary];
    details[NSLocalizedDescriptionKey] = text;
    
    return [NSError errorWithDomain:@"world" code:200 userInfo:details];
}

- (NSString *)errorDescriptionForErrorMessageCode:(NSString *)errorMessage {
    if ([errorMessage isEqualToString:@"USER_PASS_INCORRECT"]) {
        return kMainAPIErrorWrongPassword;
    }
    else if ([errorMessage isEqualToString:@"USER_NOT_FOUND"]) {
        return kMainAPIErrorLoginNotExists;
    }
    else if ([errorMessage isEqualToString:@"USER_NOT_UNIQUE"]) {
        return kMainAPIErrorUserAlreadyExists;
    }
    else if ([errorMessage isEqualToString:@"UNAUTHORIZED"]) {
        return kMainAPIErrorUnauthorized;
    }
    else if ([errorMessage isEqualToString:@"PERMISSION_DENIED"]) {
        return kMainAPIErrorPermissionDenied;
    }
    else if ([errorMessage isEqualToString:@"REQUEST_TIMEOUT"]) {
        return kMainAPIErrorRequestTimeout;
    }
    else if ([errorMessage isEqualToString:@"REGION_NOT_FOUND"]) {
        return kMainAPIErrorRegionNotFound;
    }
    else if ([errorMessage isEqualToString:@"PROVIDER_NOT_FOUND"]) {
        return kMainAPIErrorProviderNotFound;
    }
    else if ([errorMessage isEqualToString:@"ROOM_NOT_FOUND"]) {
        return kMainAPIErrorRoomNotFound;
    }
    else if ([errorMessage isEqualToString:@"ROOM_NOT_UNIQUE"]) {
        return kMainAPIErrorRoomNotUnique;
    }
    else if ([errorMessage isEqualToString:@"PAYMENT_NOT_STARTED"]) {
        return kMainAPIErrorPaymentNotStarted;
    }
    else if ([errorMessage isEqualToString:@"EMAIL_NOT_SET"]) {
        return kMainAPIErrorEmailNotSet;
    }
    else if ([errorMessage isEqualToString:@"PAYMENT_NOT_FOUND"]) {
        return kMainAPIErrorPaymentNotFound;
    }
    else if ([errorMessage isEqualToString:@"PAYMENT_NOT_FINISHED"]) {
        return kMainAPIErrorPaymentNotFinished;
    }
    else if ([errorMessage isEqualToString:@"CARD_NOT_FOUND"]) {
        return kMainAPIErrorCardNotFound;
    }
    else if ([errorMessage isEqualToString:@"USER_DELETED"]) {
        return kMainAPIErrorUserDeleted;
    }
    else if ([errorMessage isEqualToString:@"REGION_BLOCKED_FOR_PAY"]) {
        return kMainAPIErrorRegionBlockedForPay;
    }
    else if ([errorMessage isEqualToString:@"PROVIDER_BLOCKED_FOR_PAY"]) {
        return kMainAPIErrorProviderBlockedForPay;
    }
    
    return kMainAPIErrorUnknown;
}


#pragma mark - Model objects

- (id)objectFromRealmObject:(RLMObject *)rlmObj {
    if (!rlmObj) {
        return nil;
    }
    
    RLMObjectSchema *schema = rlmObj.objectSchema;
    
    NSString *className = schema.className;
    className = [className stringByReplacingCharactersInRange:NSMakeRange(className.length - 3, 3) withString:@""];
    
    id object = [[NSClassFromString(className) alloc] init];
    
    if (!object) {
        return nil;
    }
    
    for (RLMProperty *property in schema.properties) {
        if (![object respondsToSelector:NSSelectorFromString(property.name)]) {
            continue;
        }
        
        if (property.type == RLMPropertyTypeObject) {
            RLMObject *propertyObjectRLM = [rlmObj valueForKey:property.name];
            
            if (propertyObjectRLM) {
                id propertyObject = [self objectFromRealmObject:propertyObjectRLM];
                
                if (propertyObject) {
                    [object setValue:propertyObject forKey:property.name];
                }
            }
        }
        else if (property.type == RLMPropertyTypeArray) {
            RLMArray *propertyArrayRLM = [rlmObj valueForKey:property.name];
            
            if (propertyArrayRLM) {
                NSMutableArray *propertyArray = [[NSMutableArray alloc] initWithCapacity:propertyArrayRLM.count];
                
                for (RLMObject *propertyObjectRLM in propertyArrayRLM) {
                    id propertyObject = [self objectFromRealmObject:propertyObjectRLM];
                    
                    if (propertyObject) {
                        [propertyArray addObject:propertyObject];
                    }
                }
                
                [object setValue:propertyArray forKey:property.name];
            }
        }
        else {
            id propertyValue = [rlmObj valueForKey:property.name];
            
            if (propertyValue) {
                [object setValue:propertyValue forKey:property.name];
            }
        }
    }
    
    return object;
}

- (id)realmObjectFromObject:(NSObject *)obj {
    if (!obj) {
        return nil;
    }
    
    NSString* className = NSStringFromClass(obj.class);
    className = [className stringByAppendingString:@"RLM"];
    
    id rlmObject = [[NSClassFromString(className) alloc] init];
    
    if (!rlmObject) {
        return nil;
    }
    
    RLMObjectSchema *schema = [rlmObject valueForKey:@"objectSchema"];

    for (RLMProperty *property in schema.properties) {
        if (![rlmObject respondsToSelector:NSSelectorFromString(property.name)]) {
            continue;
        }
        
        if (property.type == RLMPropertyTypeObject) {
            id propertyObject = [obj valueForKey:property.name];
            
            if (propertyObject) {
                RLMObject *propertyObjectRealm = [self realmObjectFromObject:propertyObject];
                
                if (propertyObjectRealm) {
                    [rlmObject setValue:propertyObjectRealm forKey:property.name];
                }
            }
        }
        else if (property.type == RLMPropertyTypeArray) {
            NSArray *propertyArrayObj = [obj valueForKey:property.name];
            
            if (propertyArrayObj) {
                for (id propertyObject in propertyArrayObj) {
                    RLMObject *propertyObjectRealm = [self realmObjectFromObject:propertyObject];
                    
                    if (propertyObjectRealm) {
                        RLMArray *rlmArray = [rlmObject valueForKey:property.name];
                        [rlmArray addObject:propertyObjectRealm];
                    }
                }
            }
        }
        else {
            id propertyValue = [obj valueForKey:property.name];
            
            if (propertyValue) {
                [rlmObject setValue:propertyValue forKey:property.name];
            }
        }
    }
    
    return rlmObject;
}


#pragma mark - Clinics

- (Clinic *)clinicFromDict:(NSDictionary *)dict isParentClinic:(BOOL)isParent {
    Clinic *clinic = [Clinic new];
    
    clinic.parent = isParent;
    
    [TeoriusHelper setValuesForObject:clinic fromDict:dict withLinkingDict:@{@"address": @"", @"name": @"", @"regPhone": @"phone", @"note": @"work_hours", @"id": @"ID", @"districtAttachment": @"attached"}];
    
//    NSString *address = dict[@"address"];
//    if ([TeoriusHelper stringIsValid:address]){
//        clinic.address = address;
//    }
//    NSString *name = dict[@"name"];
//    if ([TeoriusHelper stringIsValid:name]){
//        clinic.name = name;
//    }
//    NSString *phone = dict[@"regPhone"];
//    if ([TeoriusHelper stringIsValid:phone]){
//        clinic.phone = phone;
//    }
//    NSString *work_hours = dict[@"note"];
//    if ([TeoriusHelper stringIsValid:work_hours]){
//        clinic.work_hours = work_hours;
//    }
//    NSString *ID = dict[@"id"];
//    if ([TeoriusHelper stringIsValid:ID]){
//        clinic.ID = ID;
//    }
    
    return clinic;
}


#pragma mark - Setters/Getters

- (RESTAPIManager *)RESTAPIManager {
    if (!_RESTAPIManager) {
        _RESTAPIManager = [RESTAPIManager new];
        
        AccountRLM *acc = self.persistencyManager.account;
        
        NSData *nsdata = [[NSString stringWithFormat:@"%@:%@", acc.login, acc.password]
                          dataUsingEncoding:NSUTF8StringEncoding];
        _RESTAPIManager.base64String = [nsdata base64EncodedStringWithOptions:0];
    }
    return _RESTAPIManager;
}

- (PersistencyManager *)persistencyManager {
    if (!_persistencyManager) {
        _persistencyManager = [PersistencyManager new];
    }
    return _persistencyManager;
}

- (User *)currentUser {
    AccountRLM *accRLM =  self.persistencyManager.account;
    UserRLM *userRLM = accRLM.family.currentMember;
    User *user = [self objectFromRealmObject:userRLM];
    
    return user;
}

- (void)setCurrentUser:(User *)currentUser {
    
    AccountRLM *accRLM = self.persistencyManager.account;
    
    Family *family = [self objectFromRealmObject:accRLM.family];
    
    if ([family.currentMember.ID isEqualToString:currentUser.ID]) {
        UserRLM *userRLM = [self realmObjectFromObject:currentUser];
        [self.persistencyManager addOrUpdateObject:userRLM];
    }
    else {
        family.currentMember = currentUser;
        [self.persistencyManager addOrUpdateObject:[self realmObjectFromObject:family]];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:notificationNameUsersDataChanged object:nil];
    
}

- (UserRLM *)currentUserRLM {
    AccountRLM *accRLM =  self.persistencyManager.account;
    return accRLM.family.currentMember;
}

- (UserRLM *)defaultUserRLM {
    AccountRLM *accRLM =  self.persistencyManager.account;
    return accRLM.family.defaultMember;
}

- (NSArray *)familyMembers {
    
    AccountRLM *accRLM =  self.persistencyManager.account;
    Family *members = [self objectFromRealmObject:accRLM.family];
    return members.members.copy;
}

- (NSArray *)measurementsArray {
    if (!_measurementsArray) {
        RLMResults *rlmRes = [self.persistencyManager myMeasurementsResult];
        NSMutableArray *array = [NSMutableArray arrayWithCapacity:rlmRes.count];
        for (RLMObject *object in rlmRes) {
            [array addObject:[self objectFromRealmObject:object]];
        }
        
        _measurementsArray = array.copy;
    }
    
    return _measurementsArray;
}


#pragma mark - Dealloc

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
